﻿using MoreInline.BOL;
using MoreInline.DAL;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class ConfirmOrder : System.Web.UI.Page
    {
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        OrdersDAL Order = new OrdersDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();
        SqlTransaction Trans;
        SqlConnection Conn;
        bool isCancel = false;
        bool isReqMod = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                if (Request.QueryString["IsRM"] != null && Request.QueryString["AssetId"] != null)
                {
                    Session["IsRM"] = Request.QueryString["IsRM"].ToString();
                    Session["AssetId"] = Request.QueryString["AssetId"].ToString();
                    if (Session["IsRM"].ToString().Equals("true"))
                    {
                        isReqMod = true;
                        Session["isRM"] = isReqMod;
                    }
                }
                if (Request.QueryString["Cancel"] != null)
                {
                    if (Request.QueryString["Cancel"].ToString().Equals("true"))
                    {
                        isCancel = true;
                        if (isCancel)
                        {
                            isReqMod = bool.Parse(Session["isRM"].ToString());
                            if (!isReqMod) {
                                Response.Redirect("~/MKT_Assets/OrderAssets.aspx");
                            }
                            else
                            {
                                Response.Redirect("~/MKT_Assets/ViewAssets.aspx");
                            }
                        }
                    }
                }

                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/MKT_Assets/ConfirmOrder.aspx?";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                   Response.Redirect(paypalRedirectUrl,false);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                    }
                    else
                    {
                        Conn = DBHelper.GetConnection();
                        Trans = Conn.BeginTransaction();
                        OrderAssetsBOL Asset =  (OrderAssetsBOL) Session["AssetObj"];
                        List<OrderAssetsDetailBOL> detail = (List<OrderAssetsDetailBOL>) Session["AllFilePaths"];
                        string OrderNum = OrderAsset.GetOrderNo();
                        string RequstType = string.Empty;
                        string Modification = string.Empty;
                        int AssetID = -1;
                        if (Session["IsRM"] != null && Session["AssetId"] != null)
                        {
                            RequstType = Session["IsRM"].ToString();
                            AssetID = int.Parse(Session["AssetId"].ToString());
                            Modification = Session["Text"].ToString();
                        }
                        if (!RequstType.Equals("True")) {
                            if (InsertData(Asset, Trans, Conn, detail))
                            {
                                //DataTable dt = Order.GetAssetDetails(Asset.ID);
                                ConfirmOrderBOL OrderInfo = new ConfirmOrderBOL();
                                OrderInfo.Amount = executedPayment.transactions.FirstOrDefault().amount.total.ToString();
                                OrderInfo.Cust_Id = Session["Cust_ID"] == null ? -1 : int.Parse(Session["Cust_ID"].ToString());
                                OrderInfo.OrderNum = OrderNum;
                                OrderInfo.AssetID = Asset.ID;
                                OrderInfo.DesignID = -1;
                                OrderInfo.Quantity = 1;
                                OrderInfo.RequestType = 1;
                                OrderInfo.IsActive = true;
                                OrderInfo.Item = executedPayment.transactions.FirstOrDefault().item_list.items.FirstOrDefault().name;
                                OrderInfo.PayerID = executedPayment.payer.payer_info.payer_id;
                                OrderInfo.PaymentId = executedPayment.transactions.FirstOrDefault().payee.merchant_id;
                                OrderInfo.CreateDate = DateTime.Now;
                                if (OrderAsset.InsertNewOrder(OrderInfo))
                                {
                                    int uid = NotiDAL.GetAdminID();
                                    NotificationInfo Ninfo = new NotificationInfo();
                                    Ninfo.OID = Asset.ID;
                                    Ninfo.UID = uid;// ManagerID;
                                    Ninfo.IsViewed = false;
                                    Ninfo.UserRoleID = 2;//4;
                                    Ninfo.FormPath = "../User/Orders.aspx";
                                    Ninfo.NotiDate = DateTime.Now;
                                    NotiDAL.InsertNotification(Ninfo, Trans, Conn);
                                    Trans.Commit();
                                    Conn.Close();
                                    Response.Redirect("~/MKT_Assets/Info.aspx?Success=" + 1 + "&id=" + OrderInfo.AssetID, false);
                                }
                            }
                        }
                        else
                        {
                            if (RequestModification(AssetID,Modification))
                            {
                                isReqMod = true;
                                DataTable dt = Order.GetAssetDetails(AssetID);
                                ConfirmOrderBOL OrderInfo = new ConfirmOrderBOL();
                                OrderInfo.Amount = executedPayment.transactions.FirstOrDefault().amount.total.ToString();
                                OrderInfo.Cust_Id = Session["Cust_ID"] == null ? -1 : int.Parse(Session["Cust_ID"].ToString());
                                OrderInfo.OrderNum = OrderNum;
                                OrderInfo.AssetID = AssetID;
                                OrderInfo.DesignID = int.Parse(dt.Rows[0]["ID"].ToString());
                                OrderInfo.Quantity = 1;
                                OrderInfo.RequestType = 2;
                                OrderInfo.IsActive = true;
                                OrderInfo.Item = executedPayment.transactions.FirstOrDefault().item_list.items.FirstOrDefault().name;
                                OrderInfo.PayerID = executedPayment.payer.payer_info.payer_id;
                                OrderInfo.PaymentId = executedPayment.transactions.FirstOrDefault().payee.merchant_id;
                                OrderInfo.CreateDate = DateTime.Now;
                                if (OrderAsset.InsertNewOrder(OrderInfo))
                                {
                                    int uid = NotiDAL.GetAdminID();
                                    NotificationInfo Ninfo = new NotificationInfo();
                                    Ninfo.OID = AssetID;
                                    Ninfo.UID = uid;
                                    Ninfo.IsViewed = false;
                                    Ninfo.UserRoleID = 2;//4;
                                    Ninfo.NotiDate = DateTime.Now;
                                    Ninfo.FormPath = "../User/Orders.aspx";
                                    NotiDAL.InsertNotification(Ninfo);
                                    Trans.Commit();
                                    Conn.Close();
                                    Response.Redirect("~/MKT_Assets/Info.aspx?Success=" + 1 + "&id=" + AssetID + "&IsRM=true", false);
                                }
                            }
                        }
                        

                    }
                }
            }
            catch (Exception ex)
            {
                isReqMod = bool.Parse(Session["isRM"].ToString());
                if (isReqMod && isCancel)
                {
                    Session.Remove("isRM");
                    Response.Redirect("~/MKT_Assets/ViewAssets.aspx");
                }
                else if (isCancel)
                {
                    Response.Redirect("~/MKT_Assets/OrderAssets.aspx");
                }
                else
                {
                    Response.Redirect("~/MKT_Assets/Info.aspx?Success=" + 0);

                }
            }
        }

        public int GetRemainingDays(string date)
        {
            DateTime ValidDate = DateTime.Parse(date);
            DateTime Now = DateTime.Now;
            TimeSpan RemainingDays = ValidDate.Subtract(Now);
            int Days = RemainingDays.Days;
            return Days;
        }
        private PayPal.Api.Payment payment;
        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }
        private Payment CreatePayment(APIContext apiContext, string redirectUrl)
        {

            string RequstType = string.Empty; 
            if (Session["IsRM"] != null)
            {
                RequstType = Session["IsRM"].ToString();
            }
            //create itemlist and add item objects to it  
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
    
            //Adding Item Details like name, currency, price etc  
            itemList.items.Add(new Item()
            {
                name = RequstType.Equals("True") == true?"Modification Request":"New Design",
                currency = "USD",
                price = RequstType.Equals("True")==true?"25":"99",
                quantity = "1",
                sku = "N/A"
            });
            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            // Configure Redirect Urls here with RedirectUrls object  
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            // Adding Tax, shipping and Subtotal details  
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = RequstType.Equals("True") == true ? "25" : "99"
            };
            //Final amount with details  
            var amount = new Amount()
            {
                currency = "USD",
                total = RequstType.Equals("True") == true ? "25" : "99", // Total must be equal to sum of tax, shipping and subtotal.  
                details = details
            };
            var transactionList = new List<Transaction>();
            // Adding description about the transaction  
            string invoicenum = OrderAsset.GetOrderNo();
            transactionList.Add(new Transaction()
            {
                description = RequstType.Equals("True") == true ? "Request For Modification":"Ordered New Design",
                invoice_number = RequstType.Equals("True") == true? Convert.ToString((new Random()).Next(100000)): Convert.ToString((new Random()).Next(100000)), //invoicenum, //Generate an Invoice No  
                amount = amount,
                item_list = itemList
            });
            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext  
            return this.payment.Create(apiContext);
        }


        public bool InsertData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }
                #region Email For Admin
                EmailNotification EN = new EmailNotification();
                EmailNotification.SendNotification(EN.GetAdminFilledObject());
                #endregion


                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

                if (CustData.Rows[0]["IsOrderRecNActive"].ToString().Equals("True"))
                {
                    #region Email For Customer
                    EmailNotification EmailData = EN.GetAdminFilledObject();
                    EmailData.SenderIdForUnSubscription = int.Parse(Session["Cust_ID"].ToString());
                    EmailData.ButtonText = "Review Your Order";
                    EmailData.NotificationType = "IsOrderRecNActive";
                    EmailData.Content = "Thank you! We received your order and have begun the order confirmation process. We will email updates as your order status changes";
                    EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                    EmailData.EmailSubject = "We received your order";
                    EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                    EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                    EmailNotification.SendNotification(EmailData);
                    #endregion
                }
                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception)
                {
                    Trans.Rollback();
                    Conn.Close();
                }
                Trans.Rollback();
                Conn.Close();
                return false;
            }
        }

        public bool RequestModification(int AssetId,string modifications)
        {
            bool result = false;
            DataTable dt = Order.GetAssetDetails(AssetId);
            if (Order.UpdateDesignRejCust(AssetId, 2, modifications))
            {
                if (OrderAsset.UpdateOrderStatus(AssetId, 2))
                {
                  //  int ManagerID = NotiDAL.GetManagerID();
                    #region Email For Admin
                    int custID = int.Parse(Session["Cust_ID"].ToString());
                    DataTable CustData = OrderAsset.GetCustDetail(custID).Tables[0];
                    EmailNotification EN = new EmailNotification();
                    EmailNotification ENotif = EN.GetAdminFilledObject();
                    ENotif.ButtonText = "View Order";
                    // ENotif.SenderIdForUnSubscription = int.Parse(Session["Cust_ID"].ToString());
                    ENotif.EmailSubject = "Request for modification";
                    ENotif.Content = CustData.Rows[0]["FirstName"] + " has requested for modification in order No: " + dt.Rows[0]["OrderNum"].ToString();
                    EmailNotification.SendNotification(ENotif);
                    #endregion
               

                    if (Order.UpdateFilePath(AssetId))
                    {
                        result = true;
                    }
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        private void SendEmailNotification(int custId)
        {
            DataTable CustData = OrderAsset.GetCustDetail(custId).Tables[0];
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            EmailNotification ENotifc = EN.GetAdminFilledObject();
            ENotifc.EmailSubject = "Package Upgrade ";
            ENotifc.Content = CustData.Rows[0]["FirstName"].ToString() + " has Successfully Upgraded to "+ CustData.Rows[0]["AccType"].ToString() + " Package.";
            EmailNotification.SendNotification(ENotifc);
            #endregion
            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = custId;
            if (CustData.Rows[0]["IsPackageConfirmNActive"].ToString().Equals("True"))
            {
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.NotificationType = "IsPackageConfirmNActive";
                EmailData.Content = "Congratulations Your Package Has Been Succesfully Upgraded to " + CustData.Rows[0]["AccType"].ToString();
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = "Package Upgraded";
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData);
            }
            #endregion
        }
    }
}