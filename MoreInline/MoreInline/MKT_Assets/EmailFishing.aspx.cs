﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class EmailFishing : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        AdvertisementBOL ObjAdvertisementBOL = new AdvertisementBOL();

        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();
        DesignBOL DBOL = new DesignBOL();
        OrdersDAL Order = new OrdersDAL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["CodeID"] != null && Request.QueryString["Email"] != null && Request.QueryString["FullCode"] != null)
                    {
                        TxtEmailMain.Text = Request.QueryString["Email"].ToString();
                        ObjCustomerProfileMasterBOL.ColumnName = "UserName";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtEmailMain.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);
                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "User Already Exist. Please Login", 0);
                            PnlGoMain.Visible = false;
                            imgadd.Visible = false;
                        }
                        else
                        {
                            BindDataList();
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void BindDataList()
        {
            int num = 1;
            ObjAdvertisementBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
            DataTable dt = ObjAdvertisementDAL.GetAdvertisementDataList(ObjAdvertisementBOL);

            DataTable dt_Ads = new DataTable();
            dt_Ads.Columns.Add(new DataColumn("ID", typeof(string)));
            dt_Ads.Columns.Add(new DataColumn("Images", typeof(string)));
            dt_Ads.Columns.Add(new DataColumn("IsLocked", typeof(string)));

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Rows[0][i].ToString() != "")
                {
                    DataRow dr = dt_Ads.NewRow();

                    dr["ID"] = num;
                    dr["Images"] = "~/upload/" + dt.Rows[0][i].ToString();

                    if (i > 0)
                    {
                        dr["IsLocked"] = true;
                    }
                    else
                    {
                        dr["IsLocked"] = false;

                    }
                    dt_Ads.Rows.Add(dr);
                    dt_Ads.AcceptChanges();
                    num++;
                }
            }
            ViewState["dtAppointmentAd"] = "upload/" + dt.Rows[0]["AppointmentAds"].ToString();
            imgadd.ImageUrl = "~/upload/" + dt.Rows[0]["AppointmentAds"].ToString();
        }


        protected void BtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (PasswordValidation())
                {
                    PnlGoMain.Visible = false; PnlGo1.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnEdits_Click(object sender, EventArgs e)
        {
            try
            {
                PnlEdit1.Visible = true; PnlGoMain.Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnSendAd_Click(object sender, EventArgs e)
        {
            try
            {
                if (EmailValidation())
                {
                    SaveCustomerDetails();
                    EmailWork(TxtEmailAddress.Text, TxtMessageEmail.Text, TxtBusinessAddress.Text);
                    PnlGo1.Visible = false; PnlGo2.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void EmailWork(string Email, string Message, string Location)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{image}", application_path + ViewState["dtAppointmentAd"].ToString());
            body = body.Replace("{message}", Message);
            body = body.Replace("{location}", Location);
            body = body.Replace("{unsubStyle}", "display:none");
            body = body.Replace("{voiceVisible}", "display:none");
            body = body.Replace("{reminderVisible}", "display:none");
            body = body.Replace("{contact}", "display:none");
            body = body.Replace("{contactVisible}", "display:inline");
            body = body.Replace("{direction}", application_path + "Emails/Direction.aspx?AssetID=" + ViewState["AssetID"].ToString());
            body = body.Replace("{directionVisible}", "display:inline");
            body = body.Replace("{web}", string.IsNullOrWhiteSpace(TxtWebAddress.Text)? "display:none": TxtWebAddress.Text);
            body = body.Replace("{Fishing}", "display:none;");
            if (String.IsNullOrEmpty(TxtWebAddress.Text))
            {
                body = body.Replace("{webVisible}", "display:none");
            }
            else
            {
                body = body.Replace("{webVisible}", "display:inline");
            }


            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("donotreply@moreinline.com", "MoreInline Testing");
            mm.To.Add(Email);
            mm.Subject = TxtMessageTitle.Value;
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);



        }
        private void SaveCustomerDetails()
        {
            string[] fullcodeArray = Request.QueryString["FullCode"].ToString().Split('-');
            string fullCode = fullcodeArray[0].ToString();

            switch (fullCode)
            {
                case "MX11":
                    ObjCustomerProfileMasterBOL.EmailsLimit = 50;
                    ObjCustomerProfileMasterBOL.AccTypeID = 1;
                    break;
                case "IN001":
                    ObjCustomerProfileMasterBOL.EmailsLimit = 1000;
                    ObjCustomerProfileMasterBOL.AccTypeID = 5;
                    break;
                case "MM02":
                    ObjCustomerProfileMasterBOL.EmailsLimit = 10000;
                    ObjCustomerProfileMasterBOL.AccTypeID = 6;
                    break;
            }

            ObjCustomerProfileMasterBOL.FirstName = string.Empty;
            ObjCustomerProfileMasterBOL.LastName = string.Empty;
            ObjCustomerProfileMasterBOL.Company = string.Empty;
            ObjCustomerProfileMasterBOL.Website = string.Empty;
            ObjCustomerProfileMasterBOL.Address = string.Empty;
            ObjCustomerProfileMasterBOL.ZipCode = string.Empty;
            ObjCustomerProfileMasterBOL.Email = TxtEmailMain.Text;
            ObjCustomerProfileMasterBOL.Phone = string.Empty;
            ObjCustomerProfileMasterBOL.UserName = string.Empty;
            ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
            ObjCustomerProfileMasterBOL.Photo = string.Empty;
            ObjCustomerProfileMasterBOL.CompanyLogo = string.Empty;
            ObjCustomerProfileMasterBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
            

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
            {
                //ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                SaveGraphics();
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }
        private void SaveGraphics()
        {
            List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
            try
            {
                if (!Regxcheck.IsValidEmail(TxtEmailAddress.Text))
                {
                    MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                    return;
                }
                // Assets.Date = DateTime.Parse(TxtDate.Text);
                Session["BEmailAddress"] = TxtEmailAddress.Text;
                Session["BAddress"] = TxtBusinessAddress.Text; 
                Session["Webaddr"] = string.IsNullOrWhiteSpace(TxtWebAddress.Text)==true?"": TxtWebAddress.Text;
                Session["Bmessage"] = TxtMessageEmail.Text;

                Assets.Date = DateTime.Now;
                Assets.Hours = DateTime.Now.Hour;
                Assets.WebUrl = TxtWebAddress.Text;
                Assets.Subject = TxtMessageTitle.Value;
                Assets.Phone = "";
                Assets.Email = TxtEmailAddress.Text;
                Assets.Location = TxtBusinessAddress.Text;
                Assets.Message = string.Empty;
                Assets.TimSpan = "PM";
                Assets.AudioPath = "";
                Assets.AddAudio = false;
                Assets.AddContact = true;
                Assets.AddDirection = true;
                Assets.AddReminder = false;
                Assets.AddWebUrl = true;
                Assets.AddUerProfile = false;
                Assets.AssestFilePath = "";
                Assets.CreatedByID = ObjCustomerProfileMasterBOL.Cust_ID;
                Assets.ApprovalStatus = 3;
                Assets.Category = 1;

                if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
                {
                    int AssetsID = Assets.ID;

                    if (OrderAsset.InsertEmailLandingAssetsMaster(Assets, Trans, Conn))
                    {
                        Trans.Commit();
                        Conn.Close();
                        ViewState["AssetID"] = Assets.ID;

                        DBOL.AssestID = AssetsID;
                        DBOL.CreatedByID = 1;
                        DBOL.FilePath = "~/" + ViewState["dtAppointmentAd"].ToString();
                        DBOL.QAID = 0;
                        DBOL.QaApproval = 1;
                        DBOL.MngrApproval = 1;
                        DBOL.CustApproval = 1;
                        DBOL.QaRemarks = "Approved";
                        DBOL.MngrRemarks = "Approved";
                        DBOL.CustRemarks = "Approved";
                        if (Order.InsertOrderdDesign(DBOL))
                        {
                        }
                        ActiveAccount(ObjCustomerProfileMasterBOL.Cust_ID);
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void ActiveAccount(int Cust_ID)
        {
            try
            {
                ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
                DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);

                if (dt.Rows[0]["IsActive"].ToString() == "True" && dt.Rows[0]["ActivationDate"].ToString() != "")
                {
                    MessageBox(div1, LblMsg, "This Account is Already Active", 0);
                    return;
                }
                else
                {
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjCustomerProfileMasterDAL.UpdateActiveCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                    {
                        DataTable dt_ActiveOn = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL, Trans, Conn);
                        DateTime ValidDate = DateTime.Parse(dt_ActiveOn.Rows[0]["ActivationDate"].ToString()).AddDays(int.Parse(dt_ActiveOn.Rows[0]["ValidationDays"].ToString()));

                        ObjCodeMasterBOL.ValidDate = ValidDate;
                        ObjCodeMasterBOL.CodeID = int.Parse(dt_ActiveOn.Rows[0]["CodeID"].ToString());

                        if (ObjCodeMasterDAL.UpdateValidDateCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                        {
                            Trans.Commit();
                            Conn.Close();
                        }
                        else
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        protected void BtnEditSend_Click(object sender, EventArgs e)
        {
            try
            {
                PnlEdit1.Visible = false; PnlEdit2.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnGoProfile_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public bool PasswordValidation()
        {
            TxtPassword.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtConfirmPassword.Attributes["style"] = "border: 1px solid #4B4A4A;";

            if (TxtPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Password", 0);
                TxtPassword.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            if (TxtConfirmPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                TxtConfirmPassword.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            if (TxtPassword.Text != TxtConfirmPassword.Text)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Passwords are not same", 0);
                TxtPassword.Attributes["style"] = "border: 1px solid red;";
                TxtConfirmPassword.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool EmailValidation()
        {
            TxtMessageTitle.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtEmailAddress.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtBusinessAddress.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtWebAddress.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtMessageEmail.Attributes["style"] = "border: 1px solid #4B4A4A;";

            if (TxtMessageTitle.Value == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Message Title", 0);
                TxtMessageTitle.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            if (TxtEmailAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Email Address", 0);
                TxtEmailAddress.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            if (TxtBusinessAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Business Address", 0);
                TxtBusinessAddress.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            if (TxtWebAddress.Text == "")
            {
                TxtWebAddress.Text = "";
               // div1.Visible = true;
              //  MessageBox(div1, LblMsg, "Enter Website", 0);
               // TxtWebAddress.Attributes["style"] = "border: 1px solid red;";
                //return false;
            }
            if (TxtMessageEmail.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Testing Message", 0);
                TxtMessageEmail.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else
            {
                return true;
            }

        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center";
                LblMsg.Text = Msg;
            }
        }

        protected void TxtEmailAddress_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TxtEmailAddress.Text))
            {
                if (Regxcheck.IsValidEmail(TxtEmailAddress.Text))
                {
                    TxtEmailAddress.Attributes["style"] = "border: 1px solid green;";
                    lblcustemail.Text = TxtEmailAddress.Text;
                    lblcustweb.Text = TxtWebAddress.Text;
                    lblLocation.Text = TxtBusinessAddress.Text;
                    lblMailMsg.Text = TxtMessage.Text;
                }
                else
                {
                    TxtEmailAddress.Attributes["style"] = "border: 1px solid red;";
                    TxtEmailAddress.Attributes["placeholder"] = "Invalid Email Address";
                }
            }
            else
            {
                TxtEmailAddress.Attributes["style"] = "border: 1px solid red;";
                TxtEmailAddress.Attributes["placeholder"] = "Email Address is Required";

            }
        }

        protected void TxtSampleEmail_TextChanged(object sender, EventArgs e)
        {


            if (!string.IsNullOrWhiteSpace(TxtSampleEmail.Text))
            {
                if (Regxcheck.IsValidEmail(TxtSampleEmail.Text))
                {
                    TxtSampleEmail.Attributes["style"] = "border: 1px solid green;";
                    lblcustemail.Text = TxtEmailAddress.Text;
                    lblcustweb.Text = TxtWebAddress.Text;
                    lblLocation.Text = TxtBusinessAddress.Text;

                }
                else
                {
                    TxtSampleEmail.Attributes["style"] = "border: 1px solid red;";
                    TxtSampleEmail.Attributes["placeholder"] = "Invalid Email Address";
                }
            }
            else
            {
                TxtSampleEmail.Attributes["style"] = "border: 1px solid red;";
                TxtSampleEmail.Attributes["placeholder"] = "Email Address is Required";
            }
        }

        protected void btnGtProf_Click(object sender, EventArgs e)
        {
            Session["Email"] = TxtEmailMain.Text;
            Session["pass"] = TxtPassword.Text;
            Session["Subject"] = TxtMessageTitle.Value;
            Response.Redirect("~/MKT_Assets/CompanyInformation.aspx", false);
        }
    }
}