﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailFishing.aspx.cs" Inherits="MoreInline.MKT_Assets.EmailFishing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Start with Advance Ad</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../agency-video/img/favicon.png" rel="icon" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <%-- ----------------------------------- Script --------------------------------------------- --%>


    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtBusinessAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script src="../dist/js/BadWords.js"></script>
    <script>
        function validate_text() {

            var compare_text = $("#TxtMessageTitle").val();
            var array = compare_text.split(" ");
            let obj = {};

            for (let i = 0; i < array.length; i++) {

                if (!obj[array[i]]) {
                    const element = array[i].toLowerCase();
                    obj[element] = true;
                }
            }

            for (let j = 0; j < swear_words_arr.length; j++) {

                if (obj[swear_words_arr[j]]) {
                    $("#TxtMessageTitle").val('');
                    alert("The message will not be sent!!!\nThe following unethical words are found:\n_______________________________\n" + swear_words_arr[j] + "\n_______________________________");
                }
            }

        }
    </script>

    <script type="text/javascript">
        function checkSpcialChar(event) {
            debugger;
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
                if (event.keyCode != 32) {
                    event.returnValue = false;
                    alert("Special characters are not allowed due to security reason.");
                    return;
                }
            }
            event.returnValue = true;
        }
    </script>
    <style type="text/css">

        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        .PullRight {
            float: right !important
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }

        .button {
            width: 210px;
        }

        .button-line-height {
            line-height: 26px;
        }
        /* ----------- responsivity ----------- */
        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }

            .main-section-header {
                font-size: 28px !important;
            }

            .show {
                display: block !important;
            }

            .hide {
                display: none !important;
            }

            .align-center {
                text-align: center !important;
            }

            .no-bg {
                background: none !important;
            }
            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }
            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }
            /*-------- container --------*/
            .container590 {
                width: 440px !important;
            }

            .container580 {
                width: 400px !important;
            }

            .main-button {
                width: 220px !important;
            }
            /*-------- secions ----------*/
            .section-img img {
                width: 440px !important;
                height: auto !important;
            }

            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }

            .main-section-header {
                font-size: 26px !important;
            }
            /* ====== divider ====== */
            /*.divider img {
                width: 280px !important;
            }*/
            /*-------- container --------*/
            /*.container590 {
                width: 280px !important;
            }

            .container590 {
                width: 280px !important;
            }

            .container580 {
                width: 260px !important;
            }*/
            /*-------- secions ----------*/
            /*.section-img img {
                width: 250px !important;
                height: auto !important;
            }*/
        }

        @media only screen and (max-width: 640px) {
            p {
                font-size: 14px;
            }

            .button {
                width: 170px;
            }

            a {
                font-size: 11px;
            }
        }

        @media only screen and (max-width: 479px) {
            p {
                font-size: 12px;
            }
        }

        @media only screen and (max-width: 479px) {
            .button {
                width: 160px;
            }
        }

        @media only screen and (max-width: 479px) {
            a {
                font-size: 10px;
            }
        }
    </style>
   <%-- <style>
        .overlap {
            position: absolute;
            top: 0px;
        }

        .buttonSize {
            width: 300px;
            height: 70px;
            font-size: 35px;
            font-weight: 100;
        }

        .buttonSend {
            width: auto;
            height: 70px;
            font-size: 28px;
            font-weight: 100;
        }

        .borderBottom {
            border-bottom: 2px solid #ABC502;
        }

        .m-r-25 {
            margin-right: 25px;
        }

        .labelOR {
            color: #007f9f;
            font-size: 35px;
            font-weight: 100;
            padding-top: 10px;
            padding-bottom: 10px;
        }

  

        @media (max-width:1199px) {
            .m-l-100 {
                margin-left: 10px;
            }
        }

        @media (max-width:1199px) {
            .p-t-150 {
                padding-top: 10px;
            }
        }
    </style>--%>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header borderBottom" style="background-color: #000000;">
                <div class="p-t-10 p-l-10 p-b-15">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="240px" height="60px" />
                </div>
            </header>

            <div class="content-wrapper m-l-0">
                <div class="m-l-100 m-r-25 p-t-10">
                    <br />
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div id="div1" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>


                    <br />

                    <div class="row">
                        <div class="col-lg-6 col-sm-6 p-t-10">
                            <%--<img src="../upload/woodwork.jpg" class="img-responsive" />--%>
                          <%--  <asp:Image ID="imgadd" ImageUrl="#" AlternateText="Appointment Ad" class="img-responsive" runat="server" />--%>
    <div bgcolor="#f2f2f2" style="margin:0 auto;padding:0;width:100% !important;">
        <!-- big image section -->
        <table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="table-layout:fixed;" width="100%">
            <tbody>
                <tr>
                    <td align="center">
                        <table border="0" width="600px" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="background-color:#fff;margin:0 auto;max-width:600px;width:inherit;">
                          
                            <tr>
                                <td align="center" class="section-img" style="padding-top:5px; background-color:#000; height:80px" >
                                    <%--<img src="{CompanyLogo}" style="display: block; width: 150px;" border="0" alt="Logo" />--%>
                                    <img src="../Images/moreinline_logo.png" style="display: block; width: 150px;" border="0" alt="Logo" />
                                </td>
                            </tr> 
                              <tr>
                               <td align="center" class="section-img" style="padding:15px;">
                                    <%--<img src="{image}" style="display: block; width: 600px;" border="0" alt="Appointment Ad" />--%>
                                     <asp:Image ID="imgadd" ImageUrl="#" AlternateText="Appointment Ad" class="img-responsive" runat="server" />
                                </td>
                            </tr> 
                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">
                                    <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                        <tr>
                                            <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">
                                    <table border="0" width="650" align="center" cellpadding="0" cellspacing="0" class="container590">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="color: #888888; font-size: 16px;  line-height: 24px;">


                                                    <div style="line-height: 24px">
                                                        <p id="messText" style="padding-left:5px;padding-right:5px;">
                                                            <asp:Label ID="lblMailMsg" runat="server" Text=""></asp:Label>
                                                            {message}
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center" style="{voiceVisible}">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height">
                                                        <a href="{voice}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/voice-message1.png" /></span><span style="vertical-align:top;padding-left:3px;">LISTEN VOICE MESSAGE</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                                <td align="center" style="{reminderVisible}">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height">
                                                        <a href="{reminder}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/reminder1.png" /></span><span style="vertical-align:top;padding-left:3px;">SAVE REMINDER</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                                <td align="center" style="{directionVisible}">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px; background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height">
                                                        <a href="{direction}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/directions1.png" /></span><span style="vertical-align:top;padding-left:3px;">DIRECTIONS</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                                <td align="center" style="{contactVisible}">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px;background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height">
                                                        <a href="tel:{contact}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/email1.png" /></span><span style="vertical-align:top;padding-left:3px;">CONTACT</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px;">
                                <td align="center" style="{webVisible}">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px;background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">
                                                    <div class="button-line-height">
                                                        <a href="{web}" target="_blank" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/web1.png" /></span><span style="vertical-align:top;padding-left:3px;">VISIT US</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 15px; line-height: 25px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: sans-serif; line-height: 20px;" bgcolor="#a9c502" width="100%">
                                    <br />
                                    <font color="black">
                                        <p id="locText" style="color: #000000;">
                                            <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label><br />
                                        </p>
                                        <p style="{Fishing}">
                                           <span id="custweb"></span><asp:Label ID="lblcustweb" runat="server" Text=""></asp:Label> | <span id="custemail">
                                                <asp:Label ID="lblcustemail" runat="server" Text=""></asp:Label></span>
                                            <br />
                                            <a href="{unsubLink}" style="{unsubStyle}">unsubscribe</a>
                                        </p>
                                        <br />
                                    </font>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>






    </div>
                            
                        </div>
                        <asp:Panel ID="PnlGoMain" runat="server">


                            <div class="col-lg-5 col-sm-6">
                                <h3 style="color: #007f9f;">Create an account to start trial</h3>

                                <asp:TextBox ID="TxtEmailMain" runat="server" CssClass="form-control m-t-10" placeholder="Email" Enabled="false"></asp:TextBox>
                                <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control m-t-10" placeholder="Password" type="password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password1"></span>
                                <asp:TextBox ID="TxtConfirmPassword" runat="server" CssClass="form-control m-t-10" placeholder="Confirm Password" type="password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password2"></span>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-2 col-sm-2 p-t-15 p-b-10">

                                <asp:Button ID="BtnNext" Text="Go to next step" runat="server" CssClass="btn btn-light-green" OnClick="BtnNext_Click" />
                                <%--<div class="text-center">
                                    <label class="labelOR">OR</label>
                                </div>--%>
                                <asp:Button ID="BtnEdits" Text="I need some edits" runat="server" CssClass="btn btn-red-white buttonSize m-b-10" OnClick="BtnEdits_Click" Visible="false" />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlGo1" runat="server" Visible="false">
                            <div class="col-lg-5 col-sm-6 p-t-10">
                                <label class="labelOR">Let's add some information to your Ad</label><br />
                                <label>Email Message Title:</label>
                                <input id="TxtMessageTitle" class="form-control m-t-10" placeholder="Email Message Title" onkeypress="return checkSpcialChar(event)" type="text" runat="server" name="name" value="" onchange="validate_text()" />
                                <%--<asp:TextBox ID="TxtMessageTitle" runat="server" CssClass="form-controlm-t-10" onchange="validate_text()" placeholder="Email Message Title"></asp:TextBox>--%><br />
                                <label>Your Business Email Address:</label>
                                <asp:TextBox ID="TxtEmailAddress" runat="server" AutoPostBack="true" OnTextChanged="TxtEmailAddress_TextChanged" CssClass="form-control m-t-10" placeholder="Your Email Address"></asp:TextBox><br />
                                <label>Your Business Address:</label>
                                <asp:TextBox ID="TxtBusinessAddress" runat="server" CssClass="form-control m-t-10" placeholder="Your Business Address"></asp:TextBox><br />
                                <label>Your Website Address:</label>
                                <asp:TextBox ID="TxtWebAddress" runat="server" CssClass="form-control m-t-10" placeholder="Your Website Address(Optional)"></asp:TextBox><br />
                                <label hidden="hidden" class="p-t-25">Send to a co-worker or your personal email for sample view</label>
                                <asp:TextBox ID="TxtSampleEmail" Visible="false" AutoPostBack="true" OnTextChanged="TxtSampleEmail_TextChanged" runat="server" CssClass="form-control m-t-10" placeholder="Send to a co-worker or your personal email for sample view"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="TxtMessageEmail" runat="server" TextMode="MultiLine" Rows="6" CssClass="form-control textarea-resize" placeholder="Enter Your Testing Message" EnableTheming="True" />
                                <br />
                                    
                                <div>
                                    <asp:Button ID="BtnSendAd" Text="Send Ad" runat="server" CssClass="btn btn-light-green m-b-5" OnClick="BtnSendAd_Click" />
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlGo2" runat="server" Visible="false">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-3 p-t-10 text-center">
                                <h1 class="p-t-150">Your ad was sent to<span style="color: #ABC502"> 1</span> email address successfully</h1>
                                <asp:Button Text="Check status" ID="btnGtProf" OnClick="btnGtProf_Click" runat="server" class="btn btn-green-white buttonSize m-b-5" />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlEdit1" runat="server" Visible="false">
                            <div class="col-lg-5 p-t-10 m-b-10">
                                <label class="labelOR">I would like to request the following changes</label><br />
                                <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="I would like to request the following changes" EnableTheming="True"></asp:TextBox>
                                <asp:FileUpload ID="AddChanges" runat="server" CssClass="p-t-10" />
                                <asp:Button ID="BtnEditSend" Text="Send Ad to Advance Ad" runat="server" CssClass="btn btn-dark-green-white buttonSend m-t-40" OnClick="BtnEditSend_Click" />
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="PnlEdit2" runat="server" Visible="false">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-3 p-t-10 m-b-10 text-center">
                                <br />
                                <br />
                                <label class="labelOR">Your changes have been submitted to Advance Ad. A quality assurance personnel will be in touch with you soon.</label><br />
                                <asp:Button ID="BtnGoProfile" Text="Go to profile page" runat="server" CssClass="btn btn-dark-green-white buttonSend m-t-40" OnClick="BtnGoProfile_Click" />
                            </div>
                        </asp:Panel>


                    </div>






                </div>
            </div>

        </div>
    </form>
    <!------------------------- Script --------------------------->
    <script>
        function ShowAudioOps() {


            var ischecked = $('#ChkAddVoice').is(":checked");
            if (ischecked) {
                $("#divaudio").removeAttr('hidden');
            }
            else {
                $("#divaudio").attr('hidden', true);
            }

        }
        function ShowAddvoice() {

            var ischecked = $('#shwadd').prop('checked', 'checked');
            if (ischecked) {
                $("#PnlRecordVoice").removeAttr('hidden');
                $("#pnlUploadVoice").attr('hidden', true);
            }
            else {
                $("#PnlRecordVoice").attr('hidden', true);
            }

        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
            $('#TxtMessageEmail').keyup(function () {
                $('#messText').text($(this).val());
            });
        $('#TxtBusinessAddress').keyup(function () {
            $('#locText').text($(this).val());
        });
        $('#TxtWebAddress').keyup(function () {
            $('#custweb').text($(this).val());
        });
        $('#TxtEmailAddress').keyup(function () {
            $('#custemail').text($(this).val());
        });
    </script>
  
    

    <script>
        $("body").on('click', '.toggle-password2', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input2 = $("#TxtConfirmPassword");
            if (input2.attr("type") === "password") {
                input2.attr("type", "text");
            } else {
                input2.attr("type", "password");
            }
        });

        $("body").on('click', '.toggle-password1', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input1 = $("#TxtPassword");
            if (input1.attr("type") === "password") {
                input1.attr("type", "text");
            } else {
                input1.attr("type", "password");
            }

        });
    </script>
</body>
</html>
