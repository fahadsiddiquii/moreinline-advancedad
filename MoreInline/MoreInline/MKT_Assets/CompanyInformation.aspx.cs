﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class CompanyInformation : System.Web.UI.Page
    {
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        OrdersDAL Order = new OrdersDAL();
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();

        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        DesignBOL DBOL = new DesignBOL();

        SqlTransaction Trans;
        SqlConnection Conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            if (!IsPostBack)
            {
                try
                {
                    if (Session["Email"] != null && Session["pass"] != null && Session["BEmailAddress"] != null && Session["BAddress"] != null && Session["Subject"] != null && Session["Bmessage"] != null)
                    {

                        string email = Session["Email"].ToString();
                        string pass = Session["pass"].ToString();
                        TxtSubject.Text = Session["Subject"].ToString();
                        TxtEmailAdd.Text = Session["BEmailAddress"].ToString();
                        TxtAddress.Text = Session["BAddress"].ToString();
                        TxtWebsite.Text = Session["Webaddr"].ToString();
                        TxtPhone.Text = "";
                        TxtLocation.Text = Session["BAddress"].ToString();
                        Txtweb.Text = Session["Webaddr"].ToString();
                        TxtMessage.Text = Session["Bmessage"].ToString();
                        if (Session["Contact"] != null)
                        {
                            TxtPhone.Text = Session["Contact"].ToString();
                        }


                        LoginWork(email, pass);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }

            }
        }

        protected void BtnNext_Click(object sender, EventArgs e)
        {

            try
            {
                if (ProfilePhoto.HasFile)
                {
                    if (Page.IsValid)
                    {
                        string fileName = Path.GetFileName(ProfilePhoto.PostedFile.FileName);
                        ViewState["ProfilePhoto"] = fileName;
                        ProfilePhoto.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                    }
                    else
                    {
                        return;
                    }
                }
                if (CompanyLogo.HasFile)
                {
                    if (Page.IsValid)
                    {
                        string fileName = Path.GetFileName(CompanyLogo.PostedFile.FileName);
                        ViewState["CompanyLogo"] = fileName;
                        CompanyLogo.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                    }
                    else
                    {
                        return;
                    }
                }
                if (Local_Validation() == true)
                {
                    Session["Contact"] = TxtNumber.Text;
                    TxtPhone.Text = TxtNumber.Text;
                    UpdateSavedProfile();
                    InsertGenricInfo();
                   // PnlCompanyInfo.Visible = false; PnlGenericInfo.Visible = false;
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }



        public bool Local_Validation()
        {
            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Company", 0);
                return false;
            }
            //else if (TxtWebsite.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Enter Website", 0);
            //    return false;
            //}
            else if (TxtNumber.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter ZipCode", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        public void UpdateSavedProfile()
        {
            try
            {
                string email = Session["Email"].ToString();
                string pass = Session["pass"].ToString();
                ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
                ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
                ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
                ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
                ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
                ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
                ObjCustomerProfileMasterBOL.Password = pass;
                ObjCustomerProfileMasterBOL.Phone = TxtNumber.Text;
                ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
                ObjCustomerProfileMasterBOL.CompanyLogo = ViewState["CompanyLogo"].ToString();
                Session["Photo"] = ViewState["ProfilePhoto"];
                ObjCustomerProfileMasterBOL.UpdatedBy = int.Parse(Session["Cust_ID"].ToString());
                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCustomerProfileMasterDAL.UpdateCustomerProfile(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Profile Updated Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void InsertGenricInfo()
        {

            //if (string.IsNullOrWhiteSpace(TxtAdSubject.Text))
            //{
            //    TxtAdSubject.Attributes["style"] = "border: 1px solid red;";
            //    TxtAdSubject.Attributes["placeholder"] = "Subject Required";
            //}
            if (!string.IsNullOrWhiteSpace(TxtEmailAdd.Text))
            {

                if (Regxcheck.IsValidEmail(TxtEmailAdd.Text))
                {
                    TxtEmailAdd.Attributes["style"] = "border: 1px solid green;";
                }
                else
                {
                    TxtEmailAdd.Attributes["style"] = "border: 1px solid red;";
                    TxtEmailAdd.Attributes["placeholder"] = "Invalid Email Address";
                }

            }
            else
            {
                TxtEmailAdd.Attributes["style"] = "border: 1px solid red;";
                TxtEmailAdd.Attributes["placeholder"] = "Email Address Required";
            }
            if (string.IsNullOrWhiteSpace(TxtPhone.Text))
            {
                TxtPhone.Attributes["style"] = "border: 1px solid red;";
                TxtPhone.Attributes["placeholder"] = "Phone Number Required";
                return;
            }
            if (string.IsNullOrWhiteSpace(TxtLocation.Text))
            {
                TxtLocation.Attributes["style"] = "border: 1px solid red;";
                TxtLocation.Attributes["placeholder"] = "Location Required";
                return;
            }
            //if (string.IsNullOrWhiteSpace(TxtMessage.Text))
            //{
            //    TxtMessage.Attributes["style"] = "border: 1px solid red;";
            //    TxtMessage.Attributes["placeholder"] = "Message Required";
            //    return;
            //}
            if (string.IsNullOrWhiteSpace(TxtSubject.Text))
            {
                TxtMessage.Attributes["style"] = "border: 1px solid red;";
                TxtMessage.Attributes["placeholder"] = "Subject Required";
                return;
            }
            //if (string.IsNullOrWhiteSpace(Txtweb.Text))
            //{
            //    Txtweb.Attributes["style"] = "border: 1px solid red;";
            //    Txtweb.Attributes["placeholder"] = "website url Address Requierd";
            //    return;
            //}


            if (Session["AudPath"] != null)
            {
                string Directory = Server.MapPath(Session["AudPath"].ToString());
                FileInfo fileInfo = new FileInfo(Directory);
                if (fileInfo.Exists)
                { Assets.AudioPath = string.IsNullOrWhiteSpace(Session["AudPath"].ToString()) == true ? "" : Session["AudPath"].ToString(); }
                else { Assets.AudioPath = ""; }
            }

            Assets.Subject = TxtSubject.Text;
            Assets.Phone = TxtPhone.Text;
            Assets.Email = TxtEmailAdd.Text;
            Assets.Location = TxtLocation.Text;
            Assets.WebUrl = Txtweb.Text;
            Assets.Message = TxtMessage.Text;
            Assets.CreatedByID = int.Parse(Session["Cust_ID"].ToString());

            if (OrderAsset.InsertGenricInfo(Assets))
            {
                Response.Redirect("~/Dashboard/Dashboard.aspx", false);
                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

                if (CustData.Rows[0]["IsOrderRecNActive"].ToString().Equals("True"))
                {
                    #region Email For Admin
                    EmailNotification EN = new EmailNotification();
                    EmailNotification.SendNotification(EN.GetAdminFilledObject());
                    #endregion

                    #region Email For Customer
                    EmailNotification EmailData = EN.GetAdminFilledObject();
                    EmailData.ButtonText = "Log in to MoreInline";
                    EmailData.SenderIdForUnSubscription = int.Parse(Session["Cust_ID"].ToString());
                    EmailData.NotificationType = "";
                    EmailData.Content = "Welcome! You have just signed up to moreinline.com.";
                    EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                    EmailData.EmailSubject = "Welcome to MoreInline";
                    EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                    EmailData.SenderIdForUnSubscription = -1;
                    EmailNotification.SendNotification(EmailData);
                    #endregion
                }


            }

        }
        public void LoginWork(string UserName, string Password)
        {
            try
            {

                ObjCustomerProfileMasterBOL.Email = UserName;
                ObjCustomerProfileMasterBOL.Password = Password;

                DataTable dt = ObjCustomerProfileMasterDAL.CustomerLogin(ObjCustomerProfileMasterBOL);


                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["IsActive"].ToString() == "True")
                    {
                        Session["Cust_ID"] = dt.Rows[0]["Cust_ID"].ToString();
                        Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                        Session["Email"] = dt.Rows[0]["Email"].ToString();
                        Session["Photo"] = dt.Rows[0]["Photo"].ToString();
                        Session["DashBoardTour"] = dt.Rows[0]["DashBoardTour"].ToString();
                        Session["OrderAssetsTour"] = dt.Rows[0]["OrderAssetsTour"].ToString();
                        CheckSnapPayment(dt);
                    }
                    else
                    { MessageBox(div1, LblMsg, "Your Account is Not Active Please Contact Admin", 0); }
                }
                else
                { MessageBox(div1, LblMsg, "Invalid Username or Password", 0); }

            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        private void CheckSnapPayment(DataTable dt)
        {
            DataTable snapdt = ObjCustomerProfileMasterDAL.GetSnapByEmail(dt.Rows[0]["Email"].ToString());
            if (snapdt != null)
            {
                CreateAssets(snapdt, dt);
            }
        }

        private void CreateAssets(DataTable snapdt, DataTable customerdt)
        {
            double amount = double.Parse(snapdt.Rows[0]["Amount"].ToString());
            int AcType = int.Parse(snapdt.Rows[0]["AccountType"].ToString());
            int custID = int.Parse(customerdt.Rows[0]["Cust_ID"].ToString());
            DataTable CustData = OrderAsset.GetCustDetail(custID).Tables[0];
            DataTable dtCustActive = ObjCustomerProfileMasterDAL.GetViewAddsByUserEmail(CustData.Rows[0]["Email"].ToString());
            int CurrentType = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
            SqlTransaction Trans;
            SqlConnection Conn;
            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
            OrderAssetsBOL Assets = new OrderAssetsBOL();
                Assets.Subject = string.Empty;
                Assets.Phone = string.Empty;
                Assets.Email = string.Empty;
                Assets.Location = string.Empty;
                Assets.WebUrl = string.Empty;
                Assets.Date = null;
                Assets.Hours = 0;
                Assets.TimSpan = string.Empty;
                Assets.Message = "";
                Assets.Date = DateTime.Now;
                Assets.AddAudio = false;
                Assets.AddContact = false;
                Assets.AddDirection = false;
                Assets.AddReminder = false;
                Assets.AddWebUrl = false;
                Assets.AddUerProfile = false;
                Assets.AssestFilePath = string.Empty;
                Assets.CreatedByID = custID;
                Assets.ApprovalStatus = 1;
                Assets.Category = 3;
            

                int UserID = 1;
                DBOL.CreatedByID = UserID;
                DBOL.QAID = 1;
                DBOL.QaApproval = 1;
                DBOL.MngrApproval = 1;
                DBOL.CustApproval = 0;
                DBOL.QaRemarks = "Approved";
                DBOL.MngrRemarks = "Approved";
                DBOL.CustRemarks = "";
                if (CurrentType == 1 || CurrentType == 5 || CurrentType == 2) ///1 is free & 5 is friend both can upgrade
                {
                    if (AcType == 3)
                    {
                        if (InsertData(Assets, Trans, Conn, AllFilePaths))
                        {
                            DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds2"].ToString();
                            DBOL.AssestID = Assets.ID;
                            if (Order.InsertOrderdDesign(DBOL))
                            {

                            }
                        }

                    }
                    else if (AcType == 4)
                    {
                        if (InsertData(Assets, Trans, Conn, AllFilePaths))
                        {

                            DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds2"].ToString();
                            DBOL.AssestID = Assets.ID;
                            if (Order.InsertOrderdDesign(DBOL))
                            {

                            }
                        }
                        Conn = DBHelper.GetConnection();
                        Trans = Conn.BeginTransaction();
                        Assets.ID = 0;
                        if (InsertData(Assets, Trans, Conn, AllFilePaths))
                        {

                            DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds4"].ToString();
                            DBOL.AssestID = Assets.ID;
                            if (Order.InsertOrderdDesign(DBOL))
                            {

                            }
                        }
                    }

                }
                else if (CurrentType == 3)
                {
                    if (AcType == 4)
                    {
                        if (InsertData(Assets, Trans, Conn, AllFilePaths))
                        {

                            DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds4"].ToString();
                            DBOL.AssestID = Assets.ID;
                            if (Order.InsertOrderdDesign(DBOL))
                            {

                            }
                        }
                    }
                }


            TransactionBOL PaymentTrns = new TransactionBOL();
            PaymentTrns.TransactionId = int.Parse(snapdt.Rows[0]["TransactionId"].ToString()); 
            PaymentTrns.Cust_Id = custID;
            if (OrderAsset.UpdateTransaction(PaymentTrns))
            {
                string limit = AcType == 3 ? "1500" : AcType == 4 ? "3200" : "";
                PaymentEntry("1", limit, double.Parse(amount.ToString()),AcType,custID);
            }
              

            
        }

        private bool PaymentEntry(string selectedValue, string limit, double amount, int AcType,int Cust_Id)
        {
            Conn = DBHelper.GetConnection();
            ObjCustomerProfileMasterDAL.UpdateAccountType(Cust_Id, AcType, Conn);
            ObjCustomerProfileMasterBOL.Cust_ID = Cust_Id;
            DataTable dt = ObjCustomerProfileMasterDAL.GetValidDaysDate(ObjCustomerProfileMasterBOL);
            int Remainingdays = GetRemainingDays(dt.Rows[0]["ValidDate"].ToString());
            int validationDays = (int.Parse(selectedValue) * 30) + Remainingdays;
            DateTime Validdate = DateTime.Parse(dt.Rows[0]["ValidDate"].ToString()).AddDays((int.Parse(selectedValue) * 30));
            ObjCodeMasterBOL.ValidationDays = validationDays.ToString();
            ObjCodeMasterBOL.ValidDate = Validdate;


            Trans = Conn.BeginTransaction();
            ObjCodeMasterBOL.CodeID = ObjCustomerProfileMasterDAL.GetCodeNo(ObjCustomerProfileMasterBOL);

            if (ObjCodeMasterDAL.UpdateDateDays(ObjCodeMasterBOL, Trans, Conn))
            {
                ObjPaymentBOL.Cust_ID = Cust_Id;
                ObjPaymentBOL.Amount = amount;

                if (ObjPaymentDAL.InsertPaymentDetail(ObjPaymentBOL, Trans, Conn))
                {
                    int EmailLimit = int.Parse(limit);
                    if (objAdvertisement.UpdateEmailsLimit(ObjPaymentBOL.Cust_ID, EmailLimit))
                    {

                        Trans.Commit();
                        Conn.Close();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    return false;
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                //MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }

        public int GetRemainingDays(string date)
        {
            DateTime ValidDate = DateTime.Parse(date);
            DateTime Now = DateTime.Now;
            TimeSpan RemainingDays = ValidDate.Subtract(Now);
            int Days = RemainingDays.Days;
            return Days;
        }
        private bool InsertData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }
                Trans.Commit();
                Conn.Close();
                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception)
                {
                    Trans.Rollback();
                    Conn.Close();
                }
                Trans.Rollback();
                Conn.Close();
                return false;
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }

            if (div.ID == "div1")
            {
                //  row1.Attributes["style"] = "margin-top: 0px;";
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void btnsave_Click(object sender, EventArgs e)
        {
            try
            {
                InsertGenricInfo();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void CustomValidatorProfile_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal size = Math.Round(((decimal)ProfilePhoto.PostedFile.ContentLength / (decimal)1024), 2);
            if (Regxcheck.IsValidImage(ProfilePhoto))
            {
                if (size > 200)
                {
                    CustomValidatorProfile.ErrorMessage = "Image size exceeds 200 KB.";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                CustomValidatorProfile.ErrorMessage = "Only .jpg,.png,.jpeg,.gif Files are allowed.";
                args.IsValid = false;
            }
        }
    }
}