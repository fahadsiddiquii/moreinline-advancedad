﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class Send : System.Web.UI.Page
    {

        ContactListBOL ObjContactListBOL = new ContactListBOL();
       
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        CustomerProfileMasterDAL Cusomer = new CustomerProfileMasterDAL();
        OrderAssetsDAL OAL = new OrderAssetsDAL();
        OrderAssetsBOL OA = new OrderAssetsBOL();
        OrdersDAL Order = new OrdersDAL();
        DataTable GenericInfodt = new DataTable();
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();

        SentEmailsLogDAL ObjSentEmailsLogDAL = new SentEmailsLogDAL();
        SentEmailLogBOL ObjSentEmailLogBOL = new SentEmailLogBOL();

        SqlTransaction Trans;
        SqlConnection Conn;
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static string LoadAds()
        {
            Send sd = new Send();
            DataTable dt = sd.BindDataList();
            String daresult = null;
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);
            daresult = DataSetToJSON(ds);
            return daresult;
            
        }

        public static string DataSetToJSON(DataSet ds)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (DataTable dt in ds.Tables)
            {
                object[] arr = new object[dt.Rows.Count + 1];

                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    arr[i] = dt.Rows[i].ItemArray;
                }

                dict.Add(dt.TableName, arr);
            }

            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }

        public DataTable BindDataList()
        {
            try
            {
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = OAL.GetCustDetail(custID).Tables[0];
                DataTable dtCustActive = Cusomer.GetViewAddsByUserEmail(CustData.Rows[0]["Email"].ToString());
                DataTable dt = Order.GetFinalizedGfx(custID);
                int actype = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
                if (dt.Rows.Count > 0)
                {

                    ViewState["AssetID"] = dt.Rows[0]["AssestID"].ToString();
                    Session["AssetsIDD"] = dt.Rows[0]["AssestID"].ToString();
                    foreach (DataRow item in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(item["FilePath"].ToString()))
                        {
                            string[] Path = item["FilePath"].ToString().Split('~');
                            item["FilePath"] = ".." + Path[1].ToString();

                            string socialmedia = item["socialmedia"].ToString().Replace("~/", "../");
                            item["socialmedia"] = socialmedia;
                        }
                        else
                        {
                            item["FilePath"] = "../Images/Inprocess.jpg";
                        }
                        if (string.IsNullOrWhiteSpace(item["WebUrl"].ToString()))
                        {
                            item["WebUrl"] = "#";
                        }

                        if (string.IsNullOrWhiteSpace(item["Subject"].ToString()))
                        {
                            string[] fileName = item["FilePath"].ToString().Split('/');
                            item["Subject"] = fileName[2].ToString();
                        }

                    }
                }
                return dt;

            }
            catch (Exception)
            {
                return null;
            }
        }



        [WebMethod]
        public static List<ListItem> GetContacts()
        {

            try
            {
                List<ListItem> Contacts = new List<ListItem>();
                ContactListBOL ObjContactListBOL = new ContactListBOL();
                ContactListDAL ObjContactListDAL = new ContactListDAL();
                ObjContactListBOL.CreatedBy = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);

                foreach (DataRow item in dt.Rows)
                {
                    Contacts.Add(new ListItem
                    {
                        Value = item["Cont_ID"].ToString(),
                        Text = item["ListTitle"].ToString()
                    });
                }
                return Contacts;
            }
            catch (Exception)
            {
                return null;
            }
        }

        [WebMethod]
        public static List<ContactListBOL> GetDetails(string id,string searchtxt)
        {
            //Write your database logic here and add items in list
            List<ContactListBOL> ContactList = new List<ContactListBOL>();
            ContactListBOL ContactListObj = new ContactListBOL();
           ContactListObj.Cont_MID = int.Parse(id);
            ContactListDAL ObjContactListDAL = new ContactListDAL();
            ContactListObj.CreatedBy = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
            DataTable dt = string.IsNullOrWhiteSpace(searchtxt)?ObjContactListDAL.GetContactListDetail(ContactListObj): ObjContactListDAL.GetContactListDetail(ContactListObj,searchtxt);
            foreach (DataRow item in dt.Rows)
            {
                ContactList.Add(new ContactListBOL() {
                    Name = item["Name"].ToString(),
                    Email = item["Email"].ToString(),
                    Title = item["Title"].ToString(),
                    Company = item["Company"].ToString(),
                    Cont_DID = int.Parse(item["Cont_DID"].ToString()),
                    IsSubscribed = bool.Parse(item["Subscribed"].ToString())
                }
                );
              
            }
            return ContactList;
        }

        [WebMethod]
        public static string DirectSend(List<ContactListBOL> Emails ,string AssetId ,string ListId,string Subject)
        {
            try
            {
                CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
                CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
                AdvertisementDAL objAdvertisement = new AdvertisementDAL();
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                string AudioPath = string.Empty;
                OrdersDAL Order = new OrdersDAL();
                Send thisobj = new Send();
                int RmainingEmails = int.Parse(thisobj.GetPreviousCount());
                if (RmainingEmails > 0)
                {
                    int custID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                    DataTable GenericInfodt = OrderAsset.GetGenericInfo(custID);
                    AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                    int assetid = int.Parse(AssetId);
                    try
                    {
                        int Checked = 0;
                        foreach (var item in Emails)
                        {

                            string LblEmail = item.Email;
                            string LblName = item.Name;
                            string LblTitle = item.Title;
                            string LblCompany = item.Company;
                            string TrgtUid = item.Cont_DID.ToString();
                            int ContactListId = int.Parse(ListId);
                            int ContactId = int.Parse(TrgtUid);
                            int RmainingEm = int.Parse(thisobj.GetPreviousCount());
                            if (RmainingEm > 0)
                            {
                                Checked++;
                                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                                DataTable dt = Order.GetClientAssetsList(custID);
                                DataRow dr = dt.Select("AssestID=" + assetid.ToString()).FirstOrDefault();
                                DataTable dtCust = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                                string Body = thisobj.ConstructAttachemnet(dt, dtCust, LblEmail, ContactId, assetid.ToString(), ContactListId);
                                string Company = thisobj.GetCustomerCompany();
                                EmailNotification.SendEmail(Body, LblEmail, Subject, Company);
                                string hrs = string.Empty;
                                string ts = string.Empty;
                                DateTime Appointmentdate = DateTime.Now;
                                if (HttpContext.Current.Session["ApTimeSpan"] != null)
                                {
                                    ts = HttpContext.Current.Session["ApTimeSpan"].ToString();
                                }
                                if (HttpContext.Current.Session["Hrs"] != null)
                                {
                                    hrs = HttpContext.Current.Session["Hrs"].ToString();
                                }

                                if (HttpContext.Current.Session["ApDate"] != null)
                                {
                                    Appointmentdate = DateTime.Parse(HttpContext.Current.Session["ApDate"].ToString());
                                }
                                int CatId = int.Parse(dr["CategoryId"].ToString());
                                int custid = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                                objAdvertisement.InsertSentEmailsLog(int.Parse(dr["AssestID"].ToString()), LblName, LblEmail, LblTitle, LblCompany, custID, Appointmentdate, hrs, ts, ContactId, CatId, AudioPath);
                                // return "Success";
                                //objAdvertisement.InsertSentEmailsLog(int.Parse(dt.Rows[0]["AssestID"].ToString()), LblName.Text, LblEmail.Text, LblTitle.Text, LblCompany.Text, custID,);

                            }
                            else
                            {
                                return "LimitExeeds";
                            }

                        }


                        if (Checked == 0)
                        {
                            return "Crequired";
                        }
                        else
                        {
                            return "Success";

                        }

                    }
                    catch (Exception ex)
                    {
                        return "error";
                    }


                    return "";
                }
                else
                {
                    {
                        return "LimitExeeds";
                    }

                }

            }
            catch (Exception)
            {
                return "error";

            }
        }

        [WebMethod]
        public static string CustomizedSend(OrderAssetsBOL AssetInfo, List<ContactListBOL> Emails,string ListId, string DdlOldVoice)
        {
            try
            {
                CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
                CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                AdvertisementDAL objAdvertisement = new AdvertisementDAL();
                OrdersDAL Order = new OrdersDAL();
                Send thisobj = new Send();
                string AudioPath = string.Empty;
                int RmainingEmails = int.Parse(thisobj.GetPreviousCount());
                int assetid = int.Parse(AssetInfo.ID.ToString());
                if (RmainingEmails > 0)
                {
                    int custID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                    DataTable GenericInfodt = OrderAsset.GetGenericInfo(custID);
                    bool isAudEnb = (bool)AssetInfo.AddAudio;
                    bool AddReminder = (bool)AssetInfo.AddReminder;
                    if (isAudEnb)
                    {
                        if (HttpContext.Current.Session["AudPath"] != null)
                        {
                            string audpth = HttpContext.Current.Session["AudPath"].ToString();
                            string Directory = thisobj.Server.MapPath(audpth);
                            FileInfo fileInfo = new FileInfo(Directory);
                            if (fileInfo.Exists)
                            {
                                AudioPath = string.IsNullOrWhiteSpace(audpth) == true ? "" : audpth;
                            }
                            else
                            {  AudioPath = ""; }
                        }
                        else
                        { //AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                            AudioPath = DdlOldVoice;
                        }
                    }
                    try
                    {
                        int Checked = 0;
                        foreach (var item in Emails)
                        {
                            string LblEmail = item.Email;
                            string LblName = item.Name;
                            string LblTitle = item.Title;
                            string LblCompany = item.Company;
                            string TrgtUid = item.Cont_DID.ToString();
                            int ContactListId = int.Parse(ListId);
                            int ContactId = int.Parse(TrgtUid);
                            int RmainingEm = int.Parse(thisobj.GetPreviousCount());
                            if (RmainingEm > 0)
                            {
                                Checked++;
                                ObjCustomerProfileMasterBOL.Cust_ID = custID;
                                DataTable dt = Order.GetClientAssetsList(custID);
                                DataRow dr = dt.Select("AssestID=" + assetid.ToString()).FirstOrDefault();
                                DataTable dtCust = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                                string Body = thisobj.ConstructCusomizeAttachemnet(dt, dtCust, AssetInfo, LblEmail, ContactId, assetid.ToString(), ContactListId);
                                string Company = thisobj.GetCustomerCompany();
                                EmailNotification.SendEmail(Body, LblEmail, AssetInfo.Subject, Company);
                                string  ts = AddReminder? AssetInfo.TimSpan:string.Empty;
                                string hrs = AddReminder ? AssetInfo.Hours.ToString():string.Empty;
                                DateTime Appointmentdate = AddReminder? DateTime.Parse(AssetInfo.Date.Value.ToShortDateString()): DateTime.Now.Date;
                                int CatId = int.Parse(dr["CategoryId"].ToString());
                                objAdvertisement.InsertSentEmailsLog(int.Parse(dr["AssestID"].ToString()), LblName, LblEmail, LblTitle, LblCompany, custID, Appointmentdate, hrs, ts, ContactId, CatId, AudioPath);

                            }
                            else
                            {return "LimitExeeds"; }
                        }
                        if (Checked == 0)
                        { return "Crequired"; }
                        else
                        {  return "Success"; }
                    }
                    catch (Exception)
                    { return "error";  }
                 //   return string.Empty;
                }
                else
                {  return "LimitExeeds"; }
            }
            catch (Exception)
            {  return "error"; }
        }



        private string GetPreviousCount()
        {
            DataTable dt = new DataTable();
            try
            {
                int ID = int.Parse(Session["Cust_ID"].ToString());
                dt = objAdvertisement.GetSentEmailCount(ID);
                //lblSent.Text = dt.Rows[0][0].ToString();
                //lblRemaining.Text = dt.Rows[0][2].ToString();
            }
            catch (Exception)
            {
                return "0";
            }
            return dt.Rows[0][2].ToString();
        }
        

        public string ConstructAttachemnet(DataTable dt, DataTable dtCust, string email, int trgtUid,string Assetid, int ContactListId)
        {
            int TrgtUid = trgtUid;
            DataRow dr = dt.Select("AssestID=" + Assetid).FirstOrDefault();
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            string UnsubscriptionPath = "https://moreinline.com/Emails/Unsubscribe.aspx?CustID=" + dr["CreatedByID"] + "&Lid=" + ContactListId + "&mail=" + email;
            string[] src = dr["FilePath"].ToString().Split('/');
            body = body.Replace("{image}", application_path + src[1].ToString() + "/" + src[2].ToString());
            body = body.Replace("{message}", dr["Massage"].ToString());
            body = body.Replace("{location}", dr["Location"].ToString());
            body = body.Replace("{custcontact}", dtCust.Rows[0]["Phone"].ToString());
            body = body.Replace("{custweb}", dtCust.Rows[0]["Website"].ToString());
            body = body.Replace("{custemail}", dtCust.Rows[0]["Email"].ToString());
            body = body.Replace("{CompanyLogo}", application_path+ "upload/"+ dtCust.Rows[0]["CompanyLogo"].ToString());
            body = body.Replace("{Fishing}", "display:inline;");
            body = body.Replace("{unsubLink}", UnsubscriptionPath);

            body = body.Replace("{unsubStyle}", "display:inline;text-decoration:none;color:#000000;");
            if (true)//if (dr["AddAudio"].ToString().Equals("True"))
            {
                body = body.Replace("{voice}", application_path + "Emails/VoiceMessage.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{voiceVisible}", "display:inline");

            }
            else
            { body = body.Replace("{voiceVisible}", "display:none"); }

            if (true)//if (dr["AddContact"].ToString().Equals("True"))
            {
                body = body.Replace("{contact}", dtCust.Rows[0]["Phone"].ToString());
                body = body.Replace("{contactVisible}", "display:inline");

            }
            else
            { body = body.Replace("{contactVisible}", "display:none"); }

             if(true)// if (dr["AddDirection"].ToString().Equals("True"))
            {
                body = body.Replace("{direction}", application_path + "Emails/Direction.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{directionVisible}", "display:inline");

            }
            else
            { body = body.Replace("{directionVisible}", "display:none"); }

             if(true) //if (dr["AddReminder"].ToString().Equals("True"))
            {
                body = body.Replace("{reminder}", application_path + "Emails/Reminder.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest&TrgtUid=" + TrgtUid);
                body = body.Replace("{reminderVisible}", "display:inline");

            }
            else
            { body = body.Replace("{reminderVisible}", "display:none"); }

             if(true) //  if (dr["AddWebUrl"].ToString().Equals("True"))
            {
                body = body.Replace("{web}", dr["WebUrl"].ToString());
                body = body.Replace("{webVisible}", "display:inline");

            }
            else
            { body = body.Replace("{webVisible}", "display:none"); }

            return body;
        }


        private string GetCustomerCompany()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);
            return dt.Rows[0]["Company"].ToString();
        }


        public string ConstructCusomizeAttachemnet(DataTable dt, DataTable dtCust, OrderAssetsBOL SendingInfo, string email, int trgtUid, string Assetid, int ContactListId)
        {
            int TrgtUid = trgtUid;
            DataRow dr = dt.Select("AssestID=" + Assetid).FirstOrDefault();
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            string UnsubscriptionPath = "https://moreinline.com/Emails/Unsubscribe.aspx?CustID=" + dr["CreatedByID"] + "&Lid=" + ContactListId + "&mail=" + email;
            string[] src = dr["FilePath"].ToString().Split('/');
            body = body.Replace("{image}", application_path + src[1].ToString() + "/" + src[2].ToString());
            body = body.Replace("{message}", SendingInfo.Message);
            body = body.Replace("{location}", dr["Location"].ToString());
            body = body.Replace("{custcontact}", dtCust.Rows[0]["Phone"].ToString());
            body = body.Replace("{custweb}", dtCust.Rows[0]["Website"].ToString());
            body = body.Replace("{custemail}", dtCust.Rows[0]["Email"].ToString());
            body = body.Replace("{CompanyLogo}", application_path + "upload/" + dtCust.Rows[0]["CompanyLogo"].ToString());
            body = body.Replace("{Fishing}", "display:inline;");
            body = body.Replace("{unsubLink}", UnsubscriptionPath);
            bool AddAudio =  (bool)SendingInfo.AddAudio;
            bool AddContact = (bool)SendingInfo.AddContact;
            bool AddDirection = (bool)SendingInfo.AddDirection;
            bool AddReminder = (bool)SendingInfo.AddReminder;
            bool AddWebUrl = (bool)SendingInfo.AddWebUrl;

            body = body.Replace("{unsubStyle}", "display:inline;text-decoration:none;color:#000000;");
            if (AddAudio)
            {
                body = body.Replace("{voice}", application_path + "Emails/VoiceMessage.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{voiceVisible}", "display:inline");

            }
            else
            { body = body.Replace("{voiceVisible}", "display:none"); }

            if (AddContact)
            {
                body = body.Replace("{contact}", dtCust.Rows[0]["Phone"].ToString());
                body = body.Replace("{contactVisible}", "display:inline");

            }
            else
            { body = body.Replace("{contactVisible}", "display:none"); }

            if (AddDirection)
            {
                body = body.Replace("{direction}", application_path + "Emails/Direction.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{directionVisible}", "display:inline");

            }
            else
            { body = body.Replace("{directionVisible}", "display:none"); }

            if (AddReminder)
            {
                body = body.Replace("{reminder}", application_path + "Emails/Reminder.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest&TrgtUid=" + TrgtUid);
                body = body.Replace("{reminderVisible}", "display:inline");

            }
            else
            { body = body.Replace("{reminderVisible}", "display:none"); }

            if (AddWebUrl)
            {
                body = body.Replace("{web}", dr["WebUrl"].ToString());
                body = body.Replace("{webVisible}", "display:inline");

            }
            else
            { body = body.Replace("{webVisible}", "display:none"); }

            return body;
        }

        public  void rdbSavedInfo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {


                Panel1.Visible = false;
               pnlSavedSend.Visible = true;

                CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
                CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
                AdvertisementDAL objAdvertisement = new AdvertisementDAL();
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                string AudioPath = string.Empty;
                OrdersDAL Order = new OrdersDAL();
                Send thisobj = new Send();
                int RmainingEmails = int.Parse(thisobj.GetPreviousCount());
                if (RmainingEmails > 0)
                {
                    int custID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                    DataTable GenericInfodt = OrderAsset.GetGenericInfo(custID);
                    AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                  //  int assetid = int.Parse(AssetId);
                    try
                    {
                        //AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                        lblMailMsg.Text = GenericInfodt.Rows[0]["message"].ToString();
                       // AudioPath = GenericInfodt.Rows[0]["Subject"].ToString();
                       // AudioPath = GenericInfodt.Rows[0]["Phone"].ToString();
                        lblcustemail.Text = GenericInfodt.Rows[0]["Email"].ToString();
                        lblLocation.Text = GenericInfodt.Rows[0]["Location"].ToString();
                        lblcustweb.Text = GenericInfodt.Rows[0]["WebUrl"].ToString();
                    }
                    catch (Exception ex)
                    {
                    }
                }
                else
                {
                    {
                    }

                }
            }
            catch (Exception)
            {

            }
        }

        protected void rdbNewInfo_CheckedChanged(object sender, EventArgs e)
        {
          
            Bind();
            Panel1.Visible = true;
            pnlSavedSend.Visible = false;

            lblMailMsg.Text = "";
            lblcustemail.Text = "";
            lblLocation.Text = "";
            lblcustweb.Text = "";

            string AudioPath = string.Empty;
            int custID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
            DataTable GenericInfodt = OrderAsset.GetGenericInfo(custID);
            AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
            //  int assetid = int.Parse(AssetId);
            try
            {
                //AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                lblMailMsg.Text = GenericInfodt.Rows[0]["message"].ToString();
                // AudioPath = GenericInfodt.Rows[0]["Subject"].ToString();
                // AudioPath = GenericInfodt.Rows[0]["Phone"].ToString();
                lblcustemail.Text = GenericInfodt.Rows[0]["Email"].ToString();
                lblLocation.Text = GenericInfodt.Rows[0]["Location"].ToString();
                lblcustweb.Text = GenericInfodt.Rows[0]["WebUrl"].ToString();
            }
            catch (Exception ex)
            {
            }


        }
        public void Bind()
        {
            int assetID = 0;
            if (Session["AssetsIDD"] != null)
            {
                assetID = int.Parse(Session["AssetsIDD"].ToString());
            }
            DataTable dt = ObjSentEmailsLogDAL.GetSentAudioPath(ObjSentEmailLogBOL, assetID);
            ddlOldVoice.DataSource = dt;
            ddlOldVoice.DataTextField = "Company";
            ddlOldVoice.DataValueField = "AudioPath";
            ddlOldVoice.DataBind();
            ddlOldVoice.Items.Insert(0, new ListItem("Select Old Voice", "0"));
            
            if (dt.Rows.Count == 0)
            {
                // MessageBox(div1, LblMsg, "All Promo Code Are Used Please Create New One ", 0);
            }
        }
    }
}
  
