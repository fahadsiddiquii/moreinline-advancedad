﻿<%@ Page Title="Order Status" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="OrderStatus.aspx.cs" Inherits="MoreInline.MKT_Assets.OrderStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        function Disabled(id) {
            debugger;
            $(id).hide();
        }
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>
    <div class="col-lg-2"></div>

    <section class="col-lg-8 connectedSortable">

        <%-- <div class="row">
            <div class="col-lg-12">
                <div style="background-color: #A9C502; width: 100%; height: 50px;">
                    <h3 class="p-l-10 p-b-10" style="color: #000; margin-top: 11px; float: left; font-family: 'Microsoft JhengHei'">Order Status</h3>
                </div>
            </div>
        </div>--%>
        <div class="row">
            <div class="col-lg-12">
                <center><h3 style="color: #A9C502; float: left; font-family: 'Microsoft JhengHei'"><b>Order Status</b></h3></center>
            </div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <asp:Repeater ID="RptOrderAssets" runat="server" OnItemDataBound="RptOrderAssets_ItemDataBound" OnItemCommand="RptOrderAssets_ItemCommand">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">Order #</th>
                                    <th class="text-center text-black">Title</th>
                                    <th class="text-center text-black">Date/Time Ordered</th>
                                    <th class="text-center text-black">Status</th>
                                    <th class="text-center text-black">Action</th>
                                    <th class="text-center text-black">Design</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center">
                                <asp:Label ID="lblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                                <asp:Label ID="LblAssestID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                                <asp:Label ID="LblFilePath" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                                <asp:Label ID="LblStatusID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StatusID") %>' />
                                <asp:Label ID="LblCustRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="lblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ApprovalStatus") %>' />

                            </td>
                            <td class="text-center">
                                <asp:Button ID="ImgPreview" ToolTip="Preview" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' OnClick="ImgPreview_Click" class="btn btn-sm btn-green" ForeColor="#ffffff" BackColor="#000000" Text="Preview" CommandName="Preview" />
                                <asp:Button ID="ImgSend" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' CommandName="Order/Reorder" OnClick="ImgSend_Click" Text="Order/Reorder" class="btn btn-sm btn-green" />
                                <asp:Button ID="ImgEdit" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' OnClick="ImgPreview_Click" Text="Request Modification" CommandName="RequestModification" class="btn btn-sm btn-green" BackColor="#FF0000" />
                            </td>
                            <td class="text-center">
                                <%--                  <button type="button" id="<%#DataBinder.Eval(Container.DataItem,"AssestID") %>" class="btn btn-sm btn-green btnDesignc " data-toggle="modal" data-target="#myModal" onclick="SetId('<%#DataBinder.Eval(Container.DataItem,"AssestID") %>','<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>','<%#DataBinder.Eval(Container.DataItem,"FilePath") %>','<%#DataBinder.Eval(Container.DataItem,"StatusID") %>','<%#DataBinder.Eval(Container.DataItem,"ApprovalStatus") %>')">Design Status</button>--%>
                                <asp:Button ID="BtnApprove" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' Text="Design Status" CommandName="Approve" class="btn btn-sm btn-green" BackColor="#FF0000" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>

        <%--  <h2>Record</h2>
		<audio controls id="audio"></audio>
    <div>
      <a class="button recordButton" id="record">Record</a>
      <a class="button recordButton" id="recordFor5">Record For 5 Seconds</a>
      <a class="button disabled one" id="pause">Pause</a>
      <a class="button disabled one" id="stop">Reset</a>

                		<div data-type="wav">
			<p>WAV Controls:</p>
			<a class="button disabled one" id="play">Play</a>
      <a class="button disabled one" id="save">Upload to Server</a>
		</div>

            </div>--%>



        <script>
            $(function () {
                $('#example1').DataTable()
                $('#example2').DataTable({
                    'paging': true,
                    'lengthChange': false,
                    'searching': false,
                    'ordering': true,
                    'info': true,
                    'autoWidth': false
                })
            })


        </script>



        <script>
            function SetId(id, OrderNum, src, StatusID, ApprovStatus) {
                debugger;
                $("#hfid").val(id);
                $("#hfStatusID").val(StatusID);
                document.getElementById("<%=lblCode.ClientID %>").innerHTML = OrderNum;
                $("#imgdsg").attr('src', src)
                if (ApprovStatus === "3") {
                    document.getElementById("<%=BtnAp.ClientID %>").style.visibility = 'hidden';
                    document.getElementById("<%=btnDAp.ClientID %>").style.visibility = 'hidden';
                    document.getElementById("<%=txtmodifications.ClientID %>").style.visibility = 'hidden';
                }

            }
        </script>

        <%-- Pop Up --%>
        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                    <div class="modal-body">
                        <div class="row p-t-5">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div id="div2" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                                </div>
                                <br />
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">

                                <asp:HiddenField runat="server" ID="hfid" />
                                <asp:HiddenField runat="server" ID="hfStatusID" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-3">
                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Order Number : </span>
                                <asp:Label Style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon" ID="lblCode" runat="server" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-12">
                                <%--<img src="#" id="imgdsg"  />--%>
                                <asp:Image ImageUrl="#" ID="img" runat="server" Style="width: 550px; height: auto; align-self: center" alt="Alternate Text" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-12">
                                <asp:Button Text="Approve" ID="BtnAp" CssClass="btn btn-success btn-sm" OnClick="BtnAp_Click" runat="server" />
                                <asp:Button Text="Request Modification" ID="btnDAp" CssClass="btn btn-sm btn-green" Style="background-color: Red;" runat="server" OnClick="btnDAp_Click" />
                                <asp:Button Text="Download" ID="BtnDownload" CssClass="btn btn-success btn-sm" OnClick="BtnDownload_Click" runat="server" />
                                <asp:TextBox runat="server" ID="txtmodifications" TextMode="MultiLine" Rows="2" CssClass="form-control textarea-resize m-t-10" placeholder="Request Modification  details.." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                    <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
                </div>
            </div>
        </div>
        <%-- End --%>
    </section>
</asp:Content>
