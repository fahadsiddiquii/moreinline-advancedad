﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Info.aspx.cs" Inherits="MoreInline.MKT_Assets.Info" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="~/bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <style>
        .bodycss {
            background: url('../Images/successbg.png');
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 60%;
        }

        .heading-css {
            margin-top:20px;
        }

       
        @media only screen and (max-width: 900px)
        {
            .bodycss{
                background-size: 90%;
            }
        }
         @media only screen and (max-width: 513px)
        {
            .heading-css{
               margin-top:50px;
            }
        }
          @media only screen and (max-width: 420px)
        {
            .heading-css{
               margin-top:70px;
               font-size:30px;
            }
        }
    </style>
</head>
<body class="bodycss">

    <form id="form1" runat="server">
        
        <div>

            <center>
   <asp:Panel runat="server" ID="pnlsuccess">
    
        <div>
            <h1 runat="server" id="h1" class="heading-css">
            </h1>
        </div>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="pnlfailure">
        
        <div> 
            <h1 class="heading-css">  
              There is Systematic Error Please Try Again 
            </h1>
        </div>
    </asp:Panel>
                <asp:panel ID="pnlOrderSuccess" runat="server" Visible="false">
                     <div>
                       <h1 runat="server" id="h2" class="heading-css">
                      </h1>
                    </div>
                </asp:panel>
                   <asp:button ID="btnGtGallery" text="Go to Gallery " Visible="false" CssClass="btn  btn-light-green" OnClick="btnGtGallery_Click"   runat="server" />
                <asp:button text="Go to Dashboard" ID="btndashboard" CssClass="btn  btn-light-green"  OnClick="Unnamed_Click" runat="server" />
                    <asp:button text="Go to Snap" ID="btnSnap" Visible="false" CssClass="btn  btn-light-green"  OnClick="btnSnap_Click" runat="server" />
</center>

        </div>
    </form>
</body>
</html>
