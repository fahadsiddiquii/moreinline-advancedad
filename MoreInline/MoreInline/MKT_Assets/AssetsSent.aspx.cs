﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class AssetsSent : System.Web.UI.Page
    {
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadList();
                    GetPreviousCount();
                }
            }
            catch (Exception)
            {
                // throw;
                Response.Redirect("~/LoginPage.aspx");
            }
        }

        private void LoadList()
        {

            DataTable dt = new DataTable();
            int ID = int.Parse(Session["Cust_ID"].ToString());
            dt = objAdvertisement.GetSentAssetLog(ID);
            RptAssetSent.DataSource = dt;
            RptAssetSent.DataBind();

        }
        private void GetPreviousCount()
        {
            DataTable dt = new DataTable();
            int ID = int.Parse(Session["Cust_ID"].ToString());
            dt = objAdvertisement.GetSentEmailCount(ID);
            lblSent.Text = dt.Rows[0][0].ToString();
            lblRemaining.Text = dt.Rows[0][2].ToString();
            lbllimit.Text = dt.Rows[0][1].ToString();
        }

        protected void RptAssetSent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string CommandName = e.CommandName.ToString();
                if (e.CommandName == "btnimg")
                {
                    ImageButton FilePath = e.Item.FindControl("LblFilePath") as ImageButton;
                    string File = FilePath.ImageUrl;
                    File = File.Replace("~/", "../");
                    imgpop.Src = File;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal1();", true);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}