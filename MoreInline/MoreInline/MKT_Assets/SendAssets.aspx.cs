﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class SendAssets : System.Web.UI.Page
    {
        ContactListBOL ObjContactListBOL = new ContactListBOL();
        ContactListDAL ObjContactListDAL = new ContactListDAL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        OrdersDAL Order = new OrdersDAL();
        DataTable GenericInfodt = new DataTable();
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();

        SqlTransaction Trans;
        SqlConnection Conn;
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["AssetID"] != null || Request.QueryString["AssetID"] != "")
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        RptSendAssets.DataSource = dt;
                        RptSendAssets.DataBind();
                        LoadContactList();
                        //loadAssets();
                        ImgViewAsset.Attributes.Add("onclick", "return false");
                        int custid = int.Parse(Session["Cust_ID"].ToString());
                        GenericInfodt = OrderAsset.GetGenericInfo(custid);
                        ViewState["GenericInfodt"] = GenericInfodt;
                    }
                    catch (Exception)
                    {
                        MessageBox(div1, LblMsg, "something went wrong", 0);
                    }
                }

            }
        }

        private string UploadAudio(HttpPostedFile file)
        {
            string[] arr = file.FileName.Split('.');
            string fname = arr[0].ToString() + DateTime.Now.Second + "" + ".wav";
            string path = Path.Combine(Server.MapPath("~/ClientAssets/Audio/"), fname);
            file.SaveAs(path);
            Session["AudPath"] = "~/ClientAssets/Audio/" + fname;
            return "~/ClientAssets/Audio/" + fname;

        }
        public void LoadContactList()
        {
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);

            DdlContactList.DataSource = dt;
            DdlContactList.DataTextField = "ListTitle";
            DdlContactList.DataValueField = "Cont_ID";
            DdlContactList.DataBind();
            DdlContactList.Items.Insert(0, new ListItem("Select Contact List", "0"));
            DdlContactList.Items.Insert(1, new ListItem("General List", "2"));

        }

        private string GetPreviousCount()
        {
            DataTable dt = new DataTable();
            try
            {
                int ID = int.Parse(Session["Cust_ID"].ToString());
                dt = objAdvertisement.GetSentEmailCount(ID);
                //lblSent.Text = dt.Rows[0][0].ToString();
                //lblRemaining.Text = dt.Rows[0][2].ToString();
            }
            catch (Exception)
            {
                return "0";
            }
            return dt.Rows[0][2].ToString();
        }
        protected void DdlContactList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlContactList.SelectedValue != "0")
            {
                if (DdlContactList.SelectedValue == "2")
                { GetGeneralListDetail(); }
                else
                { GetContactListDetail(); }
            }
            else if (DdlContactList.SelectedValue == "0")
            {
                RptSendAssets.DataSource = null;
                RptSendAssets.DataBind();
            }



        }

        protected void GetContactListDetail()
        {
            try
            {
                ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                DataTable dt = ObjContactListDAL.GetContactListDetail(ObjContactListBOL);
                if (ViewState["Contacts"] != null)
                {
                    ViewState["Contacts"] = null;
                }
                BtnSend.Visible = true;
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable dtAssets = Order.GetClientAssetsList(custID);
                DataRow dtRow = dtAssets.Select("AssestID=" + Request.QueryString["AssetID"].ToString()).FirstOrDefault();
                if (dtAssets.Rows.Count > 0)
                {
                    ImgViewAsset.Enabled = true;
                    imgDesign.ImageUrl = dtRow["FilePath"].ToString();
                    ViewState["imgAd"] = dtRow["FilePath"].ToString();
                    ViewState["CategoryName"] = dtRow["Name"].ToString();
                    ViewState["CategoryId"] = dtRow["CategoryId"].ToString();

                }
                else
                {
                    BtnSend.Visible = false;
                    ImgViewAsset.Enabled = false;
                }
                ViewState["Contacts"] = dt;
                RptSendAssets.DataSource = dt;
                RptSendAssets.DataBind();

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }


        protected void GetGeneralListDetail()
        {
            try
            {
                ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
                DataTable dt = ObjContactListDAL.GetContactListDetail(ObjContactListBOL);
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable dtAssets = Order.GetClientAssetsList(custID);
                string assetid = Request.QueryString["AssetID"].ToString();
                DataRow dtRow = dtAssets.Select("AssestID=" + assetid).FirstOrDefault();
                if (dtAssets.Rows.Count > 0)
                {
                    ImgViewAsset.Enabled = true;
                    imgDesign.ImageUrl = dtRow["FilePath"].ToString();
                    ViewState["imgAd"] = dtRow["FilePath"].ToString();
                    ViewState["CategoryName"] = dtRow["Name"].ToString();
                    ViewState["CategoryId"] = dtRow["CategoryId"].ToString();




                }
                else
                {
                    BtnSend.Visible = false;
                    ImgViewAsset.Enabled = false;
                }
                if (ViewState["Contacts"] != null)
                {
                    ViewState["Contacts"] = null;
                }
                ViewState["Contacts"] = dt;
                RptSendAssets.DataSource = dt;
                RptSendAssets.DataBind();

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div1.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }


        //private void loadAssets()
        //{
        //    int custID = int.Parse(Session["Cust_ID"].ToString());
        //    DdlAssests.DataTextField = "Subject";
        //    DdlAssests.DataValueField = "AssestID";
        //    DdlAssests.DataSource = Order.GetClientAssetsList(custID);
        //    DdlAssests.DataBind();
        //    DdlAssests.Items.Insert(0, new ListItem("Select Assets", "0"));
        //}

        //protected void DdlAssests_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataTable dt = new DataTable();
        //    if (DdlAssests.SelectedValue != "0")
        //    {
        //        BtnSend.Visible = true;
        //        int custID = int.Parse(Session["Cust_ID"].ToString());
        //        dt = Order.GetClientAssetsList(custID);
        //        dt.Select("AssestID=" + DdlAssests.SelectedValue);
        //        if (dt.Rows.Count > 0)
        //        {
        //            ImgViewAsset.Enabled = true;
        //            imgDesign.ImageUrl = dt.Rows[0]["FilePath"].ToString();
        //        }
        //    }
        //    else
        //    {
        //        BtnSend.Visible = false;
        //        ImgViewAsset.Enabled = false;
        //    }
        //}

        private string GetCustomerCompany()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);
            return dt.Rows[0]["Company"].ToString();
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            int RmainingEmails = int.Parse(GetPreviousCount());
            if (RmainingEmails > 0)
            {
                int custID = int.Parse(Session["Cust_ID"].ToString());
                int assetid = int.Parse(Request.QueryString["AssetID"].ToString());
                if (UpdateAssetInfo(custID))
                {
                    try
                    {
                        int Checked = 0;
                        foreach (RepeaterItem item in RptSendAssets.Items)
                        {
                            CheckBox ChkRow = (CheckBox)item.FindControl("ChkRow");
                            if (ChkRow.Checked == true)
                            {
                                Label LblEmail = (Label)item.FindControl("LblEmail");
                                Label TrgtUid = (Label)item.FindControl("LblCodeID");
                                Label LblName = (Label)item.FindControl("LblName");
                                Label LblTitle = (Label)item.FindControl("LblTitle");
                                Label LblCompany = (Label)item.FindControl("LblCompany");
                                int ContactId = int.Parse(TrgtUid.Text);
                                int RmainingEm = int.Parse(GetPreviousCount());
                                if (RmainingEm > 0)
                                {
                                    Checked++;
                                    ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                                    DataTable dt = Order.GetClientAssetsList(custID);
                                    DataRow dr = dt.Select("AssestID=" + assetid.ToString()).FirstOrDefault();
                                    DataTable dtCust = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                                    string Body = ContructAttachemnet(dt, dtCust, LblEmail.Text,ContactId);
                                    string Company = GetCustomerCompany();
                                    EmailNotification.SendEmail(Body, LblEmail.Text, TxtAdSubject.Value, Company);
                                    string hrs = string.Empty;
                                    string ts = string.Empty;
                                    DateTime Appointmentdate = DateTime.Now;
                                    if (Session["ApTimeSpan"]!= null)
                                    {
                                        ts = Session["ApTimeSpan"].ToString();
                                    }
                                    if (Session["Hrs"] != null)
                                    {
                                         hrs = Session["Hrs"].ToString();
                                    }
                                    
                                    if (Session["ApDate"] != null) {
                                         Appointmentdate = DateTime.Parse(Session["ApDate"].ToString());
                                    }
                                    int CatId = int.Parse(dr["CategoryId"].ToString());
                                    int custid = int.Parse(Session["Cust_ID"].ToString());
                                  //  objAdvertisement.InsertSentEmailsLog(int.Parse(dr["AssestID"].ToString()), LblName.Text, LblEmail.Text, LblTitle.Text, LblCompany.Text, custID,Appointmentdate,hrs,ts, ContactId, CatId);

                                }
                                else
                                {
                                    { MessageBox(div1, LblMsg, "Your Email Limit Exeeds Please Upgrade Your Pakage", 0); }
                                    break;
                                }

                            }
                        }

                        if (Checked == 0)
                        {
                            MessageBox(div1, LblMsg, "Select at least one to send", 0);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, Checked>1? Checked +" Emails has been sent successfully": "Email has been sent successfully", 1);
                            PnlNext.Visible = false;
                            PnlSendAssets.Visible = true;
                          ///  Response.Redirect("~/Dashboard/Dashboard.aspx");
                        }
                    }
                    catch (Exception ex)
                    { MessageBox(div1, LblMsg, ex.Message, 0); }
                }
            }
            else
            {
                { MessageBox(div1, LblMsg, "Your Email Limit Exeeds Please Upgrade Your Pakage", 0); }

            }
        }

        private bool UpdateAssetInfo(int custID)
        {
            GenericInfodt = (DataTable)ViewState["GenericInfodt"];
            DataTable dt = Order.GetClientAssetsList(custID);
            DataRow dr = dt.Select("AssestID=" + Request.QueryString["AssetID"].ToString()).FirstOrDefault();

            if (ChkAddVoice.Checked)
            {
                if (RB_Voice.SelectedValue.Equals("0"))
                {
                    Assets.AddAudio = true;
                    Assets.AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
                }
                else if (RB_Voice.SelectedValue.Equals("1"))
                {
                    Assets.AddAudio = true;
                    if (Session["AudPath"] != null)
                    {
                        string Directory = Server.MapPath(Session["AudPath"].ToString());
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            Assets.AudioPath = string.IsNullOrWhiteSpace(Session["AudPath"].ToString()) == true ? "" : Session["AudPath"].ToString();
                            Assets.AddAudio = true;
                        }
                        else
                        {
                            Assets.AudioPath = "";
                            Assets.AddAudio = true;
                        }
                    }
                }
                else if (RB_Voice.SelectedValue.Equals("2"))
                {
                    if (fuvoice.HasFile)
                    {
                        if (fuvoice.PostedFile.ContentType == "audio/wav" || fuvoice.PostedFile.ContentType == "audio/mpeg")
                        {
                            Assets.AudioPath = UploadAudio(fuvoice.PostedFile);
                            Assets.AddAudio = true;
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Only Mp3 or Waw file can be Uploaded", 0);
                        }

                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Please Upload File", 0);
                    }

                }

            }
            else
            {
                Assets.AddAudio = false;
                Assets.AudioPath = string.Empty;
            }

            if (chkAddUrl.Checked)
            {
                Assets.AddWebUrl = true;
                Assets.WebUrl = GenericInfodt.Rows[0]["WebUrl"].ToString();
            }
            else
            {
                Assets.AddWebUrl = false;
                Assets.WebUrl = string.Empty;
            }
            if (ChkAddReminder.Checked)
            {
                Assets.AddReminder = true;
                Assets.Date = Convert.ToDateTime(dr["Date"].ToString());
                Assets.TimSpan = DdlTT.SelectedItem.Text;
                Session["ApTimeSpan"] = DdlTT.SelectedItem.Text;
         
                if (!DdlHour.SelectedValue.Equals("0"))
                {
                    Assets.Hours = int.Parse(DdlHour.SelectedValue);
                    Session["Hrs"] = DdlHour.SelectedValue;
                }
                else
                {
                    MessageBox(div1, LblMsg, "Please Select Hour", 0);
                    return false;
                }
                if (datepicker.Value != "")
                {
                    if (DateTime.Parse(datepicker.Value) < DateTime.Now)
                    {
                        MessageBox(div1, LblMsg, "Date must be greater than date today", 0);
                        return false;
                    }
                    Assets.Date = DateTime.Parse(datepicker.Value);
                    Session["ApDate"] = datepicker.Value;

                }
                else
                {
                    MessageBox(div1, LblMsg, "Please Select Date", 0);
                    return false;
                }

            }
            else
            {
                Assets.AddReminder = false;
                Assets.Hours = 0;
                Assets.Date = DateTime.Now;
                Assets.TimSpan = string.Empty;
            }
            if (ChkAddContact.Checked)
            {
                Assets.AddContact = true;
                //Assets.Date = DateTime.Now;
                Assets.Phone = GenericInfodt.Rows[0]["Phone"].ToString();
            }
            else
            {
                Assets.AddContact = false;
                Assets.Phone = string.Empty;
            }
            if (ChkAddDirection.Checked)
            {
                Assets.AddDirection = true;
                Assets.Location = GenericInfodt.Rows[0]["Location"].ToString();
            }
            else
            {
                Assets.AddDirection = false;
                Assets.Location = string.Empty;
            }
            if (!string.IsNullOrWhiteSpace(TxtMessage.Text))
            {
                Assets.Message = TxtMessage.Text;
            }
            else
            {
                TxtMessage.Attributes["style"] = "border: 1px solid red;";
                return false;
            }

            if (!string.IsNullOrWhiteSpace(TxtAdSubject.Value))
            {
                Assets.Subject = TxtAdSubject.Value;
            }
            else
            {
                TxtAdSubject.Attributes["style"] = "border: 1px solid red;";
                return false;
            }


            Assets.OrderNum = dr["OrderNum"].ToString();
            Assets.ApprovalStatus = int.Parse(dr["ApprovalStatus"].ToString());
            Assets.Email = dr["Email"].ToString();

            Assets.ID = int.Parse(dr["AssestID"].ToString());
            Assets.CreatedByID = custID;
            Assets.UpdatedByID = custID;
            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            if (OrderAsset.UpdateOrderAssetsForGeneric(Assets, Trans, Conn))
            {
                Trans.Commit();
                return true;
            }
            else
                return false;


        }


        public string ContructAttachemnet(DataTable dt, DataTable dtCust, string email,int trgtUid)
        {
            int TrgtUid = trgtUid;
            DataRow dr = dt.Select("AssestID=" + Request.QueryString["AssetID"].ToString()).FirstOrDefault();
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            int listID = int.Parse(DdlContactList.SelectedValue);
            string UnsubscriptionPath = "https://moreinline.com/Emails/Unsubscribe.aspx?CustID=" + dr["CreatedByID"] + "&Lid=" + listID + "&mail=" + email;
            string[] src = dr["FilePath"].ToString().Split('/');
            body = body.Replace("{image}", application_path + src[1].ToString() + "/" + src[2].ToString());
            body = body.Replace("{message}", dr["Massage"].ToString());
            body = body.Replace("{location}", dr["Location"].ToString());
            body = body.Replace("{custcontact}", dtCust.Rows[0]["Phone"].ToString());
            body = body.Replace("{custweb}", dtCust.Rows[0]["Website"].ToString());
            body = body.Replace("{custemail}", dtCust.Rows[0]["Email"].ToString());
            body = body.Replace("{Fishing}", "display:inline;");
            body = body.Replace("{unsubLink}", UnsubscriptionPath);

            body = body.Replace("{unsubStyle}", "display:inline;text-decoration:none;color:#000000;");
            if (dr["AddAudio"].ToString().Equals("True"))
            {
                body = body.Replace("{voice}", application_path + "Emails/VoiceMessage.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{voiceVisible}", "display:inline");

            }
            else
            { body = body.Replace("{voiceVisible}", "display:none"); }

            if (dr["AddContact"].ToString().Equals("True"))
            {
                body = body.Replace("{contact}", dtCust.Rows[0]["Phone"].ToString());
                body = body.Replace("{contactVisible}", "display:inline");

            }
            else
            { body = body.Replace("{contactVisible}", "display:none"); }

            if (dr["AddDirection"].ToString().Equals("True"))
            {
                body = body.Replace("{direction}", application_path + "Emails/Direction.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest");
                body = body.Replace("{directionVisible}", "display:inline");

            }
            else
            { body = body.Replace("{directionVisible}", "display:none"); }

            if (dr["AddReminder"].ToString().Equals("True"))
            {
                body = body.Replace("{reminder}", application_path + "Emails/Reminder.aspx?AssetID=" + dr["AssestID"].ToString() + "&View=ViewAssest&TrgtUid="+ TrgtUid);
                body = body.Replace("{reminderVisible}", "display:inline");

            }
            else
            { body = body.Replace("{reminderVisible}", "display:none"); }

            if (dr["AddWebUrl"].ToString().Equals("True"))
            {
                body = body.Replace("{web}", dr["WebUrl"].ToString());
                body = body.Replace("{webVisible}", "display:inline");

            }
            else
            { body = body.Replace("{webVisible}", "display:none"); }

            return body;
        }


        private void LoadInfo()
        {
            int custid = int.Parse(Session["Cust_ID"].ToString());
            DataTable GenericInfodt = OrderAsset.GetGenericInfo(custid);
            TxtAdSubject.Value = GenericInfodt.Rows[0]["subject"].ToString();
            TxtMessage.Text = GenericInfodt.Rows[0]["message"].ToString();
        }
        protected void BtnN_Click(object sender, EventArgs e)
        {
            try
            {
                if (DdlContactList.SelectedValue == "0")
                {
                    MessageBox(div1, LblMsg, "Select contact list", 0);
                }
                int Checked = 0;
                DataTable dtList = new DataTable();
               
                DataColumn Name = new DataColumn("Name", typeof(System.String));
                DataColumn Email = new DataColumn("Email", typeof(System.String));
                DataColumn Title = new DataColumn("Title", typeof(System.String));

                dtList.Columns.AddRange(new DataColumn[] { Name, Email, Title });
                foreach (RepeaterItem item in RptSendAssets.Items)
                {
                    CheckBox ChkRow = (CheckBox)item.FindControl("ChkRow");
                    if (ChkRow.Checked == true)
                    {
                        Label LblEmail = (Label)item.FindControl("LblEmail");
                        Label LblName = (Label)item.FindControl("LblName");
                        Label LblTitle = (Label)item.FindControl("LblTitle");
                        Label LblCompany = (Label)item.FindControl("LblCompany");
                        dtList.Rows.Add(LblName.Text, LblEmail.Text, LblTitle.Text);
                        Checked++;
                    }
                    
                }

                if (Checked == 0)
                {
                    MessageBox(div1, LblMsg, "Select at least one to send", 0);
                }
                else
                {
                    LoadInfo();
                    PnlSendAssets.Visible = false;
                    PnlNext.Visible = true;
                    BtnSend.Visible = true;
                    imgAd.ImageUrl = ViewState["imgAd"].ToString();
                    Headingtag.InnerText = ViewState["CategoryName"].ToString();
                    int CategoryId = int.Parse(ViewState["CategoryId"].ToString());
                    if (CategoryId==1)
                    {
                        ChkAddReminder.Checked = true ;
                        PnlTime.Visible = true;
                        ChkAddReminder.Enabled = false;

                    }
                    RptList.DataSource = dtList;
                    RptList.DataBind();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }


        }

        protected void RB_Voice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RB_Voice.SelectedValue == "0")
            {
                PnlRecordVoice.Visible = false;
                pnlUploadVoice.Visible = false;
            }
            else if (RB_Voice.SelectedValue == "1")
            {
                PnlRecordVoice.Visible = true;
                pnlUploadVoice.Visible = false;
            }
            else if (RB_Voice.SelectedValue == "2")
            {
                PnlRecordVoice.Visible = false;
                pnlUploadVoice.Visible = true;
            }
        }

        protected void ChkAddVoice_CheckedChanged(object sender, EventArgs e)
        {
            PnlRecordVoice.Visible = false;
            PnlRDVoice.Visible = ChkAddVoice.Checked;
        }

        protected void ChkAddReminder_CheckedChanged(object sender, EventArgs e)
        {
            PnlTime.Visible = false;
            PnlTime.Visible = ChkAddReminder.Checked;
        }

        protected void RptSendAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSubscribed = e.Item.FindControl("lblSubscribed") as Label;
                CheckBox ChkRow = e.Item.FindControl("ChkRow") as CheckBox;
                if (lblSubscribed.Text.Equals("True"))
                {
                    lblSubscribed.Text = "Subscribed";
                    ChkRow.Enabled = true;
                }
                else
                {
                    lblSubscribed.Text = "Unsubscribed";
                    ChkRow.Enabled = false;

                }
            }
        }

        protected void btnDirectSend_Click(object sender, EventArgs e)
        {
            int RmainingEmails = int.Parse(GetPreviousCount());
            if (RmainingEmails > 0)
            {
                int custID = int.Parse(Session["Cust_ID"].ToString());
                int assetid = int.Parse(Request.QueryString["AssetID"].ToString());
                if (UpdateAssetInfoDirect(custID))
                {
                    try
                    {
                        int Checked = 0;
                        foreach (RepeaterItem item in RptSendAssets.Items)
                        {
                            CheckBox ChkRow = (CheckBox)item.FindControl("ChkRow");
                            if (ChkRow.Checked == true)
                            {
                                Label LblEmail = (Label)item.FindControl("LblEmail");
                                Label LblName = (Label)item.FindControl("LblName");
                                Label LblTitle = (Label)item.FindControl("LblTitle");
                                Label LblCompany = (Label)item.FindControl("LblCompany");
                                Label TrgtUid = (Label)item.FindControl("LblCodeID");
                                int ContactId = int.Parse(TrgtUid.Text);
                                int RmainingEm = int.Parse(GetPreviousCount());
                                if (RmainingEm > 0)
                                {
                                    Checked++;
                                    ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                                    DataTable dt = Order.GetClientAssetsList(custID);
                                    DataRow dr = dt.Select("AssestID=" + assetid.ToString()).FirstOrDefault();
                                    DataTable dtCust = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                                    string Body = ContructAttachemnet(dt, dtCust, LblEmail.Text, ContactId);
                                    string Company = GetCustomerCompany();
                                    EmailNotification.SendEmail(Body, LblEmail.Text, dr["Subject"].ToString(), Company);
                                    string hrs = string.Empty;
                                    string ts = string.Empty;
                                    DateTime Appointmentdate = DateTime.Now;
                                    if (Session["ApTimeSpan"] != null)
                                    {
                                        ts = Session["ApTimeSpan"].ToString();
                                    }
                                    if (Session["Hrs"] != null)
                                    {
                                        hrs = Session["Hrs"].ToString();
                                    }

                                    if (Session["ApDate"] != null)
                                    {
                                        Appointmentdate = DateTime.Parse(Session["ApDate"].ToString());
                                    }
                                    int CatId = int.Parse(dr["CategoryId"].ToString());
                                    int custid = int.Parse(Session["Cust_ID"].ToString());
                                   // objAdvertisement.InsertSentEmailsLog(int.Parse(dr["AssestID"].ToString()), LblName.Text, LblEmail.Text, LblTitle.Text, LblCompany.Text, custID, Appointmentdate, hrs, ts, custid, CatId);

                                 //objAdvertisement.InsertSentEmailsLog(int.Parse(dt.Rows[0]["AssestID"].ToString()), LblName.Text, LblEmail.Text, LblTitle.Text, LblCompany.Text, custID,);

                                }
                                else
                                {
                                    { MessageBox(div1, LblMsg, "Your Email Limit Exeeds Please Upgrade Your Pakage", 0); }
                                    break;
                                }

                            }
                        }

                        if (Checked == 0)
                        {
                            MessageBox(div1, LblMsg, "Select at least one to send", 0);
                        }
                        else
                        {
                           MessageBox(div1, LblMsg, "Email has been sent successfully", 1);
                            ///  Response.Redirect("~/Dashboard/Dashboard.aspx");
                        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "Successs()", true);
                        }
                    }
                    catch (Exception ex)
                    { MessageBox(div1, LblMsg, ex.Message, 0); }

                }
            }
            else
            {
                { MessageBox(div1, LblMsg, "Your Email Limit Exeeds Please Upgrade Your Pakage", 0); }

            }
        }


        private bool UpdateAssetInfoDirect(int custID)
        {
            GenericInfodt = (DataTable)ViewState["GenericInfodt"];
            DataTable dt = Order.GetClientAssetsList(custID);
            DataRow dr = dt.Select("AssestID=" + Request.QueryString["AssetID"].ToString()).FirstOrDefault();
            Assets.AddAudio = true;
            Assets.AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
            Assets.AddWebUrl = true;
            Assets.WebUrl = GenericInfodt.Rows[0]["WebUrl"].ToString();
            Assets.AddReminder = false;
            Assets.Hours = 0;
            Assets.Date = DateTime.Now;
            Assets.TimSpan = string.Empty;
            Assets.AddContact = true;
            Assets.Phone = GenericInfodt.Rows[0]["Phone"].ToString();
            Assets.AddDirection = true;
            Assets.Location = GenericInfodt.Rows[0]["Location"].ToString();
            Assets.Message = GenericInfodt.Rows[0]["message"].ToString();
            Assets.Subject = GenericInfodt.Rows[0]["subject"].ToString();
            Assets.OrderNum = dr["OrderNum"].ToString();
            Assets.ApprovalStatus = int.Parse(dr["ApprovalStatus"].ToString());
            Assets.Email = dr["Email"].ToString();
            Assets.ID = int.Parse(dr["AssestID"].ToString());
            Assets.CreatedByID = custID;
            Assets.UpdatedByID = custID;
            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            if (OrderAsset.UpdateOrderAssetsForGeneric(Assets, Trans, Conn))
            {
                Trans.Commit();
                return true;
            }
            else
                return false;


        }
    }
}