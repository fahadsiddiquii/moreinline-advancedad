﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class ArchiveAssets : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL ObjOrderAssetsDAL = new OrderAssetsDAL();
        OrderAssetsDAL OAL = new OrderAssetsDAL();
        OrderAssetsBOL OA = new OrderAssetsBOL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindDataList();
            }

        }
        public void BindDataList()
        {
            try
            {
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable dt = Order.GetArchiveClientAssets(custID);
                if (dt.Rows.Count > 0)
                {
                    ViewState["AssetID"] = dt.Rows[0]["AssestID"].ToString();

                    foreach (DataRow item in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(item["FilePath"].ToString()))
                        {
                            string[] Path = item["FilePath"].ToString().Split('~');
                            item["FilePath"] = ".." + Path[1].ToString();

                            string socialmedia = item["socialmedia"].ToString().Replace("~/", "");
                            item["socialmedia"] = socialmedia;
                        }
                        else
                        {
                            item["FilePath"] = "../Images/Inprocess.jpg";
                        }
                        if (string.IsNullOrWhiteSpace(item["WebUrl"].ToString()))
                        {
                            item["WebUrl"] = "#";
                        }

                    }
                }
                    RptAssets.DataSource = dt;
                    RptAssets.DataBind();
              

            }
            catch (Exception e)
            {
                MessageBox(div1, LblMsg, e.Message, 0);
            }
        }

        protected void RptAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void RptAssets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int AssetID = int.Parse(e.CommandArgument.ToString());
            //string CommandName = e.CommandName.ToString();
            //string OrderNum = (e.Item.FindControl("lblOrder") as Label).Text;
            //string LblAssestID = (e.Item.FindControl("AssetID") as HiddenField).Value;
            //string LblFilePath = (e.Item.FindControl("LblFilePath") as Label).Text;
            int StatusID = int.Parse((e.Item.FindControl("LblStatusID") as Label).Text);
            //Label LblCustRemarks = e.Item.FindControl("LblCustRemarks") as Label;

            DataTable dt;
            switch (e.CommandName)
            {
                case "Send":
                    if (Order.UpdateIsArchiveFalse(StatusID))
                    {
                        BindDataList();
                        MessageBox(div1, LblMsg, "Sent to live successfully", 1);
                    }
                    break;
                case "ViewPic":
                    dt = Order.GetImageUrl(AssetID);
                    ImgProfile.ImageUrl = dt.Rows.Count == 0 ? "../Images/Inprocess.jpg" : dt.Rows[0]["FilePath"].ToString();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    break;
               
            }
        
        }




        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
    }
}