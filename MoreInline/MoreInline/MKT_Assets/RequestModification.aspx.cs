﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class RequestModification : System.Web.UI.Page
    {
        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        AdvertisementBOL ObjAdvertisementBOL = new AdvertisementBOL();

        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = false;
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ProfilePhoto"] == null || ViewState["ProfilePhoto"].ToString() == "")
                {
                    if (ImgUpload.HasFile)
                    {
                        string fileName = Path.GetFileName(ImgUpload.PostedFile.FileName);
                        ViewState["ProfilePhoto"] = fileName;
                        ImgUpload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Upload Profile Photo", 0);
                        return;
                    }
                }
                if (Local_Validation() == true && AdsFields_Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtEmail.Text != "")
                    {
                        Match match = regex.Match(TxtEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                        else
                        {
                            ObjCustomerProfileMasterBOL.ColumnName = "Email";
                            ObjCustomerProfileMasterBOL.ColumnText = TxtEmail.Text;
                            DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                            if (dt.Rows.Count > 0)
                            {
                                MessageBox(div1, LblMsg, "This Email Already Exist", 0);
                                return;
                            }
                        }
                    }

                    if (TxtUsername.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "UserName";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtUsername.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This UserName Already Exist", 0);
                            return;
                        }
                    }

                    SaveAllDetails();
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public bool Local_Validation()
        {

            TxtWebsite.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtEmail.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtPhone.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";

            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Company Name", 0);
                return false;
            }
            else if (TxtWebsite.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Website", 0);
                TxtWebsite.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Zip Code", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                TxtEmail.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                TxtPhone.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtUsername.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter UserName", 0);
                return false;
            }
            else if (TxtPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text != TxtPassword.Text)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Passwords do not match", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool AdsFields_Validation()
        {
            TxtWebsiteInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtEmailInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtPhoneInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";

            if (TxtAdSubject.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Subject", 0);
                return false;
            }
            else if (TxtEmailInfo.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Email", 0);
                TxtEmailInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtDate.Text == "")
            {
                MessageBox(div1, LblMsg, "Select Ad Date", 0);
                return false;
            }
            else if (TxtPhoneInfo.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Phone", 0);
                TxtPhoneInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtLocation.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Location", 0);
                return false;
            }
            else if (TxtWebsiteInfo.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Website", 0);
                TxtWebsiteInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (!(ChkAddVoice.Checked || ChkAddContact.Checked || ChkAddDirection.Checked || chkAddUrl.Checked || ChkProfilePic.Checked || ChkAddReminder.Checked))
            {
                MessageBox(div1, LblMsg, "Check at Least One", 0);
                return false;
            }
            else if (ChkAddVoice.Checked && Session["AudPath"] == null)
            {
                MessageBox(div1, LblMsg, "Record Voice", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        private void SaveAllDetails()
        {
            List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
            if (AddFile.HasFile)
            {
                AllFilePaths = SaveAssetFile(AddFile);

            }
            else
            {
                if (hffilecount.Value.Count() <= 0)
                {
                    MessageBox(div1, LblMsg, "Please Upload File", 0);
                    return;
                }
            }
            Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            if (TxtEmailInfo.Text != "")
            {
                Match match = regex.Match(TxtEmailInfo.Text);
                if (!match.Success)
                {
                    MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                    return;
                }
            }

            ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
            ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
            ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
            ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
            ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
            ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
            ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
            ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
            ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
            ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
            ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
            ObjCustomerProfileMasterBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
            ObjCustomerProfileMasterBOL.AccTypeID = 1;

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
            {
                SaveOrder(AllFilePaths);
                LoginWork(TxtUsername.Text, TxtPassword.Text);
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }
        private void SaveOrder(List<OrderAssetsDetailBOL> AllFilePaths)
        {
            try
            {
                Assets.Subject = TxtAdSubject.Text;
                Assets.Phone = TxtPhoneInfo.Text;
                Assets.Email = TxtEmailInfo.Text;
                Assets.Location = TxtLocation.Text;
                Assets.Date = Convert.ToDateTime(TxtDate.Text.ToString());
                Assets.Hours = int.Parse(DdlHour.SelectedValue);
                Assets.WebUrl = TxtWebsiteInfo.Text;
                Assets.Message = TxtMessage.Text;
                if (Session["AudPath"] != null)
                {
                    string Directory = Server.MapPath(Session["AudPath"].ToString());
                    FileInfo fileInfo = new FileInfo(Directory);
                    if (fileInfo.Exists)
                    { Assets.AudioPath = string.IsNullOrWhiteSpace(Session["AudPath"].ToString()) == true ? "" : Session["AudPath"].ToString(); }
                    else { Assets.AudioPath = ""; }
                }

                if (!string.IsNullOrWhiteSpace(HfVoicePath.Value))
                    Assets.AudioPath = HfVoicePath.Value;

                Assets.AddAudio = ChkAddVoice.Checked;
                Assets.AddContact = ChkAddContact.Checked;
                Assets.AddDirection = ChkAddDirection.Checked;
                Assets.AddReminder = ChkAddReminder.Checked;
                Assets.AddWebUrl = chkAddUrl.Checked;
                Assets.AddUerProfile = ChkProfilePic.Checked;
                //  Assets.AssestFilePath = FilePath;
                Assets.CreatedByID = ObjCustomerProfileMasterBOL.Cust_ID;
                Assets.ApprovalStatus = 1;

                if (InsertData(Assets, Trans, Conn, AllFilePaths))
                {
                    Trans.Commit();
                    Conn.Close();
                    ActiveAccount(ObjCustomerProfileMasterBOL.Cust_ID);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving!", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        private bool InsertData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }
                if (count > 0)
                {
                    MessageBox(div1, LblMsg, "Order Assests Created Successfully", 1);
                }
                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception)
                {
                    Trans.Rollback();
                    Conn.Close();
                }
                Trans.Rollback();
                Conn.Close();
                return false;
            }
        }
        public void ActiveAccount(int Cust_ID)
        {
            try
            {
                ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
                DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);

                if (dt.Rows[0]["IsActive"].ToString() == "True" && dt.Rows[0]["ActivationDate"].ToString() != "")
                {
                    MessageBox(div1, LblMsg, "This Account is Already Active", 0);
                    return;
                }
                else
                {
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjCustomerProfileMasterDAL.UpdateActiveCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                    {
                        DataTable dt_ActiveOn = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL, Trans, Conn);
                        DateTime ValidDate = DateTime.Parse(dt_ActiveOn.Rows[0]["ActivationDate"].ToString()).AddDays(int.Parse(dt_ActiveOn.Rows[0]["ValidationDays"].ToString()));

                        ObjCodeMasterBOL.ValidDate = ValidDate;
                        ObjCodeMasterBOL.CodeID = int.Parse(dt_ActiveOn.Rows[0]["CodeID"].ToString());

                        if (ObjCodeMasterDAL.UpdateValidDateCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                        {
                            Trans.Commit();
                            Conn.Close();
                        }
                        else
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void LoginWork(string UserName, string Password)
        {
            try
            {
                if (UserName != "" && Password != "")
                {
                    ObjCustomerProfileMasterBOL.UserName = UserName;
                    ObjCustomerProfileMasterBOL.Password = Password;

                    DataTable dt = ObjCustomerProfileMasterDAL.CustomerLogin(ObjCustomerProfileMasterBOL);


                    if (dt.Rows.Count == 1)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["Cust_ID"] = dt.Rows[0]["Cust_ID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["Email"] = dt.Rows[0]["Email"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();

                            Response.Redirect(@"~\Dashboard\Dashboard.aspx", false);
                        }
                        else
                        { MessageBox(div1, LblMsg, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div1, LblMsg, "Invalid Email or Password", 0); }
                }
                else
                { MessageBox(div1, LblMsg, "Enter Email or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        private List<OrderAssetsDetailBOL> SaveAssetFile(FileUpload AddFile)
        {

            List<OrderAssetsDetailBOL> FilePathList = new List<OrderAssetsDetailBOL>();

            if (AddFile.HasFile == false)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "<script>alert('No File Uploaded.')</script>", false);
            }
            else
            {
                foreach (var file in AddFile.PostedFiles)
                {
                    OrderAssetsDetailBOL FilePath = new OrderAssetsDetailBOL();
                    string strFileName = "";
                    string strFilePath = "";
                    string strFolder = "";
                    strFolder = Server.MapPath("~/ClientAssets/");
                    strFileName = file.FileName;
                    strFileName = Path.GetFileName(strFileName);

                    if (!Directory.Exists(strFolder))
                    {
                        Directory.CreateDirectory(strFolder);
                    }
                    strFileName = strFileName + "-" + DateTime.Now.Second.ToString();
                    strFilePath = strFolder + strFileName;

                    if (File.Exists(strFilePath))
                    {
                        FilePath.FilePath = string.Empty;
                    }
                    else
                    {
                        AddFile.PostedFile.SaveAs(strFilePath);
                        FilePath.FilePath = "~/ClientAssets/" + strFileName;
                    }

                    FilePathList.Add(FilePath);
                }
            }
            return FilePathList;
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }


    }
}