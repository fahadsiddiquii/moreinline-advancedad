﻿using MoreInline.BOL;
using MoreInline.DAL;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class ViewAssets : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        CustomerProfileMasterDAL Cusomer = new CustomerProfileMasterDAL();
        OrderAssetsDAL ObjOrderAssetsDAL = new OrderAssetsDAL();
        OrderAssetsDAL OAL = new OrderAssetsDAL();
        OrderAssetsBOL OA = new OrderAssetsBOL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        private static string stripeKey = ConfigurationManager.AppSettings["StripeTestKey"];
        //bool isCancel = false;
        //static bool isReqMod = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int id = int.Parse(Request.QueryString["id"].ToString());
                        NotiDAL.UpdateNoti(id);
                    }
                    BindDataList();

                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void BindDataList()
        {
            try
            {
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = OAL.GetCustDetail(custID).Tables[0];
                DataTable dtCustActive = Cusomer.GetViewAddsByUserEmail(CustData.Rows[0]["Email"].ToString());
                DataTable dt = Order.GetClientAssets(custID);
                int actype = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
                if (dt.Rows.Count > 0)
                {

                    ViewState["AssetID"] = dt.Rows[0]["AssestID"].ToString();
                    foreach (DataRow item in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(item["FilePath"].ToString()))
                        {
                            string[] Path = item["FilePath"].ToString().Split('~');
                            item["FilePath"] = ".." + Path[1].ToString();

                            string socialmedia = item["socialmedia"].ToString().Replace("~/", "../");
                            item["socialmedia"] = socialmedia;
                        }
                        else
                        {
                            item["FilePath"] = "../Images/Inprocess.jpg";
                        }
                        if (string.IsNullOrWhiteSpace(item["WebUrl"].ToString()))
                        {
                            item["WebUrl"] = "#";
                        }

                        if (string.IsNullOrWhiteSpace(item["Subject"].ToString()))
                        {
                            string[] fileName = item["FilePath"].ToString().Split('/');
                            item["Subject"] = fileName[2].ToString();
                        }

                    }
                }

                if (actype.ToString().Equals("2") || actype.ToString().Equals("1"))
                {
                    DataRow row = dt.NewRow();
                    row["FilePath"] = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds1"].ToString();
                    row["AssestID"] = -1;
                    dt.Rows.Add(row);
                    DataRow row2 = dt.NewRow();
                    row2["FilePath"] = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds3"].ToString();
                    row2["AssestID"] = -1;
                    dt.Rows.Add(row2);
                }
                if (actype.ToString().Equals("3"))
                {
                    DataRow row = dt.NewRow();
                    row["FilePath"] = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds3"].ToString();
                    row["AssestID"] = -1;
                    dt.Rows.Add(row);
                }

                RptAssets.DataSource = dt;
                RptAssets.DataBind();

            }
            catch (Exception)
            {
                MessageBox(div1, LblMsg, "System Error", 0);
            }
        }


        protected void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                string FilePath = img.ImageUrl;
                string Directory = Server.MapPath(FilePath);
                FileInfo fileInfo = new FileInfo(Directory);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileInfo.Name);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.Flush();
                Response.WriteFile(fileInfo.FullName);
                Response.End();
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg1, ex.Message, 0);
            }
        }
        protected void BtnAp_Click(object sender, EventArgs e)
        {
            int AssetId = int.Parse(hfid.Value);
            int StatusID = int.Parse(hfStatusID.Value);
            string OrderNumber = lblCode.Text;
            if (Order.UpdateDesignApCust(AssetId, 1))
            {
                if (OAL.UpdateOrderStatus(AssetId, 3))
                {
                    // int ManagerID = 1; //NotiDAL.GetManagerID();
                    int uid = NotiDAL.GetAdminID();
                    NotificationInfo Ninfo = new NotificationInfo();
                    Ninfo.OID = AssetId;
                    Ninfo.UID = uid;//ManagerID;
                    Ninfo.IsViewed = false;
                    Ninfo.UserRoleID = 2;// 4;
                    Ninfo.NotiDate = DateTime.Now;
                    Ninfo.FormPath = "../User/Orders.aspx";
                    NotiDAL.InsertNotification(Ninfo);

                    #region Email For Admin
                    int custID = int.Parse(Session["Cust_ID"].ToString());
                    DataTable CustData = OAL.GetCustDetail(custID).Tables[0];
                    EmailNotification EN = new EmailNotification();
                    EmailNotification ENotif = EN.GetAdminFilledObject();
                    ENotif.EmailSubject = "Deliver Design Approved";
                    ENotif.ButtonText = "View Order";
                    ENotif.Content = CustData.Rows[0]["FirstName"] + " has approved order No: " + OrderNumber;
                    EmailNotification.SendNotification(ENotif);
                    #endregion

                    MessageBox(div1, LblMsg, "Design Approved", 1);
                    BindDataList();
                }

            }

        }

        protected void btnDAp_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtmodifications.Text))
            {
                int AssetId = int.Parse(hfid.Value);
                string OrderNumber = lblCode.Text;
                if (Order.UpdateDesignRejCust(AssetId, 2, txtmodifications.Text))
                {
                    if (OAL.UpdateOrderStatus(AssetId, 2))
                    {
                        //int ManagerID = NotiDAL.GetManagerID();
                        int uid = NotiDAL.GetAdminID();
                        NotificationInfo Ninfo = new NotificationInfo();
                        Ninfo.OID = AssetId;
                        Ninfo.UID = uid;
                        Ninfo.IsViewed = false;
                        Ninfo.UserRoleID = 2;//4;
                        Ninfo.NotiDate = DateTime.Now;
                        Ninfo.FormPath = "../User/Orders.aspx";
                        NotiDAL.InsertNotification(Ninfo);

                        #region Email For Admin
                        int custID = int.Parse(Session["Cust_ID"].ToString());
                        DataTable CustData = OAL.GetCustDetail(custID).Tables[0];
                        EmailNotification EN = new EmailNotification();
                        EmailNotification ENotif = EN.GetAdminFilledObject();
                        ENotif.ButtonText = "View Order";
                        ENotif.EmailSubject = "Your request for modification";
                        ENotif.Content = CustData.Rows[0]["FirstName"] + " has requested for modification in order No: " + OrderNumber;
                        EmailNotification.SendNotification(ENotif);
                        #endregion

                        Order.UpdateFilePath(AssetId);
                        BindDataList();
                        MessageBox(div1, LblMsg, "Request Modification Sent ", 1);
                    }
                }
            }
            else
            {
                MessageBox(div2, LblMsg1, "Please Mention Modifications!", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderStatus();", true);
            }
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {

        }

        protected void RptAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    HiddenField Asset = e.Item.FindControl("AssetID") as HiddenField;
                    int AssetID = int.Parse(Asset.Value);
                    DataTable dtAsset = ObjOrderAssetsDAL.GetOrderAssetsMaster(AssetID);
                    RepeaterItem item = e.Item;
                    Label LblApprov = item.FindControl("LblApprov") as Label;
                    Label LblPrice = item.FindControl("LblPrice") as Label;
                    Button btnDappr = item.FindControl("btnDappr") as Button;
                    Label LblStatusID = item.FindControl("LblStatusID") as Label;
                    Button BtnSend = item.FindControl("BtnSend") as Button;
                    Button BtnRequestModification = item.FindControl("BtnRequestModification") as Button;
                    Button btnArchive = item.FindControl("btnArchive") as Button;
                    HtmlControl InfoDiv = item.FindControl("divInfo") as HtmlControl;
                    HtmlControl divUpgrade = item.FindControl("divUpgrade") as HtmlControl;
                    HtmlControl divPriceLabel = item.FindControl("divPriceLabel") as HtmlControl;
               
                    if (AssetID == -1)
                    {
                        divPriceLabel.Visible = false;
                        divUpgrade.Visible = true;
                        InfoDiv.Visible = false;
                        btnDappr.Visible = false;
                        BtnSend.Visible = false;
                        BtnRequestModification.Visible = false;
                    }
                    //   Label lblRdate = item.FindControl("lblRdate") as Label;

                    if (LblApprov.Text.Equals("1"))
                    {
                        LblPrice.Attributes.CssStyle.Add("font-size", "12px");
                        divPriceLabel.Visible = true;
                        BtnRequestModification.Visible = true;
                        btnArchive.Visible = true;
                        LblPrice.Text = "$25 will be charge on each request modification.";
                    }

                    if (!LblApprov.Text.Equals("1"))
                    {
                        divPriceLabel.Visible = true;
                        btnDappr.Visible = true;
                        BtnSend.Visible = false;
                        LblPrice.Text = "Request Modification is free until you approve.";
                    }
                    if (LblApprov.Text.Equals("2"))
                    {
                        divPriceLabel.Visible = false;
                        btnDappr.Visible = false;
                        BtnSend.Visible = false;
                    }
                    if (string.IsNullOrWhiteSpace(LblStatusID.Text))
                    {
                        divPriceLabel.Visible = false;
                        btnDappr.Visible = false;
                        BtnSend.Visible = false;
                    }

                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        protected void RptAssets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int AssetID = int.Parse(e.CommandArgument.ToString());
            string CommandName = e.CommandName.ToString();
            string OrderNum = (e.Item.FindControl("lblOrder") as Label).Text;
            string LblAssestID = (e.Item.FindControl("AssetID") as HiddenField).Value;
            string LblFilePath = (e.Item.FindControl("LblFilePath") as Label).Text;
            string StatusID = (e.Item.FindControl("LblStatusID") as Label).Text;
            Label LblCustRemarks = e.Item.FindControl("LblCustRemarks") as Label;

            DataTable dt;
            switch (e.CommandName)
            {
                case "Send":
                    Response.Redirect("~/MKT_Assets/SendAssets.aspx?AssetID=" + AssetID);
                    break;
                case "ViewPic":
                    int custID = int.Parse(Session["Cust_ID"].ToString());
                    DataTable CustData = OAL.GetCustDetail(custID).Tables[0];
                    DataTable dtCustActive = Cusomer.GetViewAddsByUserEmail(CustData.Rows[0]["Email"].ToString());
                    int actype = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
                    if (AssetID == -1)
                    {
                        if (actype.ToString().Equals("2"))
                        {
                            ImgProfile.ImageUrl = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds1"].ToString();
                            ImgProfile.ImageUrl = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds3"].ToString();
                        }
                        if (actype.ToString().Equals("3"))
                        {
                            ImgProfile.ImageUrl = "../OrderDesigned/" + dtCustActive.Rows[0]["CustAds3"].ToString();
                        }
                    }
                    else
                    {
                        dt = Order.GetImageUrl(AssetID);
                        ImgProfile.ImageUrl = dt.Rows.Count == 0 ? "../Images/Inprocess.jpg" : dt.Rows[0]["FilePath"].ToString();
                    }


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    break;
                case "OrderStatus":
                    hfid.Value = LblAssestID;
                    hfStatusID.Value = StatusID;
                    lblCode.Text = OrderNum;
                    img.ImageUrl = LblFilePath;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderStatus();", true);
                    break;
                case "ReqModfication":
                    hfid.Value = LblAssestID;
                    hfStatusID.Value = StatusID;
                    lblCode.Text = OrderNum;
                    img.ImageUrl = LblFilePath;
                    btnDAp.Visible = false;
                    BtnAp.Visible = false;
                    //btnRMwithStripe.Visible = true;
                    btnRequestMDF.Visible = true;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderStatus();", true);
                    break;
                case "MoveToArchive":

                    if (Order.UpdateIsArchiveTrue(AssetID))
                    {
                        MessageBox(div2, LblMsg1, "Asset Successfully Moved To Archive ", 1);
                    }
                    else
                    {
                        MessageBox(div2, LblMsg1, "Asset Moving To Archive Fialed  ", 0);
                    }
                    BindDataList();
                    break;
            }


        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void btnRequestMDF_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtmodifications.Text))
            {
                Session["Text"] = txtmodifications.Text;
                int AssetId = int.Parse(hfid.Value);
                string OrderNumber = lblCode.Text;
                Response.Redirect("~/MKT_Assets/ConfirmOrder.aspx?IsRM=true&AssetId=" + AssetId, false);
            }
            else
            {
                MessageBox(div2, LblMsg1, "Please Mention Modifications!", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderStatus();", true);
            }
        }

        protected void btnRMwithStripe_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtmodifications.Text))
            {
                Session["Text"] = txtmodifications.Text;
                int AssetId = int.Parse(hfid.Value);
                string OrderNumber = lblCode.Text;
                Session["AssetId"] = AssetId;
                Session["IsRM"] = "2";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openStripe();", true);
            }
            else
            {
                MessageBox(div2, LblMsg1, "Please Mention Modifications!", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderStatus();", true);
            }

        }


        [WebMethod]
        public static string Charge(CDPCardBOL CDPCard)
        {
            try
            {

                ViewAssets rep = new ViewAssets();
                ConfirmOrderBOL OrderInfo = new ConfirmOrderBOL();
                CDPCard.Cust_ID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                CDPCardDAL CDP = new CDPCardDAL();
                ConfirmOrder CO = new ConfirmOrder();
                NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
                int AmuntIncent = int.Parse(CDPCard.Amount.ToString());
                int AmountDollar = (AmuntIncent * 100);
                StripeConfiguration.ApiKey = stripeKey;
                var options = new ChargeCreateOptions
                {
                    Amount = AmountDollar,
                    Currency = CDPCard.Currency,
                    Source = CDPCard.TokenId,
                    Description = "Transaction",
                };
                var service = new ChargeService();
                var response = service.Create(options);
                if (response.Status.Equals("succeeded"))
                {
                    OrdersDAL Order = new OrdersDAL();
                    SqlTransaction Trans;
                    SqlConnection Conn;
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();
                    string OrderNum = OrderAsset.GetOrderNo();
                    string RequstType = string.Empty;
                    string Modification = string.Empty;
                    int AssetID = -1;
                    if (HttpContext.Current.Session["IsRM"] != null && HttpContext.Current.Session["AssetId"] != null)
                    {
                        RequstType = HttpContext.Current.Session["IsRM"].ToString();
                        AssetID = int.Parse(HttpContext.Current.Session["AssetId"].ToString());
                        Modification = HttpContext.Current.Session["Text"].ToString();
                    }
                    if (CO.RequestModification(AssetID, Modification))
                    {
                        //isReqMod = true;
                        DataTable dt = Order.GetAssetDetails(AssetID);
                        OrderInfo.Amount = CDPCard.Amount;
                        OrderInfo.Cust_Id = CDPCard.Cust_ID;
                        OrderInfo.OrderNum = OrderNum;
                        OrderInfo.AssetID = AssetID;
                        OrderInfo.DesignID = int.Parse(dt.Rows[0]["ID"].ToString());
                        OrderInfo.Quantity = 1;
                        OrderInfo.RequestType = 2;
                        OrderInfo.IsActive = true;
                        OrderInfo.Item = "Request For Modification";
                        OrderInfo.PayerID = CDPCard.Cust_ID.ToString();
                        OrderInfo.PaymentId = response.Id;
                        OrderInfo.CreateDate = DateTime.Now;
                        if (OrderAsset.InsertNewOrder(OrderInfo))
                        {

                            NotificationInfo Ninfo = new NotificationInfo();
                            int uid = NotiDAL.GetAdminID();
                            Ninfo.OID = AssetID;
                            Ninfo.UID = uid;
                            Ninfo.IsViewed = false;
                            Ninfo.UserRoleID = 2;//4;
                            Ninfo.NotiDate = DateTime.Now;
                            Ninfo.FormPath = "../User/Orders.aspx";
                            NotiDAL.InsertNotification(Ninfo);
                            Trans.Commit();
                            Conn.Close();

                        }
                    }

                    DataTable dtCardInfo = CDP.GetCDPInformation(CDPCard.Cust_ID);
                    if (dtCardInfo.Rows.Count > 0)
                    {
                        CDP.UpdateCDPInfo(CDPCard);
                    }
                    else
                    {
                        CDP.InsertCDPCardInfo(CDPCard);
                    }

                    return OrderInfo.AssetID.ToString();
                }
                else
                {
                    return "";
                }
            }

            catch (Exception)
            {
                //HttpContext.Current.Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0);
                return "";
            }
        }
    }
}