﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Web.Services;
using MoreInline.Plans;
using Stripe;
using System.Configuration;

namespace MoreInline.MKT_Assets
{
    public partial class OrderAssets : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        private static string stripeKey = ConfigurationManager.AppSettings["StripeTestKey"];

        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        SqlTransaction Trans;
        SqlConnection Conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {


                if (!IsPostBack)
                {
                LoadCategory();
                if (Request.QueryString["OrderID"] != null)
                {
                
                    int ID = int.Parse(Request.QueryString["OrderID"]);
                    string BtnId = Request.QueryString["BtnId"].ToString();

                    if (BtnId == "ImgPreview")
                    {
                        DisableControls.Disable(this.Form.Controls);

                    }
                    else
                    {
                        // ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "<script>Disabled()</script>", false);
                        BtnUpdate.Visible = true;
                        //  BtnSavedraft.Visible = false;
                        BtnSend.Visible = false;
                    }


                    ///  LoadOrderAssetMasterData(ID, BtnId);
                }
                if (Session["OrderAssetsTour"].ToString() == "False")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "StartWizard();", true);
                }
            }
          div1.Visible = false;
                // onload();
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void LoadCategory()
        {
            try
            {
                int id = int.Parse(Session["AccTypeID"].ToString());
                DataTable dt = OrderAsset.GetCategories(id);
                ddlCategory.DataSource = dt;
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "Id";
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "0"));
            }
            catch (Exception)
            {
                
            }
      
        }
        public void onload()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

            //TxtEmail.Text = dt.Rows[0]["Email"].ToString();
            //TxtPhone.Text = dt.Rows[0]["Phone"].ToString();
            //TxtLocation.Text = dt.Rows[0]["Address"].ToString();
            //TxtWebsite.Text = dt.Rows[0]["Website"].ToString();
            //TxtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");

        }

        private void LoadOrderAssetMasterData(int Id, string BtnId)
        {
            DataTable dt = OrderAsset.GetOrderAssetsMaster(Id);
            if (dt.Rows.Count > 0)
            {


                HfAssetId.Value = dt.Rows[0]["AssestID"].ToString();
                //TxtAdSubject.Text = dt.Rows[0]["Subject"].ToString();
                //TxtPhone.Text = dt.Rows[0]["Phone"].ToString();
                //TxtEmail.Text = dt.Rows[0]["Email"].ToString();
                //TxtLocation.Text = dt.Rows[0]["Location"].ToString();
                //TxtDate.Text = DateTime.Parse(dt.Rows[0]["Date"].ToString()).ToString("yyyy-MM-dd");
                //DdlHour.SelectedValue = dt.Rows[0]["Hours"].ToString();
                //TxtWebsite.Text = dt.Rows[0]["WebUrl"].ToString();
                TxtMessage.Text = dt.Rows[0]["Massage"].ToString();

                string AudioPath = dt.Rows[0]["AudioPath"].ToString();
                HfVoicePath.Value = AudioPath;
                //DdlTT.SelectedValue = dt.Rows[0]["TimeSpan"].ToString().Equals("am") ? "1" : "0";
                //ChkAddVoice.Checked = Convert.ToBoolean(dt.Rows[0]["AddAudio"]);
                //ChkAddContact.Checked = Convert.ToBoolean(dt.Rows[0]["AddContact"]);
                //ChkAddDirection.Checked = Convert.ToBoolean(dt.Rows[0]["AddDirection"]);
                //ChkAddReminder.Checked = Convert.ToBoolean(dt.Rows[0]["AddReminder"]);
                //chkAddUrl.Checked = Convert.ToBoolean(dt.Rows[0]["AddWebUrl"]);
                //ChkProfilePic.Checked = Convert.ToBoolean(dt.Rows[0]["AddUerProfile"]);


                string[] Mpath = AudioPath.Split('~');
                try { AudioPath = Mpath[1].ToString(); }
                catch (Exception) { AudioPath = ""; }
                // Session["Patha"] = ".." + AudioPath;

                if (BtnId == "ImgPreview")
                {
                    if (dt.Rows[0]["ApprovalStatus"].ToString().Equals("0"))
                        BtnQuickOrder.Visible = true;

                    btnbtorder.Visible = true;
                    string script = "<script>Disabled('.." + AudioPath + "','" + BtnId + "')</script>";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);
                }
                else
                {
                    BtnQuickOrder.Visible = false;
                    string script = "<script>Disabled('.." + AudioPath + "','" + BtnId + "')</script>";
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);
                }
                DataTable Detaildt = OrderAsset.GetOrderAssetsDetail(Id);
                hffilecount.Value = Detaildt.Rows.Count.ToString();
                RptOrderAssets.DataSource = Detaildt;
                RptOrderAssets.DataBind();

            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(TxtMessage.Text))
            {
                MessageBox(div1, LblMsg, "Enter Requirements", 0);
                return;
            }
            Button button = (Button)sender;
            string buttonId = button.ID;
           string status = "";
            SaveOrder(buttonId,out status);
        }


        private void SaveOrder(string Btn,out string status)
        {
            status = "success";
            List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
            try
            {
                if (AddFile.HasFile)
                {
                    AllFilePaths = SaveAssetFile(AddFile);
                    Session["SelectedFile"] = AllFilePaths;

                }
                else
                {
                    if (hffilecount.Value.Count() <= 0 && Session["SelectedFile"] == null)
                    {
                        //   MessageBox(div1, LblMsg, "Please Upload File", 0);
                        //  return;
                    }
                    else if (Session["SelectedFile"] != null)
                    {
                        AllFilePaths = (List<OrderAssetsDetailBOL>)Session["SelectedFile"];
                    }
                }

                if (int.Parse(ddlCategory.SelectedValue) < 1)
                {
                    MessageBox(div1, LblMsg, "Please Select Category", 0);
                    status = "Error";
                    return ;
                }
                Assets.Category = int.Parse(ddlCategory.SelectedValue);
                Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                Assets.Subject = string.Empty;
                Assets.Phone = string.Empty;
                Assets.Email = string.Empty;
                Assets.Location = string.Empty;
                Assets.WebUrl = string.Empty;
                Assets.Date = null;
                Assets.Hours = 0;
                Assets.TimSpan = string.Empty;
                Assets.Message = TxtMessage.Text;
                Assets.Date = DateTime.Now;
                if (Session["AudPath"] != null)
                {
                    string Directory = Server.MapPath(Session["AudPath"].ToString());
                    FileInfo fileInfo = new FileInfo(Directory);
                    if (fileInfo.Exists)
                    { Assets.AudioPath = string.IsNullOrWhiteSpace(Session["AudPath"].ToString()) == true ? "" : Session["AudPath"].ToString(); }
                    else { Assets.AudioPath = ""; }
                }
                else if (!string.IsNullOrWhiteSpace(HfVoicePath.Value))
                { Assets.AudioPath = HfVoicePath.Value; }

                Assets.AddAudio = false;
                Assets.AddContact = false;
                Assets.AddDirection = false;
                Assets.AddReminder = false;
                Assets.AddWebUrl = false;
                Assets.AddUerProfile = false;
                Assets.AssestFilePath = string.Empty;
                Assets.CreatedByID = int.Parse(Session["Cust_ID"].ToString());
                switch (Btn)
                {
                    case "BtnSavedraft":
                        Assets.ApprovalStatus = 0;
                        break;
                    case "BtnSend":
                        Assets.ApprovalStatus = 1;
                        break;
                    case "BtnUpdate":
                        Assets.ApprovalStatus = 0;
                        break;
                    default:
                        Assets.ApprovalStatus = 0;
                        break;
                }


                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();
                if (Btn == "BtnSavedraft" || Btn == "BtnSend")
                {
                    Session["AssetObj"] = Assets;
                    Session["AllFilePaths"] = AllFilePaths;
                    // int ManagerID = NotiDAL.GetManagerID();
                    Clear();
                    Response.Redirect("~/MKT_Assets/ConfirmOrder.aspx", false);
                }
                else if(Btn.Equals("BtnSend2"))
                {
                    Assets.ApprovalStatus = 1;
                    Session["AssetObj"] = Assets;
                    Session["AllFilePaths"] = AllFilePaths;
                }
                else if (Btn == "BtnUpdate")
                {
                    Assets.ID = int.Parse(HfAssetId.Value);
                    Assets.UpdatedByID = int.Parse(Session["Cust_ID"].ToString());
                    UpdateData(Assets, Trans, Conn, AllFilePaths);
                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    //     LoadDetailRep();
                    Response.Redirect("~/MKT_Assets/OrderStatus.aspx", false);
                }

            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); status = "Error"; }
        }

        private bool InsertData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }
                // if (count > 0)
                //   {
                MessageBox(div1, LblMsg, "Order Assets Created Successfully", 1);
                Clear();

                // }
                #region Email For Admin
                EmailNotification EN = new EmailNotification();
                EmailNotification.SendNotification(EN.GetAdminFilledObject());
                #endregion


                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

                if (CustData.Rows[0]["IsOrderRecNActive"].ToString().Equals("True"))
                {
                    #region Email For Customer
                    EmailNotification EmailData = EN.GetAdminFilledObject();
                    EmailData.ButtonText = "Review Your Order";
                    EmailData.SenderIdForUnSubscription = int.Parse(Session["Cust_ID"].ToString());
                    EmailData.NotificationType = "IsOrderRecNActive";
                    EmailData.Content = "Thank you! We received your order and have begun the order confirmation process. We will email updates as your order status changes";
                    EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                    EmailData.EmailSubject = "We received your order";
                    EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                    EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                    EmailNotification.SendNotification(EmailData);
                    #endregion
                }
                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }

        private bool UpdateData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.UpdateOrderAssets(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }

                if (count > 0)
                    MessageBox(div1, LblMsg, "Order Assets Updated Successfully", 1);


                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception ex)
                { MessageBox(div1, LblMsg, ex.Message, 0); }

                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }
        public void Clear()
        {
            //TxtAdSubject.Text = TxtDate.Text = TxtEmail.Text = TxtLocation.Text = TxtMessage.Text = TxtPhone.Text = TxtWebsite.Text = "";
            //ChkAddContact.Checked = ChkAddDirection.Checked = ChkAddReminder.Checked = chkAddUrl.Checked = ChkAddVoice.Checked = ChkProfilePic.Checked = false;
            TxtMessage.Text = "";
        }

        private List<OrderAssetsDetailBOL> SaveAssetFile(FileUpload AddFile)
        {

            List<OrderAssetsDetailBOL> FilePathList = new List<OrderAssetsDetailBOL>();

            if (AddFile.HasFile == false)
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", "<script>alert('No File Uploaded.')</script>", false);
            }
            else
            {
                foreach (var file in AddFile.PostedFiles)
                {
                    OrderAssetsDetailBOL FilePath = new OrderAssetsDetailBOL();
                    string strFileName = "";
                    string strFilePath = "";
                    string strFolder = "";
                    int id = int.Parse(Session["Cust_ID"].ToString());
                    strFolder = Server.MapPath("~/ClientAssets/");
                    strFileName = file.FileName;
                    string[] NameContruct = strFileName.Split('.');
                    NameContruct[0] += "-" + id + "-" + DateTime.Now.Second + "";
                    strFileName = NameContruct[0].ToString() + "." + NameContruct[1].ToString();
                    strFileName = Path.GetFileName(strFileName);

                    if (!Directory.Exists(strFolder))
                    {
                        Directory.CreateDirectory(strFolder);
                    }
                    strFilePath = strFolder + strFileName;

                    if (System.IO.File.Exists(strFilePath))
                    {
                        FilePath.FilePath = string.Empty;
                    }
                    else
                    {
                        file.SaveAs(strFilePath);
                        FilePath.FilePath = "~/ClientAssets/" + strFileName;
                    }

                    FilePathList.Add(FilePath);
                }
            }
            return FilePathList;
        }

        protected void RptOrderAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string BtnId = Request.QueryString["BtnId"].ToString();
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RepeaterItem item = e.Item;
                Label FilePath = item.FindControl("FilePath") as Label;
                string[] Name = FilePath.Text.Split('/');

                FilePath.Text = Name[2].ToString();
                if (BtnId == "ImgPreview")
                {
                    Button button = item.FindControl("ImgDelete") as Button;
                    button.BackColor = System.Drawing.ColorTranslator.FromHtml("#A9C502");
                    button.Enabled = false;
                }

            }
        }

        protected void ImgDelete_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string BtnID = btn.ID.ToString();
            int Id = int.Parse(btn.CommandArgument.ToString());
            string FileName = btn.CommandName.ToString();
            if (OrderAsset.DeleteAssetsDetail(Id))
            {
                string Directory = Server.MapPath(FileName);
                FileInfo fileInfo = new FileInfo(Directory);
                if (fileInfo.Exists)
                {
                    fileInfo.Delete();
                }
                MessageBox(div1, LblMsg, "File Deleted !", 0);
                LoadDetailRep();
            }
            else
            {
                MessageBox(div1, LblMsg, "Error Occurred While Deleting", 0);
            }

        }

        private void LoadDetailRep()
        {
            int ID = int.Parse(Request.QueryString["OrderID"]);
            DataTable Detaildt = OrderAsset.GetOrderAssetsDetail(ID);
            RptOrderAssets.DataSource = Detaildt;
            RptOrderAssets.DataBind();
        }

        protected void BtnQuickOrder_Click(object sender, EventArgs e)
        {
            int ID = int.Parse(Request.QueryString["OrderID"].ToString());
            if (OrderAsset.UpdateOrderStatus(ID, 1))
            {
                BtnQuickOrder.Visible = false;
                MessageBox(div1, LblMsg, "Order Sent ", 1);

            }
            else
            {
                MessageBox(div1, LblMsg, "Order Sending Failed ", 0);
            }
        }
        [WebMethod]
        public static void EndTour()
        {
            if (HttpContext.Current.Session["OrderAssetsTour"].ToString() == "False")
            {
                CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();

                if (ObjCustomerProfileMasterDAL.UpdateCustomerTour("OrderAssets", int.Parse(HttpContext.Current.Session["Cust_ID"].ToString())))
                {
                    HttpContext.Current.Session["OrderAssetsTour"] = "True";
                }
            }
        }


        [WebMethod]
        public static string Charge(CDPCardBOL CDPCard)
        {
            try
            {
                OrderAssets rep = new OrderAssets();
                ConfirmOrderBOL OrderInfo = new ConfirmOrderBOL();
                CDPCard.Cust_ID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                CDPCardDAL CDP = new CDPCardDAL();
                ConfirmOrder CO = new ConfirmOrder();
                NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
                int AmuntIncent = int.Parse(CDPCard.Amount.ToString());
                int AmountDollar = (AmuntIncent * 100);
                StripeConfiguration.ApiKey = stripeKey;
                var options = new ChargeCreateOptions
                {
                    Amount = AmountDollar,
                    Currency = CDPCard.Currency,
                    Source = CDPCard.TokenId,
                    Description = "Transaction",
                };
                var service = new ChargeService();
                var response = service.Create(options);
                if (response.Status.Equals("succeeded"))
                {
                    OrdersDAL Order = new OrdersDAL();
                    SqlTransaction Trans;
                    SqlConnection Conn;
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();
                    OrderAssetsBOL Asset = (OrderAssetsBOL)HttpContext.Current.Session["AssetObj"];
                    List<OrderAssetsDetailBOL> detail = (List<OrderAssetsDetailBOL>)HttpContext.Current.Session["AllFilePaths"];
                    string OrderNum = OrderAsset.GetOrderNo();
                    string invoicenum = OrderAsset.GetInvoiceID();
                    if (CO.InsertData(Asset, Trans, Conn, detail))
                    {
                        //DataTable dt = Order.GetAssetDetails(Asset.ID);
                       
                        OrderInfo.Amount = CDPCard.Amount;
                        OrderInfo.Cust_Id = CDPCard.Cust_ID;
                        OrderInfo.OrderNum = OrderNum;
                        OrderInfo.AssetID = Asset.ID;
                        OrderInfo.DesignID = -1;
                        OrderInfo.Quantity = 1;
                        OrderInfo.RequestType = 1;
                        OrderInfo.IsActive = true;
                        OrderInfo.Item = "New Order";
                        OrderInfo.PayerID = CDPCard.Cust_ID.ToString();
                        OrderInfo.PaymentId = response.Id;
                        OrderInfo.CreateDate = DateTime.Now;
                        if (OrderAsset.InsertNewOrder(OrderInfo))
                        {
                            int uid = NotiDAL.GetAdminID();
                            NotificationInfo Ninfo = new NotificationInfo();
                            Ninfo.OID = Asset.ID;
                            Ninfo.UID = uid;// ManagerID;
                            Ninfo.IsViewed = false;
                            Ninfo.UserRoleID = 2;//4;
                            Ninfo.FormPath = "../User/Orders.aspx";
                            Ninfo.NotiDate = DateTime.Now;
                            NotiDAL.InsertNotification(Ninfo, Trans, Conn);
                            Trans.Commit();
                            Conn.Close();
                         
                        }
                        
                    }

                    DataTable dtCardInfo = CDP.GetCDPInformation(CDPCard.Cust_ID);
                    if (dtCardInfo.Rows.Count > 0)
                    {
                        CDP.UpdateCDPInfo(CDPCard);
                    }
                    else
                    {
                        CDP.InsertCDPCardInfo(CDPCard);
                    }

                    return OrderInfo.AssetID.ToString();
                }
                else
                {
                    return "";
                }
            }

            catch (Exception)
            {
                //HttpContext.Current.Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0);
                return "";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string status = "";
            SaveOrder("BtnSend2", out status);
            if (status.Equals("Error"))
            {

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openStripe();", true);

            }
        }
    }
}



