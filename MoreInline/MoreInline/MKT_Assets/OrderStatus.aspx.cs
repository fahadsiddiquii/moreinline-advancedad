﻿
using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class OrderStatus : System.Web.UI.Page
    {
        OrderAssetsDAL OAL = new OrderAssetsDAL();
        OrderAssetsBOL OA = new OrderAssetsBOL();
        OrdersDAL Order = new OrdersDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int id = int.Parse(Request.QueryString["id"].ToString());
                        NotiDAL.UpdateNoti(id);
                    }
                    LoadData();
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        private void LoadData()
        {
            OA.CreatedByID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = OAL.GetOrderStatus(OA);
            foreach (DataRow item in dt.Rows)
            {
                if (!string.IsNullOrWhiteSpace(item[5].ToString()))
                {
                    string[] Path = item[5].ToString().Split('~');
                    item[5] = ".." + Path[1].ToString();
                }
                else
                {
                    if (item["ApprovalStatus"].ToString().Equals("3"))
                    {

                        // string script = "<script>Disabled('#" + item["AssestID"].ToString() + "')</script>";
                        /// ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);
                    }
                }
            }
            RptOrderAssets.DataSource = dt;
            RptOrderAssets.DataBind();
        }

        protected void RptOrderAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RepeaterItem item = e.Item;
                Label lblApprovalStatus = item.FindControl("lblApprovalStatus") as Label;
                Label LblAssestID = item.FindControl("LblAssestID") as Label;
                Button BtnApprove = item.FindControl("BtnApprove") as Button;
                string LblFilePath = (e.Item.FindControl("LblFilePath") as Label).Text;
                Button BtnSnd = item.FindControl("ImgSend") as Button;
                Button ImgEdit = item.FindControl("ImgEdit") as Button;

                string Status = lblApprovalStatus.Text;
                if (Status.Equals("0"))
                {
                    BtnApprove.Enabled = false;
                    lblApprovalStatus.Text = "Draft";
                }
                else if (Status.Equals("1"))
                {
                    ImgEdit.Enabled = false;
                    BtnSnd.Enabled = false;
                    if (!string.IsNullOrWhiteSpace(LblFilePath))
                    {
                        BtnApprove.Enabled = true;
                    }
                    BtnSnd.BackColor = System.Drawing.ColorTranslator.FromHtml("#A9C502");
                    lblApprovalStatus.Text = "Order Sent";
                }
                else if (Status.Equals("2"))
                {
                    ImgEdit.Enabled = false;
                    BtnSnd.Enabled = false;
                    BtnSnd.BackColor = System.Drawing.ColorTranslator.FromHtml("#A9C502");
                    lblApprovalStatus.Text = "In Process";
                    if (string.IsNullOrWhiteSpace(LblFilePath))
                    {
                        BtnApprove.Enabled = false;
                    }
                }
                else if (Status.Equals("3"))
                {
                    ImgEdit.Enabled = false;
                    BtnSnd.Enabled = false;
                    BtnApprove.Enabled = false;
                    BtnSnd.BackColor = System.Drawing.ColorTranslator.FromHtml("#A9C502");
                    lblApprovalStatus.Text = "Recieved";
                }
            }
        }

        protected void ImgPreview_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string BtnID = btn.ID.ToString();
            int Id = int.Parse(btn.CommandArgument.ToString());
            Response.Redirect("~/MKT_Assets/OrderAssets.aspx?OrderID=" + Id + "&BtnId=" + BtnID);

        }

        protected void ImgSend_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string BtnID = btn.ID.ToString();
            int Id = int.Parse(btn.CommandArgument.ToString());
            if (OAL.UpdateOrderStatuscust(Id, 2))
            {
                int ManagerID = NotiDAL.GetManagerID();
                int uid = Session["UserID"] == null ? 1 : int.Parse(Session["UserID"].ToString());
                NotificationInfo Ninfo = new NotificationInfo();
                Ninfo.OID = Id;
                Ninfo.UID = uid;//ManagerID;
                Ninfo.IsViewed = false;
                Ninfo.UserRoleID = 2;//4;
                Ninfo.NotiDate = DateTime.Now;
                Ninfo.FormPath = "../User/Orders.aspx";
                NotiDAL.InsertNotification(Ninfo);
               
                MessageBox(div1, LblMsg, "Order Sent ", 1);
                LoadData();
            }
            else
            {
                MessageBox(div1, LblMsg, "Order Sending Failed ", 0);
            }
        }

        protected void BtnAp_Click(object sender, EventArgs e)
        {
            int AssetId = int.Parse(hfid.Value);
            int StatusID = int.Parse(hfStatusID.Value);
            if (Order.UpdateDesignApCust(AssetId, 1))
            {
                if (OAL.UpdateOrderStatus(AssetId, 3))
                {
                    //int ManagerID = NotiDAL.GetManagerID();
                    int uid = Session["UserID"] == null ? 1 : int.Parse(Session["UserID"].ToString());
                    NotificationInfo Ninfo = new NotificationInfo();
                    Ninfo.OID = AssetId;
                    Ninfo.UID = uid;//ManagerID;
                    Ninfo.IsViewed = false;
                    Ninfo.UserRoleID = 2;// 4;
                    Ninfo.NotiDate = DateTime.Now;
                    Ninfo.FormPath = "../User/Orders.aspx";
                    NotiDAL.InsertNotification(Ninfo);
                    MessageBox(div1, LblMsg, "Design Approved", 1);
                    LoadData();
                }

            }

        }

        protected void btnDAp_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtmodifications.Text))
            {
                int AssetId = int.Parse(hfid.Value);
                if (Order.UpdateDesignRejCust(AssetId, 2, txtmodifications.Text))
                {
                    if (OAL.UpdateOrderStatus(AssetId, 2))
                    {
                        ///int ManagerID = NotiDAL.GetManagerID();

                        int uid = Session["UserID"] == null ? 1 : int.Parse(Session["UserID"].ToString());
                        NotificationInfo Ninfo = new NotificationInfo();
                        Ninfo.OID = AssetId;
                        Ninfo.UID = uid;
                        Ninfo.IsViewed = false;
                        Ninfo.UserRoleID = 2;//4;
                        Ninfo.NotiDate = DateTime.Now;
                        Ninfo.FormPath = "../User/Orders.aspx";
                        NotiDAL.InsertNotification(Ninfo);
                        LoadData();
                        MessageBox(div1, LblMsg, "Request Modification Sent ", 1);
                    }
                }
            }
            else
            {
                MessageBox(div2, LblMsg1, "Please Mention Modifications !", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
        }

        protected void RptOrderAssets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string CommandName = e.CommandName.ToString();
            string OrderNum = (e.Item.FindControl("lblOrderNum") as Label).Text;
            string LblAssestID = (e.Item.FindControl("LblAssestID") as Label).Text;
            string LblFilePath = (e.Item.FindControl("LblFilePath") as Label).Text;
            string StatusID = (e.Item.FindControl("LblStatusID") as Label).Text;

            Label LblCustRemarks = e.Item.FindControl("LblCustRemarks") as Label;

            if (CommandName.Equals("Approve"))
            {
                hfid.Value = LblAssestID;
                hfStatusID.Value = StatusID;
                lblCode.Text = OrderNum;
                img.ImageUrl = LblFilePath;
                txtmodifications.Text = LblCustRemarks.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
        }

        protected void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                string FilePath = img.ImageUrl;
                string Directory = Server.MapPath(FilePath);
                FileInfo fileInfo = new FileInfo(Directory);

                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileInfo.Name);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.Flush();
                Response.WriteFile(fileInfo.FullName);
                Response.End();
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg1, ex.Message, 0);
            }
        }
    }
}