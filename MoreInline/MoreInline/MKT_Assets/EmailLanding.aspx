﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EmailLanding.aspx.cs" Inherits="MoreInline.EmailLanding" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Email Landing</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <%-- ----------------------------------- Script --------------------------------------------- --%>


    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>


    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtLocation.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <style>
        .overlap {
            position: absolute;
            top: 0px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header" style="background-color: #000000;">
                <!-- Logo -->
                <div class="p-t-10 p-l-10 p-b-15">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="240px" height="60px" />
                </div>

            </header>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper m-l-0" style="background-color: #141414;">
                <!-- Main content -->
                <section class="content container" style="padding-bottom: 0px;">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h3 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Customer Ads Promotions</b></h3>
                        </div>
                        <div class="col-lg-8"></div>
                        <div class="col-lg-2 text-right">
                            <asp:Button ID="BtnWatchVideo" runat="server" Text="Watch Demo Video" Style="width: 200px; height: 43px;" CssClass="btn btn-sm btn-green" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="div1" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="PnlStep1" runat="server">

                        <asp:DataList ID="Dl_Advertisement" runat="server" RepeatDirection="Horizontal" RepeatColumns="1" OnItemDataBound="Dl_Advertisement_ItemDataBound">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-8 m-b-30 p-l-0 p-r-0" style="background-color: #000000;">
                                                    <asp:Image ID="ImgAdvertisement" runat="server" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"Images") %>' CssClass="img-responsive" />
                                                    <asp:Image ID="ImgLock_Ad_image" Visible="false" runat="server" ImageUrl="~/Images/Lock_Ad_image.png" CssClass="img-responsive overlap" />
                                                    <asp:Label ID="LblIsLocked" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem,"IsLocked") %>' />
                                                    <div class="p-t-20 p-b-20" style="border-bottom: 2px solid #707070; border-left: 1px solid #707070; border-right: 1px solid #707070;">
                                                        <a class="p-l-18 text-white" style="cursor: pointer;">
                                                            <img src="../Images/Icons/voice-message1.png" class="img-circle" />
                                                            LISTEN VOICE MESSAGE</a>

                                                        <a class="p-l-18 text-white" style="cursor: pointer;">
                                                            <img src="../Images/Icons/email1.png" class="img-circle" />
                                                            CONTACT</a>

                                                        <a class="p-l-18 text-white" style="cursor: pointer;">
                                                            <img src="../Images/Icons/directions1.png" class="img-circle" />
                                                            DIRECTIONS</a>

                                                        <a class="p-l-18 text-white" style="cursor: pointer;">
                                                            <img src="../Images/Icons/reminder1.png" class="img-circle" />
                                                            SAVE REMINDER</a>

                                                        <a class="p-l-18 text-white" style="cursor: pointer;">
                                                            <img src="../Images/Icons/web1.png" class="img-circle" />
                                                            VISIT US</a>
                                                    </div>

                                                </div>
                                                <div class="col-lg-2">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-8 p-b-20 text-center">
                                                    <asp:Button ID="BtnRequest" runat="server" OnClick="BtnRequest_Click" Text="Request modifications" Style="background-color: red; width: 200px; height: 43px;" CssClass="btn btn-sm btn-green" Visible="false" />
                                                    <asp:Button ID="BtnStart" runat="server" OnClick="BtnStart_Click" Text="Start sending this graphics" Style="width: 200px; height: 43px;" CssClass="btn btn-sm btn-green" Visible="false" />
                                                    <asp:Button ID="BtnUpgrade" runat="server" OnClick="BtnUpgrade_Click" Text="Upgrade your plan" Style="width: 200px; height: 43px;" CssClass="btn btn-sm btn-green" Visible="false" />
                                                </div>
                                                <div class="col-lg-2"></div>

                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                        <br />
                    </asp:Panel>
                    <asp:Panel ID="PnlStep2" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h3 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Provide us with a little information</b></h3>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4 text-center p-t-15">
                                <div class="form-group-sm">
                                    <h4 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Personal / Company Information</b></h4>
                                    <br />
                                    <div class="row">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-10 text-left">
                                            <span class="text-white">Profile Photo</span>
                                            <asp:FileUpload ID="ImgUpload" runat="server" />
                                            <asp:TextBox ID="TxtFName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="First Name"></asp:TextBox>
                                            <asp:TextBox ID="TxtLName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Last Name"></asp:TextBox>
                                            <asp:TextBox ID="TxtCompany" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Company"></asp:TextBox>
                                            <asp:TextBox ID="TxtWebsite" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Website"></asp:TextBox>
                                            <asp:TextBox ID="TxtAddress" runat="server" onkeydown="return checkShortcut();" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Residential Address"></asp:TextBox>
                                            <asp:TextBox ID="TxtZipCode" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Zip Code"></asp:TextBox>
                                            <asp:TextBox ID="TxtEmail" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email"></asp:TextBox>
                                            <%--<asp:TextBox ID="TxtConfirmEmail" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Confirm Email"></asp:TextBox>--%>
                                            <asp:TextBox ID="TxtPhone" runat="server" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Contact Number"></asp:TextBox>
                                            <asp:TextBox ID="TxtUsername" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Username"></asp:TextBox>
                                            <asp:TextBox ID="TxtPassword" runat="server" type="password" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Password"></asp:TextBox>
                                            <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password1"></span>
                                            <asp:TextBox ID="TxtConfirmPassword" runat="server" type="password" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Confirm Password"></asp:TextBox>
                                            <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password2"></span>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 text-center p-t-15">
                                <div class="form-group-sm">
                                    <h4 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Ad Information</b></h4>
                                    <br />
                                    <div class="row">
                                        <div class="col-lg-1"></div>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Ad Subject: </span>
                                                <asp:TextBox ID="TxtAdSubject" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Enter message title"></asp:TextBox>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Email: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <asp:TextBox ID="TxtEmailInfo" TextMode="Email" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Enter email address"></asp:TextBox>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <asp:TextBox ID="TxtDate" TextMode="Date" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Select date of service"></asp:TextBox>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Hour: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <asp:DropDownList ID="DdlHour" runat="server" Style="border: 1px solid #4B4A4A; background-color: #A9C502; color: #000; width: 107px;" CssClass="form-control m-t-10">
                                                    <asp:ListItem Value="0" Text="Select Hours"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="DdlTT" runat="server" Style="border: 1px solid #4B4A4A; background-color: #A9C502; color: #000; width: 62px;" CssClass="form-control m-t-10 m-l-5">
                                                    <asp:ListItem Value="0" Text="pm"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="am"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <asp:TextBox ID="TxtPhoneInfo" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10 num " placeholder="Enter phone number"></asp:TextBox>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Location: &nbsp;&nbsp;</span>
                                                <asp:TextBox ID="TxtLocation" runat="server" onkeydown="return checkShortcut();" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Enter business address"></asp:TextBox>
                                            </div>
                                            <div class="input-group">
                                                <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Website: &nbsp;&nbsp;&nbsp;</span>
                                                <asp:TextBox ID="TxtWebsiteInfo" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Enter URL"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-lg-1"></div>
                                    </div>
                                </div>
                                <div class="row m-t-20 text-left">
                                    <div class="col-lg-12">
                                        <p style="color: #707070;"><b>Select any option below to accompany your Ad:</b></p>
                                    </div>
                                </div>

                                <div class="row m-t-5 text-left">
                                    <div class="col-lg-4 p-t-5">
                                        <span>
                                            <asp:CheckBox ID="ChkAddVoice" runat="server" />
                                            <img src="../Images/Icons/voice-message1.png" />
                                            <span style="color: #fff; font-size: 9px;">ADD VOICE MESSAGE</span>
                                        </span>
                                    </div>
                                    <div class="col-lg-4 p-t-5">
                                        <span>
                                            <asp:CheckBox ID="ChkAddContact" runat="server" />
                                            <img src="../Images/Icons/email1.png" />
                                            <span style="color: #fff; font-size: 9px;">ADD CONTACT</span>
                                        </span>
                                    </div>
                                    <div class="col-lg-4 p-t-5">
                                        <span>
                                            <asp:CheckBox ID="ChkAddDirection" runat="server" />
                                            <img src="../Images/Icons/directions1.png" />
                                            <span style="color: #fff; font-size: 9px;">ADD DIRECTIONS</span>
                                        </span>
                                    </div>
                                </div>

                                <div class="row m-t-10 text-left">
                                    <div class="col-lg-4">
                                        <span>
                                            <asp:CheckBox ID="chkAddUrl" runat="server" />
                                            <img src="../Images/Icons/web1.png" style="width: 25px; height: 21px;" />
                                            <span style="color: #fff; font-size: 9px;">ADD WEBSITE</span>
                                        </span>
                                    </div>
                                    <div class="col-lg-4 p-t-3">
                                        <span>
                                            <asp:CheckBox ID="ChkProfilePic" runat="server" />
                                            <span style="color: #fff; font-size: 9px;">USE MY PROFILE PHOTO</span>
                                        </span>
                                    </div>
                                    <div class="col-lg-4 p-t-5">
                                        <span>
                                            <asp:CheckBox ID="ChkAddReminder" runat="server" />
                                            <img src="../Images/Icons/reminder1.png" />
                                            <span style="color: #fff; font-size: 9px;">ADD REMINDER</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="row m-t-20 text-left">
                                    <div class="col-lg-12">
                                        <h5 style="color: #ABC502;"><b>Click the REC button to add sound to your test Ad (Optional)</b></h5>
                                        <div class="box-footer no-border" style="background-color: #DFDFDF;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img src="../Images/Icons/mic.png" alt="Alternate Text" id="record" class="img-circle" style="background-color: #A9C502; float: left;" />
                                                    <img src="../Images/Icons/mic.gif" alt="Alternate Text" id="pause" hidden class="img-circle" style="background-color: #A9C502; float: left;" />
                                                    <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message</span>
                                                    <audio controls id="audio" style="height: 23px; margin-left: 5px; vertical-align: text-top;" class="pull-right;"></audio>
                                                    <img id="stop" src="../Images/delete.png" hidden class="img-circle" style="background-color: #fff; float: right;" />
                                                    <img id="save" src="../Images/up.png" hidden class="img-circle" style="background-color: #fff; float: right;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right m-t-10 m-b-10">
                                    <asp:Button ID="BtnNext" Text="Next" runat="server" OnClick="BtnNext_Click" Style="width: 100px; height: 38px;" CssClass="btn btn-sm btn-green" />
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="PnlStep3" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h3 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Enter your first test email.</b></h3>
                            </div>
                        </div>
                        <br />
                        <div class="row" id="Rowid" runat="server">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <asp:TextBox ID="TxttestEmail1" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email1*"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail2" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email2"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail3" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email3"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail4" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email4"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail5" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email5"></asp:TextBox>
                            </div>
                            <div class="col-lg-4 text-right">
                                <asp:TextBox ID="TxttestEmail6" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email6"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail7" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email7"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail8" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email8"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail9" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email9"></asp:TextBox>
                                <asp:TextBox ID="TxttestEmail10" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email10"></asp:TextBox>
                                <br />
                                <br />
                                <asp:Button ID="BtnSendAD" runat="server" OnClick="BtnSendAD_Click" Text="SEND AD" Style="width: 150px; height: 43px;" CssClass="btn btn-sm btn-green" />
                                <asp:Button ID="BtnBackStep2" runat="server" OnClick="BtnBackStep2_Click" Text="Back" Style="width: 150px; height: 43px;" CssClass="btn btn-sm btn-green" />
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="PnlStep4" runat="server" Visible="false">
                        <div class="modal-dialog">
                            <div class="modal-content" style="background-color: #141414; border: 1px solid #A9C502;">
                                <div class="modal-body text-center">
                                    <h3 style="color: #9F9F9F;">THANK YOU FOR USING</h3>
                                    <center>
                                    <img src="../Images/moreinline_logo.png" width="300px" class="img-responsive" /></center>
                                    <small style="padding-left: 60px; color: #828282;"><i><b>Better Communication - Better Results</b></i></small>

                                    <h2 style="color: #A9C502;">Code Expired</h2>
                                    <h3 style="color: #9F9F9F;">This Code has been Expired.<br />
                                        Click here To Activate This Code.
                                    </h3>
                                    <br />
                                    <asp:Button ID="BtnActivate" runat="server" Text="Activate" CssClass="btn btn-sm btn-green" OnClick="BtnActivate_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </section>
            </div>
        </div>
        <%----------------------------------------- Pop Up -------------------------------------------------------%>
        <%----------------------------------------- Pop Up -------------------------------------------------------%>
    </form>
    <script>
        $("body").on('click', '.toggle-password2', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input2 = $("#TxtConfirmPassword");
            if (input2.attr("type") === "password") {
                input2.attr("type", "text");
            } else {
                input2.attr("type", "password");
            }
        });

        $("body").on('click', '.toggle-password1', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input1 = $("#TxtPassword");
            if (input1.attr("type") === "password") {
                input1.attr("type", "text");
            } else {
                input1.attr("type", "password");
            }

        });
    </script>
</body>
</html>

