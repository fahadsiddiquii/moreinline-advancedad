﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class Details : System.Web.UI.Page
    {
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               if (!IsPostBack)
               {
                    if (Request.QueryString["Date"] != null)
                    {
                        int CustID = int.Parse(Session["Cust_ID"].ToString());
                        DateTime Date = DateTime.Parse(Request.QueryString["Date"].ToString());
                        LoadList(Date,CustID);
                        dateOn.InnerText = Date.ToShortDateString();
                        // GetPreviousCount();
                    }

                }
            }
            catch (Exception eh)
            {
                throw;
            }
        }

        private void LoadList(DateTime Sentdate, int CustID)
        {

            DataTable dt = new DataTable();
            dt = objAdvertisement.GetSentAssetLogByDate(Sentdate,CustID);
            if (dt==null)
            {
                Response.Redirect("~/MKT_Assets/SentItems.aspx");
            }
            else
            {
                RptAssetSent.DataSource = dt;
                RptAssetSent.DataBind();
            }
          

        }


        protected void RptAssetSent_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string CommandName = e.CommandName.ToString();
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                if (e.CommandName == "btnimg")
                {
                    ImageButton FilePath = e.Item.FindControl("LblFilePath") as ImageButton;
                    string File = FilePath.ImageUrl;
                    File = File.Replace("~/", "../");
                    imgpop.Src = File;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal1();", true);
                }
                 else if (e.CommandName.Equals("Resend"))
                {
                    LinkButton LbResend = e.Item.FindControl("btnResend") as LinkButton;
                    int AssetId = int.Parse(LbResend.CommandArgument);
                    Response.Redirect("~/MKT_Assets/SendAssets.aspx?AssetID="+AssetId,false);
                }
                else if (e.CommandName == "InActive")
                {
                    int ID = int.Parse(e.CommandArgument.ToString());
                    if (objAdvertisement.UpdateEmailsLogStatus(ID, true))
                    {
                        DateTime Date = DateTime.Parse(Request.QueryString["Date"].ToString());
                        LoadList(Date,CustID);
                        MessageBox(div1, LblMsg, "You have successfully made the Appointment InActive", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating Appointment", 0);
                        DateTime Date = DateTime.Parse(Request.QueryString["Date"].ToString());
                        LoadList(Date,CustID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        protected void RptAssetSent_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                DateTime Date = DateTime.Parse(Request.QueryString["Date"].ToString());
                DataTable dt = objAdvertisement.GetSentAssetLogByDate(Date, CustID);
                if (dt.Rows.Count < 1)
                {
                    Response.Redirect("~/MKT_Assets/SentItems.aspx", false);
                }
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblIsActive = item.FindControl("LblIsActive") as Label;
                    ImageButton IBtnCheck = item.FindControl("IBtnCheck") as ImageButton;
                    ImageButton IBtnCross = item.FindControl("IBtnCross") as ImageButton;

                    if (LblIsActive.Text == "True")
                    {
                        IBtnCheck.Visible = false;
                        IBtnCross.Visible = true;
                    }
                    else
                    {
                        IBtnCross.Visible = false;
                        IBtnCheck.Visible = true;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}