﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace MoreInline.MKT_Assets
{
    /// <summary>
    /// Summary description for SaveAudio
    /// </summary>
    [WebService(Namespace = "https://moreinline.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SaveAudio : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public string Save()
        {
            int id;
            int fileCount = this.Context.Request.Files.Count;

            if (Session["Cust_ID"] != null && Session["Cust_ID"].ToString() != "")
            {
                id = int.Parse(Session["Cust_ID"].ToString());
            }
            else
            {
                id = 0;
            }

            if (fileCount > 0)
            {
                try
                {
                    HttpFileCollection files = this.Context.Request.Files;
                    string fname = "";
                    string path = "";
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFile file = files[i];
                        fname = file.FileName+DateTime.Now.Second+""+".wav";
                        path = Path.Combine(Server.MapPath("~/ClientAssets/Audio/"), fname );
                        file.SaveAs(path);
                        Session["AudPath"] = "~/ClientAssets/Audio/" + fname;


                    }
                    return "../ClientAssets/Audio/" + fname;
                }
                catch (Exception)
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }
    }
}
