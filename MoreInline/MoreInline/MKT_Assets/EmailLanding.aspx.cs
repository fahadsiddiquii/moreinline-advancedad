﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class EmailLanding : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        AdvertisementBOL ObjAdvertisementBOL = new AdvertisementBOL();

        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        DesignBOL DBOL = new DesignBOL();
        OrdersDAL Order = new OrdersDAL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                TxtDate.Text = DateTime.Now.ToString("yyyy-MM-dd");
                div1.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["FullCode"] != null && Request.QueryString["CodeID"] != null)
                    {
                        if (ActiveCodeExist(int.Parse(Request.QueryString["CodeID"])))
                        { BindDataList(); }
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void BindDataList()
        {
            int num = 1;
            ObjAdvertisementBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
            DataTable dt = ObjAdvertisementDAL.GetAdvertisementDataList(ObjAdvertisementBOL);

            DataTable dt_Ads = new DataTable();
            dt_Ads.Columns.Add(new DataColumn("ID", typeof(string)));
            dt_Ads.Columns.Add(new DataColumn("Images", typeof(string)));
            dt_Ads.Columns.Add(new DataColumn("IsLocked", typeof(string)));

            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (dt.Rows[0][i].ToString() != "")
                {
                    DataRow dr = dt_Ads.NewRow();

                    dr["ID"] = num;
                    dr["Images"] = "~/upload/" + dt.Rows[0][i].ToString();

                    if (i > 0)
                    {
                        dr["IsLocked"] = true;
                    }
                    else
                    {
                        dr["IsLocked"] = false;

                    }
                    dt_Ads.Rows.Add(dr);
                    dt_Ads.AcceptChanges();
                    num++;
                }
            }
            ViewState["dtAppointmentAd"] = "upload/" + dt.Rows[0]["AppointmentAds"].ToString();
            Dl_Advertisement.DataSource = dt_Ads;
            Dl_Advertisement.DataBind();
        }
        protected void Dl_Advertisement_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataListItem item = e.Item;
                    Image ImgLock_Ad_image = item.FindControl("ImgLock_Ad_image") as Image;
                    Label LblIsLocked = item.FindControl("LblIsLocked") as Label;
                    Button BtnRequest = item.FindControl("BtnRequest") as Button;
                    Button BtnStart = item.FindControl("BtnStart") as Button;
                    Button BtnUpgrade = item.FindControl("BtnUpgrade") as Button;

                    if (LblIsLocked.Text == "True")
                    {
                        ImgLock_Ad_image.Visible = true;
                        BtnUpgrade.Visible = true;
                    }
                    else
                    {
                        BtnRequest.Visible = true;
                        BtnStart.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        protected void BtnStart_Click(object sender, EventArgs e)
        {
            PnlStep1.Visible = false;
            PnlStep2.Visible = true;
        }

        public bool Local_Validation()
        {

            TxtWebsite.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtEmail.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtPhone.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";

            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Company Name", 0);
                return false;
            }
            else if (TxtWebsite.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Website", 0);
                TxtWebsite.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Zip Code", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                TxtEmail.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                TxtPhone.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtUsername.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter UserName", 0);
                return false;
            }
            else if (TxtPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text != TxtPassword.Text)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Passwords do not match", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void BtnNext_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ProfilePhoto"] == null || ViewState["ProfilePhoto"].ToString() == "")
                {
                    if (ImgUpload.HasFile)
                    {
                        string fileName = Path.GetFileName(ImgUpload.PostedFile.FileName);
                        ViewState["ProfilePhoto"] = fileName;
                        ImgUpload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Upload Profile Photo", 0);
                        return;
                    }
                }
                if (Local_Validation() == true && AdsFields_Validation() == true)
                {

                    PnlStep2.Visible = false;
                    PnlStep3.Visible = true;
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        protected void BtnSendAD_Click(object sender, EventArgs e)
        {
            try
            {
                if (CheckingEmailTextbox())
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtEmail.Text != "")
                    {
                        Match match = regex.Match(TxtEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }

                    if (TxtEmail.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "Email";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtEmail.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This Email Already Exist", 0);
                            return;
                        }
                    }

                    if (TxtUsername.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "UserName";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtUsername.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This UserName Already Exist", 0);
                            return;
                        }
                    }

                    SaveCustomerDetails();
                    SendGraphicsEmail();
                    LoginWork(TxtUsername.Text, TxtPassword.Text);
                }
                //Clear();
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        private void SaveCustomerDetails()
        {
            ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
            ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
            ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
            ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
            ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
            ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
            ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
            ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
            ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
            ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
            ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
            ObjCustomerProfileMasterBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
            ObjCustomerProfileMasterBOL.AccTypeID = 1;

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
            {
                //Trans.Commit();
                //Conn.Close();
                SaveGraphics();
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }
        private void SaveGraphics()
        {
            List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
            try
            {
                Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                if (TxtEmailInfo.Text != "")
                {
                    Match match = regex.Match(TxtEmailInfo.Text);
                    if (!match.Success)
                    {
                        MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                        return;
                    }
                }
                // Assets.Date = DateTime.Parse(TxtDate.Text);
                Assets.Date = string.IsNullOrEmpty(TxtDate.Text) ? (DateTime?)null : DateTime.Parse(TxtDate.Text);
                Assets.Hours = int.Parse(DdlHour.SelectedValue);
                Assets.WebUrl = TxtWebsiteInfo.Text;
                Assets.Subject = TxtAdSubject.Text;
                Assets.Phone = TxtPhoneInfo.Text;
                Assets.Email = TxtEmailInfo.Text;
                Assets.Location = TxtLocation.Text;
                Assets.Message = string.Empty;
                if (Session["AudPath"] != null)
                {
                    string Directory = Server.MapPath(Session["AudPath"].ToString());
                    FileInfo fileInfo = new FileInfo(Directory);
                    if (fileInfo.Exists)
                    { Assets.AudioPath = string.IsNullOrWhiteSpace(Session["AudPath"].ToString()) == true ? "" : Session["AudPath"].ToString(); }
                    else { Assets.AudioPath = ""; }
                }
                Assets.TimSpan = DdlTT.SelectedItem.Text;
                Assets.AddAudio = ChkAddVoice.Checked;
                Assets.AddContact = ChkAddContact.Checked;
                Assets.AddDirection = ChkAddDirection.Checked;
                Assets.AddReminder = ChkAddReminder.Checked;
                Assets.AddWebUrl = chkAddUrl.Checked;
                Assets.AddUerProfile = ChkProfilePic.Checked;
                Assets.AssestFilePath = "";
                Assets.CreatedByID = ObjCustomerProfileMasterBOL.Cust_ID;
                Assets.ApprovalStatus = 3;
                Assets.Category = 1;

                if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
                {
                    int AssetsID = Assets.ID;

                    if (OrderAsset.InsertEmailLandingAssetsMaster(Assets, Trans, Conn))
                    {
                        Trans.Commit();
                        Conn.Close();
                        ViewState["AssetID"] = Assets.ID;

                        DBOL.AssestID = AssetsID;
                        DBOL.CreatedByID = 1;
                        DBOL.FilePath = "~/" + ViewState["dtAppointmentAd"].ToString();
                        DBOL.QAID = 0;
                        DBOL.QaApproval = 1;
                        DBOL.MngrApproval = 1;
                        DBOL.CustApproval = 1;
                        DBOL.QaRemarks = "Approved";
                        DBOL.MngrRemarks = "Approved";
                        DBOL.CustRemarks = "Approved";
                        if (Order.InsertOrderdDesign(DBOL))
                        { }
                        ActiveAccount(ObjCustomerProfileMasterBOL.Cust_ID);
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void ActiveAccount(int Cust_ID)
        {
            try
            {
                ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
                DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);

                if (dt.Rows[0]["IsActive"].ToString() == "True" && dt.Rows[0]["ActivationDate"].ToString() != "")
                {
                    MessageBox(div1, LblMsg, "This Account is Already Active", 0);
                    return;
                }
                else
                {
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjCustomerProfileMasterDAL.UpdateActiveCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                    {
                        DataTable dt_ActiveOn = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL, Trans, Conn);
                        DateTime ValidDate = DateTime.Parse(dt_ActiveOn.Rows[0]["ActivationDate"].ToString()).AddDays(int.Parse(dt_ActiveOn.Rows[0]["ValidationDays"].ToString()));

                        ObjCodeMasterBOL.ValidDate = ValidDate;
                        ObjCodeMasterBOL.CodeID = int.Parse(dt_ActiveOn.Rows[0]["CodeID"].ToString());

                        if (ObjCodeMasterDAL.UpdateValidDateCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                        {
                            Trans.Commit();
                            Conn.Close();
                        }
                        else
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void LoginWork(string UserName, string Password)
        {
            try
            {
                if (UserName != "" && Password != "")
                {
                    ObjCustomerProfileMasterBOL.UserName = UserName;
                    ObjCustomerProfileMasterBOL.Password = Password;

                    DataTable dt = ObjCustomerProfileMasterDAL.CustomerLogin(ObjCustomerProfileMasterBOL);


                    if (dt.Rows.Count == 1)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["Cust_ID"] = dt.Rows[0]["Cust_ID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["Email"] = dt.Rows[0]["Email"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();
                            Session["AccTypeID"] = dt.Rows[0]["AccTypeID"].ToString();

                            Response.Redirect(@"~\Dashboard\Dashboard.aspx", false);
                        }
                        else
                        { MessageBox(div1, LblMsg, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div1, LblMsg, "Invalid Email or Password", 0); }
                }
                else
                { MessageBox(div1, LblMsg, "Enter Email or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public bool AdsFields_Validation()
        {
            TxtWebsiteInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtEmailInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtPhoneInfo.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";

            if (TxtAdSubject.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Ad Subject", 0);
                return false;
            }
            #region OldValidations
            //else if (TxtEmailInfo.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Enter Ad Email", 0);
            //    TxtEmailInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
            //    return false;
            //}
            //else if (TxtDate.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Select Ad Date", 0);
            //    return false;
            //}
            //else if (TxtPhoneInfo.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Enter Ad Phone", 0);
            //    TxtPhoneInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
            //    return false;
            //}
            //else if (TxtLocation.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Enter Ad Location", 0);
            //    return false;
            //}
            //else if (TxtWebsiteInfo.Text == "")
            //{
            //    MessageBox(div1, LblMsg, "Enter Ad Website", 0);
            //    TxtWebsiteInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
            //    return false;
            //}
            //else if (!(ChkAddVoice.Checked || ChkAddContact.Checked || ChkAddDirection.Checked || chkAddUrl.Checked || ChkProfilePic.Checked || ChkAddReminder.Checked))
            //{
            //    MessageBox(div1, LblMsg, "Check at Least One", 0);
            //    return false;
            //}
            //else if (ChkAddVoice.Checked && Session["AudPath"] == null)
            //{
            //    MessageBox(div1, LblMsg, "Record Voice", 0);
            //    return false;
            //} 
            #endregion
            //Checking unchecked checkboxes
            else if ((TxtEmailInfo.Text != "" || TxtPhoneInfo.Text != "") && !ChkAddContact.Checked)
            {
                MessageBox(div1, LblMsg, "Please Check ADD CONTACT Checkbox", 0);
                return false;
            }
            else if (TxtDate.Text != "" && !ChkAddReminder.Checked)
            {
                MessageBox(div1, LblMsg, "Please Check ADD REMINDER Checkbox", 0);
                return false;
            }
            else if (DdlHour.SelectedValue != "0" && !ChkAddReminder.Checked)
            {
                MessageBox(div1, LblMsg, "Please Check ADD REMINDER Checkbox", 0);
                return false;
            }
            else if (TxtLocation.Text != "" && !ChkAddDirection.Checked)
            {
                MessageBox(div1, LblMsg, "Please Check ADD DIRECTIONS Checkbox", 0);
                return false;
            }
            else if (TxtWebsiteInfo.Text != "" && !chkAddUrl.Checked)
            {
                MessageBox(div1, LblMsg, "Please Check ADD WEBSITE Checkbox", 0);
                return false;
            }
            // Checking Empty Textboxes
            else if ((TxtEmailInfo.Text == "" && TxtPhoneInfo.Text == "") && ChkAddContact.Checked)
            {
                MessageBox(div1, LblMsg, "Please Enter Email or Phone", 0);
                TxtEmailInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                TxtPhoneInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtDate.Text == "" && ChkAddReminder.Checked)
            {
                MessageBox(div1, LblMsg, "Please Enter Date", 0);
                return false;
            }
            else if (DdlHour.SelectedValue == "0" && ChkAddReminder.Checked)
            {
                MessageBox(div1, LblMsg, "Select Hour", 0);
                return false;
            }
            else if (TxtLocation.Text == "" && ChkAddDirection.Checked)
            {
                MessageBox(div1, LblMsg, "Please Enter Location", 0);
                return false;
            }
            else if (TxtWebsiteInfo.Text == "" && chkAddUrl.Checked)
            {
                MessageBox(div1, LblMsg, "Please Enter Website", 0);
                TxtWebsiteInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else if (TxtWebsiteInfo.Text != "" && chkAddUrl.Checked)
            {
                Regex Url = new Regex(@"((http\://|https\://|ftp\://)+(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?");

                Match matchUrl = Url.Match(TxtWebsiteInfo.Text);
                if (!matchUrl.Success)
                {
                    TxtWebsiteInfo.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                    MessageBox(div1, LblMsg, "Enter Website With Proper Format.", 0);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }
        public bool CheckingEmailTextbox()
        {
            #region OldCssCode
            //TxttestEmail1.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail2.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail3.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail4.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail5.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail6.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail7.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail8.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail9.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            //TxttestEmail10.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;"; 
            #endregion
            for (int i = 1; i < 11; i++)
            {
                TextBox Txt = Rowid.FindControl("TxttestEmail" + i) as TextBox;
                Txt.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            }
            Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

            if (TxttestEmail1.Text == "")
            {
                MessageBox(div1, LblMsg, "Email1 text field is empty!", 0);
                TxttestEmail1.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                return false;
            }
            else
            {
                for (int i = 1; i < 11; i++)
                {
                    TextBox Txt = Rowid.FindControl("TxttestEmail" + i) as TextBox;
                    if (Txt.Text != "")
                    {
                        Match match = regex.Match(Txt.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            Txt.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                            return false;
                        }
                    }

                }
                #region OldCode
                //Match match = regex.Match(TxttestEmail1.Text);
                //if (!match.Success)
                //{
                //    MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //    TxttestEmail1.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //    return false;
                //}
                //else
                //{
                //    if (TxttestEmail2.Text != "")
                //    {
                //        Match match2 = regex.Match(TxttestEmail2.Text);
                //        if (!match2.Success)
                //        {
                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail2.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }
                //    if (TxttestEmail3.Text != "")
                //    {
                //        Match match3 = regex.Match(TxttestEmail3.Text);
                //        if (!match3.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail3.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail4.Text != "")
                //    {
                //        Match match4 = regex.Match(TxttestEmail4.Text);
                //        if (!match4.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail4.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail5.Text != "")
                //    {
                //        Match match5 = regex.Match(TxttestEmail5.Text);
                //        if (!match5.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail5.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail6.Text != "")
                //    {
                //        Match match6 = regex.Match(TxttestEmail6.Text);
                //        if (!match6.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail6.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail7.Text != "")
                //    {
                //        Match match7 = regex.Match(TxttestEmail7.Text);
                //        if (!match7.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail7.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail8.Text != "")
                //    {
                //        Match match8 = regex.Match(TxttestEmail8.Text);
                //        if (!match8.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail8.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail9.Text != "")
                //    {
                //        Match match9 = regex.Match(TxttestEmail9.Text);
                //        if (!match9.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail9.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        }

                //    }

                //    if (TxttestEmail10.Text != "")
                //    {
                //        Match match10 = regex.Match(TxttestEmail10.Text);
                //        if (!match10.Success)
                //        {

                //            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                //            TxttestEmail10.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                //            return false;
                //        } 
                #endregion
                return true;
            }
        }
        public void SendGraphicsEmail()
        {
            //string emails = "";
            for (int i = 1; i < 11; i++)
            {
                TextBox Txt = Rowid.FindControl("TxttestEmail" + i) as TextBox;

                if (Txt.Text != "")
                {
                    #region OldWork
                    //if (emails == "")
                    //{
                    //    emails += Txt.Text;
                    //}
                    //else
                    //{
                    //    emails += "," + Txt.Text;
                    //} 
                    #endregion
                    EmailWork(Txt.Text);
                }

            }

        }
        public void EmailWork(string Email)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{image}", application_path + ViewState["dtAppointmentAd"].ToString());

            if (ChkAddVoice.Checked)
            {
                body = body.Replace("{voice}", application_path + "Emails/VoiceMessage.aspx?AssetID=" + ViewState["AssetID"].ToString());
                body = body.Replace("{voiceVisible}", "display:inline");

            }
            else
            { body = body.Replace("{voiceVisible}", "display:none"); }

            if (ChkAddContact.Checked)
            {
                body = body.Replace("{contact}", application_path + "Emails/Contact.aspx?AssetID=" + ViewState["AssetID"].ToString());
                body = body.Replace("{contactVisible}", "display:inline");

            }
            else
            { body = body.Replace("{contactVisible}", "display:none"); }

            if (ChkAddDirection.Checked)
            {
                body = body.Replace("{direction}", application_path + "Emails/Direction.aspx?AssetID=" + ViewState["AssetID"].ToString());
                body = body.Replace("{directionVisible}", "display:inline");

            }
            else
            { body = body.Replace("{directionVisible}", "display:none"); }

            if (ChkAddReminder.Checked)
            {
                body = body.Replace("{reminder}", application_path + "Emails/Reminder.aspx?AssetID=" + ViewState["AssetID"].ToString());
                body = body.Replace("{reminderVisible}", "display:inline");

            }
            else
            { body = body.Replace("{reminderVisible}", "display:none"); }

            if (chkAddUrl.Checked)
            {
                body = body.Replace("{web}", TxtWebsiteInfo.Text);
                body = body.Replace("{webVisible}", "display:inline");

            }
            else
            { body = body.Replace("{webVisible}", "display:none"); }




            MailMessage mm = new MailMessage("noreply@event-envite.com", Email);
            mm.Subject = TxtAdSubject.Text;
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.event-envite.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "noreply@event-envite.com";
            NetworkCred.Password = "Ingph$2018$RT674q";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }
        public void Clear()
        {
            TxtAdSubject.Text = TxtDate.Text = TxtEmailInfo.Text = TxtLocation.Text = TxtPhoneInfo.Text = TxtWebsiteInfo.Text = "";
            ChkAddContact.Checked = ChkAddDirection.Checked = ChkAddReminder.Checked = chkAddUrl.Checked = ChkAddVoice.Checked = ChkProfilePic.Checked = false;
        }
        public bool ActiveCodeExist(int Code_ID)
        {
            ObjCustomerProfileMasterBOL.CodeID = Code_ID;
            DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCode(ObjCustomerProfileMasterBOL);

            int Days;

            TimeSpan RemainingDays = DateTime.Now.Subtract(DateTime.Parse(dt.Rows[0]["EmailSendOn"].ToString()));
            Days = RemainingDays.Days;

            if (dt.Rows.Count == 0)
            {
                MessageBox(div1, LblMsg, "This Code is Already In Use", 0);
                return false;
            }
            else if (dt.Rows[0]["IsUtilized"].ToString() == "True")
            {
                MessageBox(div1, LblMsg, "This Code is Already In Use", 0);
                return false;
            }
            else if (Days > 30)
            {
                PnlStep4.Visible = true;
                return false;
            }
            else { return true; }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        protected void BtnBackStep2_Click(object sender, EventArgs e)
        {
            PnlStep3.Visible = false;
            PnlStep2.Visible = true;
        }

        protected void BtnRequest_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MKT_Assets/RequestModification.aspx?FullCode=" + Request.QueryString["FullCode"] + "&CodeID=" + Request.QueryString["CodeID"]);
        }
        protected void BtnUpgrade_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/SignUpPaid.aspx?FullCode=" + Request.QueryString["FullCode"] + "&CodeID=" + Request.QueryString["CodeID"]);
        }

        protected void BtnActivate_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["FullCode"] != null && Request.QueryString["CodeID"] != null)
            {
                if (ActivateCodeExist(int.Parse(Request.QueryString["CodeID"])))
                {
                    BindDataList();
                    PnlStep4.Visible = false;
                }
            }
        }
        public bool ActivateCodeExist(int Code_ID)
        {
            ObjCustomerProfileMasterBOL.CodeID = Code_ID;
            DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCode(ObjCustomerProfileMasterBOL);

            if (dt.Rows.Count == 0)
            {
                MessageBox(div1, LblMsg, "This Code is Already In Use", 0);
                return false;
            }
            else if (dt.Rows[0]["IsUtilized"].ToString() == "True")
            {
                MessageBox(div1, LblMsg, "This Code is Already In Use", 0);
                return false;
            }
            else { return true; }
        }
    }
}