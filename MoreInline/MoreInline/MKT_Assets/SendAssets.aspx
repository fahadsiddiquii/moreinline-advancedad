﻿<%@ Page Title="Send Assets" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="SendAssets.aspx.cs" Inherits="MoreInline.MKT_Assets.SendAssets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>

    <script>

     //   window.onload = reset_alert_count;
    </script>
    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>

    <script type="text/javascript">
        function checkAll(bx) {
            var cbs = document.getElementsByTagName('input');
            for (var i = 0; i < cbs.length; i++) {
                if (cbs[i].type == 'checkbox') {
                    cbs[i].checked = bx.checked;
                    if (cbs[i].checked == false) {
                        bx.checked = false;
                    }
                }
            }
        }
    </script>

    <script src="../dist/js/BadWords.js"></script>
    <script>
        function validate_text() {

            var compare_text = $("#ContentPlaceHolder1_TxtAdSubject").val();
            var array = compare_text.split(" ");
            let obj = {};

            for (let i = 0; i < array.length; i++) {

                if (!obj[array[i]]) {
                    const element = array[i].toLowerCase();
                    obj[element] = true;
                }
            }

            for (let j = 0; j < swear_words_arr.length; j++) {

                if (obj[swear_words_arr[j]]) {
                    $("#ContentPlaceHolder1_TxtAdSubject").val('');
                    alert("The message will not be sent!!!\nThe following unethical words are found:\n_______________________________\n" + swear_words_arr[j] + "\n_______________________________");
                }
            }

        }
    </script>

     <script type="text/javascript">
        function checkSpcialChar(event) {
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
                if (event.keyCode != 32) {
                    event.returnValue = false;
                    alert("Special characters are not allowed due to security reason.");
                    return;
                }
            }
            event.returnValue = true;
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } 
            }, seconds * 1000);
        }
    </script>


    <style>
        .LabelSpan {
             background-color: #f6f7f8 !important;
            border-color: #f6f7f8 !important;
            padding-top: 10px;
        }
    </style>

    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1" runat="server">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
        <br />
    </div>
    <asp:Panel ID="PnlSendAssets" runat="server">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4 text-center">
                <h1 style="color: #001b00;">Let's select your contacts</h1>
            </div>
            <div class="col-lg-4"></div>
        </div>
        <br />

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
                <asp:DropDownList ID="DdlContactList" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;"
                    OnSelectedIndexChanged="DdlContactList_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="col-lg-2">
                <%--<asp:DropDownList ID="DdlAssests" OnSelectedIndexChanged="DdlAssests_SelectedIndexChanged" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000;" AutoPostBack="true">
            </asp:DropDownList>--%>
            </div>
            <div class="col-lg-6 ">
                <div class="pull-right">
                    <span>Preview Selected Asset
                    <asp:ImageButton ImageUrl="~/Images/iconEye.png" Height="24" Width="24" ID="ImgViewAsset" runat="server" Style="vertical-align: bottom; padding-left: 2px;" data-toggle="modal" data-target="#myModal" OnClientClick=";return false;" />
                    </span>

                </div>
            </div>
            <div class="col-lg-2 pull-right"></div>
        </div>
        <br />
        <div class="row p-t-10">
            <div class="col-lg-1"></div>
            <div class="col-lg-10" style="overflow-x: auto;">
                <asp:Repeater ID="RptSendAssets" runat="server" OnItemDataBound="RptSendAssets_ItemDataBound">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th style="width: 5px;">
                                        <asp:CheckBox ID="ChkHeader" runat="server" onclick="checkAll(this);" />
                                    </th>
                                    <th class="text-center text-black">Name</th>
                                    <th class="text-center text-black">Email</th>
                                    <th class="text-center text-black">Title</th>
                                    <th class="text-center text-black">Company</th>
                                    <th class="text-center text-black">Subscription</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center">
                                <asp:CheckBox Text="" ID="ChkRow" runat="server" />
                                <asp:Label ID="LblCodeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cont_DID") %>' Visible="false" />
                                <asp:Label ID="LblCodeMID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cont_MID") %>' Visible="false" />
                                <%--<asp:Label ID="lblSubscribed" visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subscribed") %>' Visible="false" />--%>
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />

                            </td>
                            <td class="text-center">
                                <asp:Label ID="lblSubscribed" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subscribed") %>' />
                            </td>

                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="col-lg-1"></div>
        </div>
        <div class="row m-t-20">
            
            <div class="col-lg-11 text-center m-b-10">
                <asp:Button ID="btnDirectSend" Visible="false" OnClick="btnDirectSend_Click" runat="server"  Text="Send now using your saved company information" CssClass="btn btn-light-green m-t-5"  />
                <asp:Button ID="BtnN" runat="server"  OnClick="BtnN_Click" Text="Go to next page to customize your ad information" CssClass="btn btn-black-green m-t-5"  />
            </div>
            <div class="col-lg-1"></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PnlNext" runat="server" Visible="false">
        <div class="container-fluid">
            <h1 style="color: #001b00;" class="text-center">Customize your Ad information</h1>
            <hr />
            <div class="row">

                <%--------Left Panel with Appointment Ad Image ---------%>

                <div class="col-lg-5">
                    <h2 runat="server" id="Headingtag" style="color: #007f9f;"></h2>
                    <asp:Image ID="imgAd" AlternateText="Appointment Ad" CssClass="img-responsive" runat="server" />
                    <br />
                    <label>Sending to:</label>
                    <div style="overflow-x: auto;">
                        <asp:Repeater ID="RptList" runat="server">
                            <HeaderTemplate>
                                <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                                    <thead class="table-head">
                                        <tr style="background-color: #A9C502">
                                            <th class="text-center text-black">Name</th>
                                            <th class="text-center text-black">Email</th>
                                            <th class="text-center text-black">Title</th>
                                        </tr>
                                        <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="text-center">
                                        <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                                    </td>
                                    <td class="text-center">
                                        <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                                    </td>
                                    <td class="text-center">
                                        <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody></table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>

                </div>

                <%--------Right Panel with Details ---------%>

                <div class="col-lg-7">
                    <h2 style="color: #007f9f;">Customize your Subject Line</h2>
                    <div class="row m-b-10">
                        <div class="col-lg-12 m-t-5">
                            <label >If you do not enter any information here, the subject line will be used from company information page.</label>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group m-t-5">
                                <span class="input-group-addon LabelSpan">Ad Subject:</span>
                                <input id="TxtAdSubject" class="form-control m-t-10" placeholder="Enter message title" onkeypress="return checkSpcialChar(event)" type="text" runat="server" name="name" value="" onchange="validate_text()" />

                                <%-- <asp:TextBox ID="TxtAdSubject" runat="server" CssClass="form-control m-t-5 txtBox-style" placeholder="Enter message title"></asp:TextBox>--%>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <h2 style="color: #007f9f;">Select options below to send with your email</h2>
                    <div class="row">
                        <div class="col-lg-3 m-t-5">
                            <span>
                                <asp:CheckBox ID="ChkAddContact" runat="server" Style="float: left; margin-top: 5px;" />
                                <img src="../Images/Icons/email_black.png" style="padding-left: 2px;" />
                                <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD CONTACT</span>
                            </span>
                        </div>
                        <div class="col-lg-3 m-t-5">
                            <span>
                                <asp:CheckBox ID="ChkAddDirection" runat="server" Style="float: left; margin-top: 5px;" />
                                <img src="../Images/Icons/directions_black.png" style="padding-left: 2px;" />
                                <span style="font-size: 10px; padding: 7px 0 0 0;">ADD DIRECTIONS</span>
                            </span>
                        </div>
                        <div class="col-lg-3 m-t-5">
                            <span>
                                <asp:CheckBox ID="chkAddUrl" runat="server" Style="float: left; margin-top: 5px;" />
                                <img src="../Images/Icons/web_black.png" style="padding-left: 2px;" />
                                <span style="font-size: 10px; padding: 10px 0 0 4px;">ADD WEBSITE</span>
                            </span>
                        </div>

                    </div>
                    <hr />

                    <h2 style="color: #007f9f;">Modify company Voice Message / Add Reminder time</h2>
                    <div class="row">
                        <div class="col-lg-3 m-t-5">
                            <span>
                                <asp:CheckBox ID="ChkAddReminder" runat="server" Style="float: left; margin-top: 5px;" OnCheckedChanged="ChkAddReminder_CheckedChanged" AutoPostBack="true" />
                                <img src="../Images/Icons/reminder_black.png" style="padding-left: 2px;" />
                                <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD REMINDER</span>
                            </span>
                        </div>
                        <div class="col-lg-3 m-t-5">
                            <span>
                                <asp:CheckBox ID="ChkAddVoice" runat="server" Style="float: left; margin-top: 5px;" OnCheckedChanged="ChkAddVoice_CheckedChanged" AutoPostBack="true" />
                                <img src="../Images/Icons/voice-message_black.png" style="padding-left: 2px;" />
                                <span style="font-size: 10px;" class="text-right test-11">ADD VOICE MESSAGE</span>
                            </span>
                        </div>
                    </div>
                    <div class="row">

                        <asp:Panel ID="PnlRDVoice" runat="server" Visible="false">
                            <div class="col-lg-12 m-t-5">
                                <asp:RadioButtonList ID="RB_Voice" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RB_Voice_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem class="p-r-15" Value="0">&nbsp;Add Generic Voice</asp:ListItem>
                                    <asp:ListItem class="p-r-15" Value="1">&nbsp;Add New Voice</asp:ListItem>
                                    <asp:ListItem  Value="2">&nbsp;Upload Voice Message</asp:ListItem>
                                </asp:RadioButtonList>
                            </div>
                        </asp:Panel>
                    </div>

                    <asp:Panel ID="pnlUploadVoice" runat="server" Visible="false">
                        <asp:FileUpload ID="fuvoice" runat="server" />
                    </asp:Panel>
                    <asp:Panel ID="PnlRecordVoice" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12 m-b-10">
                                <h2 style="color: #007f9f;">Record a personal message for this Ad</h2>
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-8">
                                <div class="box-footer no-border" style="background-color: #DFDFDF;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <img src="../Images/Icons/mic.png" alt="Mic" id="record" class="img-circle test-10" style="background-color: #A9C502; float: left;" />
                                            <img src="../Images/Icons/mic.gif" alt="Mic" id="pause" hidden class="img-circle" style="background-color: #A9C502; float: left;" />

                                            <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>

                                            <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                                            <img id="stop" src="../Images/Icons/delete_voice.jpg" title="Delete" alt="delete" hidden class="img-circle" style="background-float: right;" />
                                            <img id="save" src="../Images/Icons/upload_voice.jpg" title="Upload" alt="upload" hidden class="img-circle" style="background-float: right;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr />
                    </asp:Panel>
                    <asp:Panel ID="PnlTime" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12 m-b-10">
                                <h2 style="color: #007f9f;">Appointment Time</h2>
                            </div>
                            <div class="col-lg-4">
                                <div class="input-group">
                                    <span style="background-color: #f6f7f8; border-color: #f6f7f8; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</span>
                                    <input type="date" id="datepicker" class="datepicker-inline" name="name" value="" runat="server" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 130px; height: 34px; margin-top: 5px;" />
                                </div>
                            </div>
                            <div class="col-lg-6">

                                <div class="input-group">
                                    <span style="background-color: #f6f7f8; border-color: #f6f7f8; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time:</span>
                                    <asp:DropDownList ID="DdlHour" runat="server" Style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 125px;" CssClass="form-control m-t-5">
                                        <asp:ListItem Value="0" Text="Select Time"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                        <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                        <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                        <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                        <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        <asp:ListItem Value="11" Text="11"></asp:ListItem>
                                        <asp:ListItem Value="12" Text="12"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="DdlTT" runat="server" Style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 70px;" CssClass="form-control m-t-5 m-l-5 test-5">
                                        <asp:ListItem Value="0" Text="pm"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="am"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <hr />
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 style="color: #007f9f;">Copy that will be sent with Ad</h2>
                        </div>
                        <div class="col-lg-12 m-t-5">
                            <label >If you do not enter any information here, the message will be used from company information page.</label>
                        </div>
                        <div class="col-lg-8 m-t-5">
                            <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="Enter Message here.." EnableTheming="True"></asp:TextBox>
                        </div>
                    
                    </div>
                    <hr />




                    <asp:Button ID="BtnSend" runat="server" Visible="false" OnClick="BtnSend_Click" Text="Send" CssClass="btn btn-sm btn-light-green m-t-15 m-b-20" Style="width: 85px;" />
                </div>
            </div>
        </div>
    </asp:Panel>






    <script>
        function SetImage(src) {
            $("#imgDesign").attr("src", src);
        }

    </script>
            <script type="text/javascript">
            debugger;
            function Successs() {
                $("#SModel").modal('show');
        }
    </script>
    <%-- Pop Up --%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <asp:Image ID="imgDesign" ImageUrl="#" runat="server" alt="Alternate Text" Style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
            </div>
        </div>
    </div>
    <%-- End --%>


    
    <%-- Pop Up --%>
    <div class="modal fade" id="SModel" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                  <h3>Email Sent Successfully </h3>
                    <a href="../Dashboard/Dashboard.aspx" class="btn btn-sm btn-green">Ok</a>
                </div>
            </div>

        </div>
    </div>
    <%-- End --%>



    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
