﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyInformation.aspx.cs" Inherits="MoreInline.MKT_Assets.CompanyInformation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Company Information</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="main-file/agency-video/img/favicon.png" rel="icon" />

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <%-- ----------------------------------- Script --------------------------------------------- --%>


    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtLocation.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <style>
        .borderBottom {
            border-bottom: 2px solid #ABC502;
        }

        .buttonSend {
            width: 140px;
            font-size: 28px;
            font-weight: 100;
        }

        .h-style {
            color: #019c7c;
            font-family: 'Microsoft JhengHei';
            font-weight: bold;
        }

        .LabelSpan {
            color: #000;
            background-color: #fafafa !important;
            border-color: #fafafa !important;
            padding-top: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header borderBottom" style="background-color: #000000;">
                <div class="p-t-10 p-l-10 p-b-15">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="240px" height="60px" />
                </div>
            </header>

            <div class="content-wrapper m-l-0">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div id="div1" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                    <asp:Panel ID="PnlCompanyInfo" runat="server" >
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <br />
                                <h2 class="h-style">Additional Personal / Company Information</h2>
                                <br />
                            </div>

                            <div class="col-lg-3"></div>
                            <div class="col-lg-6 m-b-10">
                                <label class=" m-t-10">Profile Photo</label>
                                <asp:FileUpload ID="ProfilePhoto" runat="server" />
                                <span class="">Image size must not exceed 200 KB. Upload only .jpg, .png, .jpeg, .gif files.</span>
                                <asp:CustomValidator ID="CustomValidatorProfile" OnServerValidate="CustomValidatorProfile_ServerValidate" ControlToValidate="ProfilePhoto" runat="server" ForeColor="Red" />
                                <br />
                                <label class=" m-t-10">Company Logo</label>
                                <asp:FileUpload ID="CompanyLogo" runat="server" />
                                <span class="">Image size must not exceed 200 KB. Upload only .jpg, .png, .jpeg, .gif files.</span>
                                <%--<asp:CustomValidator ID="CustomValidator1" OnServerValidate="CustomValidatorProfile_ServerValidate" ControlToValidate="CompanyLogo" runat="server" ForeColor="Red" />--%>
                                <br />
                                <label class=" m-t-10">First Name</label>
                                <asp:TextBox ID="TxtFName" runat="server" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                                <label class=" m-t-10">Last Name</label>
                                <asp:TextBox ID="TxtLName" runat="server" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                                <label class=" m-t-10">Company</label>
                                <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control" placeholder="Company"></asp:TextBox>
                                <label class=" m-t-10">Website</label>
                                <asp:TextBox ID="TxtWebsite" runat="server" CssClass="form-control" placeholdder="Website(Optional)"></asp:TextBox>
                                <label class=" m-t-10">Address</label>
                                <asp:TextBox ID="TxtAddress" runat="server" CssClass="form-control" placeholder="Address"></asp:TextBox>
                                <label class=" m-t-10">Zip Code</label>
                                <asp:TextBox ID="TxtZipCode" runat="server" CssClass="form-control" placeholder="Zip Code"></asp:TextBox>
                                <label class=" m-t-10">Contact Number</label>
                                <asp:TextBox ID="TxtNumber" runat="server" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" CssClass="form-control" placeholder="Contact Number"></asp:TextBox>
                                <%--      <div class="text-center">
                                    <h2 class="h-style">Account Information</h2>
                                </div>
                                <label class=" m-t-10">Email</label>
                                <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                <label class=" m-t-10">Password</label>
                                <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" placeholder="Password" type="password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password1"></span>
                                <label class=" m-t-10">Confirm Password</label>
                                <asp:TextBox ID="TxtConfirmPassword" runat="server" CssClass="form-control" placeholder="Confirm Password" type="password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password2"></span>--%>
                                <div class="text-center">
                                    <asp:Button ID="BtnNext" Text="SAVE" runat="server" CssClass="btn btn-light-green m-t-10" OnClick="BtnNext_Click" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="PnlGenericInfo" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12 text-center m-t-40">
                                <h2 class="h-style">Create generic company information for your Ads</h2>
                            </div>
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6 m-b-10 m-t-10">
                                <%-- <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">Ad Subject:</span>
                                    <asp:TextBox ID="TxtAdSubject" runat="server" CssClass="form-control m-t-5" placeholder="Enter message title"></asp:TextBox>
                                </div>--%>
                                <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: </span>
                                    <asp:TextBox ID="TxtEmailAdd" TextMode="Email" runat="server" CssClass="form-control m-t-5" placeholder="Enter email address"></asp:TextBox>
                                </div>
                                <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">&nbsp;&nbsp;&nbsp;&nbsp;Contact: </span>
                                    <asp:TextBox ID="TxtPhone" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" CssClass="form-control m-t-5 num" placeholder="Enter contact number"></asp:TextBox>
                                </div>
                                <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">&nbsp;&nbsp;&nbsp;Location:</span>
                                    <asp:TextBox ID="TxtLocation" runat="server" onkeydown="return checkShortcut();" CssClass="form-control m-t-5" placeholder="Enter business address"></asp:TextBox>
                                </div>
                                <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">&nbsp;&nbsp;&nbsp;&nbsp;Website:</span>
                                    <asp:TextBox ID="Txtweb" runat="server" CssClass="form-control m-t-5" placeholder="Enter URL(Optional)"></asp:TextBox>
                                </div>
                                <div class="input-group m-t-10">
                                    <span class="input-group-addon LabelSpan">Ad Subject Line: </span>
                                    <asp:TextBox ID="TxtSubject"  runat="server" CssClass="form-control m-t-5" placeholder="Enter Subject For your Ads"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-3"></div>
                           
                           
                            <hr />


                            <div class="col-lg-12  m-t-25">
                                <div class="row">
                                    <div class="col-lg-2 col-sm-2"></div>
                                    <div class="col-lg-8 col-sm-8">
                                        <div class="box-footer no-border" style="background-color: #DFDFDF;">
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12">
                                                    <img src="../Images/Icons/mic.png" alt="Mic" id="record" class="img-circle" style="background-color: #A9C502; float: left;" />
                                                    <img src="../Images/Icons/mic.gif" alt="Mic" id="pause" hidden class="img-circle" style="float: left;" />

                                                    <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>

                                                    <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                                                    <img id="stop" src="../Images/Icons/delete_voice.jpg" title="Delete" alt="delete" hidden class="img-circle" style="background-color: #fff; float: right;" />
                                                    <img id="save" src="../Images/Icons/upload_voice.jpg" title="Upload" alt="upload" hidden class="img-circle" style="background-color: #fff; float: right;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-12 text-center">
                                <h2 class="h-style">Record a generic message for all customers</h2>
                            </div>

                            <div class="col-lg-3"></div>
                            <div class="col-lg-9 ">
                                <ol>
                                    <li class="m-b-5">
                                        <img src="../Images/Icons/mic.png" alt="Mic" class="img-circle" style="background-color: #A9C502;" />
                                        Press to start recording.</li>
                                    <li class="m-b-5">
                                        <img src="../Images/Icons/mic.gif" alt="Mic Stop" class="img-circle" />
                                        Press to stop recording.</li>
                                    <li class="m-b-5">
                                        <img src="../Images/Icons/upload_voice.jpg" alt="upload" class="img-circle"  />
                                        Press to upload recording Or
                                         <img src="../Images/Icons/delete_voice.jpg" alt="Delete" class="img-circle"  />
                                        Press to delete recording.
                                    </li>
                                </ol>
                            </div>

                            <div class="col-lg-2"></div>
                            <div class="col-lg-8 text-center">
                                 <h2 class="h-style">Enter generic message for your Ads</h2><br />
                                 <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="We're been in business for 30 years and we'd like to do business with you. Call us today and let's get started. " EnableTheming="True"></asp:TextBox>
                            </div>
                            <div class="col-lg-2"></div>

                            <div class="col-lg-12 text-center m-b-10 m-t-40">
                                <asp:Button ID="btnsave" Text="SAVE" OnClick="btnsave_Click" runat="server" CssClass="btn btn-light-green" />
                            </div>

                        </div>
                    </asp:Panel>



                </div>

            </div>


        </div>
    </form>
    <!------------------------- Script --------------------------->
    <script>
        $("body").on('click', '.toggle-password2', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input2 = $("#TxtConfirmPassword");
            if (input2.attr("type") === "password") {
                input2.attr("type", "text");
            } else {
                input2.attr("type", "password");
            }
        });

        $("body").on('click', '.toggle-password1', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input1 = $("#TxtPassword");
            if (input1.attr("type") === "password") {
                input1.attr("type", "text");
            } else {
                input1.attr("type", "password");
            }

        });
    </script>
</body>
</html>
