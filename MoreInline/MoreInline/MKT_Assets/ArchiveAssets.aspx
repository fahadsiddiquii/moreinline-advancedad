﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="ArchiveAssets.aspx.cs" Inherits="MoreInline.MKT_Assets.ArchiveAssets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <%----------------------------------------- Script -------------------------------------------------------%>


    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }--%>
            }, seconds * 1000);
        }
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script type="text/javascript">
        function OrderStatus() {
            $('#OrderStatus').modal('show');
        }
    </script>
    <style>
        .span {
            color: #001b00;
            font-weight:bold;
        }

        .p-t-10 {
            padding-top: 10px;
        }

        .p-b-10 {
            padding-bottom: 10px;
        }

        .p-t-20 {
            padding-top: 20px;
        }

        .m-l-0 {
            margin-left: 0;
        }

        .m-r-0 {
            margin-right: 0;
        }

        .Social-Icon-Width {
            width: 5%;
        }

        .container1 {
            position: relative;
            width: 280px;
        }

        .image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 0;
            transition: .3s ease;
            background-color: #4b4f4c;
        }

        .container1:hover .overlay {
            opacity: 0.8;
        }

        .icon {
            color: white;
            font-size: 100px;
            position: absolute;
            top: 48%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .card {
            background: #fff;
            border-radius: 2px;
            /*border:3px solid #A9C502;*/
            display: inline-block;
            margin: 1rem;
            position: relative;
            width: 341px;
            height: 410px;
            padding-left: 10px;
            float: left;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

            .card-1:hover {
                box-shadow: 0 14px 28px #808080, 0 10px 10px #808080;
            }

        @media (max-width:1166px) {
            .m-t-5 {
                margin-top: 3px;
                margin-bottom: 2px;
            }

            .btn-sm {
                padding: 3px 4px;
                font-size: 11px;
            }
        }


        @media (max-width:760px) {
            .Social-Icon-Width {
                width: 7%;
            }
        }

        @media (max-width:606px) {
            .btn-green {
                font-size: 9px;
            }
        }

        @media (max-width:527px) {
            .btn-green {
                font-size: 7px;
            }
        }
    </style>

    <%----------------------------------------- Script End -------------------------------------------------------%>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h1 style="color: #001b00; font-family: 'Microsoft JhengHei'"><b>Archive Assets</b></h1>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br />
    <div class="row p-t-10 m-b-5">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 text-center">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <%--<center>
        <div class="row">
    <div class="col-lg-10">
            <div class="form-inline pull-right">
            <input type="text" class="form-control" placeholder="Search by Order Number">
            <asp:button ID="btnsearch" text="Search" runat="server" class="btn btn-green"  />
    </div>
    </div>
</div>
    </center>--%>
    <br />
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <asp:Repeater ID="RptAssets" runat="server" OnItemDataBound="RptAssets_ItemDataBound" OnItemCommand="RptAssets_ItemCommand">
                    <ItemTemplate>
                        <div class="card card-1">
                            <asp:HiddenField runat="server" ID="AssetID" Value='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                            <div class="p-l-10 p-r-15">
                                <div class="m-t-15 m-b-10">
                                    <span class="span">Order number:</span><asp:Label ID="lblOrder" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' runat="server" CssClass="p-l-10 span" /><br />
                                    <span class="span">Title:</span><asp:Label ID="lblTitle" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' runat="server" CssClass="p-l-10 span" />
                                    <br />
                                    <span class="span">Date Placed:</span><asp:Label ID="lblPdate" Text='<%#DataBinder.Eval(Container.DataItem,"OrderDate") %>' runat="server" CssClass="p-l-10 span" /><br />
                                    <span class="span">Date Received:</span><asp:Label ID="lblRdate" Text='<%#DataBinder.Eval(Container.DataItem,"DeliverDate") %>' runat="server" CssClass="p-l-10 span" /><br />
                                </div>
                                <asp:Label ID="LblFilePath" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                                <asp:Label ID="LblStatusID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ID") %>' />
                                <asp:Label ID="LblCustRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>' />
                                <asp:Label ID="LblApprov" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustApproval") %>' />

                                <div class="p-l-10">
                                    <div class="container1">
                                        <div>
                                            <img alt='Asset Image' src='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' class="img-thumbnail" style="width: 280px; height: 230px" />
                                        </div>
                                        <%-- <asp:Image runat="server" ID="Img-<%#DataBinder.Eval(Container.DataItem,"AssestID") %>" alt='Asset Image' ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' class="img-responsive" Style="width: 280px" />--%>
                                        <div class="overlay">
                                            <a class="icon" id="pop">
                                                <%--<img src="../Images/Icons/eye.png" title="Click to preview" alt="EyeView" />--%>
                                                <asp:ImageButton ID="ImgEye" runat="server" AlternateText="EyeView" ToolTip="Click to preview" ImageUrl="~/Images/Icons/eye.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' CommandName="ViewPic" />
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="m-t-10 text-right m-b-10">
                                    <asp:Button ID="BtnSend" Text="Send to live" runat="server" CssClass="btn btn-sm btn-dark-green m-r-10" CommandName="Send" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>



    </div>


    <%----------------------------------------- Page End -------------------------------------------------------%>

    <%----------------------------------------- Pop up Start -------------------------------------------------------%>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-body">
                <asp:Image ID="ImgProfile" runat="server" CssClass="img-responsive" />
            </div>
        </div>
    </div>

    <%----------------------------------------- Pop up End -------------------------------------------------------%>
</asp:Content>
