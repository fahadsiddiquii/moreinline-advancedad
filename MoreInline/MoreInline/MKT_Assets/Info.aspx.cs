﻿
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class Info : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
            OrdersDAL Order = new OrdersDAL();
            if (Request.QueryString["IsSuccess"] != null && Session["Cust_ID"] != null)
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = OrderAsset.GetCustDetail(CustID).Tables[0];
                string chek = Request.QueryString["IsSuccess"].ToString();
                if (chek.Equals("1"))
                {
                        btndashboard.Visible = true;
                        pnlOrderSuccess.Visible = false;
                       // btndashboard.Visible = false;
                        pnlsuccess.Visible = true;
                        h1.InnerText = "You Have Successfully Purchased "+ CustData.Rows[0]["AccType"].ToString()+" Package ";
                        pnlfailure.Visible = false;
                }
                else
                {
                        pnlOrderSuccess.Visible = false;
                        pnlsuccess.Visible = false;
                        pnlfailure.Visible = true;
                   
                }
            }
            else if(Request.QueryString["Success"] != null && Request.QueryString["id"] != null)
            {
                int AssetId = int.Parse(Request.QueryString["id"].ToString());
                DataTable dt = Order.GetAssetDetails(AssetId);
                string chek = Request.QueryString["Success"].ToString();
                if (chek.Equals("1"))
                {
                    pnlOrderSuccess.Visible = true;
                    pnlsuccess.Visible = false;
                    string IsRM = string.Empty;

                    if (Request.QueryString["IsRM"] != null)
                        IsRM = Request.QueryString["IsRM"].ToString();
                    else
                        IsRM = "false";



                    if (IsRM.Equals("true"))
                    {
                        h2.InnerText = "You Have Successfully Requested Modifications. Order No: " + dt.Rows[0]["OrderNum"].ToString();
                    }
                    else
                    {
                        h2.InnerText = "You Have Successfully Ordered " + dt.Rows[0]["OrderNum"].ToString();
                    }
                    
                    pnlfailure.Visible = false;
                    btnGtGallery.Visible = true;
                    btndashboard.Visible = false;
                }
                else
                {
                    pnlOrderSuccess.Visible = false;
                    pnlfailure.Visible = true;
                    btndashboard.Visible = false;

                }
            }
            else if (Request.QueryString["IsSuccess"] != null && Request.QueryString["Snap"] != null)
            {
                 string chek = Request.QueryString["IsSuccess"].ToString();
       
                if (chek.Equals("1"))
                {
                    btndashboard.Visible = false;
                        pnlOrderSuccess.Visible = false;
                        btnSnap.Visible = true;
                        pnlsuccess.Visible = true;
                        h1.InnerText = "You Have Successfully Registered New Customer";
                        pnlfailure.Visible = false;
                }
                else
                {
                    btndashboard.Visible = false;
                    pnlOrderSuccess.Visible = false;
                    btnSnap.Visible = true;
                    pnlfailure.Visible = true;
                   
                }
            }
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Dashboard/Dashboard.aspx",false);
        }

        protected void btnGtGallery_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/MKT_Assets/ViewAssets.aspx", false);
        }

        protected void btnSnap_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Snap.aspx", false);
        }
    }
}