﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public class DisableControls
    {
        public static void Disable(ControlCollection controls)
        {
            foreach (Control ctrl in controls)
            {
                
                if (ctrl.HasControls())
                {
                    Disable(ctrl.Controls);
                }

                if (ctrl is TextBox)
                {
                    ((TextBox)ctrl).Enabled = false;
                }
                else if (ctrl is DropDownList)
                {
                    ((DropDownList)ctrl).Enabled = false;  
                }
                else if (ctrl is CheckBoxList)
                {
                    ((CheckBoxList)ctrl).Enabled = false;
                }
                else if (ctrl is CheckBox)
                {
                    ((CheckBox)ctrl).Enabled = false;
                }
                else if (ctrl is ListBox)
                {
                    ((ListBox)ctrl).Enabled = false;    // -1 is the value to use for none selected in a list box
                }
                else if (ctrl is RadioButtonList)
                {
                    ((RadioButtonList)ctrl).Enabled = false;
                }
                else if (ctrl is Button)
                {
                    ((Button)ctrl).Visible = false;
                }

                else if (ctrl is FileUpload)
                {
                    ((FileUpload)ctrl).Visible = false;
                }

                else if (ctrl is Panel)
                {
                    ((Panel)ctrl).Enabled = false;
                }



                
            }
        }
    }

   
}