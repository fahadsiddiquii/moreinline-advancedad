﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.MKT_Assets
{
    public partial class SentItems : System.Web.UI.Page
    {
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
              
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        private void LoadData(int CustID)
        {
            DataSet Ds = OrderAsset.GetSentDatesAll(CustID);

            rptInvoices.DataSource = Ds;
            rptInvoices.DataBind();

        }


        protected void rptInvoices_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                string CommandName = e.CommandName.ToString();
                DateTime DateTime = DateTime.Parse(e.CommandArgument.ToString());
                if (!string.IsNullOrWhiteSpace(DateTime.ToString()))
                {
                    string date = DateTime.ToString();
                    Response.Redirect("~/MKT_Assets/Details.aspx?date=" + date, false);
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void RdSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                switch (RdSearch.SelectedValue)
                {
                    case "0":
                        PnlDay.Visible = false;
                        PnlMonth.Visible = false;
                        PnlYear.Visible = false;
                        int CustID = int.Parse(Session["Cust_ID"].ToString());
                        LoadData(CustID);
                        break;
                    case "1":
                        rptInvoices.DataSource = null;
                        rptInvoices.DataBind();
                        DdlDay.SelectedIndex = 0;
                        PnlMonth.Visible = false;
                        PnlYear.Visible = false;
                        PnlDay.Visible = true;
                        break;
                    case "2":
                        rptInvoices.DataSource = null;
                        rptInvoices.DataBind();
                        DdlMonth.SelectedIndex = 0;
                        PnlDay.Visible = false;
                        PnlYear.Visible = false;
                        PnlMonth.Visible = true;
                        break;
                    case "3":
                        rptInvoices.DataSource = null;
                        rptInvoices.DataBind();
                        DdlYear.SelectedIndex = 0;
                        PnlDay.Visible = false;
                        PnlMonth.Visible = false;
                        PnlYear.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DdlDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                string Day = DdlDay.SelectedValue;
                DataTable dt = OrderAsset.GetSentDatesDay(CustID, Day);
                rptInvoices.DataSource = dt;
                rptInvoices.DataBind();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DdlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                string Month = DdlMonth.SelectedValue;
                DataTable dt = OrderAsset.GetSentDatesMonth(CustID, Month);
                rptInvoices.DataSource = dt;
                rptInvoices.DataBind();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DdlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                string Year = DdlYear.SelectedValue;
                DataTable dt = OrderAsset.GetSentDatesYear(CustID, Year);
                rptInvoices.DataSource = dt;
                rptInvoices.DataBind();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }


    }
}