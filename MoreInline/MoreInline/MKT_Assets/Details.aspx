﻿<%@ Page Title="Assets Sent" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="MoreInline.MKT_Assets.Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>

    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal1() {
            $('#myModal1').modal('show');
        }
    </script>
    <script type="text/javascript">
        function Resend() {
            $('#Resend').modal('show');
        }
    </script>

    <style>
        .img {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 50px;
        }
    </style>

    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:scriptmanager id="ScriptManager1" runat="server"></asp:scriptmanager>
    <asp:updateprogress id="UpdateProgress1" runat="server" associatedupdatepanelid="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:updateprogress>
    <asp:updatepanel id="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:updatepanel>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h2 style="color: #001b00;">Ads Sent on <span runat="server" id="dateOn"></span></h2>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br />
    <div class="row p-t-10">
        <div class="col-lg-12">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:label id="LblMsg" runat="server"></asp:label>
                </strong>
            </div>
            <br />
        </div>
    </div>

    <div class="row p-t-10">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="overflow-x: auto;">
            <asp:repeater id="RptAssetSent" runat="server" onitemcommand="RptAssetSent_ItemCommand" onitemdatabound="RptAssetSent_ItemDataBound">
                <HeaderTemplate>
                    <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                        <thead class="table-head">
                            <tr style="color: #000; background-color: #A9C502;">
                                <th style="width: 5px;">SNo</th>
                                <th class="text-center">Asset</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Title</th>
                                <th class="text-center">Company</th>
                                <th class="text-center">Time</th>
                                <th class="text-center">Delete</th>
                                <th class="text-center" hidden="hidden">Resend</th>
                            </tr>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                <asp:Label ID="LblIsActive" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsOver") %>' Visible="false" />
                        </td>
                        <td class="text-center">
                            <asp:ImageButton ID="LblFilePath" CssClass="img" runat="server" CommandName="btnimg" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />
                        </td>
                          <td class="text-center">
                            <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>' />
                        </td>
                      
                            <%--<asp:Label ID="LblCreateDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CreateDate") %>' />--%>
                              <td class="text-center">
                                <asp:ImageButton ID="IBtnCheck" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                    CommandName="InActive" ToolTip="Active" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ID") %>' />

                                <asp:ImageButton ID="IBtnCross" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                    CommandName="Active" ToolTip="InActive" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ID") %>' OnClientClick='javascript:return confirm("Are you sure you want to inactive this appointment permanently?")'  />
                            </td>
                 
                         <td class="text-center" hidden="hidden">
                            
                             <asp:LinkButton ID="btnResend" Text="Resend" class="btn btn-small btn-success"  CommandName="Resend" ToolTip="Resend" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' runat="server" />
                            
                        </td>
                       
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:repeater>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <%----------------------------------------- Page End -------------------------------------------------------%>
    <%-- image view --%>
    <div class="modal fade" id="myModal1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #f7f8f9; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <img src="#" id="imgpop" runat="server" alt="Alternate Text" style="width: 100%; height: 100%;" />
                </div>
            </div>
        </div>
    </div>

    <%-- end --%>


    <%-- Send Mail--%>
    <div class="modal fade" id="Resend">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #f7f8f9; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <div>
                        <asp:textbox textmode="DateTimeLocal" id="ap" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- end --%>
</asp:Content>
