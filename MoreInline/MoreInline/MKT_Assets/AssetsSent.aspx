﻿<%@ Page Title="Assets Sent" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="AssetsSent.aspx.cs" Inherits="MoreInline.MKT_Assets.AssetsSent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>

    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal1() {
            $('#myModal1').modal('show');
        }
    </script>
    <style>
        .img {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 50px;
        }
    </style>

    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>


    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h2 style="color: #001b00;">Assets Sent</h2>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-1">
        </div>
        <div class="col-lg-11">
            <span style="color: #007f9f"><b>Emails Limit:</b>
                <asp:Label Style="color: #000;" Text="" ID="lbllimit" runat="server" /></span> |
            <span style="color: #007f9f"><b>Remaining Emails:</b>
                <asp:Label Style="color: #ff0000" Text="" ID="lblRemaining" runat="server" /></span>  |
           <span style="color: #007f9f"><b>Sent Emails:</b>
               <asp:Label Style="color: #000;" Text="" ID="lblSent" runat="server" /></span>
        </div>
    </div>
    <div class="row p-t-10">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="overflow-x: auto;">
            <asp:Repeater ID="RptAssetSent" runat="server" OnItemCommand="RptAssetSent_ItemCommand">
                <HeaderTemplate>
                    <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                        <thead class="table-head">
                            <tr style="color: #000; background-color: #A9C502;">
                                <th style="width: 5px;">SNo</th>
                                <th class="text-center">Asset</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Title</th>
                                <th class="text-center">Company</th>
                                <th class="text-center">Sent Date</th>
                            </tr>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                        </td>
                        <td class="text-center">
                            <asp:ImageButton ID="LblFilePath" CssClass="img" runat="server" CommandName="btnimg" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblCreateDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CreateDate") %>' />
                        </td>

                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <%----------------------------------------- Page End -------------------------------------------------------%>
    <%-- image view --%>
    <div class="modal fade" id="myModal1" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #f7f8f9; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <img src="#" id="imgpop" runat="server" alt="Alternate Text" style="width: 100%; height: 100%;" />
                </div>
            </div>
        </div>
    </div>

    <%-- end --%>
</asp:Content>
