﻿using MoreInline.BOL;
using MoreInline.DAL;
using MoreInline.Crypto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace MoreInline
{
    public partial class SignUpPaid : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        BillingInfoDAL ObjBillingInfoDAL = new BillingInfoDAL();
        BillingInfoBOL ObjBillingInfoBOL = new BillingInfoBOL();
        RegularPlanDAL ObjRegularPlanDAL = new RegularPlanDAL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        CDPCardDAL ObjCDPCardDAL = new CDPCardDAL();
        CDPCardBOL ObjCDPCardBOL = new CDPCardBOL();

        CheckingAccountDAL ObjCheckingAccountDAL = new CheckingAccountDAL();
        CheckingAccountBOL ObjCheckingAccountBOL = new CheckingAccountBOL();

        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();

        Cryptography ObjCryptography = new Cryptography();

        SqlTransaction Trans;
        SqlConnection Conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            div1.Visible = div2.Visible = false;
            //Clear();
            if (!IsPostBack)
            {
                try
                {
                    BindDdlRegularPlan();
                    BindCountryCodeList();
                    BindCountryList();
                    Session["SavedData"] = null;
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
            }
        }
        #region OLD-CODE OF SAVE ENTITIES
        //private bool SaveEntitiesCustomer_Billing()
        //{
        //    ObjBillingInfoBOL.FirstName = TxtBFname.Text;
        //    ObjBillingInfoBOL.LastName = TxtBLname.Text;
        //    ObjBillingInfoBOL.Organization = TxtOrganization.Text;
        //    ObjBillingInfoBOL.CountryCode = DdlPhone.SelectedValue;
        //    ObjBillingInfoBOL.Phone = TxtBPhone.Text;
        //    ObjBillingInfoBOL.Email = TxtBEmail.Text;
        //    ObjBillingInfoBOL.TaxID = TxtTaxID.Text;
        //    ObjBillingInfoBOL.Country = DdlCountry.SelectedValue;
        //    ObjBillingInfoBOL.Address = TxtBAddress.Text;
        //    ObjBillingInfoBOL.Address2 = TxtAddress2.Text;
        //    ObjBillingInfoBOL.City = TxtCity.Text;
        //    ObjBillingInfoBOL.Province = TxtProvince.Text;
        //    ObjBillingInfoBOL.PostalCode = TxtPostalCode.Text;

        //    if (ObjBillingInfoDAL.InsertBillingInformation(ObjBillingInfoBOL, Trans, Conn) == true)
        //    {
        //        if (Request.QueryString["FullCode"] == null && Request.QueryString["CodeID"] == null)
        //        {
        //            ObjCodeMasterBOL.CodeNumber = "";
        //            ObjCodeMasterBOL.CodeText = "";
        //            ObjCodeMasterBOL.FullCode = "";
        //            ObjCodeMasterBOL.ValidationDays = "30";
        //            ObjCodeMasterBOL.IsUtilized = 1;
        //            ObjCodeMasterBOL.IsPaid = 1;
        //            ObjCodeMasterBOL.CreatedBy = 0;

        //            if (ObjCodeMasterDAL.InsertCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
        //            {
        //                ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
        //                ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
        //                ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
        //                ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
        //                ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
        //                ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
        //                ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
        //                ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
        //                ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
        //                ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
        //                ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
        //                ObjCustomerProfileMasterBOL.CodeID = ObjCodeMasterBOL.CodeID;
        //                ObjCustomerProfileMasterBOL.Bill_ID = ObjBillingInfoBOL.Bill_ID;
        //                ObjCustomerProfileMasterBOL.CDP_ID = ObjCDPCardBOL.CDP_ID;
        //                ObjCustomerProfileMasterBOL.CA_ID = ObjCheckingAccountBOL.CA_ID;
        //                ObjCustomerProfileMasterBOL.AccTypeID = 3;
        //                ViewState["CodeID"] = ObjCodeMasterBOL.CodeID;

        //                if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
        //                {
        //                    ViewState["Cust_ID"] = ObjCustomerProfileMasterBOL.Cust_ID;
        //                    Trans.Commit();
        //                    Conn.Close();
        //                    return true;
        //                }
        //                else
        //                {
        //                    Trans.Rollback();
        //                    Conn.Close();
        //                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
        //                    return false;
        //                }
        //            }
        //            else
        //            {
        //                Trans.Rollback();
        //                Conn.Close();
        //                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
        //                return false;
        //            }
        //        }
        //        else
        //        {

        //            ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
        //            ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
        //            ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
        //            ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
        //            ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
        //            ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
        //            ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
        //            ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
        //            ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
        //            ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
        //            ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
        //            ObjCustomerProfileMasterBOL.CodeID = int.Parse(Request.QueryString["CodeID"]);
        //            ObjCustomerProfileMasterBOL.Bill_ID = ObjBillingInfoBOL.Bill_ID;
        //            ObjCustomerProfileMasterBOL.CDP_ID = ObjCDPCardBOL.CDP_ID;
        //            ObjCustomerProfileMasterBOL.CA_ID = ObjCheckingAccountBOL.CA_ID;
        //            ObjCustomerProfileMasterBOL.AccTypeID = 3;
        //            ViewState["CodeID"] = ObjCustomerProfileMasterBOL.CodeID;

        //            if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
        //            {
        //                ViewState["Cust_ID"] = ObjCustomerProfileMasterBOL.Cust_ID;
        //                Trans.Commit();
        //                Conn.Close();
        //                return true;
        //            }
        //            else
        //            {
        //                Trans.Rollback();
        //                Conn.Close();
        //                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
        //                return false;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Trans.Rollback();
        //        Conn.Close();
        //        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
        //        return false;
        //    }
        //} 
        #endregion

        private bool SaveEntitiesCustomer_Billing()
        {
            ObjCodeMasterBOL.CodeNumber = "";
            ObjCodeMasterBOL.CodeText = "";
            ObjCodeMasterBOL.FullCode = "";
            ObjCodeMasterBOL.ValidationDays = "30";
            ObjCodeMasterBOL.IsUtilized = 1;
            ObjCodeMasterBOL.IsPaid = 1;
            ObjCodeMasterBOL.CreatedBy = 0;

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            if (ObjCodeMasterDAL.InsertCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
            {
                ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
                ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
                ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
                ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
                ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
                ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
                ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
                ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
                ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
                ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
                ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
                ObjCustomerProfileMasterBOL.CodeID = ObjCodeMasterBOL.CodeID;
                ObjCustomerProfileMasterBOL.Bill_ID = 0;
                ObjCustomerProfileMasterBOL.CDP_ID = 0;
                ObjCustomerProfileMasterBOL.CA_ID = 0;
                ObjCustomerProfileMasterBOL.AccTypeID = 3;
                ViewState["CodeID"] = ObjCustomerProfileMasterBOL.CodeID;

                if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                {
                    ViewState["Cust_ID"] = ObjCustomerProfileMasterBOL.Cust_ID;
                    Session["Cust_ID"] = ViewState["Cust_ID"].ToString();
                    Trans.Commit();
                    Conn.Close();
                    return true;
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    return false;
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }
        //--------------------------- User Profile Work Starts From Here --------------------------------------//
        public bool UserProfile_Validation()
        {
            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Company Name", 0);
                return false;
            }
            else if (TxtWebsite.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Website", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Zip Code", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtUsername.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter UserName", 0);
                return false;
            }
            else if (TxtPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text != TxtPassword.Text)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Passwords do not match", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void BtnContinue_Click(object sender, EventArgs e)
        {

            try
            {
                if (ViewState["ProfilePhoto"] == null || ViewState["ProfilePhoto"].ToString() == "")
                {
                    if (ImgUpload.HasFile)
                    {
                        if (Page.IsValid)
                        {
                            string fileName = Path.GetFileName(ImgUpload.PostedFile.FileName);
                            ViewState["ProfilePhoto"] = fileName;
                            ImgUpload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Upload Profile Photo", 0);
                        return;
                    }
                }

                if (UserProfile_Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtEmail.Text != "")
                    {
                        Match match = regex.Match(TxtEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }

                    if (TxtEmail.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "Email";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtEmail.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This Email Already Exist", 0);
                            return;
                        }
                    }

                    if (TxtUsername.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "UserName";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtUsername.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This UserName Already Exist", 0);
                            return;
                        }
                    }

                    // PnlUserProfile.Visible = false;
                    // PnlBilling.Visible = true;
                    //up1.Visible = true; bl1.Visible = true;
                    // up.Visible = false; bl.Visible = false;

                    #region NEW_CODE
                    if (SaveEntitiesCustomer_Billing())
                    {
                        MainPanel.Visible = false;
                        PnlOrderSummary.Visible = true;
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error occur in signing up customer", 0);
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }
        protected void BtnB_GoBack_Click(object sender, EventArgs e)
        {
            PnlBilling.Visible = false; PnlUserProfile.Visible = true;
            //up1.Visible = false; bl1.Visible = false;
            up.Visible = true; //bl.Visible = true;
        }

        //------------------------------ User Profile Work Ends Here --------------------------------------//

        //----------------------- Add Billing Information Work Starts From Here --------------------------------------//
        //--Distinct Country List Start--//
        public void BindCountryList()
        {
            try
            {
                //DdlCountry.DataSource = CountryList();
                //DdlCountry.DataBind();
                int num = 1;
                List<string> countryList = CountryList();
                foreach (var country in countryList)
                {
                    string text = country.ToString();
                    if (text != "")
                    {
                        DdlCountry.Items.Add(new ListItem(text, num.ToString()));
                    }
                    num++;
                }
                DdlCountry.Items.Insert(0, new ListItem("Select a Country/Region", "0"));
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public static List<string> CountryList()
        {
            List<string> CultureList = new List<string>();
            CultureInfo[] getCultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

            foreach (CultureInfo getCulture in getCultureInfo)
            {
                RegionInfo getRegionInfo = new RegionInfo(getCulture.LCID);
                if (!(CultureList.Contains(getRegionInfo.Name)))
                {
                    CultureList.Add(getRegionInfo.EnglishName);
                }
            }
            CultureList.Sort();
            return CultureList.Distinct().ToList();
        }
        //--Distinct Country List End--//

        //--For Getting Code of Countries Start--//
        public void BindCountryCodeList()
        {
            try
            {
                List<string> countryList = CountryList();
                string duplicateItem = "";
                int num = 1;
                foreach (var items in countryList)
                {

                    if (duplicateItem == "")
                    {
                        string text = items.ToString();
                        string CountryNumCode = GetCode(items.ToString());
                        string[] words = CountryNumCode.Split(new string[] { "and" }, StringSplitOptions.None);
                        foreach (string word in words)
                        {
                            if (text != "" && CountryNumCode != "")
                            {
                                DdlPhone.Items.Add(new ListItem(text + " " + word, num.ToString()));
                            }
                        }
                        duplicateItem = items;
                    }
                    if (duplicateItem != items.ToString())
                    {
                        string text = items.ToString();
                        string CountryNumCode = GetCode(items.ToString());
                        string[] words = CountryNumCode.Split(new string[] { "and" }, StringSplitOptions.None);
                        foreach (string word in words)
                        {
                            if (text != "" && CountryNumCode != "")
                            {
                                DdlPhone.Items.Add(new ListItem(text + " " + word, num.ToString()));
                            }
                        }
                        duplicateItem = items;
                    }
                    num++;
                }

                DdlPhone.Items.Insert(0, new ListItem("Select Code", "0"));
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }
        public string GetCode(string country)
        {
            string code = "";
            //    JObject weatherInfo = JsonConvert.DeserializeObject<JObject>(File.ReadAllText(Server.MapPath("~/TextFile.json")));
            //    code = Convert.ToString(((JToken)weatherInfo[GetCountryCode(country)])).Replace("\"", string.Empty);
            return code;
        }
        public static string GetCountryCode(string country)
        {
            string countryCode = "";
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo culture in cultures)
            {
                RegionInfo region = new RegionInfo(culture.LCID);
                if (region.EnglishName.ToUpper().Contains(country.ToUpper()))
                {
                    countryCode = region.TwoLetterISORegionName;
                }
            }
            return countryCode;
        }
        //--For Getting Code of Countries End--//
        public bool Billing_Validation()
        {
            if (TxtBFname.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtBLname.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtBPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (DdlCountry.SelectedValue == "0")
            {
                MessageBox(div1, LblMsg, "Select Country/Region", 0);
                return false;
            }
            else if (TxtBAddress.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtCity.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter City", 0);
                return false;
            }
            else if (TxtProvince.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Province/Region", 0);
                return false;
            }
            else if (TxtPostalCode.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Postal Code", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void BtnB_Continue_Click(object sender, EventArgs e)
        {
            try
            {

                if ((Billing_Validation() == true) && (UserProfile_Validation() == true))
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtBEmail.Text != "")
                    {
                        Match match = regex.Match(TxtBEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }

                    PnlBilling.Visible = false;
                    PnlPayment.Visible = true;
                    //  pd2.Visible = true; bl.Visible = true; up1.Visible = true;
                    up.Visible = false;// bl1.Visible = false; pd.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }
        protected void BtnPnlGo_Click(object sender, EventArgs e)
        {
            PnlBilling.Visible = true;
            PnlPayment.Visible = false;
            //  pd2.Visible = false; bl.Visible = false; up1.Visible = false;
            //  up1.Visible = true; bl1.Visible = true; pd.Visible = true;
        }

        //----------------------- Add Billing Information Work Ends Here --------------------------------------//

        protected void BtnDebit_Click(object sender, ImageClickEventArgs e)
        {
            PnlButtons.Visible = false; PnlCard.Visible = true;
        }
        protected void BtnAccount_Click(object sender, ImageClickEventArgs e)
        {
            PnlButtons.Visible = false; PnlCheckingAccount.Visible = true;
        }
        protected void BtnPaypal_Click(object sender, ImageClickEventArgs e)
        {
            PnlButtons.Visible = false;
        }

        //----------------------- Credit/Debit/Prepaid Card Work Starts From Here --------------------------------------//
        public bool CDP_Validation()
        {
            if (TxtNameOnCard.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Name", 0);
                return false;
            }
            else if (TxtCardNumber.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Card Number", 0);
                return false;
            }
            else if (TxtSecurityCode.Text == "")
            {

                MessageBox(div1, LblMsg, "Enter Security Code", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool Save_Entities_CDPCardInfo()
        {
            try
            {
                ObjCDPCardBOL.NameOnCard = ObjCryptography.Encrypt(TxtNameOnCard.Text);
                ObjCDPCardBOL.CardNumber = ObjCryptography.Encrypt(TxtCardNumber.Text);
                ObjCDPCardBOL.SecurityCode = ObjCryptography.Encrypt(TxtSecurityCode.Text);
                ObjCDPCardBOL.ExpiryDate = ObjCryptography.Encrypt(TxtExpiryDate.Text);

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                //if (ObjCDPCardDAL.InsertCDPCardInfo(ObjCDPCardBOL, Trans, Conn) == true)
                //{
                //    if (SaveEntitiesCustomer_Billing() == true)
                //    {
                //        return true;
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}
                //else
                //{
                //    Trans.Rollback();
                //    Conn.Close();
                //    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                 return false;
                //}
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
                return false;
            }
        }
        protected void BtnCreditContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if ((CDP_Validation() == true) && (Billing_Validation() == true) && (UserProfile_Validation() == true))
                {
                    if (Session["SavedData"] == null || Session["SavedData"].ToString() == "")
                    {

                        if (Save_Entities_CDPCardInfo() == true)
                        {
                            Session["SavedData"] = "Filled";
                            Clear();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        }
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "This Data is already entered.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void BtnCreditGo_Click(object sender, EventArgs e)
        {
            PnlButtons.Visible = true; PnlCard.Visible = false;
        }

        //----------------------- Credit/Debit/Prepaid Card Work Ends Here --------------------------------------//

        //----------------------- Checking Account Work Starts From Here --------------------------------------//
        public bool CheckingAccount_Validation()
        {
            if (DdlAccountType.SelectedValue == "0")
            {
                MessageBox(div1, LblMsg, "Select Account Type", 0);
                return false;
            }
            else if (TxtAccName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Account Name", 0);
                return false;
            }
            else if (TxtAccNum.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Account Number", 0);
                return false;
            }
            else if (TxtRouting.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Routing Number", 0);
                return false;
            }
            else if (TxtDL_Num.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Driver's License", 0);
                return false;
            }
            else if (TxtAO_BirthDate.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter BirthDate", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool Save_Entities_CheckingAccount()
        {
            try
            {
                ObjCheckingAccountBOL.AccountType = ObjCryptography.Encrypt(DdlAccountType.SelectedValue);
                ObjCheckingAccountBOL.AccountName = ObjCryptography.Encrypt(TxtAccName.Text);
                ObjCheckingAccountBOL.AccountNumber = ObjCryptography.Encrypt(TxtAccNum.Text);
                ObjCheckingAccountBOL.RoutingNumber = ObjCryptography.Encrypt(TxtRouting.Text);
                ObjCheckingAccountBOL.DL_Number = ObjCryptography.Encrypt(TxtDL_Num.Text);
                ObjCheckingAccountBOL.AO_BirthDate = ObjCryptography.Encrypt(TxtAO_BirthDate.Text);

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCheckingAccountDAL.InsertCheckingAccount(ObjCheckingAccountBOL, Trans, Conn) == true)
                {
                    if (SaveEntitiesCustomer_Billing() == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
                return false;
            }
        }

        protected void BtnAccountContinue_Click(object sender, EventArgs e)
        {
            try
            {
                if ((CheckingAccount_Validation() == true) && (Billing_Validation() == true) && (UserProfile_Validation() == true))
                {
                    if (Session["SavedData"] == null || Session["SavedData"].ToString() == "")
                    {

                        if (Save_Entities_CheckingAccount() == true)
                        {
                            Session["SavedData"] = "Filled";
                            Clear();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        }
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "This Data is already entered.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void BtnAccountGo_Click(object sender, EventArgs e)
        {
            PnlButtons.Visible = true; PnlCheckingAccount.Visible = false;
        }
        //----------------------- Checking Account Work Ends Here --------------------------------------//

        //----------------------- openModal work starts From Here --------------------------------------//

        protected void BtnPaymentAdded_Click(object sender, EventArgs e)
        {

            myModal.Visible = false;
            MainPanel.Visible = false;
            PnlOrderSummary.Visible = true;
        }
        //----------------------- openModal Work Ends Here --------------------------------------//

        public void BindDdlRegularPlan()
        {
            DataTable dt = ObjRegularPlanDAL.GetRegularPlan();
            DdlRegularPlan.DataSource = dt;
            DdlRegularPlan.DataValueField = "RP_ID";
            DdlRegularPlan.DataTextField = "RegularPlan";
            DdlRegularPlan.DataBind();
            DdlRegularPlan.Items.Insert(0, new ListItem("Select Your Package", "0"));

        }

        //----------------------- OrderSummary work starts From Here --------------------------------------//

        public void OnChangeDdlRegularPlan()
        {
            int PlanID = int.Parse(DdlRegularPlan.SelectedValue);
            DataTable RegularPlanDT = ObjRegularPlanDAL.GetRegularPlan();
            DataRow Row = RegularPlanDT.Select("RP_ID =" + PlanID).FirstOrDefault();
            spLimit.InnerText = Row["EmailsLimit"].ToString();

            if (DdlRegularPlan.SelectedValue == "1")
            {
                LblPayment.Text = "99";
            }
            else if (DdlRegularPlan.SelectedValue == "2")
            {
                LblPayment.Text = "180";
            }
            else if (DdlRegularPlan.SelectedValue == "3")
            {
                LblPayment.Text = "270";
            }
            else if (DdlRegularPlan.SelectedValue == "4")
            {
                LblPayment.Text = "360";
            }
            else if (DdlRegularPlan.SelectedValue == "5")
            {
                LblPayment.Text = "445";
            }
            else
            {
                LblPayment.Text = "";
            }
        }

        protected void DdlRegularPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OnChangeDdlRegularPlan();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }


        private bool PaymentEntry()
        {

            ObjCodeMasterBOL.ValidationDays = (int.Parse(DdlRegularPlan.SelectedValue) * 30).ToString();

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            ObjCodeMasterBOL.CodeID = int.Parse(ViewState["CodeID"].ToString());
            if (ObjCodeMasterDAL.UpdateValidationDays(ObjCodeMasterBOL, Trans, Conn) == true)
            {
                ObjPaymentBOL.Cust_ID = int.Parse(ViewState["Cust_ID"].ToString());
                ObjPaymentBOL.Amount = double.Parse(LblPayment.Text);

                if (ObjPaymentDAL.InsertPaymentDetail(ObjPaymentBOL, Trans, Conn) == true)
                {
                    int EmailLimit = int.Parse(spLimit.InnerText);
                    if (objAdvertisement.UpdateEmailsLimit(ObjPaymentBOL.Cust_ID, EmailLimit))
                    {
                        Trans.Commit();
                        Conn.Close();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    return false;
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }
        protected void BtnPurchase_Click(object sender, EventArgs e)
        {
            try
            {
                if (DdlRegularPlan.SelectedValue == "0")
                {
                    MessageBox(div2, LblMsg2, "Select your plan", 0);
                    return;
                }
                Response.Redirect("~/PPal.aspx?Product=Regular&Amount=" + LblPayment.Text + "&Limit=" + spLimit.InnerText + "&ddl=" + DdlRegularPlan.SelectedValue, false);

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        //----------------------- OrderSummary work ends Here --------------------------------------//

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        private void Clear()
        {
            TxtFName.Text = "";
            TxtLName.Text = "";
            TxtCompany.Text = "";
            TxtWebsite.Text = "";
            TxtAddress.Text = "";
            TxtZipCode.Text = "";
            TxtEmail.Text = "";
            TxtPhone.Text = "";
            TxtUsername.Text = "";
            TxtPassword.Text = "";
            TxtConfirmPassword.Text = "";
            TxtBFname.Text = "";
            TxtBLname.Text = "";
            TxtOrganization.Text = "";
            DdlPhone.SelectedValue = "0";
            TxtBPhone.Text = "";
            TxtBEmail.Text = "";
            TxtTaxID.Text = "";
            DdlCountry.SelectedValue = "0";
            TxtBAddress.Text = "";
            TxtAddress2.Text = "";
            TxtCity.Text = "";
            TxtProvince.Text = "";
            TxtPostalCode.Text = "";
            TxtNameOnCard.Text = "";
            TxtCardNumber.Text = "";
            TxtSecurityCode.Text = "";
            TxtExpiryDate.Text = "";
            DdlAccountType.SelectedValue = "0";
            TxtAccName.Text = "";
            TxtAccNum.Text = "";
            TxtRouting.Text = "";
            TxtDL_Num.Text = "";
            TxtAO_BirthDate.Text = "";
        }

        protected void CustomValidatorImg_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal size = Math.Round(((decimal)ImgUpload.PostedFile.ContentLength / (decimal)1024), 2);
            if (Regxcheck.IsValidImage(ImgUpload))
            {
                if (size > 200)
                {
                    CustomValidatorImg.ErrorMessage = "Image size exceeds 200 KB.";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                CustomValidatorImg.ErrorMessage = "Only .jpg,.png,.jpeg,.gif Files are allowed.";
                args.IsValid = false;
            }
        }
    }
}