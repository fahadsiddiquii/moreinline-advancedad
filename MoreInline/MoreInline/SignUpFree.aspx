﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUpFree.aspx.cs" Inherits="MoreInline.SignUpFree" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Free Sign Up</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
</head>
<body>

    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header" style="background-color: #000000;">
                <!-- Logo -->
                <a href="../LoginPage.aspx" class="logo" style="background-color: #000000;">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="160px" height="35px" />
                </a>
                <nav class="navbar navbar-static-top" role="navigation" style="background-color: #000000;">
                </nav>
            </header>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper m-l-0" style="background-color: #141414;">
                <!-- Main content -->
                <section class="content container" style="padding-bottom: 0px;">
                    <div class="row">
                        <div class="col-lg-12 text-center" style="padding-right: 150px;">
                            <h3 style="color: #828282; font-family: 'Microsoft JhengHei'"><b>User Profile</b></h3>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="div1" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>

                        <div class="col-lg-2"></div>
                        <div class="col-lg-1">
                            <%--<asp:Image ID="ImgUser" runat="server" src="Images/userpic.png" CssClass="rounded-image" />--%>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group-sm">
                                <span class="text-white">Profile Photo</span>
                                <asp:FileUpload ID="ImgUpload" runat="server" />
                                <span class="text-white">Image size must not exceed 200 KB. Upload only .jpg, .png, .jpeg, .gif files.</span>
                                <asp:CustomValidator ID="CustomValidatorImg" OnServerValidate="CustomValidatorImg_ServerValidate" ControlToValidate="ImgUpload" runat="server" ForeColor="Red" />
                                <asp:TextBox ID="TxtFName" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="First Name"></asp:TextBox>
                                <asp:TextBox ID="TxtLName" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Last Name"></asp:TextBox>
                                <asp:TextBox ID="TxtCompany" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Company"></asp:TextBox>
                                <asp:TextBox ID="TxtWebsite" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Website"></asp:TextBox>
                                <asp:TextBox ID="TxtAddress" runat="server" onkeydown="return checkShortcut();" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Address"></asp:TextBox>
                                <asp:TextBox ID="TxtZipCode" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Zip Code"></asp:TextBox>

                                <asp:TextBox ID="TxtEmail" runat="server" Style="background-color: #000;" CssClass="form-control m-t-10" placeholder="Email"></asp:TextBox>
                                <%--<asp:TextBox ID="TxtConfirmEmail" runat="server" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Confirm Email"></asp:TextBox>--%>
                                <asp:TextBox ID="TxtPhone" runat="server" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Contact Number"></asp:TextBox>

                                <asp:TextBox ID="TxtUsername" runat="server" Style="background-color: #000;" CssClass="form-control m-t-10" placeholder="Username"></asp:TextBox>
                                <asp:TextBox ID="TxtPassword" runat="server" type="password" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password1"></span>
                                <asp:TextBox ID="TxtConfirmPassword" runat="server" type="password" Style="background-color: #000;" CssClass="form-control txtBox-style m-t-5" placeholder="Confirm Password"></asp:TextBox>
                                <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password2"></span>
                                <asp:Button ID="BtnFinish" runat="server" Text="Finish" OnClick="BtnFinish_Click" CssClass="btn btn-sm btn-green m-t-10" Style="width: 90px; float: right;" />
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <img src="Images/o2.png" class="img-responsive" />
                        </div>
                    </div>
                    <%-- <div class="row m-t-10">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-3 text-right">
                            
                        </div>
                        <div class="col-lg-5"></div>
                    </div>--%>
                    <br />
                </section>
            </div>


        </div>


        <%----------------------------------------- Pop Up -------------------------------------------------------%>
        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #141414; border: 1px solid #A9C502;">
                    <div class="modal-body text-center">
                        <h3 style="color: #9F9F9F;">WELCOME TO</h3>
                        <center>
                            <img src="Images/moreinline_logo.png" width="300px" class="img-responsive" /></center>
                        <small style="padding-left: 60px; color: #828282;"><i><b>Better Communication - Better Results</b></i></small>

                        <h2 style="color: #A9C502;">Confirm your email address</h2>
                        <h3 style="color: #9F9F9F;">We will send you a confirmation for access verification to the email address provided.</h3>

                        <center>
                            <a href="LoginPage.aspx" class="btn btn-sm btn-green m-t-25 m-b-30" Style="width: 140px;">START FOR FREE</a>
                        </center>
                    </div>
                </div>
            </div>
        </div>

    </form>
    <script>
        $("body").on('click', '.toggle-password2', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input2 = $("#TxtConfirmPassword");
            if (input2.attr("type") === "password") {
                input2.attr("type", "text");
            } else {
                input2.attr("type", "password");
            }
        });

        $("body").on('click', '.toggle-password1', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input1 = $("#TxtPassword");
            if (input1.attr("type") === "password") {
                input1.attr("type", "text");
            } else {
                input1.attr("type", "password");
            }

        });
    </script>
</body>



</html>
