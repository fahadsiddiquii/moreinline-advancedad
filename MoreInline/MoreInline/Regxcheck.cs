﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MoreInline
{
    public static class Regxcheck
    {
        public static bool IsValidEmail(string email)
        {
            Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

            if (!string.IsNullOrWhiteSpace(email))
            {
                Match match = regex.Match(email);
                if (!match.Success)
                    return false;
                else
                    return true;
            }
            return true;
        }

        public static bool IsValidImage(System.Web.UI.WebControls.FileUpload image)
        {
            Regex regex = new Regex(@"(.*?)\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$");


            Match match = regex.Match(image.PostedFile.FileName);
            if (!match.Success)
            {
                return false;
            }
            else { return true; }


        }

    }
}