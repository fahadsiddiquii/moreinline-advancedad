﻿using MoreInline.BOL;
using MoreInline.DAL;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class Snap_Payment : System.Web.UI.Page
    {


        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        CustomerProfileMasterDAL CustomerProfile = new CustomerProfileMasterDAL();

        CustomerProfileMasterBOL ObjCustomerProfile = new CustomerProfileMasterBOL();
        DesignBOL DBOL = new DesignBOL();
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();
        SqlTransaction Trans;
        SqlConnection Conn;
        bool isCancel = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                Session["type"] = "";
                if (Request.QueryString["Cancel"] != null)
                {
                    if (Request.QueryString["Cancel"].ToString().Equals("true"))
                    {
                        isCancel = true;
                    }
                }
                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId) && Request.QueryString["type"].ToString().Equals("paypal"))
                {
                    Session["type"] ="paypal";
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Snap-Payment.aspx?";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    Response.Redirect(paypalRedirectUrl, false);
                }
                else if (Session["type"].ToString()=="paypal")
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                    }
                    else
                    {
                        string invoicenum = OrderAsset.GetInvoiceID();
                        TransactionBOL Transaction = new TransactionBOL();
                        Transaction.Amount = executedPayment.transactions.FirstOrDefault().amount.total.ToString();
                        Transaction.Cust_Id = Session["Cust_ID"] == null ? -1 : int.Parse(Session["Cust_ID"].ToString());
                        Transaction.GUID = guid;
                        Transaction.InvoiceNumber = invoicenum;
                        Transaction.IsActive = true;
                        Transaction.Item = executedPayment.transactions.FirstOrDefault().item_list.items.FirstOrDefault().name;
                        Transaction.PayerID = executedPayment.payer.payer_info.payer_id;
                        Transaction.PaymentId = executedPayment.transactions.FirstOrDefault().payee.merchant_id;
                        Transaction.Token = executedPayment.token;
                        Transaction.TransactionDate = DateTime.Now;
                        if (OrderAsset.InsertTransaction(Transaction))
                        {
                            int TID = Transaction.TransactionId;
                            string FullName = Session["FullName"].ToString();
                            string Email = Session["Email"].ToString();
                            string lg = Session["ProfilePhoto"].ToString();
                            string AcType = Session["AcType"].ToString();
                            int Amnt = int.Parse(Session["Amount"].ToString());
                            string limit = Session["limit"].ToString();
                            CustomerProfile.InsertSnap(FullName, Email, AcType, Amnt, lg, TID, "paypal");
                            SendEmailNotification(Email, AcType, Amnt, limit, FullName);

                        }


                        Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 1 + "&Snap=true", false);
                    }
                }
                else if (Request.QueryString["type"].ToString().Equals("cash"))
                {
                    var guid = Convert.ToString((new Random()).Next(100000));
                    string ProductName = Request.QueryString["Product"] == null ? "" : Request.QueryString["Product"].ToString();

                    string invoicenum = OrderAsset.GetInvoiceID();
                    TransactionBOL Transaction = new TransactionBOL();
                    Transaction.Amount =Session["Amount"].ToString();
                    Transaction.Cust_Id = Session["Cust_ID"] == null ? -1 : int.Parse(Session["Cust_ID"].ToString());
                    Transaction.GUID = guid;
                    Transaction.InvoiceNumber = invoicenum;
                    Transaction.IsActive = true;
                    Transaction.Item = ProductName;
                    Transaction.PayerID = "cash-payer-"+guid;
                    Transaction.PaymentId = "cash-Payment-"+guid;
                    Transaction.Token = guid;
                    Transaction.TransactionDate = DateTime.Now;
                    if (OrderAsset.InsertTransaction(Transaction))
                    {
                        int TID = Transaction.TransactionId;
                        string FullName = Session["FullName"].ToString();
                        string Email = Session["Email"].ToString();
                        string lg = Session["ProfilePhoto"].ToString();
                        string AcType = Session["AcType"].ToString();
                        int Amnt = int.Parse(Session["Amount"].ToString());
                        string limit = Session["limit"].ToString();
                        CustomerProfile.InsertSnap(FullName, Email, AcType, Amnt, lg, TID,"cash");
                        SendEmailNotification(Email, AcType, Amnt, limit, FullName);

                    }


                    Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 1+"&Snap=true", false);
                }
                else
                {
                    Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0, false);
                }
                
            }
            catch (Exception ex)
            {
                if (isCancel)
                {

                    Response.Redirect("~/Snap.aspx");
                }
                else
                {
                    Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0 + "&Snap=true", false);
                }

            }

        }


     
        public int GetRemainingDays(string date)
        {
            DateTime ValidDate = DateTime.Parse(date);
            DateTime Now = DateTime.Now;
            TimeSpan RemainingDays = ValidDate.Subtract(Now);
            int Days = RemainingDays.Days;
            return Days;
        }
        private PayPal.Api.Payment payment;
        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }
        private Payment CreatePayment(APIContext apiContext, string redirectUrl)
        {
            //create itemlist and add item objects to it  
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            string ProductName = Request.QueryString["Product"] == null ? "" : Request.QueryString["Product"].ToString();
            string ProductAmount = Request.QueryString["Amount"] == null ? "" : Request.QueryString["Amount"].ToString();
            Session["Limit"] = Request.QueryString["Limit"] == null ? "" : Request.QueryString["Limit"].ToString();
            Session["ddl"] = Request.QueryString["ddl"] == null ? "" : Request.QueryString["ddl"].ToString();
            Session["AcType"] = Request.QueryString["AcType"] == null ? "" : Request.QueryString["AcType"].ToString();
            Session["Amount"] = ProductAmount;
            int ddlitm = int.Parse(Request.QueryString["ddl"].ToString());
            // string qty = (ddlitm*30).ToString();
            itemList.items.Add(new Item()
            {
                name = ProductName,
                currency = "USD",
                price = ProductAmount,
                quantity = "1",
                sku = "N/A"
            });
            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = ProductAmount
            };
            var amount = new Amount()
            {
                currency = "USD",
                total = ProductAmount, // Total must be equal to sum of tax, shipping and subtotal.  
                details = details
            };
            var transactionList = new List<Transaction>();
            string invoicenum = OrderAsset.GetInvoiceID();
            transactionList.Add(new Transaction()
            {
                description = "Transaction description",
                invoice_number = Convert.ToString((new Random()).Next(100000)), //invoicenum, //Generate an Invoice No  
                amount = amount,
                item_list = itemList
            });
            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext  
            return this.payment.Create(apiContext);
        }
        private void SendEmailNotification(string Email,string actype,int amount,string limit,string fullname)
        {
           
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            EmailNotification ENotifc = EN.GetAdminFilledObject();
            ENotifc.EmailSubject = "Package Purchased ";
            ENotifc.isButtonVisible = "false";
            string CPlan = actype == "2" ? "Appointment Level" : actype == "3" ? "Regular" : "Premium";
            ENotifc.Content = Email + " has Successfully Purchased  " + CPlan + " Package.";
            EmailNotification.SendNotification(ENotifc);
            #endregion
            #region Email For Customer
          
            
                //EmailsLimit AccType
                string CELimit =limit;
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.isButtonVisible = "false";
                EmailData.ButtonText = "";
                EmailData.CurrentPkg = CPlan;
                EmailData.CurrentLimit = CELimit;
                EmailData.PreviousPkg ="N/A";
                EmailData.PeriviousLimit = "N/A";
                EmailData.IsOrderNotif = "";
                EmailData.SenderIdForUnSubscription = -1;//ObjCustomerProfileMasterBOL.Cust_ID; Your account has been updated as requested
                EmailData.NotificationType = "IsPackageConfirmNActive";
                EmailData.Content = "Your account has been updated as requested ";
                EmailData.ReceiverEmailAddress =Email;
                EmailData.EmailSubject = "Your account has been updated as requested";
                EmailData.ReceiverName = fullname;
                EmailData.SenderIdForUnSubscription = -1;
                EmailNotification.SendNotification(EmailData);

           
            #endregion
        }


    }
}