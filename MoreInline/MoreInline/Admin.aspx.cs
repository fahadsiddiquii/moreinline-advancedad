﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class Admin : System.Web.UI.Page
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        UserSignupBOL ObjUserSignupBOL = new UserSignupBOL();

       

        private static string DateE = ConfigurationManager.AppSettings["Date"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = div2.Visible = false;
                if (!IsPostBack)
                {
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                LoginWork(TxtEmail.Text, TxtPassword.Text);
            }
            catch (Exception ex)
            { MessageBox(div2, LblMsg2, ex.Message, 0); }
        }
        public void LoginWork(string Email, string Password)
        {
            try
            {
            

                if (TxtEmail.Text != "" && TxtPassword.Text != "")
                {
                    ObjUserSignupBOL.UserEmail = Email;
                    ObjUserSignupBOL.UserPassword = Password;

                    DataTable dt = ObjUserSignupDAL.UserLogin(ObjUserSignupBOL);


                    if (dt.Rows.Count == 1)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["UserEmail"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();
                            Session["UserRoleID"] = dt.Rows[0]["UserType"].ToString();
                            Session["UserType"] = dt.Rows[0]["UserType"].ToString();

                            //DataTable dt_UserTypes = (DataTable)ViewState["dt_UserTypes"];
                            //dt_UserTypes = dt_UserTypes.Select("UserRoleID = " + dt.Rows[0]["UserType"].ToString()).CopyToDataTable();

                            //Session["UserRoles"] = dt_UserTypes.Rows[0]["UserRoles"].ToString();

                            Session["UserRoles"] = "Admin";

                            if (Session["UserRoles"].ToString() == "Designer")
                            {
                                Response.Redirect(@"User\Orders.aspx", false);
                            }
                            else if (Session["UserRoles"].ToString() == "QA")
                            {
                                Response.Redirect(@"User\DesignListQA.aspx", false);
                            }
                            else
                            {
                                //Response.Redirect(@"~\User\WelcomePage.aspx", false);
                                Response.Redirect(@"User\WelcomePage.aspx",false);
                            }

                        }
                        else
                        { MessageBox(div2, LblMsg2, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div2, LblMsg2, "Invalid Email or Password", 0); }
                }
                else
                { MessageBox(div2, LblMsg2, "Enter Email or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div2, LblMsg2, ex.Message, 0); }
        }

    }
}