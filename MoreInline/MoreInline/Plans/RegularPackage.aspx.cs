﻿using MoreInline.BOL;
using MoreInline.Crypto;
using MoreInline.DAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Stripe;
using System.Threading.Tasks;
using System.Web.Services;

namespace MoreInline.Plans
{
    public partial class RegularPackage : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
       private static string stripeKey = ConfigurationManager.AppSettings["StripeLiveKey"];
     
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();

        AppointmentLevelDAL ObjAppointmentLevelDAL = new AppointmentLevelDAL();

        RegularPlanBOL ObjRegularPlanBOL = new RegularPlanBOL();
        RegularPlanDAL ObjRegularPlanDAL = new RegularPlanDAL();

        PremiumPlanBOL ObjPremiumPlanBOL = new PremiumPlanBOL();
        PremiumPlanDAL ObjPremiumPlanDAL = new PremiumPlanDAL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();

        SqlTransaction Trans;
        SqlConnection Conn;

        int Cust_ID;
        //int Bill_ID;
        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = div2.Visible = false;

            if (!IsPostBack)
            {
                try
                {
                    Cust_ID = int.Parse(Session["Cust_ID"].ToString());

                    if (Request.QueryString["AccType"] == "3")
                    {
                        BindDdlRegularPlan();
                        LblHeading.Text = "You've added Regular Plan";
                        
                    }
                    else if (Request.QueryString["AccType"] == "4")
                    {
                        BindDdlRegularPlan_Premium();
                        LblHeading.Text = "You've added Premium Plan";
                    }
                    if (Request.QueryString["AccType"] == "2")
                    {
                        BindDdlAppointmentPlan();
                        LblHeading.Text = "You've added Appointment Level";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
            }
        }

        public void BindDdlRegularPlan()
        {
            DataTable dt = ObjRegularPlanDAL.GetRegularPlan();
            DdlRegularPlan.DataSource = dt;
            DdlRegularPlan.DataValueField = "RP_ID";
            DdlRegularPlan.DataTextField = "RegularPlan";
            DdlRegularPlan.DataBind();
            DdlRegularPlan.Items.Insert(0, new ListItem("Select Your Package", "0"));

        }

        public void BindDdlAppointmentPlan()
        {
            DataTable dt = ObjAppointmentLevelDAL.GetAppointmentLevelPlan();
            DdlRegularPlan.DataSource = dt;
            DdlRegularPlan.DataValueField = "AL_ID";
            DdlRegularPlan.DataTextField = "AppointmentPlan";
            DdlRegularPlan.DataBind();
            DdlRegularPlan.Items.Insert(0, new ListItem("Select Your Package", "0"));

        }
        public void BindDdlRegularPlan_Premium()
        {
            DataTable dt = ObjPremiumPlanDAL.GetPremiumPlan();
            DdlRegularPlan.DataSource = dt;
            DdlRegularPlan.DataValueField = "PP_ID";
            DdlRegularPlan.DataTextField = "PremiumPlan";
            DdlRegularPlan.DataBind();
            DdlRegularPlan.Items.Insert(0, new ListItem("Select Your Package", "0"));
        }

        protected void BtnConfirmPackage_Click(object sender, EventArgs e)
        {
            Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            int AccTypeID = int.Parse(Request.QueryString["AccType"]);
            Conn = DBHelper.GetConnection();
            if (ObjCustomerProfileMasterDAL.UpdateAccountType(Cust_ID, AccTypeID, Conn))
            {
                Conn.Close();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            else
            {
                Conn.Close();
            }
        }


        protected void BtnPaymentAdded_Click(object sender, EventArgs e)
        {
            //myModal.Visible = false;
            PnlOrderSummary.Visible = true;
        }


        public void OnChangeDdlRegularPlan()
        {
            int PlanID = int.Parse(DdlRegularPlan.SelectedValue);
            btnaCP.Enabled = true;
            BtnPurchase.Enabled = true;
            if (Request.QueryString["AccType"] == "3")
            {
                DataTable RegularPlanDT = ObjRegularPlanDAL.GetRegularPlan();
                DataRow Row = RegularPlanDT.Select("RP_ID =" + PlanID).FirstOrDefault();
                spLimit.InnerText = Row["EmailsLimit"].ToString();
                if (DdlRegularPlan.SelectedValue == "1")
                {
                    LblPayment.Text = "99";
                    LblNote.Text = "$99 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "2")
                {
                    LblPayment.Text = "180";
                    LblNote.Text = "$180 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "3")
                {
                    LblPayment.Text = "270";
                    LblNote.Text = "$270 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "4")
                {
                    LblPayment.Text = "360";
                    LblNote.Text = "$360 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "5")
                {
                    LblPayment.Text = "445";
                    LblNote.Text = "$445 will be charged for this package";
                }
                else
                {
                    LblPayment.Text = "";
                    LblNote.Text = "";
                }
            }
            else if (Request.QueryString["AccType"] == "4")
            {

                DataTable PremiumPlanDT = ObjPremiumPlanDAL.GetPremiumPlan();
                DataRow Row = PremiumPlanDT.Select("PP_ID =" + PlanID).FirstOrDefault();
                spLimit.InnerText = Row["EmailsLimit"].ToString();
                if (DdlRegularPlan.SelectedValue == "1")
                {
                    LblPayment.Text = "199";
                    LblNote.Text = "$199 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "2")
                {
                    LblPayment.Text = "380";
                    LblNote.Text = "$380 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "3")
                {
                    LblPayment.Text = "570";
                    LblNote.Text = "$570 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "4")
                {
                    LblPayment.Text = "760";
                    LblNote.Text = "$760 will be charged for this package";
                }
                else if (DdlRegularPlan.SelectedValue == "5")
                {
                    LblPayment.Text = "955";
                    LblNote.Text = "$955 will be charged for this package";
                }
                else
                {
                    LblPayment.Text = "";
                    LblNote.Text = "";
                }
            }
            else if (Request.QueryString["AccType"] == "2")
            {

                DataTable AppointmentLevelDT = ObjAppointmentLevelDAL.GetAppointmentLevelPlan();
                DataRow Row = AppointmentLevelDT.Select("AL_ID =" + PlanID).FirstOrDefault();
                spLimit.InnerText = Row["EmailsLimit"].ToString();
                if (DdlRegularPlan.SelectedValue == "1")
                {
                    LblPayment.Text = "12";
                    LblNote.Text = "$12 will be charged for this package";
                }
                else
                {
                    LblPayment.Text = "";
                    LblNote.Text = "";
                }
            }
        }
        protected void DdlRegularPlan_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OnChangeDdlRegularPlan();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public int GetRemainingDays(string date)
        {
            DateTime ValidDate = DateTime.Parse(date);
            DateTime Now = DateTime.Now;
            TimeSpan RemainingDays = ValidDate.Subtract(Now);
            int Days = RemainingDays.Days;
            return Days;
        }
        private bool PaymentEntry()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetValidDaysDate(ObjCustomerProfileMasterBOL);
            int Remainingdays = GetRemainingDays(dt.Rows[0]["ValidDate"].ToString());
            int validationDays = (int.Parse(DdlRegularPlan.SelectedValue) * 30) + Remainingdays;
            DateTime Validdate = DateTime.Parse(dt.Rows[0]["ValidDate"].ToString()).AddDays((int.Parse(DdlRegularPlan.SelectedValue) * 30));
            ObjCodeMasterBOL.ValidationDays = validationDays.ToString();
            ObjCodeMasterBOL.ValidDate = Validdate;

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();
            ObjCodeMasterBOL.CodeID = ObjCustomerProfileMasterDAL.GetCodeNo(ObjCustomerProfileMasterBOL);
            if (ObjCodeMasterDAL.UpdateDateDays(ObjCodeMasterBOL, Trans, Conn))
            {
                ObjPaymentBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                ObjPaymentBOL.Amount = double.Parse(LblPayment.Text);

                if (ObjPaymentDAL.InsertPaymentDetail(ObjPaymentBOL, Trans, Conn))
                {
                    int EmailLimit = int.Parse(spLimit.InnerText);
                    if (objAdvertisement.UpdateEmailsLimit(ObjPaymentBOL.Cust_ID, EmailLimit))
                    {
                        Trans.Commit();
                        Conn.Close();
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    return false;
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }

        protected void BtnPurchase_Click(object sender, EventArgs e)
        {
            try
            {
                if (DdlRegularPlan.SelectedValue == "0")
                {
                    MessageBox(div2, LblMsg2, "Select your plan", 0);
                    return;
                }

                //PaymentEntry();
                if (Request.QueryString["AccType"] == "4")
                {
                    Response.Redirect("~/PPal.aspx?Product=Premium&Amount=" + LblPayment.Text + "&Limit=" + spLimit.InnerText + "&ddl=" + DdlRegularPlan.SelectedValue + "&AcType=" + Request.QueryString["AccType"]);
                }
                else if (Request.QueryString["AccType"] == "3")
                {
                    Response.Redirect("~/PPal.aspx?Product=Regular&Amount=" + LblPayment.Text + "&Limit=" + spLimit.InnerText + "&ddl=" + DdlRegularPlan.SelectedValue + "&AcType=" + Request.QueryString["AccType"]);
                }
                else if (Request.QueryString["AccType"] == "2")
                {
                    Response.Redirect("~/PPal.aspx?Product=Appointment&Amount=" + LblPayment.Text + "&Limit=" + spLimit.InnerText + "&ddl=12" + "&AcType=" + Request.QueryString["AccType"]);
                }

            }
            catch (Exception ex)
            {
              
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void btnaCP_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["AccType"] == "4")
            {
                Session["Limit"] = spLimit.InnerText;
                Session["ddl"] = DdlRegularPlan.SelectedValue;
                Session["AcType"] = Request.QueryString["AccType"] == null ? "" : Request.QueryString["AccType"].ToString();
                Session["Amount"] = LblPayment.Text;
                Session["Product"] = "Premium";

            }
            else if (Request.QueryString["AccType"] == "3")
            {
                Session["Limit"] = spLimit.InnerText;
                Session["ddl"] = DdlRegularPlan.SelectedValue;
                Session["AcType"] = Request.QueryString["AccType"] == null ? "" : Request.QueryString["AccType"].ToString();
                Session["Amount"] = LblPayment.Text;
                Session["Product"] = "Regular";
            }
            else if (Request.QueryString["AccType"] == "2")
            {
                Session["Limit"] = spLimit.InnerText;
                Session["ddl"] = DdlRegularPlan.SelectedValue;
                Session["AcType"] = Request.QueryString["AccType"] == null ? "" : Request.QueryString["AccType"].ToString();
                Session["Amount"] = LblPayment.Text;
                Session["Product"] = "Appointment";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        }

        //public bool Card_Validation()
        //{
        //    if (String.IsNullOrEmpty(TxtNameOnCard.Text))
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Card Owner Name", 0);
        //        return false;
        //    }
        //    else if (String.IsNullOrEmpty(TxtCardNumber.Text))
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Card Number", 0);
        //        return false;
        //    }
        //    else if (String.IsNullOrEmpty(TxtSecurityCode.Text))
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Security Code", 0);
        //        return false;
        //    }
        //    else if (String.IsNullOrEmpty(TxtExpiryDate.Text))
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Expiry Date", 0);
        //        return false;
        //    }
        //    else if (TxtNameOnCard.Text.Length != 15)
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Compelete Number", 0);
        //        return false;
        //    }
        //    else if (TxtNameOnCard.Text.Length != 15)
        //    {
        //        MessageBox(div3, LblMsg3, "Enter Compelete Number", 0);
        //        return false;
        //    }
        //    else
        //    {
        //        div3.Visible = false;
        //        return true;
        //    }
        //}
        //protected void BtnConfirmPayment_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (Card_Validation())
        //        {
        //            string[] date = TxtExpiryDate.Text.Split('/');
        //            if (DateValidation(date))
        //            { return; }



        //        }
        //        else
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox(div1, LblMsg, ex.Message, 0);
        //    }
        //}


        //----------------------- OrderSummary work ends Here --------------------------------------//

        private bool DateValidation(string[] date)
        {
            int year = int.Parse(DateTime.Now.Year.ToString());
            if (date.Length > 1)
            {

                int Month = int.Parse(date[0].ToString());
                int yr = int.Parse(date[1].ToString());
                if (Month < 1 || Month > 12)
                {
                    MessageBox(div3, LblMsg3, "Please Enter Valid Month", 0);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    return false;
                }
                if (yr < year)
                {
                    MessageBox(div3, LblMsg3, "Your Card is Expired", 0);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    return false;
                }


            }
            else
            {
                MessageBox(div3, LblMsg3, "Please Enter Date in Defined Format", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                return false;
            }
            return true;
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        public CDPCardBOL GetObject()
        {
            CDPCardBOL obj = new CDPCardBOL();
            if (Session["Amount"] != null && Session["Product"] != null )
            {
                obj.Amount = Session["Amount"].ToString();
                obj.Item = Session["Product"].ToString();
            }
            return obj;
        }
        [WebMethod]
         public static string Charge(CDPCardBOL CDPCard)
        {
            try
            {
                CDPCard.Cust_ID = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
                OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
                RegularPackage rep = new RegularPackage();
                CDPCardDAL CDP = new CDPCardDAL();
                int AmuntIncent = int.Parse(rep.GetObject().Amount.ToString());
                int AmountDollar = (AmuntIncent * 100);
                StripeConfiguration.ApiKey = stripeKey;
                var options = new ChargeCreateOptions
                {
                    Amount = AmountDollar,
                    Currency = CDPCard.Currency,
                    Source = CDPCard.TokenId,
                    Description = "Transaction",
                };
                var service = new ChargeService();
                var response = service.Create(options);
                if (response.Status.Equals("succeeded"))
                {
                 
                    string invoicenum = OrderAsset.GetInvoiceID();
                    TransactionBOL Transaction = new TransactionBOL();
                    Transaction.Amount = rep.GetObject().Amount;
                    Transaction.Cust_Id = rep.UserID();
                    Transaction.GUID = "stripepayment";
                    Transaction.InvoiceNumber = invoicenum;
                    Transaction.IsActive = true;
                    Transaction.Item = rep.GetObject().Item.ToString();
                    Transaction.PayerID = rep.UserID().ToString();
                    Transaction.PaymentId = response.Id;
                    Transaction.Token = CDPCard.TokenId;
                    Transaction.TransactionDate = DateTime.Now;
                    PPal Payment = new PPal();
                    Payment.InsertTransaction(Transaction);
                    DataTable dtCardInfo = CDP.GetCDPInformation(CDPCard.Cust_ID);
                    if (dtCardInfo.Rows.Count>0)
                    {
                        CDP.UpdateCDPInfo(CDPCard);
                    }
                    else
                    {
                        CDP.InsertCDPCardInfo(CDPCard);
                    }
                
                    return "Success";

                }
                else
                {
                    return "Failed";
                    //   HttpContext.Current.Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0);
                }
            }
            catch (Exception e)
            {
                //HttpContext.Current.Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0);
                return "Failed";
            }
        }

        private  int UserID()
        {
            int id = int.Parse(Session["Cust_ID"].ToString());
            return id;
        }

    }
}