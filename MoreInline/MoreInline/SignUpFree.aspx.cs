﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class SignUpFree : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        string FullCode;


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
                div1.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["Cust_ID"] != null)
                    {
                        ActiveAccount(int.Parse(Request.QueryString["Cust_ID"]));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void ActiveAccount(int Cust_ID)
        {
            try
            {
                ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
                DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);

                if (dt.Rows[0]["IsActive"].ToString() == "True" && dt.Rows[0]["ActivationDate"].ToString() != "")
                {
                    MessageBox(div1, LblMsg, "This Account is Already Active", 0);
                    return;
                }
                else
                {
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjCustomerProfileMasterDAL.UpdateActiveCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                    {
                        DataTable dt_ActiveOn = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL, Trans, Conn);
                        DateTime ValidDate = DateTime.Parse(dt_ActiveOn.Rows[0]["ActivationDate"].ToString()).AddDays(int.Parse(dt_ActiveOn.Rows[0]["ValidationDays"].ToString()));

                        ObjCodeMasterBOL.ValidDate = ValidDate;
                        ObjCodeMasterBOL.CodeID = int.Parse(dt_ActiveOn.Rows[0]["CodeID"].ToString());

                        if (ObjCodeMasterDAL.UpdateValidDateCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                        {
                            Trans.Commit();
                            Conn.Close();
                            LoginWork(dt.Rows[0]["UserName"].ToString(), dt.Rows[0]["Password"].ToString());
                        }
                        else
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void LoginWork(string UserName, string Password)
        {
            try
            {
                if (UserName != "" && Password != "")
                {
                    ObjCustomerProfileMasterBOL.UserName = UserName;
                    ObjCustomerProfileMasterBOL.Password = Password;

                    DataTable dt = ObjCustomerProfileMasterDAL.CustomerLogin(ObjCustomerProfileMasterBOL);


                    if (dt.Rows.Count == 1)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["Cust_ID"] = dt.Rows[0]["Cust_ID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["Email"] = dt.Rows[0]["Email"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();

                            Response.Redirect(@"~\Dashboard\Dashboard.aspx", false);
                        }
                        else
                        { MessageBox(div1, LblMsg, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div1, LblMsg, "Invalid Email or Password", 0); }
                }
                else
                { MessageBox(div1, LblMsg, "Enter Email or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center";
                LblMsg.Text = Msg;
            }
        }

        public bool Local_Validation()
        {
            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Company Name", 0);
                return false;
            }
            else if (TxtWebsite.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Website", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Zip Code", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtUsername.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter UserName", 0);
                return false;
            }
            else if (TxtPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text == "")
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                return false;
            }
            else if (TxtConfirmPassword.Text != TxtPassword.Text)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, "Passwords do not match", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void BtnFinish_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ProfilePhoto"] == null || ViewState["ProfilePhoto"].ToString() == "")
                {
                    if (ImgUpload.HasFile)
                    {
                        if (Page.IsValid)
                        {
                            string fileName = Path.GetFileName(ImgUpload.PostedFile.FileName);
                            ViewState["ProfilePhoto"] = fileName;
                            ImgUpload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                        }
                        else
                        {
                            return;
                        }

                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Upload Profile Photo", 0);
                        return;
                    }
                }

                if (Local_Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtEmail.Text != "")
                    {
                        Match match = regex.Match(TxtEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }

                    if (TxtEmail.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "Email";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtEmail.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This Email Already Exist", 0);
                            return;
                        }
                    }

                    if (TxtUsername.Text != "")
                    {
                        ObjCustomerProfileMasterBOL.ColumnName = "UserName";
                        ObjCustomerProfileMasterBOL.ColumnText = TxtUsername.Text;
                        DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfileExist(ObjCustomerProfileMasterBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This UserName Already Exist", 0);
                            return;
                        }
                    }

                    SaveCodeEntries();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        //public string GetOrderNo()
        //{
        //    try
        //    {
        //        DataTable dt;

        //        dt = ObjCodeMasterDAL.GetCodeNo(ObjCodeMasterBOL);
        //        if (dt.Rows.Count > 0)
        //        {
        //            string[] OrderNo = dt.Rows[0]["FullCode"].ToString().Split('-');

        //            string Number = OrderNo[OrderNo.Length - 1];
        //            int Code = int.Parse(Number) + 1;

        //            for (int i = 0; i < Number.Length; i++)
        //            {
        //                if (Code.ToString().Length < 5)
        //                {
        //                    Value += Number[i];
        //                    Length = Value.Length + Code.ToString().Length;
        //                }
        //                else
        //                {
        //                    Length = Code.ToString().Length;
        //                }
        //                if (Length >= 5)
        //                {
        //                    FullCode = "MM02-" + Value + Code.ToString();
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            FullCode = "MM02-00001";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox(div1, LblMsg, ex.Message, 0);
        //    }
        //    return FullCode;
        //}

        public string GetCodeNumber()
        {
            try
            {
                int temp;
                do
                {
                    Random RandomNumber = new Random();
                    int number = RandomNumber.Next(10000, 99999);
                    string checkNumber = "SELECT COUNT(*) FROM Tbl_CodeMaster WHERE CodeNumber= " + number;
                    SqlCommand UserExist = new SqlCommand(checkNumber, DBHelper.GetConnection());
                    int UserNameObj = Convert.ToInt32(UserExist.ExecuteScalar());

                    if (UserNameObj != 0)
                    {
                        temp = 1;
                    }
                    else
                    {
                        FullCode = "MM02-" + number.ToString();
                        temp = 0;
                    }

                }
                while (temp == 1);
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
            return FullCode;
        }

        public void SaveCodeEntries()
        {
            try
            {
                //@CodeNumber,@CodeText,@FullCode,@ValidationDays,@IsPaid,@CreatedBy

                #region New
                string[] OrderNo = GetCodeNumber().Split('-');

                ObjCodeMasterBOL.CodeNumber = OrderNo[1];
                ObjCodeMasterBOL.CodeText = OrderNo[0];
                ObjCodeMasterBOL.FullCode = OrderNo[0] + "-" + OrderNo[1];
                ObjCodeMasterBOL.ValidationDays = "15";
                ObjCodeMasterBOL.IsUtilized = 1;
                ObjCodeMasterBOL.IsPaid = 0;
                ObjCodeMasterBOL.CreatedBy = 0;

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCodeMasterDAL.InsertCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                {
                    #region New
                    //Cust_ID,FirstName,LastName,Company,Website,Address,ZipCode,Email,Phone,UserName,Password,Photo,CodeID,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,IsActive,IsDelete

                    ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
                    ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
                    ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
                    ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
                    ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
                    ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
                    ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
                    ObjCustomerProfileMasterBOL.Phone = TxtPhone.Text;
                    ObjCustomerProfileMasterBOL.UserName = TxtUsername.Text;
                    ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
                    ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
                    ObjCustomerProfileMasterBOL.CodeID = ObjCodeMasterBOL.CodeID;
                    ObjCustomerProfileMasterBOL.AccTypeID = 1;


                    if (ObjCustomerProfileMasterDAL.InsertCustomerProfileMaster(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();

                        SendConfirmationAccountEmail(ObjCustomerProfileMasterBOL.Cust_ID);
                        ClearControls();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                    #endregion
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void SendConfirmationAccountEmail(int Cust_ID)
        {
            #region EmailSignUpFree

            string link = application_path + "SignUpFree.aspx?Cust_ID=" + Cust_ID;
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/EmailSignUpFree.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{0}", TxtUsername.Text);//UserName
            body = body.Replace("{1}", TxtEmail.Text);//Email Address
            body = body.Replace("{2}", link);//Link

            #endregion

            MailMessage mm = new MailMessage("noreply@event-envite.com", TxtEmail.Text);
            mm.Subject = TxtUsername.Text + " Please confirm your email address";
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.event-envite.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "noreply@event-envite.com";
            NetworkCred.Password = "Ingph$2018$RT674q";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }

        protected void CustomValidatorImg_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal size = Math.Round(((decimal)ImgUpload.PostedFile.ContentLength / (decimal)1024), 2);
            if (Regxcheck.IsValidImage(ImgUpload))
            {
                if (size > 200)
                {
                    CustomValidatorImg.ErrorMessage = "Image size exceeds 200 KB.";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                CustomValidatorImg.ErrorMessage = "Only .jpg,.png,.jpeg,.gif Files are allowed.";
                args.IsValid = false;
            }

        }

        public void ClearControls()
        {
            TxtFName.Text = TxtLName.Text = TxtCompany.Text = TxtWebsite.Text =
            TxtAddress.Text = TxtZipCode.Text = TxtEmail.Text = TxtPhone.Text =
            TxtUsername.Text = TxtPassword.Text = "";
            ViewState["ProfilePhoto"] = null;
        }
    }
}