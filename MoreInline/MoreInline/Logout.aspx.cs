﻿using System;


namespace MoreInline
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect(@"~\LoginPage.aspx");
        }
    }
}