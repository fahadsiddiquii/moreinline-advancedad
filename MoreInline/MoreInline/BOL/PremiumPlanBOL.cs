﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class PremiumPlanBOL
    {
        public int PP_ID { get; set; }
        public string PremiumPlan { get; set; }
    }
}