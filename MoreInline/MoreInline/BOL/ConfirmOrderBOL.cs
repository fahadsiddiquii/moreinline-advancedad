﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class ConfirmOrderBOL
    {
        public string PaymentId { get; set; }
        public int Quantity { get; set; }
        public string PayerID { get; set; }
        public int Cust_Id { get; set; }
        public int AssetID { get; set; }
        public int DesignID { get; set; }
        public string Item { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime OrderDate { get; set; }
        public string Amount { get; set; }
        public int RequestType { get; set; }
        public bool IsActive { get; set; }
        public string OrderNum { get; set; }


    }
}