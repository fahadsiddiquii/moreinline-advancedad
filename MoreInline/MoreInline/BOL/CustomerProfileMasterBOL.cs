﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class CustomerProfileMasterBOL
    {
        //Cust_ID	FirstName	LastName	Company	Website	Address	ZipCode	Email	Phone	UserName	Password	Photo	CodeID	CreatedBy	CreatedOn	UpdatedBy	UpdatedOn	IsActive	IsDelete

        public int Cust_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Website { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Photo { get; set; }
        public int CodeID { get; set; }
        public int Bill_ID { get; set; }
        public int CDP_ID { get; set; }
        public int CA_ID { get; set; }
        public int AccTypeID { get; set; }
        public int EmailsLimit { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public string CompanyLogo { get; set; }
        public string ColumnName { get; set; }
        public string ColumnText { get; set; }
    }
}