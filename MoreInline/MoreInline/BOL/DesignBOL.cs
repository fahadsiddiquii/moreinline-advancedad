﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class DesignBOL
    {
        public int ID { get; set; }
        public int AssestID { get; set; }
        public string FilePath { get; set; }
        public int CreatedByID { get; set; }
        public int QAID { get; set; }
        public int QaApproval { get; set; }
        public int MngrApproval { get; set; }
        public int CustApproval { get; set; }
        public string QaRemarks { get; set; }
        public string MngrRemarks { get; set; }
        public string CustRemarks { get; set; }

    }
}