﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class RegularPlanBOL
    {
        public int RP_ID { get; set; }
        public string RegularPlan { get; set; }
    }
}