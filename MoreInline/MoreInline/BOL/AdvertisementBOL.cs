﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class AdvertisementBOL
    {
        //AdsID	CustomerName	CustomerEmail	CodeID	AppointmentAds	CustAds1	CustAds2	CustAds3	CustAds4	EmailStatusID

        public int AdsID { get; set; }
        public string CompanyName { get; set; }
        public string CustomerEmail { get; set; }
        public int CodeID { get; set; }
        public string AppointmentAds { get; set; }
        public string LockedAppointmentAds { get; set; }
        public string CustAds1 { get; set; }
        public string CustAds2 { get; set; }
        public string CustAds3 { get; set; }
        public string CustAds4 { get; set; }
        public int EmailStatusID { get; set; }
        public int CreatedBy { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
    }
}