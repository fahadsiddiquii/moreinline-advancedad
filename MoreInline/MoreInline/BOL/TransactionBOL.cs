﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class TransactionBOL
    {
        public string GUID { get; set; }
        public string PaymentId { get; set; }
        public string Token { get; set; }
        public string PayerID { get; set; }
        public int Cust_Id { get; set; }
        public string Item { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Amount { get; set; }
        public bool IsActive { get; set; }
        public string InvoiceNumber { get; set; }
        public int TransactionId { get; set; }

    }
}