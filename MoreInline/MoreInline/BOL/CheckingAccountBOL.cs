﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class CheckingAccountBOL
    {
        //CA_ID AccountType AccountName AccountNumber   RoutingNumber DL_Number   AO_BirthDate

        public int CA_ID { get; set; }
        public string AccountType { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
        public string DL_Number { get; set; }
        public string AO_BirthDate { get; set; }

    }
}