﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class CDPCardBOL
    {
        // CDP_ID NameOnCard  CardNumber SecurityCode    ExpiryDate

        public int CDP_ID { get; set; }
        public string NameOnCard { get; set; }
        public string CardNumber { get; set; }
        public string SecurityCode { get; set; }
        public string ExpiryDate { get; set; }
        public string TokenId { get; set; }
        public string Amount { get; set; }
        public string  Currency  { get; set; }
        public string Item { get; set; }
        public int Cust_ID { get; set; }
    }
}