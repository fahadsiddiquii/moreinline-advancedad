﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class PaymentBOL
    {
        public int Payment_ID { get; set; }
        public int Cust_ID { get; set; }
        public double Amount { get; set; }
    }
}