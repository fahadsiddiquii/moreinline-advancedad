﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class NotificationInfo
    {
        public int id { get; set; }
        public Nullable<int> OID { get; set; }
        public Nullable<int> UID { get; set; }
        public Nullable<int> UserRoleID { get; set; }
        public Nullable<bool> IsViewed { get; set; }
        public string FormPath { get; set; }
        public Nullable<System.DateTime> NotiDate { get; set; }
    }
}