﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class SentEmailLogBOL
    {

        public int ID { get; set; }

        public int AssestID { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Title { get; set; }

        public string Company { get; set; }

        public DateTime CreateDate { get; set; }

        public int CreateBy { get; set; }

        public bool IsOver { get; set; }

        public DateTime AppointmentDate { get; set; }

        public string Hours { get; set; }

        public string TimeSpan { get; set; }

        public int ContactId { get; set; }

        public int CategoryId { get; set; }

        public string AudioPath { get; set; }

    }
}