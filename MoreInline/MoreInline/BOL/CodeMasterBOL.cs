﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class CodeMasterBOL
    {
        //CodeID CodeNumber  CodeText FullCode    ValidDate ValidationDays  CodeStatus IsUtilized  IsPaid CreatedBy   CreatedOn UpdatedBy   UpdatedOn IsActive    IsDelete

        public int CodeID { get; set; }
        public string CodeNumber { get; set; }
        public string CodeText { get; set; }
        public string FullCode { get; set; }
        public DateTime ValidDate { get; set; }
        public string ValidationDays { get; set; }
        public int CodeStatus { get; set; }
        public int IsUtilized { get; set; }
        public int IsPaid { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

    }
}