﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class UserSignupBOL
    {
        //UserID	FirstName	LastName	UserEmail	UserPassword	UserPhone	UserAddress	UserType	Photo	CreatedBy	CreatedOn	UpdatedBy	UpdatedOn	IsActive	IsDelete

        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public string UserPhone { get; set; }
        public string UserAddress { get; set; }
        public int UserType { get; set; }
        public string Photo { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int UpdatedBy { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

        //////////////////////////////////////

        //UserRoleID UserRoles   UserLimits

        public int UserRoleID { get; set; }
        public string UserRoles { get; set; }
        public int UserLimits { get; set; }
    }
}