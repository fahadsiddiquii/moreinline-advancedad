﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class WebsiteBOL
    {
        public int id { get; set; }
        public string WebURL { get; set; }
        public string logoPath { get; set; }
        public string BannerPath { get; set; }
        public string BannerText { get; set; }
        public int Cust_ID { get; set; }
    }
}