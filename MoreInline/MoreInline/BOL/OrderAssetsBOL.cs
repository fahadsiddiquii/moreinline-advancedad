﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline.BOL
{
    public class OrderAssetsBOL
    {
        public int ID { get; set; }
        public string Subject { get; set; }
        public string Email { get; set; }
        public string OrderNum { get; set; }
        public string Phone { get; set; }
        public string Location { get; set; }
        public DateTime? Date { get; set; }
        public int Hours { get; set; }
        public string WebUrl { get; set; }
        public string Message { get; set; }
        public string AudioPath { get; set; }
        public bool? AddAudio { get; set; }
        public bool? AddContact { get; set; }
        public bool? AddDirection { get; set; }
        public bool? AddReminder { get; set; }
        public bool? AddWebUrl { get; set; }
        public bool? AddUerProfile { get; set; }
        public string AssestFilePath { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedByID { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedByID { get; set; }
        public int ApprovalStatus { get; set; }
        public bool IsActive { get; set; }
        public string TimSpan { get; set; }
        public int Category { get; set; }
        


    }
}