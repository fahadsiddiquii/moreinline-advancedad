﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class LoginPage : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();

        private static string DateE = ConfigurationManager.AppSettings["Date"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                div2.Visible = false;
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }

            //if (div.ID == "div1")
            //{
            //    row1.Attributes["style"] = "margin-top: 0px;";
            //}
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                LoginWork(TxtUserName.Text, TxtPassword.Text);
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }

        }
        public void LoginWork(string UserName, string Password)
        {
            try
            {
                //string Date = DateE;
                //string Date1 = DateTime.Now.ToString("yyyy/MM/dd");

                //if (DateTime.Parse(Date) <= DateTime.Parse(Date1))
                //{
                //    MessageBox(div1, LblMsg, "Timeout expired. The timeout period elapsed prior to obtaining a connection from the pool. This may have occurred because all pooled connections were in use and max pool size was reached.", 0);
                //    return;
                //}

                if (TxtUserName.Text != "" && TxtPassword.Text != "")
                {
                    ObjCustomerProfileMasterBOL.Email = UserName;
                    ObjCustomerProfileMasterBOL.Password = Password;

                    DataTable dt = ObjCustomerProfileMasterDAL.CustomerLogin(ObjCustomerProfileMasterBOL);


                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["Cust_ID"] = dt.Rows[0]["Cust_ID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["Email"] = dt.Rows[0]["Email"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();
                            Session["PayerID"] = dt.Rows[0]["PayerID"].ToString();
                            Session["PaymentId"] = dt.Rows[0]["PaymentId"].ToString();
                            Session["AccTypeID"] = dt.Rows[0]["AccTypeID"].ToString();
                            Session["DashBoardTour"] = dt.Rows[0]["DashBoardTour"].ToString();
                            Session["OrderAssetsTour"] = dt.Rows[0]["OrderAssetsTour"].ToString();
                            Response.Redirect(@"~\Dashboard\Dashboard.aspx", false);
                        }
                        else
                        { MessageBox(div1, LblMsg, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div1, LblMsg, "Invalid Username or Password", 0); }
                }
                else
                { MessageBox(div1, LblMsg, "Enter Username or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            try
            {
                if (Message_Validation())
                {
                    string body = Email_Construction();
                    SendEmail(body);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg2, ex.Message, 0);
            }
        }

        public bool Message_Validation()
        {
            TxtName.Attributes["style"] = "border: 1px solid #4B4A4A";
            TxtEmail.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtMessage.Attributes["style"] = "border: 1px solid #4B4A4A;";
            if (string.IsNullOrWhiteSpace(TxtName.Text))
            {
                TxtName.Attributes["style"] = "border: 1px solid red";
                MessageBox(div2, LblMsg2, "Please Enter Name", 0);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(TxtEmail.Text))
            {
                TxtEmail.Attributes["style"] = "border: 1px solid red";
                MessageBox(div2, LblMsg2, "Please Enter Email", 0);
                return false;
            }
            else if (string.IsNullOrWhiteSpace(TxtMessage.Text))
            {
                TxtMessage.Attributes["style"] = "border: 1px solid red";
                MessageBox(div2, LblMsg2, "Please Enter Message", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        private string Email_Construction()
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{name}", TxtName.Text);
            body = body.Replace("{email}", TxtEmail.Text);
            body = body.Replace("{contact}", TxtNumber.Text);
            body = body.Replace("{company}", TxtCompany.Text);
            body = body.Replace("{message}", TxtMessage.Text);
            return body;
        }
        private void SendEmail(string body)
        {
            MailMessage mm = new MailMessage("donotreply@moreinline.com", "stan.ferrell64@gmail.com");
            mm.Subject = "MoreInline Viewer Email";
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }

        protected void BtnMX11Go_Click(object sender, EventArgs e)
        {
            try
            {
                string[] code = TxtMX11.Text.Split('-');
                if (code[0] != "MX11")
                {
                    MessageBox(div1, LblMsg, "Wrong Code", 0);
                    return;
                }
                else
                {
                    DataTable dt = ObjAdvertisementDAL.GetGetCodeIDEmail(TxtMX11.Text);
                    if (dt.Rows.Count > 0)
                    {
                        Response.Redirect(application_path+ "MKT_Assets/EmailFishing.aspx?CodeID="+dt.Rows[0]["CodeID"].ToString() + "&Email="+ dt.Rows[0]["CustomerEmail"].ToString() + "&FullCode=" +TxtMX11.Text);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Code not exist.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnIN001Go_Click(object sender, EventArgs e)
        {
            try
            {
                string[] code = TxtIN001.Text.Split('-');
                if (code[0] != "IN001")
                {
                    MessageBox(div1, LblMsg, "Wrong Code", 0);
                    return;
                }
                else
                {
                    DataTable dt = ObjAdvertisementDAL.GetGetCodeIDEmail(TxtIN001.Text);
                    if (dt.Rows.Count > 0)
                    {
                        Response.Redirect(application_path + "MKT_Assets/EmailFishing.aspx?CodeID=" + dt.Rows[0]["CodeID"].ToString() + "&Email=" + dt.Rows[0]["CustomerEmail"].ToString() + "&FullCode=" + TxtIN001.Text);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Code not exist.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnMM02Go_Click(object sender, EventArgs e)
        {
            try
            {
                string[] code = TxtMM02.Text.Split('-');
                if (code[0] != "MM02")
                {
                    MessageBox(div1, LblMsg, "Wrong Code", 0);
                    return;
                }
                else
                {
                    DataTable dt = ObjAdvertisementDAL.GetGetCodeIDEmail(TxtMM02.Text);
                    if (dt.Rows.Count > 0)
                    {
                        Response.Redirect(application_path + "MKT_Assets/EmailFishing.aspx?CodeID=" + dt.Rows[0]["CodeID"].ToString() + "&Email=" + dt.Rows[0]["CustomerEmail"].ToString() + "&FullCode=" + TxtMM02.Text);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Code not exist.", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
    }
}