﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Emails
{
    public partial class VoiceMessage : System.Web.UI.Page
    {
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Request.QueryString["AssetID"] != null)
                    {
                        DataTable dt = new DataTable();

                        int AssetId = int.Parse(Request.QueryString["AssetID"].ToString());
                        if (Request.QueryString["View"] != null)
                        {
                            if (Request.QueryString["View"].ToString().Equals("ViewAssest"))
                            {
                                dt = OrderAsset.GetAssetVoice(AssetId);
                                LoadOrderAssetMasterData(dt, Request.QueryString["View"].ToString());
                            }
                        }
                        else
                        {
                            dt = OrderAsset.GetEmailLandingAssetsMaster(AssetId);
                            LoadOrderAssetMasterData(dt, "");
                        }


                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private void LoadOrderAssetMasterData(DataTable dt,string Pg)
        {

            try
            {
                if (dt.Rows.Count > 0)
                {
                    HfAssetId.Value = dt.Rows[0][0].ToString();
                    string AudioPath = !string.IsNullOrWhiteSpace(Pg) ? dt.Rows[0][0].ToString() : dt.Rows[0][8].ToString();
                    HfVoicePath.Value = AudioPath;
                    string[] Mpath = AudioPath.Split('~');
                    try { AudioPath = Mpath[1].ToString(); }
                    catch (Exception) { AudioPath = ""; }
                    Session["Patha"] = ".." + AudioPath;
                    string script = "<script>SetAudio('.." + AudioPath + "')</script>";
                    //      audio.Src = ".." + AudioPath;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);
                }
            }
            catch (Exception)
            {
            }
        }

    }
}