﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Unsubscribe.aspx.cs" Inherits="MoreInline.Emails.Unsubscribe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Advance Ad | Unsubscription Center</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->
    <link href="../agency-video/img/favicon.png" rel="icon" />
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <!-- jQuery 3 -->
    <script src="../../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../../dist/js/adminlte.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header" style="background-color: #000000;">
                <!-- Logo -->
                <a href="../LoginPage.aspx" class="logo" style="background-color: #000000;">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="160px" height="35px" />
                </a>
                <nav class="navbar navbar-static-top" role="navigation" style="background-color: #000000;">
                </nav>
            </header>

            <div class="content-wrapper m-l-0" style="background-color: #f7f8f9;">
                <asp:Panel ID="Panel1" runat="server">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4 text-center m-t-40">
                            <h1>Email Notification Opt-Out</h1>
                            <br />
                            <p>Would you like to opt out of this email list?</p>
                            <br />
                            <asp:Button ID="BtnUnsubscribe" Text="Yes, I would like to opt out" runat="server" CssClass="btn btn-black-green" OnClick="BtnUnsubscribe_Click" />
                        </div>

                    </div>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" Visible="false">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 text-center m-t-40">
                            <h3>You have successfully unsubscribed this email.</h3>
                        </div>
                    </div>
                </asp:Panel>
            </div>



        </div>
    </form>
</body>
</html>
