﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Emails
{
    public partial class Contact : System.Web.UI.Page
    {
        OrderAssetsDAL objOrderAssetsDAL = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            if (Request.QueryString["View"] != null)
            {
                if (Request.QueryString["View"].ToString().Equals("ViewAssest"))
                    dt = objOrderAssetsDAL.GetAssetContacts(int.Parse(Request.QueryString["AssetID"]));
            }
            else
            { dt = objOrderAssetsDAL.GetEmailLandingAssetsMaster(int.Parse(Request.QueryString["AssetID"])); }
                
            if (dt.Rows[0]["Email"].ToString() != "")
            {
                LblEmail.Text = dt.Rows[0]["Email"].ToString();
            }
            else
            {
                SpanEmail.Visible = false;
            }
            if (dt.Rows[0]["Phone"].ToString() != "")
            {
                LblPhone.Text = dt.Rows[0]["Phone"].ToString();
            }
            else
            {
                SpanPhone.Visible = false;
            }
            
        }
    }
}