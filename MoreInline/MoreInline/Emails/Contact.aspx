﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="MoreInline.Emails.Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Contact</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        .styleDiv {
            box-shadow: 5px 0px 50px #ABC502;
            border: 1px solid #FFFFFF;
            border-radius: 8px;
            opacity: 1;
        }
    </style>
</head>
<body style="background-color: #000; overflow-x: hidden;">
    <form id="form1" runat="server">
        <h1 class="text-center" style="padding-top: 20px;"><span style="color: #fff;">More</span><span style="color: #ABC502;">Inline</span></h1>
        <br /><br /><br />  
        <div class="text-center styleDiv" style="width: 350px; margin: 0 auto; background-color: #DFDFDF; padding-bottom: 40px;">
            <br />
            <h1>Contact</h1>
            <br />
            <asp:Label Text="Email:" ID="SpanEmail" runat="server" />
            <asp:Label ID="LblEmail" runat="server" />
            <br />
            <asp:Label Text="Phone:" ID="SpanPhone" runat="server" />
            <asp:Label ID="LblPhone" runat="server" />
        </div>
    </form>
</body>
</html>
