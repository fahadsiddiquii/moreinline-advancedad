﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Emails
{
    public partial class Reminder : System.Web.UI.Page
    {
        OrderAssetsDAL objOrderAssetsDAL = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ImgBtnGoogle_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

         
            DataTable dt = new DataTable();
            DataTable dtAssets = new DataTable();
            if (Request.QueryString["View"] != null)
            {
                if (Request.QueryString["View"].ToString().Equals("ViewAssest"))
                {
                    dt = objOrderAssetsDAL.GetAssetReminder(int.Parse(Request.QueryString["AssetID"]), int.Parse(Request.QueryString["TrgtUid"])).Tables[0];
                    dtAssets = objOrderAssetsDAL.GetAssetReminder(int.Parse(Request.QueryString["AssetID"]), int.Parse(Request.QueryString["TrgtUid"])).Tables[1];
                }
            }
            else
            { dt = objOrderAssetsDAL.GetEmailLandingAssetsMaster(int.Parse(Request.QueryString["AssetID"])); }

            #region OldDateCode

            //string URl = "http://www.google.com/calendar/event?action=TEMPLATE&dates=" + yyyy + MM + dd + "%2F" + yyyy + MM + (dd + 1) + "&text=" + dt.Rows[0]["Subject"].ToString() + "&location=" + dt.Rows[0]["Location"].ToString() + "&details=" + dt.Rows[0]["Subject"].ToString() + "&ctz=" + dt.Rows[0]["date"].ToString();

            #endregion
            string Hours = "";

            if (dt.Rows[0]["Hours"].ToString().Length == 1)
            {
                Hours = "0" + dt.Rows[0]["Hours"].ToString();
            }
            else { Hours = dt.Rows[0]["Hours"].ToString(); }

            if (dt.Rows[0]["TimeSpan"].ToString() =="pm")
            {
                DateTime hour = DateTime.Parse(Hours+" "+"PM");
                Hours = hour.ToString("HH");
            }

            string StartDate = DateTime.Parse(dt.Rows[0]["AppointmentDate"].ToString()).ToString("yyyyMMdd") + "T" + Hours + "0000";
            string EndDate = DateTime.Parse(dt.Rows[0]["AppointmentDate"].ToString()).AddDays(1).ToString("yyyyMMdd") + "T" + Hours + "0000";
            string URl = "http://www.google.com/calendar/event?action=TEMPLATE&dates=" + StartDate + "%2F" + EndDate + "&text=" + dtAssets.Rows[0]["Subject"].ToString() + "&location=" + dtAssets.Rows[0]["Location"].ToString() + "&details=" + dtAssets.Rows[0]["Subject"].ToString();
            Response.Write("<script>window.open('" + URl + "','_blank')</script>");
            }
            catch (Exception s)
            {
                Response.Write("<script>alert('Event is not set by organizer ')</script>");
            }
        }

        protected void ImgBtnOutlook_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

           
            DataTable dt = new DataTable();
            DataTable dtAssets = new DataTable();
            if (Request.QueryString["View"] != null)
            {
                if (Request.QueryString["View"].ToString().Equals("ViewAssest"))
                {
                    dt = objOrderAssetsDAL.GetAssetReminder(int.Parse(Request.QueryString["AssetID"]), int.Parse(Request.QueryString["TrgtUid"])).Tables[0];
                    dtAssets = objOrderAssetsDAL.GetAssetReminder(int.Parse(Request.QueryString["AssetID"]), int.Parse(Request.QueryString["TrgtUid"])).Tables[1];
                }
            }
            else
            { dt = objOrderAssetsDAL.GetEmailLandingAssetsMaster(int.Parse(Request.QueryString["AssetID"])); }

            //string StartDate = DateTime.Parse(dt.Rows[0]["Date"].ToString()).ToString("yyyyMMdd");
            //string EndDate = DateTime.Parse(dt.Rows[0]["Date"].ToString()).AddDays(1).ToString("yyyyMMdd");
            string Hours = "";

            if (dt.Rows[0]["Hours"].ToString().Length == 1)
            {
                Hours = "0" + dt.Rows[0]["Hours"].ToString();
            }
            else { Hours = dt.Rows[0]["Hours"].ToString(); }
            if (dt.Rows[0]["TimeSpan"].ToString() == "pm")
            {
                DateTime hour = DateTime.Parse(Hours + " " + "PM");
                Hours = hour.ToString("HH");
            }
            string StartDate = DateTime.Parse(dt.Rows[0]["AppointmentDate"].ToString()).ToString("yyyyMMdd") + "T" + Hours + "0000";
            string EndDate = DateTime.Parse(dt.Rows[0]["AppointmentDate"].ToString()).AddDays(1).ToString("yyyyMMdd") + "T" + Hours + "0000";

            string filePath = Server.MapPath("~/iCal/" + MakeHourEvent(dtAssets.Rows[0]["Subject"].ToString(), dtAssets.Rows[0]["Location"].ToString(), StartDate, EndDate, dtAssets.Rows[0]["Subject"].ToString()));

            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filePath);

            Response.WriteFile(filePath);
            Response.Flush();

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            }
            catch (Exception s)
            {
                Response.Write("<script>alert('Event is not set by organizer ')</script>");
            }

        }

        public string MakeHourEvent(string subject, string location, string STdate, string ETdate, string DESCRIPTION)
        {
            string filePath = string.Empty;
            string path = HttpContext.Current.Server.MapPath(@"~\iCal\");
            string name = subject + DateTime.Now.Second.ToString();
            name += ".ics";
            filePath = path + name;
            StreamWriter writer = new StreamWriter(filePath);
            writer.WriteLine("BEGIN:VCALENDAR");
            writer.WriteLine("VERSION:2.0");
            writer.WriteLine("PRODID:-//hacksw/handcal//NONSGML v1.0//EN");
            writer.WriteLine("BEGIN:VEVENT");

            writer.WriteLine("DTSTART:" + STdate);
            writer.WriteLine("DTEND:" + ETdate);
            writer.WriteLine("SUMMARY:" + subject);
            writer.WriteLine("DESCRIPTION:" + DESCRIPTION);
            writer.WriteLine("LOCATION:" + location);
            writer.WriteLine("END:VEVENT");
            writer.WriteLine("END:VCALENDAR");
            writer.Close();

            return name;

        }
    }
}