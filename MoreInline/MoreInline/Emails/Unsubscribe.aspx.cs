﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Emails
{
    public partial class Unsubscribe : System.Web.UI.Page
    {
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Type"] != null && Request.QueryString["CustID"] != null)
                {
                    string type = Request.QueryString["Type"].ToString();
                    ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Request.QueryString["CustID"].ToString());
                    DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                    if (dt.Rows[0][type].ToString() == "False")
                    {
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                    }
                }
                else if (Request.QueryString["mail"] != null && Request.QueryString["CustID"] != null)
                {
                    Panel1.Visible = true;
                }
                else if (Request.QueryString["AdsID"] != null)
                {
                    int AdsID = int.Parse(Request.QueryString["AdsID"].ToString());
                    DataTable dt = ObjAdvertisementDAL.GetAdvertisement(AdsID);
                    if (dt.Rows[0]["IsSubscribed"].ToString() == "False")
                    {
                        Panel1.Visible = false;
                        Panel2.Visible = true;
                    }
                    else
                    {
                        Panel1.Visible = true;
                        Panel2.Visible = false;
                    }
                }
            }
           
        }

        protected void BtnUnsubscribe_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["Type"] != null && Request.QueryString["CustID"] != null)
            {
                string type = Request.QueryString["Type"].ToString();
                int CustID = int.Parse(Request.QueryString["CustID"].ToString());
                if (ObjCustomerProfileMasterDAL.Unsubscribe(CustID, type))
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                }
            }
            else if (Request.QueryString["Lid"] != null && Request.QueryString["CustID"] != null && Request.QueryString["mail"] != null)
            {
                int ListID = int.Parse(Request.QueryString["Lid"].ToString());
                string Email = Request.QueryString["mail"].ToString();
                int CustID = int.Parse(Request.QueryString["CustID"].ToString());
                if (ObjCustomerProfileMasterDAL.Unsubscribe(CustID,Email, ListID))
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                }

            }
            else if (Request.QueryString["AdsID"] != null)
            {
                int AdsID = int.Parse(Request.QueryString["AdsID"].ToString());
                if (ObjAdvertisementDAL.UnsubscribeEmail(AdsID))
                {
                    Panel1.Visible = false;
                    Panel2.Visible = true;
                }
            }
        }
    }
}