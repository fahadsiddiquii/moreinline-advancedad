﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Emails
{
    public partial class Direction : System.Web.UI.Page
    {
        OrderAssetsBOL objOrderAssetsBOL = new OrderAssetsBOL();
        OrderAssetsDAL objOrderAssetsDAL = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            if (Request.QueryString["View"] != null)
            {
                if (Request.QueryString["View"].ToString().Equals("ViewAssest"))
                    dt = objOrderAssetsDAL.GetAssetDirection(int.Parse(Request.QueryString["AssetID"]));
            }
            else
            { dt = objOrderAssetsDAL.GetEmailLandingAssetsMaster(int.Parse(Request.QueryString["AssetID"])); }

            txtDestination.Text = dt.Rows[0]["Location"].ToString();
        }

        

    }
}