﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reminder.aspx.cs" Inherits="MoreInline.Emails.Reminder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reminder</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body style="background-color: #171616;">
    <form id="form1" runat="server">
        <h1 class="text-center" style="padding-top: 20px;"><span style="color: #fff;">More</span><span style="color: #ABC502;">Inline</span></h1>
        <br />
        <br />
        <h2 class="text-center" style="color: #fff;">Reminder</h2>
        <br />
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-6">
                    <asp:ImageButton ID="ImgBtnGoogle" ImageUrl="~/Images/Icons/icons-google-calendar.png" runat="server" OnClick="ImgBtnGoogle_Click" style="width: 300px; height: 300px;" />
                    <h3 style="color: #fff;">Google</h3>
                </div>
                <div class="col-lg-6">
                    <asp:ImageButton ID="ImgBtnOutlook" ImageUrl="~/Images/Icons/icons-microsoft-outlook.png" runat="server" OnClick="ImgBtnOutlook_Click" style="width: 300px; height: 300px;" />
                    <h3 style="color: #fff;">Outlook</h3>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
