﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Snap.aspx.cs" Inherits="MoreInline.Snap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Advance Ad Snap</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="agency-video/img/favicon.png" rel="icon" />
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <link href="css/MoreInline_Style.css" rel="stylesheet" />
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />

    <link rel="stylesheet" href="../vendor/css/bundle.min.css" />
    <link rel="stylesheet" href="../vendor/css/owl.carousel.min.css"/>
    <link rel="stylesheet" href="../vendor/css/jquery.fancybox.min.css"/>
    <link rel="stylesheet" href="../vendor/css/cubeportfolio.min.css"/>
    <link href="../agency-video/css/tooltipster.min.css" rel="stylesheet" />
    <link href="../agency-video/css/settings.css" rel="stylesheet" />

    <link href="../agency-video/css/navigation.css" rel="stylesheet" />
    <link href="../agency-video/css/style.css" rel="stylesheet" />
    <style>

        .foo {
    display: block;
    position: relative;
    width: 100%;
    margin: auto;
    cursor: pointer;
    border: 0;
    height: 40px;
    border-radius: 5px;
    outline: 0;
}
.foo:hover:after {
    background: #dfdfdf;
}
.foo:after {
    transition: 200ms all ease;
    border-bottom: 3px solid rgba(0,0,0,.2);
    background: #A9c502;
    text-shadow: 0 2px 0 rgba(0,0,0,.2);
    color: #fff;
    font-size: 20px;
    text-align: center;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    display: block;
    content: 'Upload Logo';
    line-height: 38px;
    border-radius: 5px;
}
    </style>
</head>
<body style="background-color: #f7f8f9; overflow-x: hidden;">
  

    <form id="form1" runat="server">
          <asp:ScriptManager runat="server" ID="sm1" />
        <h1 class="text-center" style="padding-top: 20px;"><span style="color: #000;">More</span><span style="color: #ABC502;">Inline</span></h1>
        <div class="container">
            <h1 style="color: #007f9f;" class="text-center">SNAP</h1>
            <br />
            <div class="row">
                <div class="col-md-6">
                         <div class="form-group">
                        <label for="TxtFname">Full Name </label>
                        <asp:TextBox ID="TxtFname" runat="server" CssClass="form-control" required placeholder="Full Name" autofocus="true"></asp:TextBox>
                    </div>
                      <div class="form-group">
                        <label for="txtEmail">Enter Company Email</label>
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" required placeholder="Email Address" autofocus="true"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                
                     <div class="form-group">
                        <label for="logo">Company Logo</label>
                                   <asp:FileUpload ID="logo" CssClass=""  ViewStateMode="Enabled" runat="server"  />
                    </div>
                      <div class="form-group">
                        <label for="txtWeb">Enter Company Website</label>
                        <asp:TextBox ID="txtWeb" runat="server" CssClass="form-control" required placeholder="Website" autofocus="true"></asp:TextBox>
                    </div>
                </div>
            </div>
             
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="heading-title darkcolor wow fadeInUp" data-wow-delay="10ms">
                        <h4 class="font-normal heading_space_half"> Select Package </h4>
                    </div>
                </div>
            </div>

           
                        <div class="owl-carousel owl-theme no-dots" id="price-slider">
       
                <div class="item">
                    <div class="col-md-12">
                        <div class="pricing-item wow fadeInUp animated" id="div12"  style="height:243px;">
                            <div class="pricing-price darkcolor"><span class="pricing-currency">$12.00 </span> /<span class="pricing-duration">Year</span></div>
                            <asp:UpdatePanel ID="one" runat="server">
                                <ContentTemplate> 
                                      <asp:Button class="button" ID="btn12" Text="Choose plan" runat="server" OnClientClick="set12()"  OnClick="btn12_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                                
                             
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="col-md-12">
                        <div class="pricing-item wow fadeInUp animated " id="div99" style="height:243px;">
                            <div class="pricing-price darkcolor"><span class="pricing-currency">$99.00 </span> /<span class="pricing-duration">month</span></div>
                            <asp:UpdatePanel  ID="two" runat="server">
                                <ContentTemplate> 
                                    <asp:Button class="button" ID="btn99" Text="Choose plan" runat="server" OnClientClick="set99()" OnClick="btn99_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                             
                           
                        </div>
                    </div>
                </div>
      
                <div class="item">
                    <div class="col-md-12">
                        <div class="pricing-item wow fadeInUp animated" id="DIV199" style="height:243px;">
                            <div class="pricing-price darkcolor"><span class="pricing-currency">$199.00</span> /<span class="pricing-duration">month</span></div>
                            <asp:UpdatePanel  ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                            <asp:Button class="button" ID="btn199" Text="Choose plan" runat="server" OnClientClick="set199()" OnClick="btn199_Click"  />
                            </ContentTemplate>
                                </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
 

            </div>
          <div class="row">
                <div class="col-md-6 col-sm-6 text-center">
                    <asp:Button CssClass="button btn-bitbucket" Text="Pay with PayPal or Credit Card" OnClick="btnPay_Click"  ID="btnPay" runat="server" />
                </div>
               <div class="col-md-6 col-sm-6 text-center">
                    <asp:Button CssClass="button btn-bitbucket" Text="Pay with Cash" OnClick="BtnCashPay_Click"   ID="BtnCashPay" runat="server" />
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-center wow fadeIn" data-wow-delay="300ms">
                    <div class="heading-title darkcolor wow fadeInUp" data-wow-delay="300ms">
                        <h3 class="font-normal darkcolor heading_space_half"> Our Portfolio </h3>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="grid-mosaic" class="cbp cbp-l-grid-mosaic-flat">
                        <!--Item 1-->
                        <div class="cbp-item brand graphics">
                            <img src="agency-video/img/aGeneric_Appointment_Template_MoreInline.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/aGeneric_Appointment_Template_MoreInline.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Sweet Cup</h4>-->
                                </div>
                            </div>
                        </div>
                        <!--Item 2-->
                        <div class="cbp-item brand graphics design">
                            <img src="agency-video/img/cGeneric_App_Template_MoreInline.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/cGeneric_App_Template_MoreInline.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Minimal Things</h4>-->
                                </div>
                            </div>
                        </div>
                        <!--Item 3-->
                        <div class="cbp-item design digital graphics">
                            <img src="agency-video/img/dGeneric_App_Template_MoreInline.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/dGeneric_App_Template_MoreInline.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Semantic Collection</h4>-->
                                </div>
                            </div>
                        </div>
                        <!--Item 4-->
                        <div class="cbp-item brand graphics">
                            <img src="agency-video/img/eGeneric_App_Template_MoreInline.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/eGeneric_App_Template_MoreInline.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Wall Clock</h4>-->
                                </div>
                            </div>
                        </div>
                        <!--Item 5-->
                        <div class="cbp-item graphics design design">
                            <img src="agency-video/img/Generic_App_Template_MoreInline.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/Generic_App_Template_MoreInline.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Reading Content</h4>-->
                                </div>
                            </div>
                        </div>
                        <!--Item 6-->
                        <div class="cbp-item graphics design design">
                            <img src="agency-video/img/SBS_FLY_0002.jpg" alt="">
                            <div class="gallery-hvr whitecolor">
                                <div class="center-box">
                                    <a href="agency-video/img/SBS_FLY_0002.jpg" class="opens" data-fancybox="gallery" title="Zoom In"> <i class="fa fa-search-plus"></i></a>
                                    <!--<a href="javascript:void(0)" class="opens" title="View Details"> <i class="fas fa-link"></i></a>-->
                                    <!--<h4 class="w-100">Reading Content</h4>-->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
   
        </div>
    </form>

    <script>
        function set199() {
            $("#DIV199").addClass("selected");
            $("#div99").removeClass("selected");
            $("#div12").removeClass("selected");
        }
        function set99() {
            $("#div99").addClass("selected");
            $("#div12").removeClass("selected");
            $("#DIV199").removeClass("selected");
        }
        function set12() {
            $("#div12").addClass("selected");
            $("#DIV199").removeClass("selected");
            $("#div99").removeClass("selected");
        }
    </script>


    <%-- ----------------------------------- Script --------------------------------------------- --%>

    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="../vendor/js/bundle.min.js"></script>
    <!--to view items on reach-->
    <script src="../vendor/js/jquery.appear.js"></script>
    <!--Owl Slider-->
    <script src="../vendor/js/owl.carousel.min.js"></script>
    <!--Parallax Background-->
    <script src="../vendor/js/parallaxie.min.js"></script>
    <!--Cubefolio Gallery-->
    <script src="../vendor/js/jquery.cubeportfolio.min.js"></script>
    <!--Fancybox js-->
    <script src="../vendor/js/jquery.fancybox.min.js"></script>
    <!--wow js-->
    <script src="../vendor/js/wow.js"></script>
    <!--number counters-->
    <script src="../agency-video/js/jquery-countTo.js"></script>
    <!--tooltip js-->
    <script src="../agency-video/js/tooltipster.min.js"></script>
    <!--Revolution SLider-->
    <script src="../vendor/js/jquery.themepunch.tools.min.js"></script>
    <script src="../vendor/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
    <script src="../vendor/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="../vendor/js/extensions/revolution.extension.video.min.js"></script>
    <!--map js

    -->
    <!--<script src="https://maps.google.com/maps/api/js?key=AIzaSyCo_pcAdFNbTDCAvMwAD19oRTuEmb9M50c"></script>-->
    <!--<script src="../agency-video/js/map.js"></script>-->
    <!--custom functions and script-->
    <script src="../agency-video/js/functions.js"></script>



    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
            async defer>
    </script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script> -->

</body>
</html>
