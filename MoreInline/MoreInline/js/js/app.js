function restore(){
  //$("#record, #live").removeClass("disabled");
  //$("#pause").replaceWith('<a class="button one" id="pause">Pause</a>');
    //$(".one").addClass("disabled");
    $("#audio").attr("src", "");
    Fr.voice.stop();
    $("#stop").attr("hidden", "hidden");
    $("#save").attr("hidden", "hidden");
}



function makeWaveform(){
  var analyser = Fr.voice.recorder.analyser;

  var bufferLength = analyser.frequencyBinCount;
  var dataArray = new Uint8Array(bufferLength);

  /**
   * The Waveform canvas
   */
  var WIDTH = 500,
      HEIGHT = 200;

  var canvasCtx = $("#level")[0].getContext("2d");
  canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

  function draw() {
    var drawVisual = requestAnimationFrame(draw);

    analyser.getByteTimeDomainData(dataArray);

    canvasCtx.fillStyle = 'rgb(200, 200, 200)';
    canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
    canvasCtx.lineWidth = 2;
    canvasCtx.strokeStyle = 'rgb(0, 0, 0)';

    canvasCtx.beginPath();

    var sliceWidth = WIDTH * 1.0 / bufferLength;
    var x = 0;
    for(var i = 0; i < bufferLength; i++) {
      var v = dataArray[i] / 128.0;
      var y = v * HEIGHT/2;

      if(i === 0) {
        canvasCtx.moveTo(x, y);
      } else {
        canvasCtx.lineTo(x, y);
      }

      x += sliceWidth;
    }
    canvasCtx.lineTo(WIDTH, HEIGHT/2);
    canvasCtx.stroke();
  };
  draw();
}

$(document).ready(function(){
  $(document).on("click", "#record:not(.disabled)", function(){
   Fr.voice.record($("#live").is(":checked"), function(){
       //$(".recordButton").addClass("disabled");
       debugger;
        $("#record").attr("hidden", "hidden");
        $("#pause").removeAttr("hidden");
       
      //$("#live").addClass("disabled");
      //$(".one").removeClass("disabled");

     // makeWaveform();
   });
  });

  $(document).on("click", "#recordFor5:not(.disabled)", function(){
    Fr.voice.record($("#live").is(":checked"), function(){
      $(".recordButton").addClass("disabled");

      $("#live").addClass("disabled");
      $(".one").removeClass("disabled");

     // makeWaveform();
    });

    Fr.voice.stopRecordingAfter(100000, function(){
      alert("Recording stopped after 5 seconds");
    });
  });

  $(document).on("click", "#pause:not(.disabled)", function(){
    //if($(this).hasClass("resume")){
    //  Fr.voice.resume();
    //  $(this).replaceWith('<a class="button one" id="pause">Pause</a>');
    //}else{
      Fr.voice.pause();
      $("#pause").attr("hidden", "hidden");
      $("#record").removeAttr("hidden");
      $("#stop").removeAttr("hidden");
      $("#save").removeAttr("hidden");

  
     // $(this).replaceWith('<img src="../Images/Icons/mic.png" alt="Alternate Text"  id="record" class="img-circle" style="background-color: #A9C502; float: left;" />');
      Fr.voice.export(function (url) {
          $("#audio").attr("src", url);
          $("#audio")[0].play();
      }, "URL");
  ///  }
  });

  $(document).on("click", "#stop:not(.disabled)", function(){
    restore();
  });

  $(document).on("click", "#play:not(.disabled)", function(){
    if($(this).parent().data("type") === "mp3"){
      Fr.voice.exportMP3(function(url){
        $("#audio").attr("src", url);
        $("#audio")[0].play();
      }, "URL");
    }else{
        Fr.voice.export(function (url) {
            debugger;
        $("#audio").attr("src", url);
        $("#audio")[0].play();
      }, "URL");
    }
    restore();
  });

  $(document).on("click", "#download:not(.disabled)", function(){
    if($(this).parent().data("type") === "mp3"){
      Fr.voice.exportMP3(function(url){
        $("<a href='" + url + "' download='MyRecording.mp3'></a>")[0].click();
      }, "URL");
    }else{
      Fr.voice.export(function(url){
        $("<a href='" + url + "' download='MyRecording.wav'></a>")[0].click();
      }, "URL");
    }
    restore();
  });

  $(document).on("click", "#base64:not(.disabled)", function(){
    if($(this).parent().data("type") === "mp3"){
      Fr.voice.exportMP3(function(url){
        console.log("Here is the base64 URL : " + url);
        alert(url);
                 $("<a href='"+ url +"' target='_blank'></a>")[0].click();
      }, "base64");
      alert(url);
    }else{
      Fr.voice.export(function(url){
        console.log("Here is the base64 URL : " + url);
        alert(url);

        $("<a href='"+ url +"' target='_blank'></a>")[0].click();
      }, "base64");
    }
    restore();
  });

  $(document).on("click", "#save:not(.disabled)", function () {

   
      if ($(this).parent().data("type") === "mp3") {
          Fr.voice.exportMP3(upload, "blob");
      } else {
          Fr.voice.export(upload, "blob");
      }
      function upload(blob) {
          debugger;
          var formData = new FormData();
          formData.append('file', blob);
          var res = confirm("Once You Upload You Can Not Record & Delete Again for This Message !");
          if (res){
              $.ajax({
                  url: "../MKT_Assets/SaveAudio.asmx/Save",
                  type: 'POST',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (data) {
                      $('#basicUsage').html();
                      $('#basicUsage').text('Recording Uploaded !').css("color", "#A9C502");
                      $("#record").addClass("disabled");
                      $("#save").attr("hidden", "hidden");
                      $("#stop").attr("hidden", "hidden");
                      //   $("#audio").attr("src", data);
                      // $("#audio")[0].play();
                  }
              });
            }
    }
   
    //restore();
  });
});
