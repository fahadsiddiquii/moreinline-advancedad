﻿

var checkedEmails = [];
var listId = "";

var assetId = "";
LoadGraphics();
function LoadGraphics() {
    $.ajax({
        type: 'post',
        url: '../MKT_Assets/Send.aspx/LoadAds',
        contentType: "application/json; charset=utf-8",
        data: {},
        datatype: "json",
        success: function (response) {

            var jd = JSON.parse(response.d);
            console.log(jd);
            console.log(jd.Table1[0][1]);
            var cntr = $("#adsContainer").html('');
            for (var i = 0; i < jd.Table1.length; i++) {
                if (jd.Table1[i] !== null) {
                    $('<div class="carouselImage" style="background-size: cover;"><input type="hidden" id="hfCatId" name="name" value="' + jd.Table1[i][33] + '" /><div class="text-center" style="font-weight:600;color: #007f9f;">' + jd.Table1[i][0] + '</div><img id="' + jd.Table1[i][3] + '" data-tab="bright" src="' + jd.Table1[i][1] + '" ></div>').appendTo(cntr)
                }
            }
            LoadStyle();
        },
        error: function (error) {
            console.log(error);
        }
    })
}



$(document).ready(function () {
    $('#loading-image').hide();
    $('#search').hide();
    $('#loading-imageDirect').hide();
    $("#checkAll").on("change", checkAll);
   
    
});

function checkAll() {

    
    $('.chk:checkbox').not('checkAll').prop('checked', this.checked);
    if (!this.checked) {
    }

}

function LoadStyle() {
 
    var $imagesCarousel = $('.carouselOfImages').flickity({
        contain: true,
        autoPlay: false,
        wrapAround: true,
        friction: 0.3
    });
    function resizeCells() {
        var flkty = $imagesCarousel.data('flickity');
        var $current = flkty.selectedIndex
        var $length = flkty.cells.length
        if ($length < '1') {
            $imagesCarousel.flickity('destroy');
        }
      
        $('.carouselOfImages .carouselImage').removeClass("nextToSelected");
        $('.carouselOfImages .carouselImage').eq($current - 1).addClass("nextToSelected");
        if ($current + 1 == $length) {
            var $endCell = "0"
        } else {
            var $endCell = $current + 1
        }
        $('.carouselOfImages .carouselImage').eq($endCell).addClass("nextToSelected");
       
    };
    resizeCells();

    $imagesCarousel.on('scroll.flickity', function () {
        resizeCells();
    });


    var imageSrc = $(".carouselImage.is-selected img").attr('src');
    $(".imageAdds img").attr("src", imageSrc);
    //alert(imageSrc);


    $(".carouselImage img").click(function () {
        var $this = $(this);

        // var imageID = $this.attr('data-tab');
        var imageID = $this.attr('id');
        var imageSrc = $this.attr('src');
        assetId = imageID;
        //  alert(imageID);

        $('.' + imageID).removeClass('hide');
        $('.' + imageID + ' .product-detail-image img').attr('src', imageSrc);
    });

    $('.product-detail-close,.product-detail').on('click', function () {
        $('.product-detail').addClass('hide');
    });

    $('.modal-video').on('hidden.bs.modal', function (e) {
        $('.modal-video iframe').attr('src', $('.modal-video iframe').attr('src'));
    });

    //autoPlayYouTubeModal();

    //function autoPlayYouTubeModal() {
    //    var trigger = $("body").find('[data-the-video]');
    //    trigger.click(function () {
    //        var theModal = $(this).data("target"),
    //            videoSRC = $(this).attr("data-the-video"),
    //            videoSRCauto = videoSRC + "&autoplay=1";
    //        $(theModal + ' iframe').attr('src', videoSRCauto);
    //        $(theModal + ' button.close').click(function () {
    //            $(theModal + ' iframe').attr('src', videoSRC);
    //        });
    //        $('.modal-video').click(function () {
    //            $(theModal + ' iframe').attr('src', videoSRC);
    //        });
    //    });
    //}

    $(window).on('load resize', function () {
        var $window = $(window);
        $('.modal-fill-vert .modal-body > *').height(function () {
            return $window.height() - 60;
        });
    });
}


function LoadContactList() {
    $.ajax({
        type: "POST",
        url: "../MKT_Assets/Send.aspx/GetContacts",
        data: '{}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (r) {
            var ddlContacts = $("[id*=DdlContact]");
            ddlContacts.empty().append('<option selected="selected" value="0">Please select</option>');
            ddlContacts.append('<option " value="2">General List</option>');
            $.each(r.d, function () {
                ddlContacts.append($("<option></option>").val(this['Value']).html(this['Text']));
            });
        }
    });
}

function GetList() {
    
    var id = $('#DdlContact :selected').val();
    var searchtxt = $('#search').val();
    if (id !== "0") {
        listId = id;
        var row = "";
        $.ajax({
            type: "POST",
            url: "../MKT_Assets/Send.aspx//GetDetails",
            data: '{id:"' + id + '",searchtxt:"' + searchtxt + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d !== null && msg.d !== undefined) {
                    $("#checkAll").removeAttr("disabled");
                    $("#checkAll").prop('checked', false);
                    $('#search').hide();
                }
                $('#search').show();
                $("#tbllist tbody").html('');
                $.each(msg.d, function (index, obj) {
                    if (obj.IsSubscribed) {
                        row += "<tr><td> <input type='checkbox' class='chk' style='width:18px; height:18px;' name='checkBox' value=" + obj.Cont_DID + " /></td><td>" + obj.Name + "</td><td>" + obj.Email + "</td><td>" + obj.Title + "</td><td>" + obj.Company + "</td><td>Subscribed</td></tr>";
                    }
                    else {
                        row += "<tr><td> <input type='checkbox' class='chk' disabled style='width:18px; height:18px;' name='checkBox' value=" + obj.Cont_DID + " /></td><td>" + obj.Name + "</td><td>" + obj.Email + "</td><td>" + obj.Title + "</td><td>" + obj.Company + "</td><td>Unsubscribed</td></tr>";

                    }
                });

                $("#tbllist tbody").append(row);
            
            }
        });
    }
    else {
        $("#tbllist tbody").html('');
        $('#search').hide();
        $("#checkAll").prop("disabled", true);
    }
}


$(function () {
    //Assign Click event to Button.
    $("#btnGet").click(function () {
        checkedEmails = [];
        var message = '';
        $("#tbllist input[type=checkbox]:checked").each(function (index) {
            var row = $(this).closest("tr")[0];
            var Contacts = {};
            if (row.cells[3].innerHTML !== "Title") {
                Contacts.Cont_DID = row.cells[0].children[0].value;
                Contacts.name = row.cells[1].innerHTML;
                Contacts.email = row.cells[2].innerHTML;
                Contacts.title = row.cells[3].innerHTML;
                Contacts.company = row.cells[4].innerHTML;
                message += row.cells[2].innerHTML;
                checkedEmails.push(Contacts);
            }
            //checkedEmails.push(row.cells[2].innerHTML);
        });
        if (message === undefined || message === '') {
            $("#semail").prop("disabled", false);
        
            swal('Select Contact!', 'select atleast one contact from list', 'warning')
        }
        else {
            $("#semail").prop("disabled", true);
            $("#contactModal").modal("hide");
        }
        return false;
    });
});

function DirectSend() {

    
    var CategoryId = $("div.carouselImage.is-selected > input").val();
    
    if (CategoryId==="1") {
        swal("","Click on Send With New Information button to send Appointment Ad","info")
        return false;
    }
    Swal.mixin({
        input: 'text',
        confirmButtonText: 'Ok',
        showCancelButton: true,
        progressSteps: ['1'],
        inputValidator: (value) => {
            return !value && 'Subject Text Is Rquired!'
        }
    }).queue([
     {
         title: 'Enter Email Subject'
     }
    ]).then((result) => {
        
        if (result.value) {
            var answers = result.value[0];
            var assetid = $("div.carouselImage.is-selected > img").attr("id");
            if (checkedEmails.length > 0 && assetid !== "") {
                DirectPost(checkedEmails, assetid, answers);
            }
            else {
                swal('Failed!', 'select graphic or contact ', 'error')
            }
        }
      
    })
}

function DirectInfo() {
    $.ajax({
        type: "POST",
        url: "../MKT_Assets/Send.aspx/rdbSavedInfoM",
        data: {},
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
         //   $("#btnDirectSend").attr('disabled', false);

            if (msg.d === "Success") {
             //   checkedEmails = [];
               // listId = "";
                //  swal('Sent!', 'Emails sent successfully', 'success')
            }
            if (msg.d === "LimitExeeds") {
              //  swal('Limit Exeeds', 'Your Emails sending limit exceeded !', 'warning')
            }
            if (msg.d === "Crequired") {
            //    swal('Contect Required', 'Please select contect !', 'info')
            }
            if (msg.d === "error") {
              //  swal('Failed!', 'Sending Failed ', 'error')
            }
        },
          error: function () {
        }
    });
}


function DirectPost(Emails, assetId, answers) {
    
    $('#loading-imageDirect').show();
    $("#btnDirectSend").attr('disabled', true);
    var subject = answers;
    $.ajax({
        type: "POST",
        url: "../MKT_Assets/Send.aspx/DirectSend",
        data: JSON.stringify({ Emails: Emails, AssetId: assetId, ListId: listId, Subject: subject }),
        contentType: "application/json; charset=utf-8",
        success: function (msg) {
            $("#btnDirectSend").attr('disabled', false);
            
            if (msg.d === "Success") {
                checkedEmails = [];
                listId = "";
              //  swal('Sent!', 'Emails sent successfully', 'success')
            }
            if (msg.d === "LimitExeeds") {
                swal('Limit Exeeds', 'Your Emails sending limit exceeded !', 'warning')
            }
            if (msg.d === "Crequired") {
                swal('Contect Required', 'Please select contect !', 'info')
            }
            if (msg.d === "error") {
                swal('Failed!', 'Sending Failed ', 'error')
            }
        },
        complete: function (data) {
            swal({
                title: "Emails Successfully Sent",
                text: "Do you want to continue sending ?",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: '#019C7C',
                confirmButtonText: 'Yes, Stay in this page',
                cancelButtonText: "No, Go to sent emails history !"
            }).then(
               function (isConfirm) {
                   
                   if (isConfirm.value) {
                       location.reload();
                   }
                   else {
                       window.location.href = '/MKT_Assets/AssetsSent.aspx'
                   }
               });
            $('#loading-imageDirect').hide();
            $("#btnDirectSend").attr('disabled', false);
        },
        error: function () {
            $('#loading-imageDirect').hide();
            $("#btnDirectSend").attr('disabled', false);
        }
    });
}


function HideLabel(Text) {
    var seconds = 9;
    setTimeout(function () {
        if (Text == "div1") {
            document.getElementById("<%=div1.ClientID %>").style.display = "none";
        }
    }, seconds * 1000);
}
function ShowAudioOps() {
    

    var ischecked = $('#ChkAddVoice').is(":checked");
    if (ischecked) {
        $("#divaudio").removeAttr('hidden');
    }
    else {
        $("#divaudio").attr('hidden', true);
    }

}
function Showupload() {

    var ischecked = $('#shwupload').prop('checked', 'checked');
    if (ischecked) {
        $("#pnlUploadVoice").removeAttr('hidden');
        $("#PnlRecordVoice").attr('hidden', true);
    }
    else {
        $("#pnlUploadVoice").attr('hidden', true);
    }

}
function HideVoiceOps() {

    var ischecked = $('#addgenric').prop('checked', 'checked');
    if (ischecked) {
        $("#pnlOldVoice").removeAttr('hidden');
        $("#PnlRecordVoice").attr('hidden', true);
        $("#pnlUploadVoice").attr('hidden', true);
    }
    else {

    }

}
function ShowAddvoice() {

    var ischecked = $('#shwadd').prop('checked', 'checked');
    if (ischecked) {
        $("#PnlRecordVoice").removeAttr('hidden');
        $("#pnlUploadVoice").attr('hidden', true);
        $("#pnlOldVoice").attr('hidden', true);
    }
    else {
        $("#PnlRecordVoice").attr('hidden', true);
    }

}
function ShowTimeOps() {
    
    var ischecked = $('#ChkAddReminder').is(":checked");
    if (ischecked) {
        $("#PnlTime").removeAttr('hidden');
    }
    else {
        $("#PnlTime").attr('hidden', true);
    }
}

function SendWithPreview() {

    document.getElementById('<%=Uctest.getClientID()%>').value

}
//function ChecAll(th) {
//    
//    $('.chk:checkbox').not(this).prop('checked', this.checked);
//    if (!this.checked) {
//        //  $("#semail").prop("disabled", false);
//    }
//}
function SendWithMod() {

    var assetid = $("div.carouselImage.is-selected > img").attr("id");
    var Subject = $("#TxtAdSubject").val();
    var ChkAddContact = $('#ChkAddContact').is(":checked");
    var ChkAddDirection = $('#ChkAddDirection').is(":checked");
    var chkAddUrl = $('#chkAddUrl').is(":checked");
    var ChkAddReminder = $('#ChkAddReminder').is(":checked");
    var ChkAddVoice = $('#ChkAddVoice').is(":checked");
    var Addgenric = $('#addgenric').is(":checked");
    var Shwupload = $('#shwupload').is(":checked");
    var Shwadd = $('#shwadd').is(":checked");

    var fileUpload = $("#fuvoice").get(0);
    var files = fileUpload.files;

    var ddlvalue = $('#ContentPlaceHolder1_ddlOldVoice option:selected');
    var ddlOldVoices = ddlvalue.val();

    var DateEv = $("#datepicker").val();
    var Hour = $("#DdlHour :selected").val();
    var Timespan = $("#DdlTT :selected").text();
    var Message = $("#TxtMessage").val();
    if (Subject === "") {
        $("#TxtAdSubject").css('border', '1px solid red');
        return false;
    }
    else {
        $("#TxtAdSubject").css('border', '1px solid green');
    }
    if (ChkAddReminder) {
        
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0');
        var yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;
        if (DateEv === "") {
            swal('Date Required', 'Please select date !', 'info');
            return false;
        }
        else if (DateEv < today) {
            swal('Wrong Date', 'Date must be greater than date today!', 'erorr');
            return false;
        }

        if (Hour === "0") {
            swal('Time Required', 'Please select Time !', 'info');
            return false;
        }

    }

    if (ChkAddVoice) {
        if (Shwadd !== true && Addgenric !== true && Shwupload !== true) {
            swal('Voice Required', 'Please select any one mode of voice !', 'info');
            return false;
        }

        if (Shwupload) {
            console.log(files);
            if (files.length == 0) {

                swal('Voice File Required', 'Please upload voice file !', 'info');
                return false;
            }
        }
        if (Shwadd) {
            var msg = $('#basicUsage').text();
            if (msg !== "Recording Uploaded !") {
                swal('Voice Recording Required', 'Please record voice and click upload button!', 'info');
                return false;
            }

        }

    }


    $('#loading-image').show();
    var AssetInfo = {};
    AssetInfo.ID = assetid;
    AssetInfo.Subject = Subject;
    AssetInfo.Date = DateEv;
    AssetInfo.Hours = Hour;
    AssetInfo.Message = Message;
    AssetInfo.AddAudio = ChkAddVoice;
    AssetInfo.AddContact = ChkAddContact;
    AssetInfo.AddDirection = ChkAddDirection;
    AssetInfo.AddReminder = ChkAddReminder;
    AssetInfo.AddWebUrl = chkAddUrl;
    AssetInfo.TimSpan = Timespan;
    ////////////////////

    if (checkedEmails.length > 0 && assetid !== "") {
        $("#BtnSend").attr('disabled', true);
        $.ajax({
            type: "POST",
            url: "../MKT_Assets/Send.aspx//CustomizedSend",
            data: JSON.stringify({ AssetInfo: AssetInfo, Emails: checkedEmails, ListId: listId, DdlOldVoice: ddlOldVoices }),
            contentType: "application/json; charset=utf-8",
            success: function (msg) {
                $("#BtnSend").attr('disabled', false);
                
                if (msg.d === "Success") {
                    checkedEmails = [];
                    listId = "";
                    //  swal('Sent!', 'Emails sent successfully', 'success')
                   // location.reload();
                }
                if (msg.d === "LimitExeeds") {
                    swal('Limit Exeeds', 'Your Emails sending limit exceeded !', 'warning')
                }
                if (msg.d === "Crequired") {
                    swal('Contect Required', 'Please select contect !', 'info')
                }
                if (msg.d === "error") {
                    swal('Failed!', 'Sending Failed ', 'error')
                    $('#loading-image').hide();
                }
            },
            complete: function (data) {
                swal({
                    title: "Emails Successfully Sent",
                    text: "Do you want to continue sending ?",
                    type: "success",
                    showCancelButton: true,
                    confirmButtonColor: '#019C7C',
                    confirmButtonText: 'Yes, Stay in this page',
                    cancelButtonText: "No, Go to sent emails history !"
                }).then(
                function (isConfirm) {
                
                if (isConfirm.value) {
                    location.reload();
                }
                else {
                    window.location.href = '/MKT_Assets/AssetsSent.aspx'
                }
                });

                $('#loading-image').hide();
                $("#BtnSend").attr('disabled', false);
            },
            error: function () {
                $('#loading-image').hide();
                $("#BtnSend").attr('disabled', false);
            }
        });
    }
    else {
        swal('Failed!', 'select graphic or contact ', 'error')
        $("#BtnSend").attr('disabled', false);
        return false;

    }








}

function Confirm() {
    
    
    swal({
        title: "Do you want leave",
        text: "Do you want to leave without selecting contacts ?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: '#019C7C',
        confirmButtonText: 'Yes, Stay in this window ',
        cancelButtonText: "No!"
    }).then(
          function (isConfirm) {

              if (isConfirm.value) {
                
                  return false;
              }
              else {
                  $("#contactModal .close").click()
              }
          });

}


