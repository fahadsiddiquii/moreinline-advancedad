﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.Reports
{
    public partial class PaymentReport : System.Web.UI.Page
    {
        OrdersDAL ObjOrderDAL = new OrdersDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void BindRepeater()
        {
            DataTable dt = ObjOrderDAL.GetPaymentOrderReport();
            RptPayment.DataSource = dt;
            RptPayment.DataBind();
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void RptPayment_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label RequestType = e.Item.FindControl("LblReqType") as Label;

                switch (RequestType.Text)
                {
                    case "1":
                        RequestType.Text = "New Order";
                        break;
                    case "2":
                        RequestType.Text = "Modification";
                        break;
                }

            }
        }
    }
}