﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Reports
{
    public partial class DeliveredOrders : System.Web.UI.Page
    {
        OrderAssetsDAL ObjOrderAssetsDAL = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRepeater();
            }
        }
        public void BindRepeater()
        {
            DataTable dt = ObjOrderAssetsDAL.GetDeliveredOrdersReport();
            RptPendingOrders.DataSource = dt;
            RptPendingOrders.DataBind();
        }
    }
}