﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="InProcessOrders.aspx.cs" Inherits="MoreInline.Reports.InProcessOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>


    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>In Process Orders Report</b></h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>

            <%--   <div class="col-lg-12 text-right">
                <asp:Button ID="BtnAdd" runat="server" Text="Add New" CssClass="btn btn-info" />
            </div>--%>
        </div>


        <br />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">

                <asp:Repeater ID="RptPendingOrders" runat="server">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">SNo.</th>
                                    <th class="text-center text-black">Order #</th>
                                    <th class="text-center text-black">Ordered By</th>
                                    <th class="text-center text-black">Order Subject</th>
                                    <th class="text-center text-black">Order Date/Time</th>
                                    <th class="text-center text-black">QA</th>
                                    <th class="text-center text-black">Designer</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center text-white" style="width: 5px">
                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CreatedDate") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="LblQA" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QA") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="LblDesigner" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Designer") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>


    <%----------------------------------------- Pop Up -------------------------------------------------------%>
</asp:Content>
