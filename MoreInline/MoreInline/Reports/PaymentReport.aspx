﻿<%@ Page Title="Payment Report" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="PaymentReport.aspx.cs" Inherits="MoreInline.Reports.PaymentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>


    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container-fluid m-l-10 m-r-10" >

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #019c7c;">Orders & Request Modification Report</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>
        </div>

        <br />

        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">

                <asp:Repeater ID="RptPayment" runat="server" OnItemDataBound="RptPayment_ItemDataBound">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">SNo.</th>
                                    <th class="text-center text-black">Customer</th>
                                    <th class="text-center text-black">Payer ID</th>
                                    <th class="text-center text-black">Payment ID</th>
                                    <th class="text-center text-black">Order No</th>
                                    <th class="text-center text-black">Order Date</th>
                                    <th class="text-center text-black">Request Type</th>
                                    <th class="text-center text-black">Asset Order No</th>
                                    <th class="text-center text-black">Created On</th>
                                    <th class="text-center text-black">Amount</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center" style="width: 5px">
                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                <asp:Label ID="LblId" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem,"OID") %>' />
                                <asp:Label ID="LblCustID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem,"CustID") %>' />
                                <asp:Label ID="LblAssetID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem,"AssetID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblCustName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblPayerId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PayerID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblPaymentId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblOrderNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNumber") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblOrderDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderDate") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblReqType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RequestType") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblAssetOrder" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblAssetOrderNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CreateDate") %>' />
                            </td>
                             <td class="text-center">
                                <asp:Label ID="LblAmount" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Amount") %>' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>

    </div>






</asp:Content>
