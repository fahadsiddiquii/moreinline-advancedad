﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="MoreInline.Dashboard.Dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .col-centered {
            float: none;
            margin: 0 auto;
        }

        .carousel-control {
            width: 8%;
            width: 0px;
        }

            .carousel-control.left,
            .carousel-control.right {
                margin-right: 40px;
                margin-left: 32px;
                background-image: none;
                opacity: 1;
            }

            .carousel-control > a > span {
                color: white;
                font-size: 29px !important;
            }

        .carousel-col {
            position: relative;
            min-height: 1px;
            padding: 5px;
            float: left;
        }

        .active > div {
            display: none;
        }

            .active > div:first-child {
                display: block;
            }

        /*xs*/
        @media (max-width: 767px) {
            .carousel-inner .active.left {
                left: -50%;
            }

            .carousel-inner .active.right {
                left: 50%;
            }

            .carousel-inner .next {
                left: 50%;
            }

            .carousel-inner .prev {
                left: -50%;
            }

            .carousel-col {
                width: 50%;
            }

            .active > div:first-child + div {
                display: block;
            }
        }

        /*sm*/
        @media (min-width: 768px) and (max-width: 991px) {
            .carousel-inner .active.left {
                left: -50%;
            }

            .carousel-inner .active.right {
                left: 50%;
            }

            .carousel-inner .next {
                left: 50%;
            }

            .carousel-inner .prev {
                left: -50%;
            }

            .carousel-col {
                width: 50%;
            }

            .active > div:first-child + div {
                display: block;
            }
        }

        /*md*/
        @media (min-width: 992px) and (max-width: 1199px) {
            .carousel-inner .active.left {
                left: -33%;
            }

            .carousel-inner .active.right {
                left: 33%;
            }

            .carousel-inner .next {
                left: 33%;
            }

            .carousel-inner .prev {
                left: -33%;
            }

            .carousel-col {
                width: 33%;
            }

            .active > div:first-child + div {
                display: block;
            }

                .active > div:first-child + div + div {
                    display: block;
                }
        }

        /*lg*/
        @media (min-width: 1200px) {
            .carousel-inner .active.left {
                left: -25%;
            }

            .carousel-inner .active.right {
                left: 25%;
            }

            .carousel-inner .next {
                left: 25%;
            }

            .carousel-inner .prev {
                left: -25%;
            }

            .carousel-col {
                width: 25%;
            }

            .active > div:first-child + div {
                display: block;
            }

                .active > div:first-child + div + div {
                    display: block;
                }

                    .active > div:first-child + div + div + div {
                        display: block;
                    }
        }



        .row {
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -15px;
            margin-left: -15px;
        }

        #fixedbutton {
            position: relative;
            margin-top: 50px;
            bottom: 15px;
            left: 10px;
        }

        .AdSize {
            width: 30%;
        }

        .button {
            background-color: #A9C502;
            border: none;
            color: white;
            padding: 13px 30px 11px 30px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 30px;
        }

            .button:hover {
                background-color: #f1f1f1;
                color: black;
            }

        /*@media (max-width:1199px) {
            #fixedbutton {
                position: absolute;
            }
        }*/

        .upgrade {
            color: #fff;
            text-shadow: 1px 1px 2px black, 0 0 1em blue, 0 0 0.2em darkblue;
        }

        .sideborder {
            border-left: 1px solid #5B5B5B;
        }



        .dashbuttons {
            width: 250px;
        }
    </style>
    <link href="../css/tour.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>


    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>



    <div class="row justify-content-center p-t-10 p-b-5" style="background-color: #A9C502;">

        <div class="col-lg-1"></div>
        <div class="col-lg-3">
            <img src="../Images/Icons/help.png" /><span style="color: #001b00; padding-left: 5px;"><b>Getting Started in DASHBOARD</b></span>

        </div>
        <div class="col-lg-5">
            <p runat="server" id="spwebsite" visible="false" style="color: #001b00; margin-left: 35px; padding-top: 5px;"><b>Want a </b><a href="../websites/DemoWebsite.aspx" class="upgrade">Website?</a></p>
        </div>
        <div id="red" runat="server" class="col-lg-3 text-center">
            <%--<span style="color: #fff;"><b>Trial Version</b></span>--%>
            <b>
                <asp:Label ID="LblVersion" runat="server" Style="color: #001B00;"></asp:Label></b>
            <b>
                <asp:Label ID="LblTrialVersion" runat="server" Style="color: red;"></asp:Label></b>
            <b>
                <asp:Panel ID="PnlUpgrade" runat="server"><a href="Upgrade.aspx" class="upgrade test-10">UPGRADE</a></asp:Panel>
            </b>
        </div>
    </div>
    <div class="row justify-content-center m-t-20">
        <div class="col-lg-12 text-center">
            <h3 style="color: #019c7c;">DASHBOARD</h3>
        </div>
    </div>

    <div class="row p-b-10">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>

    <div class="row justify-content-center">

        <div class="col-lg-3 text-center">
            <div class="dashboardResponsive">
                <h4 style="color: #001B00;">CONTACTS</h4>
                <br />
                <a href="../Contacts/AddNewContact.aspx" class="btn btn-black-green dashbuttons test-1">
                    <img src="../Images/Icons/addContacts.png" class="pull-left"  style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left" >Add New Contacts</span></a>

                <a href="../Contacts/UploadContacts.aspx" class="btn btn-black-green dashbuttons m-t-5 test-2">
                    <img src="../Images/Icons/uploadContacts.png" class="pull-left"  style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">Upload Contacts</span></a>

                <a href="../Contacts/ViewContacts.aspx" class="btn btn-black-green dashbuttons m-t-5 test-3">
                    <img src="../Images/Icons/viewContacts.png" class="pull-left"  style="width: 45px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">View Contacts&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
            </div>
        </div>
        <div class="col-lg-3 text-center sideborder">
            <div class="dashboardResponsive">
                <h4 style="color: #001B00;">MARKETING ASSETS</h4>
                <br />
                <a href="../MKT_Assets/OrderAssets.aspx" class="btn btn-black-green dashbuttons test-4">
                    <img src="../Images/Icons/orderAssets.png" class="pull-left" style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">Order Assets</span></a>

                <a href="../MKT_Assets/ViewAssets.aspx" class="btn btn-black-green dashbuttons m-t-5 test-5" >
                    <img src="../Images/Icons/viewAssets.png" class="pull-left"  style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left" >View Assets</span></a>

                   <a href="../MKT_Assets/Send.aspx" class="btn btn-black-green dashbuttons m-t-5 test-7">
                    <img src="../Images/Icons/assetsSent.png" class="pull-left"  style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">&nbsp;Send Assets </span></a>


                <%--<a href="../MKT_Assets/SendAssets.aspx" class="btn btn-dark-green m-t-5 test-6" style="width: 100%;">
                    <img src="../Images/Icons/sendAssets.png" style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard">Send Assets</span></a>--%>
                <%--<a href="../MKT_Assets/OrderStatus.aspx" class="btn btn-dark-green m-t-5 test-6" style="width: 100%;">
                    <img src="../Images/Icons/sendAssets.png" style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard">Order Status</span></a>--%>
                <a href="../MKT_Assets/AssetsSent.aspx" class="btn btn-black-green dashbuttons m-t-5 test-8">
                    <img src="../Images/Icons/mail.png" class="pull-left"  style="width: 46px; height: 50px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">&nbsp;Assets Sent</span></a>

                 <a href="../MKT_Assets/SentItems.aspx" style="height:64px;" class="btn btn-black-green dashbuttons m-t-5 test-9">
                    <img src="../Images/Icons/appointment.png" class="pull-left"  style="width: 40px; height:42px;" />
                    <span class="font-fix-dashboard p-t-15 pull-left">&nbsp;Appointment Calendar</span></a>
            </div>
        </div>


    </div>
    <br />
    <br />
    <hr />
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-11 col-md-10 col-centered">
                <div class="carousel slide" id="carousel" data-ride="carousel" data-type="multi">
                    <div id="adsContainer" class="carousel-inner">
                    </div>
                    <div class="left carousel-control">
                        <a href="#carousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </div>
                    <div class="right carousel-control">
                        <a href="#carousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <button id="fixedbutton" class="button" onclick="StartWizard();return false"><b>Take Tour</b></button>
    </div>

    


    <%--    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 text-center" style="background-color: #222222;">
            <br />
            <br />
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-11">
                    <p style="color: #A9C502;" class="pull-left"><b>Invite 10 associates and get 3 months free subscription</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-4 form-group-sm p-t-5">
                    <asp:TextBox ID="TxtName" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A; background-color: #000;" placeholder="Name"></asp:TextBox>
                </div>
                <div class="col-lg-4 form-group-sm p-t-5">
                    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A; background-color: #000;" placeholder="Email"></asp:TextBox>
                </div>
                <div class="col-lg-2 p-t-5">
                    <asp:Button ID="BtnInvite" runat="server" Text="Invite" CssClass="btn btn-sm btn-dark-green" Style="width: 100px;" />
                </div>
                <div class="col-lg-1"></div>
            </div>
            <div class="row p-t-5">
                <div class="col-lg-1"></div>
                <div class="col-lg-11">
                    <p style="color: #fff;" class="p-t-5 pull-left">
                        You have invited 
             <asp:Label ID="LblInvitedAccounts" runat="server" Text="0"></asp:Label>
                        out of 
              <asp:Label ID="LblActiveAccounts" runat="server" Text="10"></asp:Label>
                        active accounts
                    </p>
                </div>
            </div>
            <br />

        </div>
        <div class="col-lg-3"></div>
    </div>--%>



    <%----------------------------------------- Page End -------------------------------------------------------%>
    <%----------------------------------------- Pop Up -------------------------------------------------------%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" style="border: 1px solid #A9C502;">
                <div class="modal-body text-center">
                    <h3 style="color: #9F9F9F;">THANK YOU FOR USING</h3>
                    <div class="justify-content-center">
                        <img src="../Images/moreinline_black.png" width="300" class="img-responsive img-circle" />
                    </div>
                    <small style="padding-left: 60px; color: #828282;"><i><b>Better Communication - Better Results</b></i></small>

                    <h2 style="color: #A9C502;">REMINDER</h2>
                    <h3 style="color: #9F9F9F;">Your Free&nbsp;<span>More</span><span style="color: #ABC502;">Inline</span><br />
                        Trial Version will expire in 
                            <asp:Label ID="LblDays" runat="server" Style="color: red;"></asp:Label>.<br />
                        Please upgrade your account.</h3>
                    <%--Your Free Trial Version will expire in " " days. Please upgrade your account.--%>
                    <div class="justify-content-center">
                        <asp:Button ID="BtnClose" runat="server" Text="Close" OnClick="BtnClose_Click" CssClass="btn btn-sm btn-dark-green m-t-25 m-b-30" Style="width: 140px;"></asp:Button>
                        <%--<a href="LoginPage.aspx" class="btn btn-sm btn-dark-green m-t-25 m-b-30" Style="width: 140px;">START FOR FREE</a>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../js/Dashboard.js"></script>
    <script src="../js/tour.js"></script>

    <script>
        function StartWizard() {
            $('body').tour({
                steps: [{
                    title: 'Welcome',
                    content: '<p>Hello! Welcome to Advance Ad</p>'
                }, {
                    title: 'Add New Contacts',
                    content: '<p>You can add new contacts by clicking here.</p>',
                    element: '.test-1',
                    position: 'top'
                }, {
                    title: 'Upload Contacts',
                    content: '<p>You can upload contacts by clicking here.</p>',
                    element: '.test-2'
                }, {
                    title: 'View Contacts',
                    content: '<p>You can view your contacts by clicking here.</p>',
                    element: '.test-3'
                }, {
                    title: 'Order Assets',
                    content: '<p>You can order by clicking here.</p>',
                    element: '.test-4'
                }, {
                    title: 'View Assets',
                    content: '<p>You can view your assets by clicking here.</p>',
                    element: '.test-5'
                }, {
                    title: 'Order Status',
                    content: '<p>You can see status of your orders by clicking here.</p>',
                    element: '.test-6'
                }, {
                    title: 'Assets Sent',
                    content: '<p>You can view your sent assets here.</p>',
                    element: '.test-7'
                }, {
                    title: 'Watch Your Appointment Calendar',
                    content: '<p>You can watch your Appointment Calendar by clicking here.</p>',
                    element: '.test-8'
                }, {
                    title: 'Send Assets To Your Customers',
                    content: '<p>You can send your assets to your customers by clicking here.</p>',
                    element: '.test-9'
                }, {
                   
                     title: 'Upgrade Package',
                    content: '<p>You can upgrade your package by clicking here.</p>',
                    element: '.test-10'
                }, {
                    title: 'Add Voice',
                    content: '<p>If you record your voice and want to send it then check this checkbox.</p>',
                    element: '.test-11'
                }, {
                    title: 'Add Contact',
                    content: '<p>If you write contact number and want to send it then check this checkbox.</p>',
                    element: '.test-12'
                }, {
                    title: 'Add Location',
                    content: '<p>If you write location and want to send it then check this checkbox.</p>',
                    element: '.test-13'
                }, {
                    title: 'Add Date',
                    content: '<p>If you write date and want to send it then check this checkbox.</p>',
                    element: '.test-14'
                }, {
                    title: 'Add Website',
                    content: '<p>If you write website and want to send it then check this checkbox.</p>',
                    element: '.test-15'
                }]
            });

            $('body').data('tour').start();

            $('body').on('tourend', function () {
                EndTour();
            });

            $('body').on('stepchange', function (e, previousStep, nextStep) {
                console.log(previousStep, nextStep);
            });
        }
    </script>
    <script>
       function EndTour() {
            $.ajax({
                type: 'post',
                url: '../Dashboard/Dashboard.aspx/EndTour',
                contentType: "application/json; charset=utf-8",
                data: {},
                datatype: "json",
                success: function (response) {

                },
                error: function (error) {
                    console.log(error);
                }
            })
        }
    </script>

    <script>
        //$('.carousel[data-type="multi"] .item').each(function () {
        //    var next = $(this).next();
        //    if (!next.length) {
        //        next = $(this).siblings(':first');
        //    }
        //    next.children(':first-child').clone().appendTo($(this));

        //    for (var i = 0; i < 2; i++) {
        //        next = next.next();
        //        if (!next.length) {
        //            next = $(this).siblings(':first');
        //        }

        //        next.children(':first-child').clone().appendTo($(this));
        //    }
        //});
    </script>



    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36251023-1']);
        _gaq.push(['_setDomainName', 'jqueryscript.net']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</asp:Content>
