﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using MoreInline.DAL;
using MoreInline.BOL;

namespace MoreInline.Dashboard
{
    public partial class Upgrade : System.Web.UI.Page
    {
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = ObjCustomerProfileMasterDAL.GetExpiredCustomers();
            int custID = int.Parse(Session["Cust_ID"].ToString());
            DataTable CustData = OrderAsset.GetCustDetail(custID).Tables[0];
            if (dt.Rows.Count < 1)
            {
               
                int pkg = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
                switch (pkg)
                {
                    case 2:
                        PrmDiv.Visible = true;
                        RegDiv.Visible = true;
                        AplvDiv.Visible = false;
                        break;
                    case 3:
                        RegDiv.Visible = false;
                        PrmDiv.Visible = true;
                        AplvDiv.Visible = true;
                        break;
                    case 4:
                        PrmDiv.Visible = false;
                        RegDiv.Visible = true;
                        AplvDiv.Visible = true;
                        break;
                    default:
                        break;
                }
            }
        }

        //protected void BtnAddCart199_Click(object sender, EventArgs e)
        //{
        //    if (Session["AccTypeID"]!= null)
        //    {
        //        string AccountType = Session["AccTypeID"].ToString();
        //        Response.Redirect("~/Plans/RegularPackage.aspx?AccType="+ AccountType, false);
        //    }
         
        //}
    }
}