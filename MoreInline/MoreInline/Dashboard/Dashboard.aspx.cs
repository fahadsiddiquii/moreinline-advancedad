﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.Dashboard
{
    public partial class Dashboard : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                div1.Visible = false;
                if (!IsPostBack)
                {
                    LabelUpgradeHide();
                    AccountName();
                    RemainingDays();
                    DataTable dt = ObjCustomerProfileMasterDAL.GetWebsiteData(int.Parse(Session["Cust_ID"].ToString()));

                    if (dt.Rows.Count < 1)
                        spwebsite.Visible = false;
                    else
                        spwebsite.Visible = true;


                    if (Session["DashBoardTour"].ToString() == "False")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "StartWizard();", true);
                    }
                }

            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void LabelUpgradeHide()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
            if (dt.Rows[0]["AccTypeID"].ToString() == "6")
            {
                PnlUpgrade.Visible = false;
            }
            else
            {
                PnlUpgrade.Visible = true;
            }
        }

        public void AccountName()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            LblVersion.Text = ObjCustomerProfileMasterDAL.GetAccountType(ObjCustomerProfileMasterBOL);
        }

        public void RemainingDays()
        {
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetExistActiveCustomerAccount(ObjCustomerProfileMasterBOL);

            DateTime ValidDate = DateTime.Parse(dt.Rows[0]["ValidDate"].ToString());
            DateTime Now = DateTime.Now;
            int Days;
            string Time = " Days";
            TimeSpan Test = TimeSpan.Parse("00:00:00.00");

            TimeSpan RemainingDays = ValidDate.Subtract(Now);


            Days = RemainingDays.Days;

            if (RemainingDays > Test && Days == 0)
            {
                if (RemainingDays.Hours > 0)
                {
                    Days = RemainingDays.Hours;
                    Time = " Hours";
                }
                else
                {
                    Time = " Few Minutes";
                }
            }

            LblTrialVersion.Text = Days.ToString() + Time + " Left";

            if (Days <= 3 && Days > 0)
            {
                LblDays.Text = Days.ToString() + Time;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }

            else if (Time == " Hours")
            {
                LblDays.Text = Days.ToString() + Time;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            else if (Days <= 0)
            {
                Response.Redirect(@"~\Dashboard\Upgrade.aspx");
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                red.Attributes["style"] = "border:1px solid Red;";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "HidePopup", "$('#myModal').modal('hide')", true);
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }



        [WebMethod]
        public static List<NotificationModel> GetNotifications()
        {

            NotificationModel Notiofication = new NotificationModel();
            int uid = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
            int UTID = -1;
            List<NotificationModel> List = Notiofication.GetData(uid, UTID);
            return List;
        }

        [WebMethod]
        public static string Notify()
        {
            NotificationModel Notiofication = new NotificationModel();
            int uid = int.Parse(HttpContext.Current.Session["Cust_ID"].ToString());
            int UTID = -1;
            string result = Notiofication.Notify(uid, UTID);
            return result;
        }


        [WebMethod]
        public static void EndTour()
        {
            if (HttpContext.Current.Session["DashBoardTour"].ToString() == "False")
            {
                CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();

                if (ObjCustomerProfileMasterDAL.UpdateCustomerTour("DashBoard", int.Parse(HttpContext.Current.Session["Cust_ID"].ToString())))
                {
                    HttpContext.Current.Session["DashBoardTour"] = "True";
                }
            }
        }

        [WebMethod]
        public static string[] LoadAds()
        {
            AdditionalAdDAL ObjAdDal = new AdditionalAdDAL();
            DataTable dt = ObjAdDal.GetAds();
            string[] arry = new string[dt.Rows.Count];
            int i = 0;
            foreach (DataRow item in dt.Rows)
            {
                string path = item["ImageUrl"].ToString().Replace("~","..");
                arry[i] = path;
                i++;
            }
            return arry;
        }
    }
}