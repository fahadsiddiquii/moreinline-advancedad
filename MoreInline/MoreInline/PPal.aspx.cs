﻿using MoreInline.BOL;
using MoreInline.DAL;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class PPal : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        DesignBOL DBOL = new DesignBOL();
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();
        AdvertisementDAL objAdvertisement = new AdvertisementDAL();
        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();
        SqlTransaction Trans;
        SqlConnection Conn;
        bool isCancel = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            APIContext apiContext = PaypalConfiguration.GetAPIContext();
            try
            {
                if (Request.QueryString["Cancel"] != null)
                {
                    if (Request.QueryString["Cancel"].ToString().Equals("true"))
                    {
                        isCancel = true; 
                    }
                }
                string payerId = Request.Params["PayerID"];
                if (string.IsNullOrEmpty(payerId))
                {
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/PPal.aspx?";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                   Response.Redirect(paypalRedirectUrl,false);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                    }
                    else
                    {
                        string invoicenum = OrderAsset.GetInvoiceID();
                        TransactionBOL Transaction = new TransactionBOL();
                        Transaction.Amount = executedPayment.transactions.FirstOrDefault().amount.total.ToString();
                        Transaction.Cust_Id = Session["Cust_ID"] == null ? -1 : int.Parse(Session["Cust_ID"].ToString());
                        Transaction.GUID = guid;
                        Transaction.InvoiceNumber = invoicenum;
                        Transaction.IsActive = true;
                        Transaction.Item = executedPayment.transactions.FirstOrDefault().item_list.items.FirstOrDefault().name;
                        Transaction.PayerID = executedPayment.payer.payer_info.payer_id;
                        Transaction.PaymentId = executedPayment.transactions.FirstOrDefault().payee.merchant_id;
                        Transaction.Token = executedPayment.token;
                        Transaction.TransactionDate = DateTime.Now;
                        InsertTransaction(Transaction);
                        Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 1, false);
                    }
                }
            }
            catch (Exception )
            {
                if (isCancel)
                {
                    
                    Response.Redirect("~/Dashboard/Upgrade.aspx");
                }
                else
                {
                    Response.Redirect("~/MKT_Assets/Info.aspx?IsSuccess=" + 0);
                }

            }
        }

        private bool PaymentEntry(string selectedValue,string limit,double amount,int AcType)
        {
            Conn = DBHelper.GetConnection();
            ObjCustomerProfileMasterDAL.UpdateAccountType(int.Parse(Session["Cust_ID"].ToString()), AcType, Conn);
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetValidDaysDate(ObjCustomerProfileMasterBOL);
            int Remainingdays = GetRemainingDays(dt.Rows[0]["ValidDate"].ToString());
            int validationDays = (int.Parse(selectedValue) * 30) + Remainingdays;
            DateTime Validdate = DateTime.Parse(dt.Rows[0]["ValidDate"].ToString()).AddDays((int.Parse(selectedValue) * 30));
            ObjCodeMasterBOL.ValidationDays = validationDays.ToString();
            ObjCodeMasterBOL.ValidDate = Validdate;

         
            Trans = Conn.BeginTransaction();
            ObjCodeMasterBOL.CodeID = ObjCustomerProfileMasterDAL.GetCodeNo(ObjCustomerProfileMasterBOL);
           
            if (ObjCodeMasterDAL.UpdateDateDays(ObjCodeMasterBOL, Trans, Conn))
            {
                ObjPaymentBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
                ObjPaymentBOL.Amount = amount;

                if (ObjPaymentDAL.InsertPaymentDetail(ObjPaymentBOL, Trans, Conn))
                {
                    int EmailLimit = int.Parse(limit);
                    if (objAdvertisement.UpdateEmailsLimit(ObjPaymentBOL.Cust_ID, EmailLimit))
                    {
                      
                        Trans.Commit();
                        Conn.Close();
                        SendEmailNotification(ObjPaymentBOL.Cust_ID);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    //MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    return false;
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                //MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                return false;
            }
        }

        public int GetRemainingDays(string date)
        {
            DateTime ValidDate = DateTime.Parse(date);
            DateTime Now = DateTime.Now;
            TimeSpan RemainingDays = ValidDate.Subtract(Now);
            int Days = RemainingDays.Days;
            return Days;
        }
        private PayPal.Api.Payment payment;
        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };
            this.payment = new Payment()
            {
                id = paymentId
            };
            return this.payment.Execute(apiContext, paymentExecution);
        }
        private Payment CreatePayment(APIContext apiContext, string redirectUrl)
        {
            //create itemlist and add item objects to it  
            var itemList = new ItemList()
            {
                items = new List<Item>()
            };
            string ProductName = Request.QueryString["Product"] == null ? "" : Request.QueryString["Product"].ToString();
            string ProductAmount = Request.QueryString["Amount"] == null ? "" : Request.QueryString["Amount"].ToString();
            Session["Limit"] = Request.QueryString["Limit"] == null ? "" : Request.QueryString["Limit"].ToString();
            Session["ddl"] = Request.QueryString["ddl"] == null ? "" : Request.QueryString["ddl"].ToString();
            Session["AcType"] = Request.QueryString["AcType"] == null ? "" : Request.QueryString["AcType"].ToString();
            Session["Amount"] = ProductAmount;
            int ddlitm = int.Parse(Request.QueryString["ddl"].ToString());
           // string qty = (ddlitm*30).ToString();
            itemList.items.Add(new Item()
            {
                name = ProductName,
                currency = "USD",
                price = ProductAmount,
                quantity = "1",
                sku = "N/A"
            });
            var payer = new Payer()
            {
                payment_method = "paypal"
            };
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = ProductAmount
            };
            var amount = new Amount()
            {
                currency = "USD",
                total = ProductAmount, // Total must be equal to sum of tax, shipping and subtotal.  
                details = details
            };
            var transactionList = new List<Transaction>();
            string invoicenum = OrderAsset.GetInvoiceID();
            transactionList.Add(new Transaction()
            {
                description = "Transaction description",
                invoice_number = Convert.ToString((new Random()).Next(100000)), //invoicenum, //Generate an Invoice No  
                amount = amount,
                item_list = itemList
            });
            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };
            // Create a payment using a APIContext  
            return this.payment.Create(apiContext);
        }
        private void SendEmailNotification(int custId)
        {
            DataTable CustData = OrderAsset.GetCustDetail(custId).Tables[0];
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            EmailNotification ENotifc = EN.GetAdminFilledObject();
            ENotifc.EmailSubject = "Package Upgrade ";
            ENotifc.isButtonVisible = "false";
            ENotifc.Content = CustData.Rows[0]["FirstName"].ToString() + " has Successfully Upgraded to "+ CustData.Rows[0]["AccType"].ToString() + " Package.";
            EmailNotification.SendNotification(ENotifc);
            #endregion
            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = custId;
            if (CustData.Rows[0]["IsPackageConfirmNActive"].ToString().Equals("True"))
            {
                string PELimit = Session["Plimit"].ToString();
                string PPlan = Session["AT"].ToString();
                //EmailsLimit AccType
                string CELimit = CustData.Rows[0]["EmailsLimit"].ToString();
                string CPlan = CustData.Rows[0]["AccType"].ToString();
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.ButtonText = "";
                EmailData.CurrentPkg = CPlan;
                EmailData.CurrentLimit = CELimit;
                EmailData.PreviousPkg = PPlan;
                EmailData.PeriviousLimit = PELimit;
                EmailData.IsOrderNotif = "1";
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.NotificationType = "IsPackageConfirmNActive";
                EmailData.Content = "";
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = "Your MOREINLINE Subscription plan is upgraded.";
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData);
                

            }
            #endregion
        }

        private bool InsertData(OrderAssetsBOL Assets, SqlTransaction Trans, SqlConnection Conn, List<OrderAssetsDetailBOL> AllFilePaths)
        {
            if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
            {

                int count = 0;
                foreach (OrderAssetsDetailBOL item in AllFilePaths)
                {
                    item.AssestID = Assets.ID;
                    if (OrderAsset.InsertOrderAssestsDetail(item, Trans, Conn))
                    {
                        count++;
                    }

                }
                Trans.Commit();
                Conn.Close();
                return true;
            }
            else
            {
                try
                {
                    foreach (OrderAssetsDetailBOL item in AllFilePaths)
                    {
                        string Directory = Server.MapPath(item.FilePath);
                        FileInfo fileInfo = new FileInfo(Directory);
                        if (fileInfo.Exists)
                        {
                            fileInfo.Delete();
                        }
                    }
                }
                catch (Exception)
                {
                    Trans.Rollback();
                    Conn.Close();
                }
                Trans.Rollback();
                Conn.Close();
                return false;
            }
        }

        public void InsertTransaction(TransactionBOL Transaction)
        {
            {

            
                string limit = Session["Limit"].ToString();
                double amount = double.Parse(Session["Amount"].ToString());
                string ddl = Session["ddl"].ToString();
                int AcType = int.Parse(Session["AcType"].ToString());
                int custID = int.Parse(Session["Cust_ID"].ToString());
                DataTable CustData = OrderAsset.GetCustDetail(custID).Tables[0];
                DataTable dtCustActive = ObjCustomerProfileMasterDAL.GetViewAddsByUserEmail(CustData.Rows[0]["Email"].ToString());
                int CurrentType = int.Parse(CustData.Rows[0]["AccTypeID"].ToString());
                Session["AT"] = CustData.Rows[0]["AccType"].ToString();
                Session["Plimit"] = CustData.Rows[0]["EmailsLimit"].ToString();
                SqlTransaction Trans;
                SqlConnection Conn;
                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();
                List<OrderAssetsDetailBOL> AllFilePaths = new List<OrderAssetsDetailBOL>();
                OrderAssetsBOL Assets = new OrderAssetsBOL();
                if (OrderAsset.InsertTransaction(Transaction))
                {
                    Assets.Subject = string.Empty;
                    Assets.Phone = string.Empty;
                    Assets.Email = string.Empty;
                    Assets.Location = string.Empty;
                    Assets.WebUrl = string.Empty;
                    Assets.Date = null;
                    Assets.Hours = 0;
                    Assets.TimSpan = string.Empty;
                    Assets.Message = "";
                    Assets.Date = DateTime.Now;
                    Assets.AddAudio = false;
                    Assets.AddContact = false;
                    Assets.AddDirection = false;
                    Assets.AddReminder = false;
                    Assets.AddWebUrl = false;
                    Assets.AddUerProfile = false;
                    Assets.AssestFilePath = string.Empty;
                    Assets.CreatedByID = int.Parse(Session["Cust_ID"].ToString());
                    Assets.ApprovalStatus = 1;
                    Assets.Category = 3;

                    //////

                    int UserID = 1;
                    DBOL.CreatedByID = UserID;
                    DBOL.QAID = 1;
                    DBOL.QaApproval = 1;
                    DBOL.MngrApproval = 1;
                    DBOL.CustApproval = 0;
                    DBOL.QaRemarks = "Approved";
                    DBOL.MngrRemarks = "Approved";
                    DBOL.CustRemarks = "";
                    if (CurrentType == 1 || CurrentType == 5 || CurrentType == 2) ///1 is free & 5 is friend both can upgrade
                    {
                        if (AcType == 3)
                        {
                            if (InsertData(Assets, Trans, Conn, AllFilePaths))
                            {
                                DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds2"].ToString();
                                DBOL.AssestID = Assets.ID;
                                if (Order.InsertOrderdDesign(DBOL))
                                {

                                }
                            }

                        }
                        else if (AcType == 4)
                        {
                            if (InsertData(Assets, Trans, Conn, AllFilePaths))
                            {

                                DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds2"].ToString();
                                DBOL.AssestID = Assets.ID;
                                if (Order.InsertOrderdDesign(DBOL))
                                {

                                }
                            }
                            Conn = DBHelper.GetConnection();
                            Trans = Conn.BeginTransaction();
                            Assets.ID = 0;
                            if (InsertData(Assets, Trans, Conn, AllFilePaths))
                            {

                                DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds4"].ToString();
                                DBOL.AssestID = Assets.ID;
                                if (Order.InsertOrderdDesign(DBOL))
                                {

                                }
                            }
                        }

                    }
                    else if (CurrentType == 3)
                    {
                        if (AcType == 4)
                        {
                            if (InsertData(Assets, Trans, Conn, AllFilePaths))
                            {

                                DBOL.FilePath = "~/OrderDesigned/" + dtCustActive.Rows[0]["CustAds4"].ToString();
                                DBOL.AssestID = Assets.ID;
                                if (Order.InsertOrderdDesign(DBOL))
                                {

                                }
                            }
                        }
                    }

                    PaymentEntry(ddl, limit, amount, AcType);
                  
                }
            }
        }
    }
}