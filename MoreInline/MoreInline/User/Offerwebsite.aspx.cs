﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class Offerwebsite : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        CustomerProfileMasterDAL Customer = new CustomerProfileMasterDAL();
        WebsiteDAL WebsiteAL = new WebsiteDAL();
        WebsiteBOL Website = new WebsiteBOL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    GetCustomers();
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
            }
        }
        private void GetCustomers()
        {
            DataTable dt = Customer.GetCustForWeb();
            ddlCustomers.DataTextField = "Company";
            ddlCustomers.DataValueField = "Cust_ID";
            ddlCustomers.DataSource = dt;
            ddlCustomers.DataBind();
            ddlCustomers.Items.Insert(0, new ListItem("Select Customer", "0"));
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {

            try
            {
                #region Validations
                if (!FuImage.HasFile)
                {
                    MessageBox(div1, LblMsg, "Select Banner Image", 0);
                    return;
                }
                if (!FuLogo.HasFile)
                {
                    MessageBox(div1, LblMsg, "Select Logo Image", 0);
                    return;
                }
                //if (!FuTemplate.HasFiles)
                //{
                //    MessageBox(div1, LblMsg, "Select Website Files Select All File", 0);
                //    return;
                //}
                //if (string.IsNullOrWhiteSpace(txtIntrotext.Text))
                //{
                //    MessageBox(div1, LblMsg, "Enter Introduction Text", 0);
                //    return;
                //}
                //if (string.IsNullOrWhiteSpace(txtUrl.Text))
                //{
                //    MessageBox(div1, LblMsg, "Enter URL Page Name eg:Index.aspx", 0);
                //    return;
                //}
                if ((int.Parse(ddlCustomers.SelectedValue)) <= 0)
                {
                    MessageBox(div1, LblMsg, "Select Customer", 0);
                    return;
                }
                #endregion
                #region Variables Assignment
                int id = int.Parse(ddlCustomers.SelectedValue);
                string CompanyName = Regex.Replace(ddlCustomers.SelectedItem.Text, @"\s+", String.Empty);
                //string IndexPage = Uploader(FuTemplate, CompanyName);
                string BannerImage = Uploader(FuImage, CompanyName);
                string LogoPath = Uploader(FuLogo, CompanyName);
                string Directory = "/Website/" + CompanyName + "/";
                LogoPath = Directory + FuLogo.FileName;
                BannerImage = Directory + FuImage.FileName;
                //string url = application_path + Directory + IndexPage;
                //string IntroText = txtIntrotext.Text;
                #endregion

                Website.Cust_ID = id;
                Website.logoPath = LogoPath;
                Website.BannerPath = BannerImage;
                //Website.WebURL = url;
                //Website.BannerText = IntroText;
                if (WebsiteAL.InsertCustomerWebsite(Website))
                {
                    MessageBox(div1, LblMsg, "Website Created Successfully", 1);
                    //txtIntrotext.Text = string.Empty;
                    //txtUrl.Text = string.Empty;
                    ddlCustomers.SelectedIndex = 0;
                }
                else
                {
                    MessageBox(div1, LblMsg, "Error while insertion ", 0);
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }
        private string Uploader(FileUpload AddFile, string CompanyName)
        {

            string IndexFile = string.Empty;
            string strFolder = Server.MapPath("~/Website/" + CompanyName);
            if (!Directory.Exists(strFolder))
            {
                Directory.CreateDirectory(strFolder);
            }
            foreach (var file in AddFile.PostedFiles)
            {
                string strFileName = "";
                string strFilePath = "";
                strFileName = file.FileName;
                strFileName = Path.GetFileName(strFileName);
                strFilePath = strFolder + "\\" + strFileName;
                file.SaveAs(strFilePath);
                if (strFileName.Contains(".aspx") && (!strFileName.Contains(".aspx.cs") && !strFileName.Contains(".aspx.designer.cs")))
                    IndexFile = strFileName;
            }

            return IndexFile;
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div1.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
    }
}