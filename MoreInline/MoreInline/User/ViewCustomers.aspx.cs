﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class ViewCustomers : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        AdvertisementBOL ObjAdvertisementBOL = new AdvertisementBOL();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;

                if (!IsPostBack)
                {
                    if (PnlActive.Visible)
                    {
                        LblName.Text = "Active Customers";
                        BindActive();
                    }

                }

                //TabName.Value = Request.Form[TabName.UniqueID];
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        public void BindActive()
        {
            DataTable dt = ObjCustomerProfileMasterDAL.GetViewCustomers();

            RptViewCustomers.DataSource = dt;
            RptViewCustomers.DataBind();

        }

        public void BindPending()
        {
            DataTable dtAds = ObjCustomerProfileMasterDAL.GetViewPendingCustomers(ObjCustomerProfileMasterBOL);
            DataTable dtCustActive = ObjCustomerProfileMasterDAL.GetViewCustomers();

            if (dtCustActive.Rows.Count > 0)
            {
                for (int j = 0; j < dtCustActive.Rows.Count; j++)
                {
                    for (int i = 0; i < dtAds.Rows.Count; i++)
                    {
                        if (dtAds.Rows[i]["CustomerEmail"].ToString() == dtCustActive.Rows[j]["Email"].ToString())
                        {
                            dtAds.Rows[i].Delete();
                            dtAds.AcceptChanges();
                            i= dtAds.Rows.Count;
                        }
                    }
                }
            }
            RptViewPendingCustomers.DataSource = dtAds;
            RptViewPendingCustomers.DataBind();
        }
        public void BindExpire()
        {
            //DataTable dt = ObjCustomerProfileMasterDAL.GetViewCodeExpireCustomers(ObjCustomerProfileMasterBOL);
            DataTable dt = ObjCustomerProfileMasterDAL.GetExpiredCustomers();
            RptExpire.DataSource = dt;
            RptExpire.DataBind();
        }
        protected void RptViewCustomers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int CustID = int.Parse(e.CommandArgument.ToString());
            int UserID = 0;
            if (Session["UserID"] != null)
            {
                UserID = int.Parse(Session["UserID"].ToString());
            }
            else
            {
                Response.Redirect("~LoginPage.aspx", false);
            }

            //if (e.CommandName == "ViewEye")
            //{
            //    DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerOrderDetails(int.Parse(e.CommandArgument.ToString()));

            //    RptOrderDetails.DataSource = dt;
            //    RptOrderDetails.DataBind();

            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "OrderDetailsModal();", true);
            //}
            switch (e.CommandName)
            {
                case "Active":
                    if (UpdateCustomerActivation(CustID, 0, UserID))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the customer InActive", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating customer", 0);
                    }
                    BindActive();
                    break;
                case "InActive":
                    if (UpdateCustomerActivation(CustID, 1, UserID))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the customer Active", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating customer", 0);
                    }
                    BindActive();
                    break;
                case "details":
                    Response.Redirect("~/User/CustomerDetails.aspx?CustID=" + CustID, false);
                    break;
            }
        }
        protected void RptViewPendingCustomers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int AdsID = int.Parse(e.CommandArgument.ToString());
                Label LblAppointmentAds = e.Item.FindControl("LblAppointmentAds") as Label;
                Label LblLockedAppointmentAds = e.Item.FindControl("LblLockedAppointmentAds") as Label;
                Label LblFullCode = e.Item.FindControl("LblFullCode") as Label;
                Label LblName = e.Item.FindControl("LblName") as Label;
                Label LblEmail = e.Item.FindControl("LblEmail") as Label;

                switch (e.CommandName)
                {
                    case "ViewUnLocked":
                        ImgUnLocked.ImageUrl = "~/upload/" + LblAppointmentAds.Text;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        break;

                    case "ViewLocked":
                        ImgLocked.ImageUrl = "~/upload/" + LblLockedAppointmentAds.Text;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "LockedModal();", true);
                        break;

                    case "DeleteCust":
                        DeletePendingCustomer(AdsID);
                        BindPending();
                        break;
                    case "EditCust":
                        TxtCompanyName.Text = LblName.Text;
                        TxtCustomerEmail.Text = LblEmail.Text;
                        ViewState["AdsID"] = AdsID;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "EditCustomerModal();", true);
                        break;
                }
                //if (e.CommandName == "ViewUnLocked")
                //{
                //    ImgUnLocked.ImageUrl = "~/upload/" + LblAppointmentAds.Text;
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                //}
                //else if (e.CommandName == "ViewLocked")
                //{
                //    ImgLocked.ImageUrl = "~/upload/" + LblLockedAppointmentAds.Text;
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "LockedModal();", true);
                //}
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        protected void RptExpire_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int CustID = int.Parse(e.CommandArgument.ToString());
            int UserID = int.Parse(Session["UserID"].ToString());
            switch (e.CommandName)
            {
                case "Active":
                    if (UpdateCustomerActivation(CustID, 0, UserID))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the customer InActive", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating customer", 0);
                    }
                    BindExpire();
                    break;
                case "InActive":
                    if (UpdateCustomerActivation(CustID, 1, UserID))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the customer Active", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating customer", 0);
                    }
                    BindExpire();
                    break;
            }

        }
        //protected void BtnNext_Click(object sender, EventArgs e)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "MessageModal();", true);
        //}
        protected void BtnSendCode_Click(object sender, EventArgs e)
        {
            try
            {
                int Checked = 0;
                foreach (RepeaterItem item in RptViewPendingCustomers.Items)
                {
                    CheckBox ChkRow = (CheckBox)item.FindControl("ChkRow");
                    if (ChkRow.Checked == true)
                    {
                        Label LblFullCode = (Label)item.FindControl("LblFullCode");
                        Label LblEmail = (Label)item.FindControl("LblEmail");
                        Label LblEmailStatus = (Label)item.FindControl("LblEmailStatus");
                        Label LblLockedAppointmentAds = (Label)item.FindControl("LblLockedAppointmentAds");
                        Label LblAdsID = (Label)item.FindControl("LblAdsID");
                        Label LblCodeID = (Label)item.FindControl("LblCodeID");
                        Checked++;
                        SendConfirmationAccountEmail(LblAdsID.Text,LblFullCode.Text, LblCodeID.Text, LblEmail.Text, application_path + "upload/" + LblLockedAppointmentAds.Text, TxtMessage.Text);
                        ObjAdvertisementBOL.AdsID = int.Parse(LblAdsID.Text);
                        ObjAdvertisementDAL.UpdateAdvertisementStatus(ObjAdvertisementBOL);
                    }
                }

                if (Checked == 0)
                {
                    MessageBox(div1, LblMsg, "Select at least one to send", 0);
                }
                else
                {
                    BindPending();
                    MessageBox(div1, LblMsg, "Email has been sent successfully", 1);
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void SendConfirmationAccountEmail(string AdsID,string FullCode, string CodeID, string Email, string LockedAppointmentAds, string Message)
        {
            #region EmailSignUpFree

            string link = application_path + "MKT_Assets/EmailFishing.aspx?CodeID=" + CodeID + "&Email=" + Email + "&FullCode=" + FullCode;
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/EmailSendAds.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{link}", link);//Link
            body = body.Replace("{LockedAppointmentAds}", LockedAppointmentAds);//AppointmentAds
            body = body.Replace("{message}", Message);
            body = body.Replace("{unsubscribe}", application_path+"Emails/Unsubscribe.aspx?AdsID=" +AdsID);

            //body = body.Replace("{Code}", FullCode);//UserName
            //body = body.Replace("{LockedImg}", application_path + "Images/Locked.png");//AppointmentAds
            //body = body.Replace("{HalfLockedImg}", application_path + "Images/HalfLockedImg.png");//HalfLockedImg

            #endregion

            //MailMessage mm = new MailMessage("donotreply@moreinline.com", Email);
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("donotreply@moreinline.com", "MoreInline Invitation");
            mm.To.Add(Email);
            mm.Subject = "Appointment Ads";
            mm.Body = body;
            mm.IsBodyHtml = true;
           
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }

        private void DeletePendingCustomer(int AdsID)
        {
            try
            {
                DataTable dt = ObjCustomerProfileMasterDAL.GetDeletingPendingCustomer(AdsID);
                int CodeID = int.Parse(dt.Rows[0]["CodeID"].ToString());
                ObjCustomerProfileMasterDAL.DeleteCode(CodeID);

                ObjCustomerProfileMasterDAL.DeleteAd(AdsID);
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }

        }

        private bool UpdateCustomerActivation(int Cust_ID, int IsActive, int UpdatedBy)
        {
            try
            {
                ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
                ObjCustomerProfileMasterBOL.IsActive = IsActive;
                ObjCustomerProfileMasterBOL.UpdatedBy = UpdatedBy;
                if (ObjCustomerProfileMasterDAL.UpdateCustomerActivation(ObjCustomerProfileMasterBOL))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
                return false;
            }

        }

        private bool UpdateCustomer(int AdsID)
        {
            try
            {
                ObjAdvertisementBOL.AdsID = AdsID;
                ObjAdvertisementBOL.CompanyName = TxtCompanyName.Text;
                ObjAdvertisementBOL.CustomerEmail = TxtCustomerEmail.Text;
                if (ObjAdvertisementDAL.UpdateCustomer(ObjAdvertisementBOL))
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
                return false;
            }
        }

        protected void btnEditCust_Click(object sender, EventArgs e)
        {
            try
            {
                int AdsID = int.Parse(ViewState["AdsID"].ToString());
                if (UpdateCustomer(AdsID))
                {
                    BindPending();
                    MessageBox(div1, LblMsg, "Customer Updated Successfully", 1);
                }
                else
                {
                    BindPending();
                    MessageBox(div1, LblMsg, "Unable to update customer", 0);
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void RptViewCustomers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblIsActive = item.FindControl("LblIsActive") as Label;
                    ImageButton IBtnCheck = item.FindControl("IBtnCheck") as ImageButton;
                    ImageButton IBtnCross = item.FindControl("IBtnCross") as ImageButton;

                    if (LblIsActive.Text == "True")
                    {
                        IBtnCheck.Visible = true;
                        IBtnCross.Visible = false;
                    }
                    else
                    {
                        IBtnCross.Visible = true;
                        IBtnCheck.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void RptViewPendingCustomers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblSubscription = e.Item.FindControl("LblSubscription") as Label;
                    CheckBox ChkRow = e.Item.FindControl("ChkRow") as CheckBox;
                    if (LblSubscription.Text.Equals("True"))
                    {
                        LblSubscription.Text = "Subscribed";
                        ChkRow.Enabled = true;
                    }
                    else
                    {
                        LblSubscription.Text = "Unsubscribed";
                        ChkRow.Enabled = false;

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void RptExpire_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblIsActiveExp = item.FindControl("LblIsActiveExp") as Label;
                    ImageButton IBtnCheckExp = item.FindControl("IBtnCheckExp") as ImageButton;
                    ImageButton IBtnCrossExp = item.FindControl("IBtnCrossExp") as ImageButton;

                    Label lblSubscribed = e.Item.FindControl("lblSubscribed") as Label;
                    CheckBox ChkRowExp = e.Item.FindControl("ChkRowExp") as CheckBox;
                    if (lblSubscribed.Text.Equals("True"))
                    {
                        lblSubscribed.Text = "Subscribed";
                        ChkRowExp.Enabled = true;
                    }
                    else
                    {
                        lblSubscribed.Text = "Unsubscribed";
                        ChkRowExp.Enabled = false;

                    }

                    if (LblIsActiveExp.Text == "True")
                    {
                        IBtnCheckExp.Visible = true;
                        IBtnCrossExp.Visible = false;
                    }
                    else
                    {
                        IBtnCrossExp.Visible = true;
                        IBtnCheckExp.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnSendEmail_Click(object sender, EventArgs e)
        {
            try
            {
                if (EmailValidation())
                {
                    int Checked = 0;
                    foreach (RepeaterItem item in RptExpire.Items)
                    {
                        CheckBox ChkRow = (CheckBox)item.FindControl("ChkRowExp");
                        Label LblCustID = (Label)item.FindControl("LblCustID");
                        Label LblExpEmail = (Label)item.FindControl("LblExpEmail");

                        if (ChkRow.Checked == true)
                        {
                            Checked++;
                            int Cust_ID = int.Parse(LblCustID.Text);
                            //string CustName = ObjCustomerProfileMasterDAL.GetCustomerName(Cust_ID);
                            //SendExpireAccountEmail(CustName, LblExpEmail.Text, TxtEmailSubject.Text, TxtEmailMsg.Text);
                            SendEmailNotification(Cust_ID, TxtEmailSubject.Text, TxtEmailMsg.Text);
                            MessageBox(div1, LblMsg, "Email has been sent successfully", 1);
                        }
                    }

                    if (Checked == 0)
                    {
                        MessageBox(div1, LblMsg, "Select at least one to send", 0);
                    }
                    else
                    {
                        BindExpire();
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }


        private void SendEmailNotification(int Cust_ID, string EmailSubject, string Message)
        {
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            // EmailNotification.SendNotification(EN.GetAdminFilledObject());
            #endregion


            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = Cust_ID;
            DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

            if (CustData.Rows[0]["IsPackageExpNActive"].ToString().Equals("True"))
            {
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.ButtonText = "";
                EmailData.isButtonVisible = "1";
                EmailData.NotificationType = "IsPackageExpNActive";
                EmailData.Content = Message;
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = EmailSubject;
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData);
            }
            #endregion
        }

        //public void SendExpireAccountEmail(string CustName, string Email, string EmailSubject, string Message)
        //{
        //    #region EmailExpiry
        //    string body = string.Empty;
        //    using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/ExpireEmail.html")))
        //    {
        //        body = reader.ReadToEnd();
        //    }
        //    body = body.Replace("{logo}", "https://moreinline.com/Images/moreinline_logo.png");
        //    body = body.Replace("{receiver}", CustName);
        //    body = body.Replace("{message}", Message);
        //    body = body.Replace("{companySender}", "MOREINLINE");
        //    body = body.Replace("{senderAddress}", "754 Peachtree St NW-Suite 9 - Atlanta, GA 30308");
        //    body = body.Replace("{senderContact}", "888-900-2095");
        //    body = body.Replace("{senderWeb}", "moreinline.com");
        //    body = body.Replace("{senderEmail}", "sales@moreinline.com");
        //    #endregion

        //    MailMessage mm = new MailMessage("donotreply@moreinline.com", Email);
        //    mm.Subject = EmailSubject;
        //    mm.Body = body;
        //    mm.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "mail.moreinline.com";
        //    smtp.EnableSsl = false;
        //    NetworkCredential NetworkCred = new NetworkCredential();
        //    NetworkCred.UserName = "donotreply@moreinline.com";
        //    NetworkCred.Password = "12@1EJqOgg6meMwk";
        //    smtp.UseDefaultCredentials = true;
        //    smtp.Credentials = NetworkCred;
        //    smtp.Port = 587;
        //    smtp.Send(mm);
        //}

        private bool EmailValidation()
        {
            TxtEmailSubject.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";
            TxtEmailMsg.Attributes["style"] = "border: 1px solid #4B4A4A; background-color: #000;";

            if (String.IsNullOrWhiteSpace(TxtEmailSubject.Text))
            {
                TxtEmailSubject.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                MessageBox(div1, LblMsg, "Enter Email Subject", 0);
                return false;
            }
            else if (String.IsNullOrWhiteSpace(TxtEmailMsg.Text))
            {
                TxtEmailMsg.Attributes["style"] = "border: 1px solid red; background-color: #000;";
                MessageBox(div1, LblMsg, "Enter Email Message", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void BtnActive_Click(object sender, EventArgs e)
        {
            LblName.Text = "Active Customers";
            PnlActive.Visible = true;
            PnlPending.Visible = PnlExpire.Visible = pnlPrePaid.Visible = false;
            BindActive();
        }

        protected void BtnPending_Click(object sender, EventArgs e)
        {
            LblName.Text = "Pending Customers";
            PnlPending.Visible = true;
            PnlActive.Visible = PnlExpire.Visible = pnlPrePaid.Visible = false;
            BindPending();
        }

        protected void BtnExpire_Click(object sender, EventArgs e)
        {
            LblName.Text = "Expired Customers";
            PnlExpire.Visible = true;
            PnlActive.Visible = PnlPending.Visible = pnlPrePaid.Visible = false;
            BindExpire();
        }


        protected void RptPrePaid_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            var email = e.CommandArgument.ToString();
            Response.Redirect(@"~//User/AddNewCustomer.aspx?Email=" + email);
        }

        protected void RptPrePaid_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;
                    
                    Label lblstatus = e.Item.FindControl("lblStatus") as Label;
                    lblstatus.Text = lblstatus.Text.Equals("True") ? "Registered" : "Unregistered";
                    
                  
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void btnprepaid_Click(object sender, EventArgs e)
        {
            pnlPrePaid.Visible = true;
            PnlExpire.Visible = PnlActive.Visible = PnlPending.Visible = false;
            RptPrePaid.DataSource = ObjCustomerProfileMasterDAL.GetPrepaidCustomers();
            RptPrePaid.DataBind();
        }
    }
}