﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class DesignListQA : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL orderAssetsDal = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = div2.Visible = false;
            if (!IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int id = int.Parse(Request.QueryString["id"].ToString());
                    NotiDAL.UpdateNoti(id);
                }
                BindDataList(DdlDesignList.SelectedItem.Text);
            }
        }
        public void BindDataList(string SelectText)
        {
            try
            {
                DataTable dt = Order.GetDesignList();
                if (SelectText != "All")
                {
                    dt = dt.Select("CustApproval = '" + SelectText + "'").CopyToDataTable();
                }

                Dl_DesignList.DataSource = dt;
                Dl_DesignList.DataBind();
            }
            catch (Exception)
            {
                Dl_DesignList.DataSource = null;
                Dl_DesignList.DataBind();
                MessageBox(div1, LblMsg, "No Data", 0);
            }
        }

        protected void Dl_DesignList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Label LblOrderNum = e.Item.FindControl("LblOrderNum") as Label;
            string CommandName = e.CommandName.ToString();
            Label LblAssestId = e.Item.FindControl("LblAssestId") as Label;
            Label LblQaRemarks = e.Item.FindControl("LblQaRemarks") as Label;
            Label LblDid = e.Item.FindControl("LblDid") as Label;
            Label LblManagerid = e.Item.FindControl("LblManagerid") as Label;

            if (CommandName.Equals("Download"))
            {

                string FilePath = e.CommandArgument.ToString();
                string Directory = Server.MapPath(FilePath);

                try
                {
                    FileInfo fileInfo = new FileInfo(Directory);
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + fileInfo.Name);
                    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.Flush();
                    Response.WriteFile(fileInfo.FullName);
                    Response.End();
                }
                catch (Exception)
                {
                }
            }
            else if (CommandName.Equals("Accept"))
            {
                int AssetID = int.Parse(e.CommandArgument.ToString());

                if (Order.UpdateDesignApproval(AssetID, 1))
                {
                    //NotificationInfo Ninfo = new NotificationInfo();
                    //Ninfo.OID = AssetID;
                    //Ninfo.UID = int.Parse(LblManagerid.Text);
                    //Ninfo.IsViewed = false;
                    //Ninfo.UserRoleID = 4;
                    //Ninfo.NotiDate = DateTime.Now;
                    //NotiDAL.InsertNotification(Ninfo);
                    MessageBox(div1, LblMsg, "Design Approved", 1);
                }
                BindDataList(DdlDesignList.SelectedItem.Text);
            }
            else if (CommandName.Equals("NotApproved"))
            {
                hfid.Value = LblAssestId.Text;
                lblCode.Text = LblOrderNum.Text;
                TxtMessage.Text = LblQaRemarks.Text;
                hfmnid.Value = LblDid.Text;
                hfdid.Value = LblManagerid.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void Dl_DesignList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Image ImgApproved = e.Item.FindControl("ImgApproved") as Image;
                    Image ImgNotApproved = e.Item.FindControl("ImgNotApproved") as Image;

                    Label LblCustApproval = e.Item.FindControl("LblCustApproval") as Label;
                    Label HfQaApproval = e.Item.FindControl("HfQaApproval") as Label;
                    Button BtnDownload = e.Item.FindControl("BtnDownload") as Button;
                    Button BtnNotApproved = e.Item.FindControl("BtnNotApproved") as Button;
                    Button BtnAccept = e.Item.FindControl("BtnAccept") as Button;
                    Label LblMngApproval = e.Item.FindControl("LblMngApproval") as Label;
                    
                    if (LblCustApproval.Text.Equals("Approved"))
                    { ImgApproved.Visible = true; }
                    else if (LblCustApproval.Text.Equals("Not Approved"))
                    { ImgNotApproved.Visible = true; }

                    if (HfQaApproval.Text.Equals("Approved") && !LblCustApproval.Text.Equals("Not Approved") && !LblMngApproval.Text.Equals("Not Approved"))
                    {
                        BtnAccept.Enabled = false;
                        BtnAccept.Text = "Send to Manager";
                        BtnNotApproved.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        protected void BtnUoload_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(TxtMessage.Text))
                {
                    int AssetId = int.Parse(hfid.Value);
                    if (Order.UpdateDesignRejected(AssetId, 2, TxtMessage.Text))
                    {
                        if (orderAssetsDal.UpdateOrderStatus(AssetId, 2))
                        {
                            //NotificationInfo Ninfo = new NotificationInfo();
                            //Ninfo.OID = AssetId;
                            //Ninfo.UID = int.Parse(hfdid.Value);
                            //Ninfo.IsViewed = false;
                            //Ninfo.UserRoleID = 5;
                            //Ninfo.NotiDate = DateTime.Now;
                            //NotiDAL.InsertNotification(Ninfo);
                            MessageBox(div1, LblMsg, "Design Rejected", 1);
                        }
                    }
                    BindDataList(DdlDesignList.SelectedItem.Text);
                }
                else
                {
                    MessageBox(div2, LblMsg1, "You Can't Reject Design Without Reasoning ", 0);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
            }
            catch (Exception ez)
            {
                MessageBox(div2, LblMsg1, ez.Message.ToString(), 0);
            }

        }

        protected void DdlDesignList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataList(DdlDesignList.SelectedItem.Text);
        }

    }
}