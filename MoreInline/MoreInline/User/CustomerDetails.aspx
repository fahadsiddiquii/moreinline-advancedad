﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="CustomerDetails.aspx.cs" Inherits="MoreInline.User.CustomerDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .body {
            background-color: #fafafa;
        }

        .card {
            background: #fff;
            border-radius: 2px;
            /*border:3px solid #A9C502;*/
            display: inline-block;
            height: 374px;
            margin: 1rem;
            position: relative;
            width: 390px;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

            .card-1:hover {
                box-shadow: 0 14px 28px #808080, 0 10px 10px #808080;
            }
    </style>

    <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"> 
    </script>
    <div class="row">
        <div class="col-lg-2 col-sm-3 col-md-3">
            <img runat="server" alt="UserImage" id="img" width="200" height="200" />
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9">
            <div class="row">
                <div class="col-lg-3">
                    <h4>First Name : <span runat="server" id="fname"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Email : <span runat="server" id="email"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Company : <span runat="server" id="company"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Joining Date : <span runat="server" id="joiningdate"></span></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <h4>Last Name : <span runat="server" id="lname"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Contact : <span runat="server" id="contact"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Address : <span runat="server" id="address"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Zip Code : <span runat="server" id="zipcode"></span></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <h4>Account Type : <span runat="server" id="actype"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Email Limit : <span runat="server" id="elimit"></span></h4>
                </div>
                <div class="col-lg-3">
                    <h4>Web Address : <span runat="server" id="webaddress"></span></h4>
                </div>
                <%-- <div class="col-lg-3" >
                     <h5>Zip Code : <span runat="server" id="Span4"></span></h5> 
               </div>--%>
            </div>
        </div>

    </div>
    <br />
    <hr />
    <br />
    <center>
     <div style="width:90%">
    <div class="row">
        <div class="body col-lg-12 col-sm-12 col-md-12">
        <asp:repeater ID="rptInvoices" runat="server" OnItemDataBound="rptInvoices_ItemDataBound">
           <itemtemplate> 
            <div  class="card card-1">
                <div id='<%#DataBinder.Eval(Container.DataItem,"InvoiceNumber") %>' style="background-color:#fff;">
               <center>
                   <h3>Invoice</h3>
               </center>
                <center>
                <div style="width:329px; height:260px" class="text-left">
               <label>Date: <%#DataBinder.Eval(Container.DataItem,"Tdate") %></label><br />
               <label>Customer ID: <%#DataBinder.Eval(Container.DataItem,"Cust_ID") %></label><br />
               <label>Customer Name: <asp:label text="text" id="custName" runat="server" /></label><br />
              <label> Payment ID:<%#DataBinder.Eval(Container.DataItem,"Paymentid") %></label><br />
         <%--     <label> Payment ID:<%#DataBinder.Eval(Container.DataItem,"PayerID") %></label><br /><br />--%>
               
                <table class="table table-bordered table-hover">
                   <thead  class="table-head">
                     <tr style="background-color: #A9C502;">
                     <th class="text-center text-black">Invoice #</th>
                     <th class="text-center text-black">Item</th>
                     <th class="text-center text-black">Month</th>
                     <th class="text-center text-black">Amount </th>
                     </tr>
                    </thead>
                 <tbody>
                   <tr>
                     <td><%#DataBinder.Eval(Container.DataItem,"InvoiceNumber") %></td>
                     <td><%#DataBinder.Eval(Container.DataItem,"item") %></td>
                     <td>1</td>
                     <td><%#DataBinder.Eval(Container.DataItem,"Amount") %></td>
                   </tr>
                </tbody>
             </table>
                   <span class="pull-left p-l-10"><b>ADVANCE AD</b></span>
                   <span class="pull-right">Total : <%#DataBinder.Eval(Container.DataItem,"Amount") %></span>
                </div>
            </center>
                    </div>
               
                <a href="#" id='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' class="btn btn-sm btn-flat btn-github pull-right m-r-10"  onmouseover="Capture(this)"  name='<%#DataBinder.Eval(Container.DataItem,"InvoiceNumber") %>' >Download</a>
            </div>
           </itemtemplate>
        </asp:repeater>
        </div> 
    </div>
         </div>
        </center>
    <script>
        var element;
        function Capture(btn) {
            debugger;
            var id = "#" + $(btn).attr("name");
            var newData;
            element = $(id);
            var getCanvas;
            ///  for (var i = 0; i < 2; i++) {
            Dwonloadrpt(element, btn);
            ///  }
        }
        function Dwonloadrpt(element, btn) {
            html2canvas(element, {
                onrendered: function (canvas) {
                    debugger;
                    getCanvas = canvas;
                    var imgageData = getCanvas.toDataURL("image/png");
                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
                    $(btn).attr("download", "Invoice.png").attr("href", newData);
                    //$(btn).on("click", function() {
                    //    alert("working");
                    //})
                }
            });
        }

    </script>
</asp:Content>
