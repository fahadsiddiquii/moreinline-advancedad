﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class AdsBroadcast : System.Web.UI.Page
    {

        DAL.AdsBroadcast ObjAds = new DAL.AdsBroadcast();
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        DesignBOL DBOL = new DesignBOL();
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL orderAssetsDal = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
               
                if (!IsPostBack)
                {
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        private void BindRepeater()
        {
            DataTable dt = ObjAds.GetBroadcastHistory();
            RptAds.DataSource = dt;
            RptAds.DataBind();
        }


        protected void RptAds_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int Img_ID = int.Parse(e.CommandArgument.ToString());

                switch (e.CommandName)
                {
                    case "btnimg":
                        ImageButton FilePath = e.Item.FindControl("FilePath") as ImageButton;
                        string File = FilePath.ImageUrl;
                        File = File.Replace("~/", "../");
                        imgpop.Src = File;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        break;
                }
            }
            catch (Exception)
            {

            }

        }


        protected void BtnUpload_Click(object sender, EventArgs e)
        {
           
                try
                {
                    SendingProcess();
                    string strFolder = Server.MapPath("~/BroadcastImages/");
                    if (!Directory.Exists(strFolder))
                    {
                        Directory.CreateDirectory(strFolder);
                    }
                    string strFileName = ImgFile.FileName;
                    ObjAds.AdUrl = "~/BroadcastImages/" + strFileName;
                    ObjAds.SendOn = DateTime.Now;
                    ObjAds.SendBy = int.Parse(Session["UserID"].ToString());
                    if (ObjAds.InsertAdsBroadcast(ObjAds))
                    {
                        string strFilePath = strFolder + strFileName;
                        ImgFile.SaveAs(strFilePath);
                        BindRepeater();
                        MessageBox(div1, LblMsg, "Ad Broadcast Successfully!", 1);
                    }
                }
                catch (Exception)
                {

                }
           
        }

        private void SendingProcess()
        {
            try
            {
                if (ImgFile.HasFile)
                {
                    DataTable CustomerIds = ObjAds.GetCustomersIds();

                    foreach (DataRow row in CustomerIds.Rows)
                    {
                        int CustID = int.Parse(row[0].ToString());
                        string strFileName = "";
                        string strFilePath = "";
                        string FilePath = "";
                        string strFolder = "";


                        strFolder = Server.MapPath("~/OrderDesigned/");
                        strFileName = ImgFile.FileName;
                        int id = CustID;
                        string[] NameContruct = strFileName.Split('.');
                        NameContruct[0] += "-" + id + "-" + DateTime.Now.Second + "";
                        strFileName = NameContruct[0].ToString() + "." + NameContruct[1].ToString();
                        strFileName = Path.GetFileName(strFileName);

                        if (!Directory.Exists(strFolder))
                        {
                            Directory.CreateDirectory(strFolder);
                        }
                        strFilePath = strFolder + strFileName;


                        try
                        {
                            ImgFile.SaveAs(strFilePath);
                            FilePath = "~/OrderDesigned/" + strFileName;
                        }
                        catch (IOException)
                        {
                            MessageBox(div1, LblMsg, "System Faced Error Try Later", 0);
                            return;
                        }



                        Assets.Category = OrderAsset.GetCategory();
                        Assets.Subject = string.Empty;
                        Assets.Phone = string.Empty;
                        Assets.Email = string.Empty;
                        Assets.Location = string.Empty;
                        Assets.WebUrl = string.Empty;
                        Assets.Date = null;
                        Assets.Hours = 0;
                        Assets.TimSpan = string.Empty;
                        Assets.Message = "";
                        Assets.Date = DateTime.Now;
                        Assets.AddAudio = false;
                        Assets.AddContact = false;
                        Assets.AddDirection = false;
                        Assets.AddReminder = false;
                        Assets.AddWebUrl = false;
                        Assets.AddUerProfile = false;
                        Assets.AssestFilePath = string.Empty;
                        Assets.CreatedByID = CustID;
                        Assets.ApprovalStatus = 1;

                        Conn = DBHelper.GetConnection();
                        Trans = Conn.BeginTransaction();

                        if (OrderAsset.InsertOrderAssests(Assets, Trans, Conn))
                        {
                            int UserID = int.Parse(Session["UserID"].ToString());
                            DBOL.AssestID = Assets.ID;
                            DBOL.CreatedByID = UserID;
                            DBOL.FilePath = FilePath;
                            DBOL.QAID = 1;
                            DBOL.QaApproval = 1;
                            DBOL.MngrApproval = 1;
                            DBOL.CustApproval = 1;
                            DBOL.QaRemarks = "Approved";
                            DBOL.MngrRemarks = "Approved";
                            DBOL.CustRemarks = "Approved";

                            if (Order.InsertOrderdDesign(DBOL,Trans,Conn))
                            {
                                orderAssetsDal.UpdateOrderStatus(Assets.ID, 1,Trans,Conn);
                                NotificationInfo Ninfo = new NotificationInfo();
                                Ninfo.OID = Assets.ID;
                                Ninfo.UID = CustID;
                                Ninfo.IsViewed = false;
                                Ninfo.UserRoleID = -1;
                                Ninfo.NotiDate = DateTime.Now;
                                Ninfo.FormPath = "../MKT_Assets/ViewAssets.aspx";
                                NotiDAL.InsertNotification(Ninfo,Trans,Conn);

                                // SendEmailNotification(CustID, Assets.ID);
                                Trans.Commit();
                                Conn.Close();


                            }
                            else
                            {
                                MessageBox(div1, LblMsg, "File Upload Failed", 0);
                                Trans.Rollback();
                                Conn.Close();
                            }

                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "File Upload Failed", 0);
                            Trans.Rollback();
                            Conn.Close();
                        }

                    }
                }
                else
                {
                    MessageBox(div1, LblMsg, "Please Upload File", 0);
                    return;
                }

            }
            catch (Exception)
            {

            }
        }


        private void SendEmailNotification(int custId, int AssetId)
        {
            EmailNotification EN = new EmailNotification();

            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = custId;
            DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
            DataTable AssetInfo = orderAssetsDal.GetOrderAssetsMaster(AssetId);
            if (CustData.Rows[0]["IsDesignCrActive"].ToString().Equals("True"))
            {
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.ButtonText = "Review Your Order";
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.NotificationType = "IsDesignCrActive";
                EmailData.Content = "Your order #" + AssetInfo.Rows[0]["OrderNum"].ToString() + " is ready for your review. Please accept the delivery or request a revision if needed";
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = " Your order is ready for your review ";
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData);
            }
            #endregion
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }


    }
}