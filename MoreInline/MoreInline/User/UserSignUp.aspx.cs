﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class UserSignUp : System.Web.UI.Page
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        UserSignupBOL ObjUserSignupBOL = new UserSignupBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        private static string DateE = ConfigurationManager.AppSettings["Date"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = div2.Visible = false;
                if (!IsPostBack)
                {
                    GetUserTypes();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        public void GetUserTypes()
        {
            DataTable dt = ObjUserSignupDAL.GetUserTypes(ObjUserSignupBOL);
            ViewState["dt_UserTypes"] = dt;

            DdlUserType.DataSource = dt;
            DdlUserType.DataTextField = "UserRoles";
            DdlUserType.DataValueField = "UserRoleID";
            DdlUserType.DataBind();
            DdlUserType.Items.Insert(0, new ListItem("Select User Type", "0"));
        }
        public bool Local_Validation()
        {
            if (DdlUserType.SelectedValue == "0")
            {
                MessageBox(div1, LblMsg, "Please Select Account Type", 0);
                return false;
            }
            else if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtUEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtUPass.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else if (TxtUCPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                return false;
            }
            else if (TxtUCPassword.Text != TxtUPass.Text)
            {
                MessageBox(div1, LblMsg, "Passwords do not match", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        public void SaveEntries()
        {
            try
            {
                #region New
                ObjUserSignupBOL.FirstName = TxtFName.Text;
                ObjUserSignupBOL.LastName = TxtLName.Text;
                ObjUserSignupBOL.UserEmail = TxtUEmail.Text;
                ObjUserSignupBOL.UserPassword = TxtUPass.Text;
                ObjUserSignupBOL.UserPhone = TxtPhone.Text;
                ObjUserSignupBOL.UserAddress = TxtAddress.Text;
                ObjUserSignupBOL.UserType = int.Parse(DdlUserType.SelectedValue);
                ObjUserSignupBOL.Photo = ViewState["ProfilePhoto"].ToString();

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjUserSignupDAL.InsertUserProfile(ObjUserSignupBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    ClearControls();
                    MessageBox(div1, LblMsg, "Account Created Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void ClearControls()
        {
            TxtFName.Text = "";
            TxtLName.Text = "";
            TxtUEmail.Text = "";
            TxtUPass.Text = "";
            TxtUCPassword.Text = "";
            TxtPhone.Text = "";
            TxtAddress.Text = "";
            DdlUserType.SelectedValue = "0";

            ViewState["ProfilePhoto"] = null;
        }

        protected void BtnSignUp_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    DataTable dt_UserTypes = (DataTable)ViewState["dt_UserTypes"];
                    //UserRoleID	UserRoles	UserLimits	Limits
                    dt_UserTypes = dt_UserTypes.Select("UserRoleID = " + DdlUserType.SelectedValue).CopyToDataTable();

                    if (int.Parse(dt_UserTypes.Rows[0]["UserLimits"].ToString()) <= int.Parse(dt_UserTypes.Rows[0]["Limits"].ToString()))
                    {
                        MessageBox(div1, LblMsg, "User Limit Exceeded", 0);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }

                if (ViewState["ProfilePhoto"] == null || ViewState["ProfilePhoto"].ToString() == "")
                {
                    if (Upload.HasFile)
                    {
                        string fileName = Path.GetFileName(Upload.PostedFile.FileName);
                        ViewState["ProfilePhoto"] = fileName;
                        Upload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Upload Profile Photo", 0);
                        return;
                    }
                }
                if (Local_Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtUEmail.Text != "")
                    {
                        Match match = regex.Match(TxtUEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }
                    if (TxtUEmail.Text != "")
                    {
                        ObjUserSignupBOL.UserEmail = TxtUEmail.Text;
                        DataTable dt = ObjUserSignupDAL.GetUserExist(ObjUserSignupBOL);

                        if (dt.Rows.Count > 0)
                        {
                            MessageBox(div1, LblMsg, "This Email Already Exist", 0);
                            return;
                        }
                    }

                    SaveEntries();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                LoginWork(TxtEmail.Text, TxtPassword.Text);
            }
            catch (Exception ex)
            { MessageBox(div2, LblMsg2, ex.Message, 0); }
        }
        public void LoginWork(string Email, string Password)
        {
            try
            {
                string Date = DateE;
                string Date1 = DateTime.Now.ToString("yyyy/MM/dd");

                if (DateTime.Parse(Date) <= DateTime.Parse(Date1))
                {
                    MessageBox(div1, LblMsg, "Timeout expired. The timeout period elapsed prior to obtaining a connection from the pool. This may have occurred because all pooled connections were in use and max pool size was reached.", 0);
                    return;
                }

                if (TxtEmail.Text != "" && TxtPassword.Text != "")
                {
                    ObjUserSignupBOL.UserEmail = Email;
                    ObjUserSignupBOL.UserPassword = Password;

                    DataTable dt = ObjUserSignupDAL.UserLogin(ObjUserSignupBOL);


                    if (dt.Rows.Count == 1)
                    {
                        if (dt.Rows[0]["IsActive"].ToString() == "True")
                        {
                            Session["UserID"] = dt.Rows[0]["UserID"].ToString();
                            Session["UserName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                            Session["UserEmail"] = dt.Rows[0]["UserEmail"].ToString();
                            Session["Photo"] = dt.Rows[0]["Photo"].ToString();
                            Session["UserRoleID"] = dt.Rows[0]["UserType"].ToString();
                            Session["UserType"] = dt.Rows[0]["UserType"].ToString();

                            DataTable dt_UserTypes = (DataTable)ViewState["dt_UserTypes"];
                            dt_UserTypes = dt_UserTypes.Select("UserRoleID = " + dt.Rows[0]["UserType"].ToString()).CopyToDataTable();

                            Session["UserRoles"] = dt_UserTypes.Rows[0]["UserRoles"].ToString();

                            if (Session["UserRoles"].ToString() == "Designer")
                            {
                                Response.Redirect(@"~\User\Orders.aspx", false);
                            }
                            else if (Session["UserRoles"].ToString() == "QA")
                            {
                                Response.Redirect(@"~\User\DesignListQA.aspx", false);
                            }
                            else
                            {
                                //Response.Redirect(@"~\User\WelcomePage.aspx", false);
                                Server.Transfer(@"~\User\WelcomePage.aspx", false);
                            }

                        }
                        else
                        { MessageBox(div2, LblMsg2, "Your Account is Not Active Please Contact Admin", 0); }
                    }
                    else
                    { MessageBox(div2, LblMsg2, "Invalid Email or Password", 0); }
                }
                else
                { MessageBox(div2, LblMsg2, "Enter Email or Password", 0); }
            }
            catch (Exception ex)
            { MessageBox(div2, LblMsg2, ex.Message, 0); }
        }

    }
}