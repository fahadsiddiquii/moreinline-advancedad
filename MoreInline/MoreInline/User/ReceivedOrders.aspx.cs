﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class ReceivedOrders : System.Web.UI.Page
    {

        ReceivedOrdersDAL Orders = new ReceivedOrdersDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["NotiId"] != null)
                {
                    int id = int.Parse(Request.QueryString["NotiId"].ToString());
                    NotiDAL.UpdateNoti(id);
                }
                LoadRecords();
            }
        }


        protected void RptReceivedOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                HiddenField HfDid = e.Item.FindControl("HfDid") as HiddenField;
                HiddenField HfQAID = e.Item.FindControl("HfQAID") as HiddenField;

                string DID = HfDid.Value;
                string Qaid = HfQAID.Value;
                RepeaterItem item = e.Item;

                Label lblApprovalStatus = item.FindControl("lblApprovalStatus") as Label;
                DropDownList ddlUsers = item.FindControl("ddlUsers") as DropDownList;
                ddlUsers.DataTextField = "FullName";
                ddlUsers.DataValueField = "UserID";
                ddlUsers.DataSource = Orders.GetUsersName(5);
                ddlUsers.DataBind();
                ddlUsers.SelectedValue = DID;
                ddlUsers.Items.Insert(0, new ListItem("Select", "-1"));

                DropDownList ddlQAs = item.FindControl("ddlQAs") as DropDownList;
                ddlQAs.DataTextField = "FullName";
                ddlQAs.DataValueField = "UserID";
                ddlQAs.DataSource = Orders.GetUsersName(6);
                ddlQAs.DataBind();
                ddlQAs.SelectedValue = Qaid;
                ddlQAs.Items.Insert(0, new ListItem("Select", "-1"));
                Button BrnSnd = item.FindControl("ImgPreview") as Button;

                //if (!string.IsNullOrWhiteSpace(DID) && !string.IsNullOrWhiteSpace(Qaid))
                //{
                //    ddlQAs.Enabled = false;
                //    ddlUsers.Enabled = false;
                //}

                string Status = lblApprovalStatus.Text;
                if (Status.Equals("1"))
                {
                    lblApprovalStatus.Text = "Pending";
                }
                else if (Status.Equals("2"))
                {
                    lblApprovalStatus.Text = "In Process";
                    //BrnSnd.Enabled = false;
                
                    BrnSnd.Text = BrnSnd.ToolTip = "Sent";

                }
                else if (Status.Equals("3"))
                {
                    lblApprovalStatus.Text = "Delivered";
                    //BrnSnd.Enabled = false;
                    BrnSnd.Text = BrnSnd.ToolTip = "Sent";
                }
            }
        }

        protected void RptReceivedOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            DropDownList ddlUsers = e.Item.FindControl("ddlUsers") as DropDownList;
            DropDownList ddlQAs = e.Item.FindControl("ddlQAs") as DropDownList;
            HiddenField hfCustID = e.Item.FindControl("hfCustID") as HiddenField;
            int CustID = int.Parse(hfCustID.Value.ToString());
            int AssetID = int.Parse(e.CommandArgument.ToString());
            int UserID = int.Parse(Session["UserID"].ToString());
            int designerID = int.Parse(ddlUsers.SelectedValue.ToString());
            int QualityAs = int.Parse(ddlQAs.SelectedValue.ToString());

            Label LblLogID = e.Item.FindControl("LblLogID") as Label;
            Button BrnSnd = e.Item.FindControl("ImgPreview") as Button;

            if (designerID != -1 && QualityAs != -1)
            {
                try
                {
                    if (BrnSnd.Text == "Sent")
                    {
                        Orders.SP_UpdateOrderLog(designerID, QualityAs, int.Parse(LblLogID.Text));
                       
                    }
                    else if (Orders.SP_InsertOrderLog(designerID, QualityAs, CustID, AssetID, UserID))
                    {
                        MessageBox(div1, LblMsg, "Task Assigned To Respective Designer & QA", 1);
                        Orders.UpdateOrderStatus(AssetID, 2);
                        //NotificationInfo Ninfo = new NotificationInfo();
                        //Ninfo.OID = AssetID;
                        //Ninfo.UID = 1;//designerID;
                        //Ninfo.IsViewed = false;
                        //Ninfo.UserRoleID = 2;
                        //Ninfo.NotiDate = DateTime.Now;
                        //NotiDAL.InsertNotification(Ninfo);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "System Faced Error Please Try Later", 0);
                    }
                }
                catch (Exception)
                {

                    MessageBox(div1, LblMsg, "System Faced Error Please Try Later", 0);
                }
                LoadRecords();
            }
            else
            {
                MessageBox(div1, LblMsg, "Please Select Designer & QA", 0);
            }
        }

        private void LoadRecords()
        {
            RptReceivedOrder.DataSource = Orders.GetReceivedOrders();
            RptReceivedOrder.DataBind();
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
    }
}