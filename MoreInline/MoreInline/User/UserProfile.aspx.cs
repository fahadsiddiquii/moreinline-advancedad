﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class UserProfile : System.Web.UI.Page
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        UserSignupBOL ObjUserSignupBOL = new UserSignupBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    GetUserTypes();

                    if (Request.QueryString["UserID"] == null)
                    {
                        GetProfile(int.Parse(Session["UserID"].ToString()));
                    }
                    else if (Request.QueryString["UserID"] != null)
                    {
                        GetProfile(int.Parse(Request.QueryString["UserID"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        public void GetUserTypes()
        {
            DataTable dt = ObjUserSignupDAL.GetUserTypes(ObjUserSignupBOL);
            ViewState["dt_UserTypes"] = dt;

            DdlUserType.DataSource = dt;
            DdlUserType.DataTextField = "UserRoles";
            DdlUserType.DataValueField = "UserRoleID";
            DdlUserType.DataBind();
            DdlUserType.Items.Insert(0, new ListItem("Select User Type", "0"));
        }

        public void GetProfile(int UserID)
        {
            ObjUserSignupBOL.UserID = UserID;
            DataTable dt = ObjUserSignupDAL.GetUserProfile(ObjUserSignupBOL);

            TxtFName.Text = dt.Rows[0]["FirstName"].ToString();
            TxtLName.Text = dt.Rows[0]["LastName"].ToString();
            TxtUEmail.Text = dt.Rows[0]["UserEmail"].ToString();
            TxtUPass.Text = dt.Rows[0]["UserPassword"].ToString();
            TxtPhone.Text = dt.Rows[0]["UserPhone"].ToString();
            TxtAddress.Text = dt.Rows[0]["UserAddress"].ToString();
            DdlUserType.SelectedValue = dt.Rows[0]["UserType"].ToString();
            ImgProfile.ImageUrl = "~/upload/" + dt.Rows[0]["Photo"].ToString();
            ViewState["ProfilePhoto"] = dt.Rows[0]["Photo"].ToString();
        }
        public bool Local_Validation()
        {
            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtUEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else if (TxtPhone.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtUPass.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Upload.HasFile)
                {
                    string fileName = Path.GetFileName(Upload.PostedFile.FileName);
                    ViewState["ProfilePhoto"] = fileName;
                    Upload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                }

                if (Local_Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                    if (TxtUEmail.Text != "")
                    {
                        Match match = regex.Match(TxtUEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }
                }

                if (Request.QueryString["UserID"] == null)
                {
                    UpdateSavedProfile(int.Parse(Session["UserID"].ToString()));
                }
                else if (Request.QueryString["UserID"] != null)
                {
                    UpdateSavedProfile(int.Parse(Request.QueryString["UserID"].ToString()));
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void UpdateSavedProfile(int UserID)
        {
            try
            {
                #region New
                ObjUserSignupBOL.FirstName = TxtFName.Text;
                ObjUserSignupBOL.LastName = TxtLName.Text;
                ObjUserSignupBOL.UserEmail = TxtUEmail.Text;
                ObjUserSignupBOL.UserPassword = TxtUPass.Text;
                ObjUserSignupBOL.UserPhone = TxtPhone.Text;
                ObjUserSignupBOL.UserAddress = TxtAddress.Text;
                ObjUserSignupBOL.UserType = int.Parse(DdlUserType.SelectedValue);
                ObjUserSignupBOL.Photo = ViewState["ProfilePhoto"].ToString();
                ObjUserSignupBOL.UserID = UserID;

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjUserSignupDAL.UpdateUserProfile(ObjUserSignupBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    //ClearControls();
                    EnableControls(false);

                    if (Request.QueryString["UserID"] == null)
                    {
                        GetProfile(int.Parse(Session["UserID"].ToString()));
                    }
                    else if (Request.QueryString["UserID"] != null)
                    {
                        GetProfile(int.Parse(Request.QueryString["UserID"].ToString()));
                    }

                    BtnEdit.Visible = true;
                    BtnSave.Visible = false;

                    MessageBox(div1, LblMsg, "Profile Updated Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void ClearControls()
        {
            TxtFName.Text = "";
            TxtLName.Text = "";
            TxtUEmail.Text = "";
            TxtUPass.Text = "";
            TxtPhone.Text = "";
            TxtAddress.Text = "";
            DdlUserType.SelectedValue = "0";

            ViewState["ProfilePhoto"] = null;
        }

        public void EnableControls(bool State)
        {
            TxtFName.Enabled = State;
            TxtLName.Enabled = State;
            TxtUEmail.Enabled = State;
            TxtUPass.Enabled = State;
            TxtPhone.Enabled = State;
            TxtAddress.Enabled = State;
            DdlUserType.Enabled = State;
            Upload.Enabled = State;
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            EnableControls(true);

            BtnEdit.Visible = false;
            BtnSave.Visible = true;
        }
    }
}