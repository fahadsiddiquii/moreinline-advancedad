﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class DesignsApproval : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL orderAssetsDal = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = div2.Visible = false;
            if (!IsPostBack)
            {
                if (Request.QueryString["NotiId"] != null)
                {
                    int id = int.Parse(Request.QueryString["NotiId"].ToString());
                    NotiDAL.UpdateNoti(id);
                }
                BindDataList(DdlDesignList.SelectedItem.Value);
            }
        }


        public void BindDataList(string SelectValue)
        {
            try
            {
                DataTable dt = Order.GetDesignMngr();
                if (SelectValue != "0")
                {
                    dt = dt.Select("CustApproval = '" + SelectValue + "'").CopyToDataTable();
                }
                Dl_DesignList.DataSource = dt;
                Dl_DesignList.DataBind();
            }
            catch (Exception)
            {
                Dl_DesignList.DataSource = null;
                Dl_DesignList.DataBind();
                MessageBox(div1, LblMsg, "No Data", 0);
            }
        }

        protected void Dl_DesignList_ItemCommand(object source, DataListCommandEventArgs e)
        {
            Label LblOrderNum = e.Item.FindControl("LblOrderNum") as Label;
            Label LblManagerid = e.Item.FindControl("LblManagerid") as Label;
            Label LblCust_ID = e.Item.FindControl("LblCust_ID") as Label;
            Label LblQAID = e.Item.FindControl("LblQAID") as Label;
            string CommandName = e.CommandName.ToString();
            string LblAssestID = (e.Item.FindControl("LblAssestID") as Label).Text;
            if (CommandName.Equals("Download"))
            {

                string FilePath = e.CommandArgument.ToString();
                string Directory = Server.MapPath(FilePath);

                try
                {
                    FileInfo fileInfo = new FileInfo(Directory);
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + fileInfo.Name);
                    Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    Response.Flush();
                    Response.WriteFile(fileInfo.FullName);
                    Response.End();
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
            }
            else if (CommandName.Equals("Accept"))
            {
                int AssetID = int.Parse(e.CommandArgument.ToString());

                if (Order.UpdateDesignApMngr(AssetID, 1))
                {
                    NotificationInfo Ninfo = new NotificationInfo();
                    Ninfo.OID = AssetID;
                    Ninfo.UID = int.Parse(LblCust_ID.Text);
                    Ninfo.IsViewed = false;
                    Ninfo.UserRoleID = -1;
                    Ninfo.NotiDate = DateTime.Now;
                    Ninfo.FormPath = "../User/DesignsApproval.aspx";
                    NotiDAL.InsertNotification(Ninfo);
                    MessageBox(div1, LblMsg, "Design Approved", 1);
                }
                BindDataList(DdlDesignList.SelectedItem.Value);
            }
            else if (CommandName.Equals("NotApproved"))
            {
                lblCode.Text = LblOrderNum.Text;
                hfid.Value = LblAssestID;
                hfQid.Value = LblQAID.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }

        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void Dl_DesignList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Image ImgApproved = e.Item.FindControl("ImgApproved") as Image;
                    Image ImgNotApproved = e.Item.FindControl("ImgNotApproved") as Image;

                    HiddenField MngrApproval = e.Item.FindControl("MngrApproval") as HiddenField;
                    HiddenField HdnCustApproval = e.Item.FindControl("HdnCustApproval") as HiddenField;

                    Button BtnDownload = e.Item.FindControl("BtnDownload") as Button;
                    //  Button BtnReject = e.Item.FindControl("BtnReject") as Button;
                    Button BtnAccept = e.Item.FindControl("BtnAccept") as Button;
                    Button BtnNotApproved = e.Item.FindControl("BtnNotApproved") as Button;

                    if (HdnCustApproval.Value.Equals("1"))
                    { ImgApproved.Visible = true; }
                    else if (HdnCustApproval.Value.Equals("2"))
                    { ImgNotApproved.Visible = true; }

                    if (MngrApproval.Value.Equals("1") && !HdnCustApproval.Value.Equals("2"))
                    {
                        ///   BtnReject.Enabled = false;
                        BtnAccept.Enabled = false;
                        BtnAccept.Text = "Send to Customer";
                        BtnNotApproved.Enabled = false;
                    }

                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TxtMessage.Text))
            {
                int AssetId = int.Parse(hfid.Value);
                if (Order.UpdateDesignRejMng(AssetId, 2, TxtMessage.Text))
                {
                    if (orderAssetsDal.UpdateOrderStatus(AssetId, 2))
                    {
                        //NotificationInfo Ninfo = new NotificationInfo();
                        //Ninfo.OID = AssetId;
                        //Ninfo.UID = int.Parse(hfQid.Value);
                        //Ninfo.IsViewed = false;
                        //Ninfo.UserRoleID = 6;
                        //Ninfo.NotiDate = DateTime.Now;
                        //NotiDAL.InsertNotification(Ninfo);
                        MessageBox(div1, LblMsg, "Design Rejected", 1);
                    }

                }
            }
            else
            {
                MessageBox(div2, LblMsg1, "You Can't Reject Design Without Reasoning ", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
        }

        protected void DdlDesignList_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDataList(DdlDesignList.SelectedItem.Value);
        }
    }
}