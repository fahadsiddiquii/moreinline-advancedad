﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class ManageAds : System.Web.UI.Page
    {
        AdditionalAdBOL ObjAdBol = new AdditionalAdBOL();
        AdditionalAdDAL ObjAdDal = new AdditionalAdDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    BindRepeater();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            int Userid = int.Parse(Session["UserID"].ToString());
            if (ImgFile.HasFile)
            {
                string fileName = Path.GetFileName(ImgFile.PostedFile.FileName);
                ImgFile.SaveAs(Server.MapPath("~/AdsImages/"+ fileName));

                if (ObjAdDal.InsertAdditionalAds(fileName, Userid))
                {
                    MessageBox(div1, LblMsg, "Ad is successfully Uploaded.", 1);
                    BindRepeater();
                }
                else
                {
                    MessageBox(div1, LblMsg, "Error occured while uploading Ad.", 0);
                }

            }
            else
            {
                MessageBox(div1, LblMsg, "Upload Image", 0);
            }
        }

        private void BindRepeater()
        {
            DataTable dt = ObjAdDal.GetAds();
            RptAds.DataSource = dt;
            RptAds.DataBind();
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        private bool UpdateAdsActivation(int Img_Id, int IsActive, int UpdatedBy)
        {
            try
            {
                ObjAdBol.Img_ID = Img_Id;
                ObjAdBol.IsActive = IsActive;
                ObjAdBol.UpdatedBy = UpdatedBy;
                if (ObjAdDal.UpdateAdsActivation(ObjAdBol))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
                return false;
            }

        }

        protected void RptAds_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int Img_ID = int.Parse(e.CommandArgument.ToString());
            int Userid = int.Parse(Session["UserID"].ToString());
            switch (e.CommandName)
            {
                case "Active":
                    if (UpdateAdsActivation(Img_ID, 0, Userid))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the Ad InActive", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating Ad", 0);
                    }
                    BindRepeater();
                    break;
                case "InActive":
                    if (UpdateAdsActivation(Img_ID, 1, Userid))
                    {
                        MessageBox(div1, LblMsg, "You have successfully made the Ad Active", 1);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Error in updating Ad", 0);
                    }
                    BindRepeater();
                    break;
                case "DeleteImg":
                    if (ObjAdDal.DeleteAD(Img_ID))
                    {
                        MessageBox(div1, LblMsg, "You have successfully deleted the Ad", 1);
                    }
                    BindRepeater();
                    break;
                case "btnimg":
                    ImageButton FilePath = e.Item.FindControl("FilePath") as ImageButton;
                    string File = FilePath.ImageUrl;
                    File = File.Replace("~/", "../");
                    imgpop.Src = File;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    break;
            }
        }

        protected void RptAds_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblIsActive = item.FindControl("LblIsActive") as Label;
                    ImageButton IBtnCheck = item.FindControl("IBtnCheck") as ImageButton;
                    ImageButton IBtnCross = item.FindControl("IBtnCross") as ImageButton;

                    if (LblIsActive.Text == "True")
                    {
                        IBtnCheck.Visible = true;
                        IBtnCross.Visible = false;
                    }
                    else
                    {
                        IBtnCross.Visible = true;
                        IBtnCheck.Visible = false;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
