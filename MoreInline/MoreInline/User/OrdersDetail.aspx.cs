﻿using MoreInline.BOL;
using MoreInline.DAL;
using MoreInline.MKT_Assets;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class OrdersDetail : System.Web.UI.Page
    {
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["AssetId"] != null)
                {
                    int AssetId = int.Parse(Request.QueryString["AssetId"].ToString());
                    LoadOrderAssetMasterData(AssetId);
                }
            }

        }

        protected void RptOrderAssets_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label FilePath = e.Item.FindControl("FilePath") as Label;
                Image ImgFile = e.Item.FindControl("ImgFile") as Image;

                string[] Extention = FilePath.Text.Split('.');

                if (Extention[1] == "pdf")
                {
                    ImgFile.ImageUrl = "~/Images/Icons/PdfLogo.png";
                }
                else if (Extention[1] == "rtf")
                {
                    ImgFile.ImageUrl = "~/Images/Icons/RtfLogo.png";
                }
                else if (Extention[1] == "doc")
                {
                    ImgFile.ImageUrl = "~/Images/Icons/DocLogo.png";
                }
                else
                {
                    ImgFile.ImageUrl = "~/Images/Icons/galleryLogo.png";
                }
            }
        }


        private void LoadOrderAssetMasterData(int Id)
        {
            //DataTable dt = OrderAsset.GetOrderAssetsMaster(Id);
            //if (dt.Rows.Count > 0)
            //{
            //    HfAssetId.Value = dt.Rows[0][0].ToString();

            //    TxtMessage.Text = dt.Rows[0][10].ToString();
            //    string AudioPath = dt.Rows[0][11].ToString();
            //    ChkProfilePic.Checked = Convert.ToBoolean(dt.Rows[0][17]);
            //    HfVoicePath.Value = AudioPath;
            //    string[] Mpath = AudioPath.Split('~');
            //    try { AudioPath = Mpath[1].ToString(); }
            //    catch (Exception) { AudioPath = ""; }
            //    Session["Patha"] = ".." + AudioPath;
            //    string script = "<script>SetAudio('.." + AudioPath + "')</script>";

            //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);

            //    DataTable Detaildt = OrderAsset.GetOrderAssetsDetail(Id);
            //    hffilecount.Value = Detaildt.Rows.Count.ToString();
            //    RptOrderAssets.DataSource = Detaildt;
            //    RptOrderAssets.DataBind();
            DataTable dt = OrderAsset.GetOrderAssetsMaster(Id);
            if (dt.Rows.Count > 0)
            {
                HfAssetId.Value = dt.Rows[0]["AssestID"].ToString();
                //TxtAdSubject.Text = dt.Rows[0]["Subject"].ToString();
                //TxtPhone.Text = dt.Rows[0]["Phone"].ToString();
                //TxtEmail.Text = dt.Rows[0]["Email"].ToString();
                //TxtLocation.Text = dt.Rows[0]["Location"].ToString();
                //TxtDate.Text = DateTime.Parse(dt.Rows[0]["Date"].ToString()).ToString("yyyy-MM-dd");
                //DdlHour.SelectedValue = dt.Rows[0]["Hours"].ToString();
                //DdlTT.SelectedValue = dt.Rows[0]["TimeSpan"].ToString();
                //TxtWebsite.Text = dt.Rows[0]["WebUrl"].ToString();
                TxtMessage.Text = dt.Rows[0]["Massage"].ToString();

                string AudioPath = dt.Rows[0]["AudioPath"].ToString();
                HfVoicePath.Value = AudioPath;
                //DdlTT.SelectedValue = dt.Rows[0]["TimeSpan"].ToString().Equals("am") ? "1" : "0";
                //ChkAddVoice.Checked = Convert.ToBoolean(dt.Rows[0]["AddAudio"]);
                //ChkAddContact.Checked = Convert.ToBoolean(dt.Rows[0]["AddContact"]);
                //ChkAddDirection.Checked = Convert.ToBoolean(dt.Rows[0]["AddDirection"]);
                //ChkAddReminder.Checked = Convert.ToBoolean(dt.Rows[0]["AddReminder"]);
                //chkAddUrl.Checked = Convert.ToBoolean(dt.Rows[0]["AddWebUrl"]);
                //ChkProfilePic.Checked = Convert.ToBoolean(dt.Rows[0]["AddUerProfile"]);


                string[] Mpath = AudioPath.Split('~');
                try { AudioPath = Mpath[1].ToString(); }
                catch (Exception) { AudioPath = ""; }
                // Session["Patha"] = ".." + AudioPath;

                DataTable Detaildt = OrderAsset.GetOrderAssetsDetail(Id);
                hffilecount.Value = Detaildt.Rows.Count.ToString();
                RptOrderAssets.DataSource = Detaildt;
                RptOrderAssets.DataBind();


            }
        }

        protected void BtnDownload_Click(object sender, EventArgs e)
        {

            Button btn = (Button)sender;
            string BtnID = btn.ID.ToString();
            int Id = int.Parse(btn.CommandArgument.ToString());
            string FileName = btn.CommandName;
            string Directory = Server.MapPath(FileName);

            try
            {
                FileInfo fileInfo = new FileInfo(Directory);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment;filename=" + fileInfo.Name);
                Response.AddHeader("Content-Length", fileInfo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.Flush();
                Response.WriteFile(fileInfo.FullName);
                Response.End();
            }
            catch (Exception)
            {
            }
        }

        protected void RptOrderAssets_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                Label FilePath = e.Item.FindControl("FilePath") as Label;
                string File = FilePath.Text;
                File = File.Replace("~/", "../");

                if (e.CommandName == "View")
                {
                    Response.Write("<script>window.open('" + File + "','_blank')</script>");
                }
            }
            catch (Exception)
            {
            }
        }
    }
}