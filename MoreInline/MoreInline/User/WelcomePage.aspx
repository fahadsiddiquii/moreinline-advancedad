﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="WelcomePage.aspx.cs" Inherits="MoreInline.User.WelcomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .button {
            width: 100%;
            height: 60px;
            font-weight: bold;
            font-size: larger;
            padding-top: 15px;
            color: #fff;
            background-color: #001b00;
            font-weight: bold;
        }

            .button:hover, .button:active, .button.hover {
                background-color: #A9C502;
                color: #001b00;
            }
    </style>
    <%-- ------------------------------------------------------------------------------------------------------------------------------------- --%>
        <script>
        $(document).ready(function () {
            LoadNotifications();
        });

        function LoadNotifications() {
            $.ajax({
                type: 'post',
                url: '../User/WelcomePage.aspx/GetNotifications',
                contentType: "application/json; charset=utf-8",
                data: {},
                datatype: "json",
                success: function (response) {
                    debugger;
                    for (var i = 0; i < response.d.length; i++) {
                      
                        updateNotificationCount2();
                    }
                    $('#headercount').text(response.d.length);

                    $('#dropdown').empty();
                    if (response.d.length == 0) {
                        $('#dropdown').append($('<li>Currently You Have No New Notifications.</li>'));
                    }
                    var items = [];
                    $.each(response.d, function (i, item) {
                        debugger;
                        items.push('<li><a href=' + item.Path + ' >  ' + item.Code + ' ' + item.Text + '</a></li>');
                    });
                    $('#dropdown').append(items.join(''));
                    $('.dropdown-toggle').attr("aria-expanded", "true")
                    $('#dropdown').attr("aria-expanded", "true")
                },
                error: function (error) {
                    debugger;
                    console.log(error);
                }
            })
        }

        function updateNotificationCount2() {
            debugger;
            $('span.count').show();
            var count = 0;
            count = parseInt($('span.count').html()) || 0;
            count++;
            PlaySound();
            // $('span.noti').css("color", "white");
            $('span.count').css({ "background-color": "red", "color": "white" });
            $('span.count').html(count);

        }

        function PlaySound() {
            var sound = document.getElementById("Naudio");
            sound.play()
        }

    </script>
    <%-- <center><h1 class="centered"><b>Welcome <asp:Label ID="LblUserName" runat="server" ></asp:Label></b></h1></center>--%>

    <asp:Panel ID="PnlAdminManager" runat="server" Visible="false">
        <div class="container">
            <div class="row m-t-40">
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <a href="../User/CreateCodes.aspx" class="btn button m-t-10">Create/View Codes</a>
                    <a href="../User/UserAccounts.aspx" class="btn button m-t-10">View Team Member</a>
                </div>
                <div class="col-lg-3">
                    <a href="../User/AddNewCustomer.aspx" class="btn button m-t-10">Add New Customer</a>
                    <a href="../User/ViewCustomers.aspx" class="btn button m-t-10">View Customers</a>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlQA" runat="server" Visible="false">
    </asp:Panel>
</asp:Content>
