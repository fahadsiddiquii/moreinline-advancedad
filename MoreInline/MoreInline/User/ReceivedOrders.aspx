﻿<%@ Page Title="Received Orders" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ReceivedOrders.aspx.cs" Inherits="MoreInline.User.ReceivedOrders" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>


    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #A9C502;"><b>Received Orders</b></h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>

            <%--   <div class="col-lg-12 text-right">
                <asp:Button ID="BtnAdd" runat="server" Text="Add New" CssClass="btn btn-info" />
            </div>--%>
        </div>


        <br />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">

                <asp:Repeater ID="RptReceivedOrder" runat="server" OnItemDataBound="RptReceivedOrder_ItemDataBound" OnItemCommand="RptReceivedOrder_ItemCommand">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">Order #</th>
                                    <th class="text-center text-black">Full Name</th>
                                    <th class="text-center text-black">Title</th>
                                    <th class="text-center text-black">Date/Time Ordered</th>
                                    <th class="text-center text-black">Status</th>
                                    <th class="text-center text-black">Select Designer</th>
                                    <th class="text-center text-black">Select QA</th>
                                    <th class="text-center text-black">Action</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>

                        <tr>
                            <td class="text-center text-white">
                                <asp:Label ID="lblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FullName") %>' />
                                <asp:HiddenField runat="server" ID="hfCustID" Value='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ApprovalStatus") %>' />
                                <asp:Label ID="LblLogID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem,"ID") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:DropDownList runat="server" ID="ddlUsers" CssClass="form-control" Style="background-color: #A9C502; color: #000; height: 20px; padding: 0px;">
                                </asp:DropDownList>
                                <asp:HiddenField runat="server" ID="HfDid" Value='<%#DataBinder.Eval(Container.DataItem,"Dids") %>' />
                            </td>
                            <td class="text-center text-white">
                                <asp:DropDownList runat="server" ID="ddlQAs" CssClass="form-control" Style="background-color: #A9C502; color: #000; height: 20px; padding: 0px;">
                                </asp:DropDownList>
                                <asp:HiddenField runat="server" ID="HfQAID" Value='<%#DataBinder.Eval(Container.DataItem,"QID") %>' />
                            </td>
                            <td class="text-center text-white">

                                <asp:Button ID="ImgPreview" ToolTip="Send" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' class="btn btn-sm btn-green" ForeColor="#ffffff" BackColor="#000000" Text="Send" CommandName="Send" />

                            </td>
                        </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>


    <%----------------------------------------- Pop Up -------------------------------------------------------%>
</asp:Content>
