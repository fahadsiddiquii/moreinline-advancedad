﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class UserAccounts : System.Web.UI.Page
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        UserSignupBOL ObjUserSignupBOL = new UserSignupBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = false;
            if (!IsPostBack)
            {
                Bind();
            }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        public void Bind()
        {
            DataTable dt = ObjUserSignupDAL.GetUserAccounts(ObjUserSignupBOL);

            RptViewUsers.DataSource = dt;
            RptViewUsers.DataBind();
        }

        protected void RptViewUsers_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    RepeaterItem item = e.Item;

                    Label LblIsActive = item.FindControl("LblIsActive") as Label;
                    ImageButton IBtnCheck = item.FindControl("IBtnCheck") as ImageButton;
                    ImageButton IBtnCross = item.FindControl("IBtnCross") as ImageButton;

                    if (LblIsActive.Text == "True")
                    {
                        IBtnCheck.Visible = true;
                        IBtnCross.Visible = false;
                    }
                    else
                    {
                        IBtnCross.Visible = true;
                        IBtnCheck.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void RptViewUsers_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int UserID = int.Parse(e.CommandArgument.ToString());
                Label LblPhoto = e.Item.FindControl("LblPhoto") as Label;

                if (e.CommandName == "ViewPic")
                {
                    if (LblPhoto.Text != "")
                    {
                        ImgProfile.ImageUrl = "~/upload/" + LblPhoto.Text;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                    else
                    {
                        MessageBox(div1, LblMsg, "Profile Picture not Exist", 0);
                    }
                }
                else if (e.CommandName == "Edit")
                {
                    Response.Redirect("~/User/UserProfile.aspx?UserID=" + UserID, false);
                }
                else if (e.CommandName == "Active")
                {
                    try
                    {
                        ObjUserSignupBOL.UserID = UserID;
                        ObjUserSignupBOL.IsActive = 0;
                        ObjUserSignupBOL.UpdatedBy = int.Parse(Session["UserID"].ToString());
                        try
                        {
                            Conn = DBHelper.GetConnection();
                            Trans = Conn.BeginTransaction();
                            if (ObjUserSignupDAL.UpdateAccountActivation(ObjUserSignupBOL, Trans, Conn) == false)
                            {
                                Trans.Rollback();
                                Conn.Close();

                                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                            }
                            else
                            {
                                Trans.Commit();
                                Conn.Close();
                                Bind();

                                MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                            }
                        }
                        catch (Exception)
                        {
                            Trans.Rollback();
                            Conn.Close();

                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }

                    }
                    catch (Exception ex)
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, ex.Message, 0);
                    }
                }
                else if (e.CommandName == "InActive")
                {
                    try
                    {
                        ObjUserSignupBOL.UserID = UserID;
                        ObjUserSignupBOL.IsActive = 1;
                        ObjUserSignupBOL.UpdatedBy = int.Parse(Session["UserID"].ToString());
                        try
                        {
                            Conn = DBHelper.GetConnection();
                            Trans = Conn.BeginTransaction();
                            if (ObjUserSignupDAL.UpdateAccountActivation(ObjUserSignupBOL, Trans, Conn) == false)
                            {
                                Trans.Rollback();
                                Conn.Close();
                                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                            }
                            else
                            {
                                Trans.Commit();
                                Conn.Close();
                                Bind();
                                MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                            }
                        }
                        catch (Exception)
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                        }
                    }
                    catch (Exception ex)
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, ex.Message, 0);
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}