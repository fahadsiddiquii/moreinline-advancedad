﻿<%@ Page Title="Offer Website" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="Offerwebsite.aspx.cs" Inherits="MoreInline.User.Offerwebsite" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../Websites/css/websites_style.css" rel="stylesheet" />
    <style>
        .LabelSpan {
            background-color: #f6f7f8 !important;
            border: #f6f7f8 !important;
        }
    </style>

    <div class="container">

        <div class="row justify-content-center">
            <div class="col-lg-7 text-center">
                <h3 class="p-l-18" style="color: #019c7c;">Create Website For Customers</h3>
            </div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label>
                    </strong>
                </div>
                <br />
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Customer:&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <asp:DropDownList runat="server" ID="ddlCustomers" CssClass="form-control" Style="background-color: #007f9f; color: #fff;"></asp:DropDownList>
                </div>
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Upload Logo:</span>
                    <asp:FileUpload runat="server" ID="FuLogo" CssClass="form-control m-t-5 txtBox-style" />
                </div>
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Upload Image:&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <asp:FileUpload runat="server" ID="FuImage" CssClass="form-control m-t-5 txtBox-style" />
                </div>
                <%--<div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Enter Website Name:</span>
                    <asp:TextBox ID="txtUrl" runat="server" required CssClass="form-control m-t-5" placeholder="Enter Website Name"></asp:TextBox>
                </div>--%>
                <div class="col-lg-12 text-right m-t-10 m-b-10">
                    <asp:Button ID="BtnSend" Text="Create Site" runat="server" OnClick="BtnSend_Click" CssClass="btn btn-sm btn-light-green" />
                </div>
            </div>
            <%--<div class="col-lg-5">
                <div class="input-group m-t-10" >
                    <span class="input-group-addon LabelSpan">Upload Template:</span>
                    <asp:FileUpload runat="server" AllowMultiple="true" ID="FuTemplate" CssClass="form-control m-t-5 txtBox-style" />
                </div>
            </div>--%>
        </div>

        <%--<asp:TextBox ID="txtIntrotext" TextMode="MultiLine" required runat="server" Rows="5" CssClass="form-control textarea-resize m-t-10" placeholder="Enter Banner Text.." EnableTheming="True"></asp:TextBox>--%>

       
        
    </div>
</asp:Content>
