﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class CreateCodes : System.Web.UI.Page
    {
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            div1.Visible = div2.Visible = false;
            if (!IsPostBack)
            {
                Bind();
            }
        }

        public void BindDdlCodeType()
        {
            DdlCodeType.Items.Insert(0, new ListItem("Select Code Type", "0"));
            DdlCodeType.Items.Insert(1, new ListItem("MX11", "1"));
            DdlCodeType.Items.Insert(2, new ListItem("IN001", "2"));
            DdlCodeType.Items.Insert(3, new ListItem("MM02", "3"));
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void DdlCodeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                OnChangeCodeType();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void OnChangeCodeType()
        {
            try
            {
                LblCodeName.Text = "";
                if (ViewState["State"].ToString() == "New")
                {
                    if (DdlCodeType.SelectedValue != "0")
                    {
                        // GetOrderNo();
                        GetCodeNumber();
                    }
                    else { MessageBox(div2, LblMsg1, "Select Code Type", 0); }

                    switch (DdlCodeType.SelectedValue)
                    {
                        case "1":
                            LblCodeName.Text = "Free Trial Code";
                            break;
                        case "2":
                            LblCodeName.Text = "Friend Code";
                            break;
                        case "3":
                            LblCodeName.Text = "Sales Code";
                            break;
                    }

                    //if (DdlCodeType.SelectedValue == "2")
                    //{
                    //    DdlPaid.Visible = true;
                    //    DdlPaid.SelectedValue = "0";
                    //}
                    //else
                    //{
                    //    DdlPaid.Visible = false;
                    //    DdlPaid.SelectedValue = "0";
                    //}
                }
                else if (ViewState["State"].ToString() == "Edit")
                {
                    string[] OrderNo = TxtFullCode.Text.Split('-');

                    TxtFullCode.Text = DdlCodeType.SelectedItem.Text + "-" + OrderNo[1];

                    //if (DdlCodeType.SelectedValue == "2")
                    //{
                    //    DdlPaid.Visible = true;
                    //    DdlPaid.SelectedValue = "0";
                    //}
                    //else
                    //{
                    //    DdlPaid.Visible = false;
                    //    DdlPaid.SelectedValue = "0";
                    //}
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void ClearControls()
        {
            TxtFullCode.Text = "";
            TxtValidDays.Text = "";
            DdlCodeType.SelectedValue = "0";
            ViewState["State"] = null;
        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControls();
                ViewState["State"] = "New";
                DdlCodeType.Items.Clear();
                BindDdlCodeType();

                BtnCreate.Visible = true;
                BtnSave.Visible = false;
                //GetOrderNo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }


        //public void GetOrderNo()
        //{
        //    try
        //    {
        //        DataTable dt;

        //        dt = ObjCodeMasterDAL.GetCodeNo(ObjCodeMasterBOL);
        //        if (dt.Rows.Count > 0)
        //        {
        //            string[] OrderNo = dt.Rows[0]["FullCode"].ToString().Split('-');

        //            string Number = OrderNo[OrderNo.Length - 1];
        //            int Code = int.Parse(Number) + 1;

        //            for (int i = 0; i < Number.Length; i++)
        //            {
        //                if (Code.ToString().Length < 5)
        //                {
        //                    Value += Number[i];
        //                    Length = Value.Length + Code.ToString().Length;
        //                }
        //                else
        //                {
        //                    Length = Code.ToString().Length;
        //                }
        //                if (Length >= 5)
        //                {
        //                    TxtFullCode.Text = DdlCodeType.SelectedItem.Text + "-" + Value + Code.ToString();
        //                    break;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            TxtFullCode.Text = DdlCodeType.SelectedItem.Text + "-00001";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox(div2, LblMsg1, ex.Message, 0);
        //    }
        //}

        public void GetCodeNumber()
        {
            int temp;
            do
            {
                Random RandomNumber = new Random();
                int number = RandomNumber.Next(10000, 99999);
                string checkNumber = "SELECT COUNT(*) FROM Tbl_CodeMaster WHERE CodeNumber= "+number;
                SqlCommand UserExist = new SqlCommand(checkNumber, DBHelper.GetConnection());
                int UserNameObj = Convert.ToInt32(UserExist.ExecuteScalar());

                if (UserNameObj != 0)
                {
                    temp = 1;
                }
                else
                {
                    TxtFullCode.Text = DdlCodeType.SelectedItem.Text + "-" + number.ToString();
                    temp = 0;
                }
            }
            while (temp == 1);
        }

        public void SaveEntries()
        {
            try
            {
                //@CodeNumber,@CodeText,@FullCode,@ValidationDays,@IsPaid,@CreatedBy

                #region New
                string[] OrderNo = TxtFullCode.Text.Split('-');

                ObjCodeMasterBOL.CodeNumber = OrderNo[1];
                ObjCodeMasterBOL.CodeText = OrderNo[0];
                ObjCodeMasterBOL.FullCode = TxtFullCode.Text;
                ObjCodeMasterBOL.ValidationDays = TxtValidDays.Text;
                ObjCodeMasterBOL.IsUtilized = 0;
                //ObjCodeMasterBOL.IsPaid = int.Parse(DdlPaid.SelectedValue);
                ObjCodeMasterBOL.CreatedBy = int.Parse(Session["UserID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCodeMasterDAL.InsertCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    ClearControls();
                    MessageBox(div1, LblMsg, "Code Created Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public bool Local_Validation()
        {
            if (DdlCodeType.SelectedValue == "0")
            {
                MessageBox(div2, LblMsg1, "Please Select Code Type", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                return false;
            }
            else if (TxtValidDays.Text == "")
            {
                MessageBox(div2, LblMsg1, "Enter Days", 0);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void BtnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Local_Validation() == true)
                {
                    //if (DdlCodeType.SelectedValue == "2")
                    //{
                    //    if (DdlPaid.SelectedValue == "0")
                    //    {
                    //        MessageBox(div2, LblMsg1, "Please Select IsPaid", 0);
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    //        return;
                    //    }
                    //}
                    SaveEntries();
                    Bind();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg1, ex.Message, 0);
            }
        }

        public void Bind()
        {
            DataTable dt = ObjCodeMasterDAL.GetCodeMaster(ObjCodeMasterBOL);

            RptViewCodes.DataSource = dt;
            RptViewCodes.DataBind();
        }

        protected void RptViewCodes_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                int CodeID = int.Parse(e.CommandArgument.ToString());
                ViewState["CodeID"] = CodeID;
                Label LblFullCode = e.Item.FindControl("LblFullCode") as Label;
                Label LblValidationDays = e.Item.FindControl("LblValidationDays") as Label;
                Label LblCodeText = e.Item.FindControl("LblCodeText") as Label;
                Label LblIsPaid = e.Item.FindControl("LblIsPaid") as Label;


                if (e.CommandName == "Edit")
                {
                    ViewState["State"] = "Edit";

                    DdlCodeType.Items.Clear();
                    BindDdlCodeType();

                    TxtFullCode.Text = LblFullCode.Text;
                    TxtValidDays.Text = LblValidationDays.Text;
                    DdlCodeType.Items.FindByText(LblCodeText.Text).Selected = true;
                    OnChangeCodeType();

                    //if (DdlCodeType.SelectedValue == "2")
                    //{ DdlPaid.SelectedValue = LblIsPaid.Text; }

                    BtnCreate.Visible = false;
                    BtnSave.Visible = true;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Local_Validation() == true)
                {
                    //if (DdlCodeType.SelectedValue == "2")
                    //{
                    //    if (DdlPaid.SelectedValue == "0")
                    //    {
                    //        MessageBox(div2, LblMsg1, "Please Select IsPaid", 0);
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    //        return;
                    //    }
                    //}
                    UpdateEntries();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg1, ex.Message, 0);
            }
        }

        public void UpdateEntries()
        {
            try
            {
                //@CodeNumber,@CodeText,@FullCode,@ValidationDays,@IsPaid,@CreatedBy

                #region New
                string[] OrderNo = TxtFullCode.Text.Split('-');

                ObjCodeMasterBOL.CodeText = OrderNo[0];
                ObjCodeMasterBOL.CodeNumber = OrderNo[1];
                ObjCodeMasterBOL.FullCode = TxtFullCode.Text;
                ObjCodeMasterBOL.ValidationDays = TxtValidDays.Text;
                //ObjCodeMasterBOL.IsPaid = int.Parse(DdlPaid.SelectedValue);
                ObjCodeMasterBOL.UpdatedBy = int.Parse(Session["UserID"].ToString());
                ObjCodeMasterBOL.CodeID = int.Parse(ViewState["CodeID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCodeMasterDAL.UpdateCodeMaster(ObjCodeMasterBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    ClearControls();
                    ViewState["CodeID"] = null;
                    Bind();
                    MessageBox(div1, LblMsg, "Code Updated Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div2, LblMsg1, "Error Occurred While Saving", 0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox(div2, LblMsg1, ex.Message, 0);
            }
        }
    }
}