﻿<%@ Page Title="User Limits" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="UserLimits.aspx.cs" Inherits="MoreInline.User.UserLimits" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function HideLabel() {
            var seconds = 9;
            setTimeout(function () {
                document.getElementById("<%=div1.ClientID %>").style.display = "none";
            }, seconds * 1000);
        }
    </script>
    <%-- ------------------------------------------------------------------------------- Page ---------------------------------------------------------------- --%>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 text-center">
            <h3 class="p-l-18" style="color: #019c7c; font-family: 'Microsoft JhengHei'"><b>User Limits</b></h3>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row p-t-10">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
            <br />
            <%--UserRoleID	UserRoles	UserLimits--%>
            <asp:Repeater ID="RptUserLimits" runat="server">
                <HeaderTemplate>
                    <table id="example1" class="table table-bordered" border="0">
                        <thead class="table-head">
                            <tr style="background-color: #A9C502">
                                <th class="text-center">SNo.</th>
                                <th class="text-center">Roles</th>
                                <th class="text-center">User Limits</th>
                                <th class="text-center">User Utilized</th>
                            </tr>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                            <asp:Label ID="LblUserRoleID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserRoleID") %>' Visible="false" />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblUserRoles" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserRoles") %>' />
                        </td>
                        <td class="text-center">
                            <asp:TextBox ID="TxtUserLimits" runat="server" Width="55px" CssClass="form-control center" TextMode="Number"
                                Text='<%#DataBinder.Eval(Container.DataItem,"UserLimits") %>' OnTextChanged="TxtUserLimits_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblLimits" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Limits") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="col-lg-1"></div>
    </div>
</asp:Content>
