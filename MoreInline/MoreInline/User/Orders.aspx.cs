﻿
using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class Orders : System.Web.UI.Page
    {
        OrdersDAL Order = new OrdersDAL();
        OrderAssetsDAL orderAssetsDal = new OrderAssetsDAL();
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        DesignBOL DBOL = new DesignBOL();
        public string HiddenClassName { get; private set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserID"] != null)
                {
                    if (Request.QueryString["id"] != null)
                    {
                        int id = int.Parse(Request.QueryString["id"].ToString());
                        NotiDAL.UpdateNoti(id);
                    }
                    int UserID = int.Parse(Session["UserID"].ToString());
                    RptReceivedOrder.DataSource = Order.GetOrdersForDesigners(UserID);
                    RptReceivedOrder.DataBind();
                    if (Session["UserRoles"].ToString() == "Manager")
                    {
                        HiddenClassName = "display:none";
                    }
                }

            }

        }
        protected void RptReceivedOrder_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string CommandName = e.CommandName.ToString();
            string Id = e.CommandArgument.ToString();
           string Cust_ID = (e.Item.FindControl("Cust_ID") as Label).Text;
            string OrderNum = (e.Item.FindControl("lblOrderNum") as Label).Text;
            if (CommandName.Equals("Detail"))
            {
                Response.Redirect("~/User/OrdersDetail.aspx?AssetId=" + Id);
            }
            else if (CommandName.Equals("Upload"))
            {
                hfid.Value = Id;
                hfQid.Value = Cust_ID;
                lblCode.Text = OrderNum;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
            }
            else if (CommandName.Equals("btnimg"))
            {
                ImageButton FilePath = e.Item.FindControl("FilePath") as ImageButton;
                string File = FilePath.ImageUrl;
                File = File.Replace("~/", "../");
                imgpop.Src = File;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal2();", true);
            }
            else if (CommandName.Equals("custstatus"))
            {
                Label custapp = e.Item.FindControl("LblCustApproval") as Label;
                lblCA.InnerText = custapp.Text;
                Button CustRemarks = e.Item.FindControl("Btnstuts") as Button;
                txtCr.Text = CustRemarks.CommandArgument;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal3();", true);
            }
        }
        protected void RptReceivedOrder_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                RepeaterItem item = e.Item;
                Label lblApprovalStatus = item.FindControl("lblApprovalStatus") as Label;
                Label lblcustappid = item.FindControl("lblcustappid") as Label;
                Label LblCustRemarks = item.FindControl("LblCustRemarks") as Label;
                
                Button btnUpload = item.FindControl("BtnUpload") as Button;
                Button Btnstuts = item.FindControl("Btnstuts") as Button;
                ImageButton FilePath = item.FindControl("FilePath") as ImageButton;
                HtmlControl th_upload = item.FindControl("th_upload") as HtmlControl;
                HtmlControl td_upload = item.FindControl("td_upload") as HtmlControl;

                if (Session["UserRoles"].ToString() == "Manager")
                { td_upload.Visible = false; }

                if (string.IsNullOrWhiteSpace(FilePath.ImageUrl))
                {
                    FilePath.ImageUrl = "../Images/Inprocess.jpg";
                }

                if (LblCustRemarks.Text.Equals("null") || string.IsNullOrWhiteSpace(LblCustRemarks.Text))
                {
                    Btnstuts.Visible = false;
                }
                else
                {
                    Btnstuts.Visible = true;
                }
                string Status = lblApprovalStatus.Text;
                if (Status.Equals("2"))
                {
                    if (lblcustappid.Text.Equals("2"))
                    {
                        lblApprovalStatus.Text = "Requested for Modification";
                    }
                    else
                    {
                        lblApprovalStatus.Text = "Delivered";
                    }
                }
                else if (Status.Equals("3"))
                {
                    if (lblcustappid.Text.Equals("1"))
                    {
                        lblApprovalStatus.Text = "Approved";
                        btnUpload.Enabled = false;
                        btnUpload.BackColor = System.Drawing.ColorTranslator.FromHtml("#eb4034");
                        btnUpload.Text = btnUpload.ToolTip = "Uploaded";
                    }
           
                  
                }
                else if (Status.Equals("1"))
                {
                    lblApprovalStatus.Text = "Received";
                    lblApprovalStatus.Attributes["class"] = "text-success";
                    lblApprovalStatus.Attributes["style"] = "font-weight:bold;";
                }
           

            }
        }
        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnUoload_Click(object sender, EventArgs e)
        {
            try
            {
                string strFileName = "";
                string strFilePath = "";
                string FilePath = "";
                string strFolder = "";
                int AssetId = int.Parse(hfid.Value);
                int CustId = int.Parse(hfQid.Value);
                if (AddFile.HasFile)
                {

                    strFolder = Server.MapPath("~/OrderDesigned/");
                    strFileName = AddFile.FileName;
                    int id = CustId;
                    string[] NameContruct = strFileName.Split('.');
                    NameContruct[0] += "-" + id + "-" + DateTime.Now.Second + "";
                    strFileName = NameContruct[0].ToString() + "." + NameContruct[1].ToString();
                    strFileName = Path.GetFileName(strFileName);

                    if (!Directory.Exists(strFolder))
                    {
                        Directory.CreateDirectory(strFolder);
                    }
                    strFilePath = strFolder + strFileName;

                    if (File.Exists(strFilePath))
                    {
                        MessageBox(div1, LblMsg, "File Name Exists", 0);
                        return;
                    }
                    else
                    {
                        try
                        {
                            AddFile.SaveAs(strFilePath);
                            FilePath = "~/OrderDesigned/" + strFileName;
                        }
                        catch (IOException) { MessageBox(div1, LblMsg, "System Faced Error Try Later", 0); }
                    }
                }
                else
                {
                    MessageBox(div2, LblMsg1, "Please Upload File", 0);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    return;
                }

                if (!string.IsNullOrWhiteSpace(FilePath))
                {
                    int UserID = int.Parse(Session["UserID"].ToString());
                    DBOL.AssestID = AssetId;
                    DBOL.CreatedByID = UserID;
                    DBOL.FilePath = FilePath;
                    DBOL.QAID = 1;
                    DBOL.QaApproval = 1;
                    DBOL.MngrApproval = 1;
                    DBOL.CustApproval = 0;
                    DBOL.QaRemarks = "Approved";
                    DBOL.MngrRemarks = "Approved";
                    DBOL.CustRemarks = "";
                    DataTable dt = Order.CheckDesign(AssetId);
                    if (dt.Rows.Count > 0)
                    {
                        if (Order.UpdateOrderdDesign(AssetId, FilePath))
                        {
                            string file = dt.Rows[0][1].ToString();
                            string Directory = Server.MapPath(file);
                            FileInfo fileInfo = new FileInfo(Directory);
                            //if (fileInfo.Exists)
                            //{
                            //    fileInfo.Delete();
                            //}
                            orderAssetsDal.UpdateOrderStatus(AssetId, 2);
                            NotificationInfo Ninfo = new NotificationInfo();
                            Ninfo.OID = AssetId;
                            Ninfo.UID = CustId;
                            Ninfo.IsViewed = false;
                            Ninfo.UserRoleID = -1;
                            Ninfo.NotiDate = DateTime.Now;
                            Ninfo.FormPath = "../MKT_Assets/ViewAssets.aspx";
                            NotiDAL.InsertNotification(Ninfo);
                            SendEmailNotification(CustId, AssetId);
                            RptReceivedOrder.DataSource = Order.GetOrdersForDesigners(UserID);
                            RptReceivedOrder.DataBind();
                            MessageBox(div1, LblMsg, "File Uploaded !", 1);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "File Upload Failed", 0);
                        }
                    }
                    else
                    {
                        if (Order.InsertOrderdDesign(DBOL))
                        {
                            orderAssetsDal.UpdateOrderStatus(AssetId, 1);
                            NotificationInfo Ninfo = new NotificationInfo();
                            Ninfo.OID = AssetId;
                            Ninfo.UID = CustId;
                            Ninfo.IsViewed = false;
                            Ninfo.UserRoleID = -1;
                            Ninfo.NotiDate = DateTime.Now;
                            Ninfo.FormPath = "../MKT_Assets/ViewAssets.aspx";
                            NotiDAL.InsertNotification(Ninfo);

                            SendEmailNotification(CustId, AssetId);

                            RptReceivedOrder.DataSource = Order.GetOrdersForDesigners(UserID);
                            RptReceivedOrder.DataBind();
                            MessageBox(div1, LblMsg, "File Uploaded !", 1);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "File Upload Failed", 0);
                        }
                    }

                }
            }
            catch (Exception)
            {

            }

        }

        private void SendEmailNotification(int custId,int AssetId)
        {
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            // EmailNotification.SendNotification(EN.GetAdminFilledObject());
            #endregion


            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = custId;
            DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);
            DataTable AssetInfo = orderAssetsDal.GetOrderAssetsMaster(AssetId);
            if (CustData.Rows[0]["IsDesignCrActive"].ToString().Equals("True"))
            {
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.ButtonText = "Review Your Order";
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.NotificationType = "IsDesignCrActive";
                EmailData.Content = "Your order #"+ AssetInfo.Rows[0]["OrderNum"].ToString()+ " is ready for your review. Please accept the delivery or request a revision if needed";
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = " Your order is ready for your review ";
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData); 
            }
            #endregion
        }
    }
}