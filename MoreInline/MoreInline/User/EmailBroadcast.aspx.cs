﻿using MoreInline.BOL;
using MoreInline.DAL;
using MoreInline.MKT_Assets;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class EmailBroadcast : System.Web.UI.Page
    {
        CustomerProfileMasterDAL ObjCustProfile = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL Customer = new CustomerProfileMasterBOL();
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindRepeater();
            }
        }


        private void BindRepeater()
        {
            DataTable dtCust = ObjCustProfile.GetCustomerProfile();
            RptContacts.DataSource = dtCust;
            RptContacts.DataBind();
        }

        protected void RptContacts_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Label lblSubscribed = e.Item.FindControl("lblSubscribed") as Label;
                CheckBox ChkRow = e.Item.FindControl("ChkRow") as CheckBox;
                if (lblSubscribed.Text.Equals("True"))
                {
                    lblSubscribed.Text = "Subscribed";
                    ChkRow.Enabled = true;
                }
                else
                {
                    lblSubscribed.Text = "Unsubscribed";
                    ChkRow.Enabled = false;

                }
            }
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TxtAdSubject.Text) && !string.IsNullOrWhiteSpace(TxtMessage.Text))
            {
                #region main
                try
                {
                    int Checked = 0;
                    foreach (RepeaterItem item in RptContacts.Items)
                    {
                        
                        CheckBox ChkRow = (CheckBox)item.FindControl("ChkRow");
                        if (ChkRow.Checked == true)
                        {
                            Checked++;
                            Label CustID = (Label)item.FindControl("LblCustID");
                            Label LblEmail = (Label)item.FindControl("LblEmail");
                            Label LblName = (Label)item.FindControl("LblName");
                            Label lblSubscribed = (Label)item.FindControl("lblSubscribed");
                            Label LblCompany = (Label)item.FindControl("LblCompany");
                            EmailNotification EN = new EmailNotification();
                            #region Email For Customer
                            Customer.Cust_ID = int.Parse(CustID.Text);
                            DataTable CustData = ObjCustProfile.GetCustomerProfile(Customer);
                            if (CustData.Rows[0]["IsPromoActive"].ToString().Equals("True"))
                            {
                                EmailNotification EmailData = EN.GetAdminFilledObject();
                                EmailData.SenderIdForUnSubscription = Customer.Cust_ID;
                                EmailData.NotificationType = "IsPromoActive";
                                EmailData.Content = TxtMessage.Text;
                                EmailData.ReceiverEmailAddress = LblEmail.Text;
                                EmailData.EmailSubject = TxtAdSubject.Text;
                                EmailData.ReceiverName = LblName.Text;
                                if (!FUImage.HasFile) {
                                    EmailNotification.SendNotification(EmailData);
                                }
                                else
                                {
                                    string strFileName = "";
                                    string strFolder = "";
                                    string strFilePath = "";
                                    strFolder = Server.MapPath("~/upload/");
                                    strFileName = FUImage.FileName;
                                    string[] NameContruct = strFileName.Split('.');
                                    NameContruct[0] += "-" + Convert.ToString((new Random()).Next(9999)) + "-" + DateTime.Now.Second + "";
                                    strFileName = NameContruct[0].ToString() + "." + NameContruct[1].ToString();
                                    strFileName = Path.GetFileName(strFileName);
                                    strFilePath = strFolder + strFileName;
                                    FUImage.SaveAs(strFilePath);
                                    string Body = ContructAttachemnet(EmailData, CustData, "/upload/"+strFileName);
                                    EmailNotification.SendEmail(Body, LblEmail.Text, TxtAdSubject.Text, EmailData.CompanyName);
                                }
                            }
                            #endregion

                            MessageBox(div1, LblMsg, "Email has been sent successfully", 1);
                        }
                    }

                    if (Checked == 0)
                    {
                        MessageBox(div1, LblMsg, "Select at least one to send", 0);
                    }
                }
                catch (Exception ex)
                { MessageBox(div1, LblMsg, ex.Message, 0); }
                #endregion
            }
            else
            {
                MessageBox(div1, LblMsg, "Email Subject & Message is Required ", 0);
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div1.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        public string ContructAttachemnet(EmailNotification NotificationInfo, DataTable dtCust,string imagepath)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerAdEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            string UnsubscriptionPath = "https://moreinline.com/Emails/Unsubscribe.aspx?CustID=" + NotificationInfo.SenderIdForUnSubscription.ToString() + "&Type=" + NotificationInfo.NotificationType;
         
            body = body.Replace("{image}", application_path + imagepath);
            body = body.Replace("{message}", NotificationInfo.Content);
            body = body.Replace("{location}", NotificationInfo.CompanyAddress);
            body = body.Replace("{custcontact}", NotificationInfo.Contact);
            body = body.Replace("{custweb}", NotificationInfo.Website);
            body = body.Replace("{custemail}", NotificationInfo.CompanyEMailAddress);
            body = body.Replace("{Fishing}", "display:inline;");
            body = body.Replace("{unsubLink}", UnsubscriptionPath);
            body = body.Replace("{unsubStyle}", "display:inline;text-decoration:none;color:#000000;");
            body = body.Replace("{voiceVisible}", "display:none");
            body = body.Replace("{contactVisible}", "display:none");
            body = body.Replace("{directionVisible}", "display:none");
            body = body.Replace("{reminderVisible}", "display:none");
            body = body.Replace("{webVisible}", "display:none");
            return body;
        }
    }


}