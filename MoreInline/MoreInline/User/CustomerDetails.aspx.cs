﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class CustomerDetails : System.Web.UI.Page
    {
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["CustID"] != null)
            {
                int CustID = int.Parse(Request.QueryString["CustID"].ToString());
                LoadData(CustID);
            }
        }
        private  void LoadData(int CustID)
        {
            DataSet Ds = OrderAsset.GetCustDetail(CustID);
            DataTable dt1 = Ds.Tables[0] as DataTable;
            DataTable dt2 = Ds.Tables[1] as DataTable;
            string FilePath ="../upload/"+ dt1.Rows[0]["Photo"].ToString();
            img.Src = FilePath;
            fname.InnerText = dt1.Rows[0]["FirstName"].ToString();
            lname.InnerText = dt1.Rows[0]["LastName"].ToString();
            company.InnerText = dt1.Rows[0]["Company"].ToString();
            address.InnerText = dt1.Rows[0]["Address"].ToString();
            zipcode.InnerText = dt1.Rows[0]["ZipCode"].ToString();
            email.InnerText = dt1.Rows[0]["Email"].ToString();
            contact.InnerText = dt1.Rows[0]["Phone"].ToString();
            joiningdate.InnerText = dt1.Rows[0]["CreatedOn"].ToString();
            actype.InnerText = dt1.Rows[0]["AccType"].ToString();
            elimit.InnerText = dt1.Rows[0]["EmailsLimit"].ToString();
           
            rptInvoices.DataSource = dt2;
            rptInvoices.DataBind();

        }

        protected void rptInvoices_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    Label custName = e.Item.FindControl("custName") as Label;
                    custName.Text = fname.InnerText;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}