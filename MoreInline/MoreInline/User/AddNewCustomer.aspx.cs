﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class AddNewCustomer : System.Web.UI.Page
    {
        CodeMasterDAL ObjCodeMasterDAL = new CodeMasterDAL();
        CodeMasterBOL ObjCodeMasterBOL = new CodeMasterBOL();

        AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
        AdvertisementBOL ObjAdvertisementBOL = new AdvertisementBOL();

        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                
                if (!IsPostBack)
                {
                    if (Request.QueryString["Email"] != null)
                    {
                        TxtEmail.Text = Request.QueryString["Email"].ToString();
                    }
                    Bind();
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
        public void Bind()
        {
            //DataTable Pending_dt = ObjCustomerProfileMasterDAL.GetViewPendingCustomers(ObjCustomerProfileMasterBOL);
            DataTable dt = ObjCodeMasterDAL.GetCodeMaster(ObjCodeMasterBOL);
            
            //if (Pending_dt.Rows.Count > 0)
            //{
            //    for (int j = 0; j < Pending_dt.Rows.Count; j++)
            //    {
            //        for (int i = 0; i < dt.Rows.Count; i++)
            //        {
            //            if (Pending_dt.Rows[j]["CodeID"].ToString() == dt.Rows[i]["CodeID"].ToString())
            //            {
            //                dt.Rows[i].Delete();
            //                dt.AcceptChanges();
            //            }
            //        }
            //    }
            //}

            DdlCode.DataSource = dt;
            DdlCode.DataTextField = "FullCode";
            DdlCode.DataValueField = "CodeID";
            DdlCode.DataBind();
            DdlCode.Items.Insert(0, new ListItem("Select User Type", "0"));

            if (dt.Rows.Count == 0)
            {
                MessageBox(div1, LblMsg, "All Promo Code Are Used Please Create New One ", 0);
            }
        }

        public bool Validation()
        {
            if (TxtCName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Company", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else if (DdlCode.SelectedValue == "0")
            {
                MessageBox(div1, LblMsg, "Select Code", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = ObjAdvertisementDAL.CheckEmail(TxtEmail.Text);
                DataTable dtCust = ObjAdvertisementDAL.CheckActiveEmail(TxtEmail.Text);
                if (dt.Rows.Count > 0)
                {
                    MessageBox(div1, LblMsg, "Email already exist", 0);
                    return;
                }
                else if (dtCust.Rows.Count > 0)
                {
                    MessageBox(div1, LblMsg, "This email is already an active member.", 0);
                    return;
                }
                else
                {
                    if (ViewState["AppointmentAds"] == null || ViewState["AppointmentAds"].ToString() == "")
                    {
                        if (UploadAppointment.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadAppointment.PostedFile.FileName);
                            ViewState["AppointmentAds"] = fileName;
                            UploadAppointment.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadAppointment.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Appointment Ad", 0);
                            return;
                        }
                    }

                    if (ViewState["LockedAppointmentAds"] == null || ViewState["LockedAppointmentAds"].ToString() == "")
                    {
                        if (UploadLockAppointment.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadLockAppointment.PostedFile.FileName);
                            ViewState["LockedAppointmentAds"] = fileName;
                            UploadLockAppointment.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadLockAppointment.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Locked Appointment Ad", 0);
                            return;
                        }
                    }

                    if (ViewState["CustomerAds1"] == null || ViewState["CustomerAds1"].ToString() == "")
                    {
                        if (UploadCust1.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadCust1.PostedFile.FileName);
                            ViewState["CustomerAds1"] = fileName;
                            UploadCust1.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadCust1.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Customer Locked Ad 1", 0);
                            return;
                        }
                    }
                    if (ViewState["CustomerAds2"] == null || ViewState["CustomerAds2"].ToString() == "")
                    {
                        if (UploadCust2.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadCust2.PostedFile.FileName);
                            ViewState["CustomerAds2"] = fileName;
                            UploadCust2.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadCust2.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Customer Unlocked Ad 1 ", 0);
                            return;
                        }
                    }

                    if (ViewState["CustomerAds3"] == null || ViewState["CustomerAds3"].ToString() == "")
                    {
                        if (UploadCust3.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadCust3.PostedFile.FileName);
                            ViewState["CustomerAds3"] = fileName;
                            UploadCust3.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadCust3.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Customer Locked Ad 2 ", 0);
                            return;
                        }
                    }

                    if (ViewState["CustomerAds4"] == null || ViewState["CustomerAds4"].ToString() == "")
                    {
                        if (UploadCust4.HasFile)
                        {
                            string fileName = Path.GetFileName(UploadCust4.PostedFile.FileName);
                            ViewState["CustomerAds4"] = fileName;
                            UploadCust4.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                            UploadCust4.SaveAs(Server.MapPath("~/OrderDesigned/") + fileName);
                        }
                        else
                        {
                            MessageBox(div1, LblMsg, "Add Customer Unlocked Ad 2 ", 0);
                            return;
                        }
                    }

                    if (Validation() == true)
                    {
                        Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");

                        if (TxtEmail.Text != "")
                        {
                            Match match = regex.Match(TxtEmail.Text);
                            if (!match.Success)
                            {
                                MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                                return;
                            }
                        }
                        SaveEntries();
                        Bind();
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void SaveEntries()
        {
            try
            {
                //CustomerName,CustomerEmail,CodeID,AppointmentAds,CustAds1,CustAds2,CustAds3,CustAds4,EmailStatusID,CreatedBy
                int CodeID = int.Parse(DdlCode.SelectedValue);
                ObjCodeMasterDAL.UpdateUtilizationCode(CodeID);

                ObjAdvertisementBOL.CompanyName = TxtCName.Text;
                ObjAdvertisementBOL.CustomerEmail = TxtEmail.Text;
                ObjAdvertisementBOL.CodeID = CodeID;
                ObjAdvertisementBOL.AppointmentAds = ViewState["AppointmentAds"].ToString();
                ObjAdvertisementBOL.LockedAppointmentAds = ViewState["LockedAppointmentAds"].ToString();
                ObjAdvertisementBOL.CustAds1 = ViewState["CustomerAds1"].ToString();
                ObjAdvertisementBOL.CustAds2 = ViewState["CustomerAds2"].ToString();
                ObjAdvertisementBOL.CustAds3 = ViewState["CustomerAds3"].ToString();
                ObjAdvertisementBOL.CustAds4 = ViewState["CustomerAds4"].ToString();
                ObjAdvertisementBOL.EmailStatusID = 1;
                ObjAdvertisementBOL.CreatedBy = int.Parse(Session["UserID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjAdvertisementDAL.InsertAdvertisement(ObjAdvertisementBOL, Trans, Conn) == true)
                {

                    
                    ObjCustomerProfileMasterDAL.UpdateSnapStatus(ObjAdvertisementBOL.CustomerEmail);
                    Trans.Commit();
                    Conn.Close();
                    ClearControls();
                    MessageBox(div1, LblMsg, "Customer Added Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void ClearControls()
        {
            TxtCName.Text = TxtEmail.Text = "";
            Bind();
            ViewState["AppointmentAds"] = ViewState["CustomerAds1"] = ViewState["CustomerAds2"] = ViewState["CustomerAds3"] = ViewState["CustomerAds4"] = null;
        }
    }
}