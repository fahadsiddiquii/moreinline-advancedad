﻿<%@ Page Title="Team Members" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="UserAccounts.aspx.cs" Inherits="MoreInline.User.UserAccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%----------------------------------------- Scripts -------------------------------------------------------%>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } <%--else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #019c7c; font-family: 'Microsoft JhengHei'"><b>Team Members</b></h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">
                <asp:Repeater ID="RptViewUsers" runat="server" OnItemDataBound="RptViewUsers_ItemDataBound" OnItemCommand="RptViewUsers_ItemCommand">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">SNo.</th>
                                    <th class="text-center text-black">Full Name</th>
                                    <th class="text-center text-black">User Email</th>
                                    <th class="text-center text-black">User Type</th>
                                    <th class="text-center text-black">Profile Pic</th>
                                    <th class="text-center text-black">Active</th>
                                    <th class="text-center text-black">Action</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <%--UserID,FirstName,LastName,UserEmail,Photo,IsActive,UserRoles--%>
                            <td class="text-center">
                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                <asp:Label ID="LblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' Visible="false" />
                                <asp:Label ID="LblIsActive" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsActive") %>' Visible="false" />
                                <asp:Label ID="LblPhoto" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Photo") %>' Visible="false" />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblFirstName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FirstName") %>' />
                                <asp:Label ID="LblLastName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LastName") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblUserEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserEmail") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblUserRoles" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserRoles") %>' />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ImgEye" runat="server" ImageUrl="~/Images/Icons/icon_eye.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' CommandName="ViewPic" />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="IBtnCheck" runat="server" ImageUrl="~/Images/Icons/check.png" Width="30px"
                                    CommandName="Active" ToolTip="Active" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' />

                                <asp:ImageButton ID="IBtnCross" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                    CommandName="InActive" ToolTip="InActive" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ImgEdit" runat="server" ImageUrl="~/Images/MoreInline Edit Icon.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"UserID") %>' CommandName="Edit" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <!-- ===================================== Bootstrap Modal Dialog ========================================================= -->

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-body">
                <div class="well-lg">
                    <asp:Image ID="ImgProfile" runat="server" CssClass="img-responsive" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
