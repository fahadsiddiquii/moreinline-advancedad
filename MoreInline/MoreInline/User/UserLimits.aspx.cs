﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class UserLimits : System.Web.UI.Page
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();
        UserSignupBOL ObjUserSignupBOL = new UserSignupBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    Bind();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
        }
        public void Bind()
        {
            DataTable dt = ObjUserSignupDAL.GetUserTypes(ObjUserSignupBOL);

            RptUserLimits.DataSource = dt;
            RptUserLimits.DataBind();
        }
        protected void TxtUserLimits_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox Txt = (sender as TextBox);
                RepeaterItem item = Txt.NamingContainer as RepeaterItem;
                Label LblUserRoleID = item.FindControl("LblUserRoleID") as Label;
                Label LblLimits = item.FindControl("LblLimits") as Label;
                TextBox TxtUserLimits = item.FindControl("TxtUserLimits") as TextBox;

                if (int.Parse(Txt.Text) > 11)
                {
                    MessageBox(div1, LblMsg, "Maximum Limit 11 are Allowed", 0);
                    return;
                }
                if (Txt.Text == "" || int.Parse(Txt.Text) < 0)
                {
                    MessageBox(div1, LblMsg, "Atleast One", 0);
                }
                if (int.Parse(Txt.Text) < int.Parse(LblLimits.Text))
                {
                    MessageBox(div1, LblMsg, "User Utilized Must be Greater Than Limit.", 0);
                    return;
                }

                UpdateUserLimits(int.Parse(LblUserRoleID.Text), int.Parse(Txt.Text));
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void UpdateUserLimits(int UserRoleID, int UserLimits)
        {
            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            ObjUserSignupBOL.UserRoleID = UserRoleID;
            ObjUserSignupBOL.UserLimits = UserLimits;

            try
            {
                if (ObjUserSignupDAL.UpdateUserLimits(ObjUserSignupBOL, Trans, Conn) == false)
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                else
                {
                    Trans.Commit();
                    Conn.Close();
                    Bind();
                    MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                }
            }
            catch (Exception)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }
    }
}