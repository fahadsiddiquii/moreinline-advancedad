﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.User
{
    public partial class WelcomePage : System.Web.UI.Page
    {
        CustomerProfileMasterDAL CPM = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (Session["UserName"] != null || Session["UserName"].ToString() != "")
                {
                    if (!IsPostBack)
                    {
                        SendExpiryNotfication();
                    }
                    if (Session["UserRoles"].ToString() == "Admin" || Session["UserRoles"].ToString() == "Manager")
                    {
                        PnlAdminManager.Visible = true;
                    }
                    

                }
                else
                {
                    Response.Redirect(@"~\User\UserSignUp.aspx", false);
                }
            }
            catch (Exception)
            {
                Response.Redirect(@"~\User\UserSignUp.aspx", false);
            }
        }

        private void SendExpiryNotfication()
        {
            DataTable  dt = CPM.GetAboutExpCust();
            int index = 0;
            if (dt.Rows.Count>0)
            {
                #region Email For Customer
                int id = int.Parse(dt.Rows[index]["Cust_ID"].ToString());
                ObjCustomerProfileMasterBOL.Cust_ID = id;
                DataTable CustData = CPM.GetCustomerProfile(ObjCustomerProfileMasterBOL);
                if (CustData.Rows[0]["IsPackageExpNActive"].ToString().Equals("True"))
                {
                    EmailNotification EN = new EmailNotification();
                    EmailNotification EmailData = EN.GetAdminFilledObject();
                    EmailData.ButtonText = "Upgrade";
                    EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                    EmailData.NotificationType = "IsPackageExpNActive";
                    EmailData.Content = @"Dear Stan, Your Free MoreInline Trial Version will expire after 3 days. Please upgrade your account by signing to your dashboard and click Upgrade.Follow the steps to reactivate your account.";
                    EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                    EmailData.EmailSubject = " Your trial is ending Soon!";
                    EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                    EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                    EmailNotification.SendNotification(EmailData);
                }
                #endregion
                CPM.UpdateExpStatus(id);
                index++;
            }
        }

        [WebMethod]
        public static List<NotificationModel> GetNotifications()
        {
            
            NotificationModel Notiofication = new NotificationModel();
            int uid = int.Parse(HttpContext.Current.Session["UserID"].ToString());
            int UTID = int.Parse(HttpContext.Current.Session["UserType"].ToString());
            List<NotificationModel> List = Notiofication.GetData(uid, UTID);
            return List;
        }

        [WebMethod]
        public static string Notify()
        {
            NotificationModel Notiofication = new NotificationModel();
            int uid = int.Parse(HttpContext.Current.Session["UserID"].ToString());
            int UTID = int.Parse(HttpContext.Current.Session["UserType"].ToString());
            string result = Notiofication.Notify(uid, UTID);
            return result;
        }
        
    }
}