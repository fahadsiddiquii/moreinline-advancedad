﻿<%@ Page Title="Manage Ads" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ManageAds.aspx.cs" Inherits="MoreInline.User.ManageAds" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%----------------------------------------- Scripts -------------------------------------------------------%>

    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } <%-- else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <style>
        .img {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 60px;
        }

            .img:hover {
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }
    </style>
    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #007f9f;">Manage Ads</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label>
                    </strong>
                </div>
                <br />
            </div>

            <div class="col-lg-12">
                <asp:FileUpload ID="ImgFile" runat="server"></asp:FileUpload>
                <asp:Button ID="BtnUpload" Text="Upload" runat="server" CssClass="btn btn-light-green m-t-10" OnClick="BtnUpload_Click" />
            </div>
        </div>


        <br />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">
                <asp:Repeater ID="RptAds" runat="server" OnItemCommand="RptAds_ItemCommand" OnItemDataBound="RptAds_ItemDataBound">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">SNo.</th>
                                    <th class="text-center text-black">Ad</th>
                                    <th class="text-center text-black">Active</th>
                                    <th class="text-center text-black">Action</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <%--CodeID,CodeNumber,CodeText,FullCode,ValidationDays--%>
                            <td class="text-center">
                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                <asp:Label ID="LblIsActive" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsActive") %>' Visible="false" />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="FilePath" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"ImageUrl") %>' CssClass="img" runat="server" CommandName="btnimg" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Img_ID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="IBtnCheck" runat="server" ImageUrl="~/Images/Icons/check.png" Width="30px"
                                    CommandName="Active" ToolTip="Active" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Img_ID") %>' />

                                <asp:ImageButton ID="IBtnCross" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                    CommandName="InActive" ToolTip="InActive" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Img_ID") %>' />
                            </td>
                            <td class="text-center">
                                <asp:ImageButton ID="ImgDelete" ToolTip="Delete Ad" runat="server" ImageUrl="~/Images/Icons/bin.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Img_ID") %>' CommandName="DeleteImg" OnClientClick='javascript:return confirm("Do you want to delete?")' />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>

    <%-- image view --%>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #f7f8f9; border: 1px solid #4B4A4A;">
                <div class="modal-body">
                    <img src="#" id="imgpop" runat="server" alt="Alternate Text" style="width: 100%; height: 100%;" />
                </div>
            </div>
        </div>
    </div>

    <%-- end --%>
</asp:Content>
