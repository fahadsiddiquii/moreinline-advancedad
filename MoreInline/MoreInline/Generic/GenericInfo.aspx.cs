﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.Generic
{
    public partial class GenericInfo : System.Web.UI.Page
    {
        OrderAssetsDAL OrderAsset = new OrderAssetsDAL();
        OrderAssetsBOL Assets = new OrderAssetsBOL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadInfo();
                }
                catch (Exception ex)
                {
                    MessageBox(div1, LblMsg, ex.Message, 0);
                }
            }
        }

   

        private void LoadInfo()
        {
            int custid = int.Parse(Session["Cust_ID"].ToString());
            DataTable GenericInfodt = OrderAsset.GetGenericInfo(custid);
            TxtEmailAdd.Text = GenericInfodt.Rows[0]["Email"].ToString();
            TxtLocation.Text = GenericInfodt.Rows[0]["Location"].ToString();
            TxtPhone.Text = GenericInfodt.Rows[0]["Phone"].ToString();
            Txtweb.Text = GenericInfodt.Rows[0]["WebUrl"].ToString();
            TxtMessage.Text = GenericInfodt.Rows[0]["message"].ToString();

            HfAssetId.Value = GenericInfodt.Rows[0]["AudioPath"].ToString();
            string AudioPath = GenericInfodt.Rows[0]["AudioPath"].ToString();
            HfVoicePath.Value = AudioPath;
            string[] Mpath = AudioPath.Split('~');
            try { AudioPath = Mpath[1].ToString(); }
            catch (Exception) { AudioPath = ""; }
            Session["Patha"] = ".." + AudioPath;
            string script = "<script>SetAudio('.." + AudioPath + "')</script>";
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, false);
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validations())
                {
                    int custid = int.Parse(Session["Cust_ID"].ToString());
                    Assets.UpdatedByID = custid;
                    if (OrderAsset.UpdateGinfo(Assets))
                    {
                        LoadInfo();
                        MessageBox(div1, LblMsg, "Information Updated", 1);
                    }
                    else
                    {
                        LoadInfo();
                        MessageBox(div1, LblMsg, "Updation Failed", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        private bool Validations()
        {
            TxtEmailAdd.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtLocation.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtPhone.Attributes["style"] = "border: 1px solid #4B4A4A;";
            Txtweb.Attributes["style"] = "border: 1px solid #4B4A4A;";

            if (!string.IsNullOrWhiteSpace(TxtEmailAdd.Text))
            { Assets.Email = TxtEmailAdd.Text; }
            else
            {
                TxtEmailAdd.Attributes["style"] = "border: 1px solid red;";
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }

            if (!string.IsNullOrWhiteSpace(TxtLocation.Text))
            { Assets.Location = TxtLocation.Text; }
            else
            {
                TxtLocation.Attributes["style"] = "border: 1px solid red;";
                MessageBox(div1, LblMsg, "Enter Location", 0);
                return false;
            }

            if (!string.IsNullOrWhiteSpace(TxtMessage.Text))
            { Assets.Message = TxtMessage.Text; }
            else
            {
                TxtMessage.Attributes["style"] = "border: 1px solid red;";
                MessageBox(div1, LblMsg, "Enter Message", 0);
                return false;
            }

            if (!string.IsNullOrWhiteSpace(TxtPhone.Text))
            { Assets.Phone = TxtPhone.Text; }
            else
            {
                TxtPhone.Attributes["style"] = "border: 1px solid red;";
                MessageBox(div1, LblMsg, "Enter Contact Number", 0);
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Txtweb.Text))
            { Assets.WebUrl = Txtweb.Text; }
            else
            {
                Txtweb.Attributes["style"] = "border: 1px solid red;";
                MessageBox(div1, LblMsg, "Enter Website", 0);
                return false;
            }
            if (Session["AudPath"] != null)
            {
                string audpth = HttpContext.Current.Session["AudPath"].ToString();
                string Directory = Server.MapPath(audpth);
                FileInfo fileInfo = new FileInfo(Directory);
                if (fileInfo.Exists)
                {
                    Assets.AudioPath = string.IsNullOrWhiteSpace(audpth) == true ? "" : audpth;

                }
                else
                {
                    Assets.AudioPath = "";
                }
            }
            return true;
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }

            if (div.ID == "div1")
            {
                //  row1.Attributes["style"] = "margin-top: 0px;";
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }
    }
}