﻿<%@ Page Title="Generic Settings" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="GenericInfo.aspx.cs" Inherits="MoreInline.Generic.GenericInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <%-- ----------------------------------- Script Start --------------------------------------------- --%>

    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>
    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtLocation.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
    <script>
        function SetAudio(path) {
            $("#audio").attr("src", path);
        }

    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <style>
        .LabelSpan {
            background-color: #f6f7f8 !important;
            border-color: #f6f7f8 !important;
            padding-top: 10px;
        }
    </style>
    <%-- ----------------------------------- Script End --------------------------------------------- --%>


    <%-------------------------------------- Page Start ------------------------------------------------%>

    <div class="row">
        <div class="col-lg-12 text-center">
            <h1 style="color: #019c7c;">Company Information For Your Ads</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 p-b-10">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-2"></div>
            <div class="col-lg-5 col-sm-6 m-b-10 m-t-10">
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">&nbsp;&nbsp;&nbsp;&nbsp;Email: </span>
                    <asp:TextBox ID="TxtEmailAdd" TextMode="Email" runat="server" CssClass="form-control m-t-5" placeholder="Enter email address"></asp:TextBox>
                </div>
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">&nbsp;Contact: </span>
                    <asp:TextBox ID="TxtPhone" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" CssClass="form-control m-t-5 num" placeholder="Enter contact number"></asp:TextBox>
                </div>
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Location:</span>
                    <asp:TextBox ID="TxtLocation" runat="server" onkeydown="return checkShortcut();" CssClass="form-control m-t-5" placeholder="Enter business address"></asp:TextBox>
                </div>
                <div class="input-group m-t-10">
                    <span class="input-group-addon LabelSpan">Website:</span>
                    <asp:TextBox ID="Txtweb" runat="server" CssClass="form-control m-t-5" placeholder="Enter URL"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-2"></div>
            <div class="col-lg-5 col-sm-6 m-t-10">
                <label style="color: #A9C502">Email Message:</label>
                <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="Enter Message here.." EnableTheming="True"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-sm-2"></div>
            <div class="col-lg-8 col-sm-8 m-t-10">
                <div class="box-footer no-border" style="background-color: #DFDFDF;">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <img src="../Images/Icons/mic.png" alt="Mic" id="record" class="img-circle chz" style="background-color: #A9C502; float: left;" />
                            <img src="../Images/Icons/mic.gif" alt="Mic" id="pause" hidden class="img-circle" style="float: left;" />

                            <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>

                            <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                            <img id="stop" src="../Images/Icons/delete_voice.jpg" title="Delete" alt="delete" hidden class="img-circle" style="background-color: #fff; float: right;" />
                            <img id="save" src="../Images/Icons/upload_voice.jpg" title="Upload" alt="upload" hidden class="img-circle generic" style="background-color: #fff; float: right;" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
             <div class="col-lg-2 col-sm-2"></div>
            <div class="col-lg-8 col-sm-8 m-t-10">
                <ol>
                    <li class="m-b-5">
                        <img src="../Images/Icons/mic.png" alt="Mic" class="img-circle" style="background-color: #A9C502;" />
                        Press to start recording.</li>
                    <li class="m-b-5">
                        <img src="../Images/Icons/mic.gif" alt="Mic Stop" class="img-circle" />
                        Press to stop recording.</li>
                    <li class="m-b-5">
                        <img src="../Images/Icons/upload_voice.jpg" alt="upload" class="img-circle" />
                        Press to upload recording Or
                                         <img src="../Images/Icons/delete_voice.jpg" alt="Delete" class="img-circle" />
                        Press to delete recording.
                    </li>
                </ol>
            </div>
        </div>
        <div class="row m-t-10">
            <div class="col-lg-3 col-sm-2"></div>
            <div class="col-lg-5 col-sm-6">
                <a href="../Dashboard/Dashboard.aspx" class="btn btn-sm btn-black-green m-b-10" style="width: 90px;">Dashboard</a>
                <asp:Button ID="BtnEdit" runat="server" OnClick="BtnEdit_Click" Text="Update" CssClass="btn btn-sm btn-light-green m-b-10" Style="width: 90px;" />
                <asp:Button ID="BtnSave" runat="server" Visible="false" Text="Save" CssClass="btn btn-sm btn-light-green m-b-10" Style="width: 90px;" />
            </div>
            <div class="col-lg-5"></div>
        </div>
    </div>
    <br />
     <asp:HiddenField runat="server" ID="HfAssetId" />
        <asp:HiddenField runat="server" ID="HfVoicePath" />
    

    <%-------------------------------------- Page End --------------------------------------------------%>
</asp:Content>
