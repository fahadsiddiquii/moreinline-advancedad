﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class Snap : System.Web.UI.Page
    {
        CustomerProfileMasterDAL CustomerProfile = new CustomerProfileMasterDAL();
        PaymentDAL ObjPaymentDAL = new PaymentDAL();
        PaymentBOL ObjPaymentBOL = new PaymentBOL();
        CustomerProfileMasterBOL ObjCustomerProfile = new CustomerProfileMasterBOL();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                try
                {
                 
                    string ptype = Request.QueryString["p"].ToString();
                    Session["s12"] = ptype.Equals("12")? "selected":"";
                    Session["s99"] = ptype.Equals("99") ? "selected" : ""; 
                    Session["s199"] = ptype.Equals("199") ? "selected" : "";
                }
                catch (Exception)
                {
                    Session["s12"] = "selected";
                    Session["s99"] = ""; 
                    Session["s199"] =  "";
                    Session["pkg"] = 12;
                }

            }
        }

        protected void btn12_Click(object sender, EventArgs e)
        {
            Session["s12"] = "selected";
            Session["s199"] = "";
            Session["s99"] = "";
            Session["pkg"] = 12;
      
         
        }

        protected void btn199_Click(object sender, EventArgs e)
        {
            Session["s199"] = "selected";
            Session["s99"] = "";
            Session["s12"] = "";
            Session["pkg"] = 199;
        
        }

        protected void btn99_Click(object sender, EventArgs e)
        {
            Session["s99"] = "selected";
            Session["s199"] = "";
            Session["s12"] = "";
            Session["pkg"] = 99;
        
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            SaveSnap("paypal");
        }

        protected void BtnCashPay_Click(object sender, EventArgs e)
        {
            SaveSnap("cash");
        }

        private void SaveSnap(string type)
        {


            try
            {
                string FullName = TxtFname.Text;
                string Email = txtEmail.Text;
                string Website = txtWeb.Text;
                string lg = string.Empty;
                int AcType = 0;
                int limit = 0;
                int Amount = 0;
                    if (logo.HasFile)
                    {
                        if (Page.IsValid)
                        {
                            string fileName = Path.GetFileName(logo.PostedFile.FileName);
                            logo.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                            lg = "~/upload/" + fileName;
                            Session["ProfilePhoto"] = lg;
                        }
                        else
                        {
                            return;
                        }
                    }
              

                if (Session["s99"].ToString() != "" && Session["pkg"] != null)
                {
                    limit = 1500;
                    AcType = 3;
                    Amount = 99;
                }
                if (Session["s199"].ToString() != "" && Session["pkg"] != null)
                {
                    limit = 3200;
                    AcType = 4;
                    Amount = 199;
                }
                if (Session["s12"].ToString() != "" && Session["pkg"] != null)
                {
                    limit = 1500;
                    AcType = 2;
                    Amount = 12;
                }
                Session["FullName"] = FullName;
                Session["Email"] = Email;
                Session["AcType"] = AcType;
                Session["Amount"] = Amount;
                Session["limit"] = limit;
                Response.Redirect("~/Snap-Payment.aspx?Amount=" + Amount + "&Limit=" + limit + "&ddl=12" + "&AcType=" + AcType+"&type="+type);


            }
            catch (Exception ex)
            {

            }
        }

        protected void logo_DataBinding(object sender, EventArgs e)
        {

        }
    }
}