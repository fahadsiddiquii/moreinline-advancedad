﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class AdminMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
                if ((Session["UserName"] != null || Session["UserName"].ToString() != "") && (Session["Cust_ID"] != null || Session["Cust_ID"].ToString() != "")
                    && (Session["Email"] != null || Session["Email"].ToString() != ""))
                {

                    LblUserName.Text = Session["UserName"].ToString();

                    if (Session["Photo"].ToString() != "")
                    {
                        ImgLogin.ImageUrl = "~/upload/" + Session["Photo"].ToString();
                        ImgLogin1.ImageUrl = "~/upload/" + Session["Photo"].ToString();
                    }
                    else
                    {
                        ImgLogin.ImageUrl = "~/dist/img/user2-160x160.jpg";
                    }

                    

                }
                else
                {
                    Response.Redirect(@"~\LoginPage.aspx");
                }
            }
            catch (Exception)
            {
                Response.Redirect(@"~\LoginPage.aspx");
            }
        }
    }
}