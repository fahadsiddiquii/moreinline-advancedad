﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ClosedXML.Excel;

namespace MoreInline.Contacts
{
    public partial class ViewContacts : System.Web.UI.Page
    {
        ContactListBOL ObjContactListBOL = new ContactListBOL();
        ContactListDAL ObjContactListDAL = new ContactListDAL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                BtnDeleteList.Visible = false;
                BtnDownload.Visible = false;
                if (!IsPostBack)
                {
                    LoadContactList();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        public void LoadContactList()
        {
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);

            DdlContactList.DataSource = dt;
            DdlContactList.DataTextField = "ListTitle";
            DdlContactList.DataValueField = "Cont_ID";
            DdlContactList.DataBind();
            DdlContactList.Items.Insert(0, new ListItem("Select Contact List", "0"));
            DdlContactList.Items.Insert(1, new ListItem("General List", "2"));

        }

        protected void DdlContactList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DdlContactList.SelectedValue != "0")
            {
                if (DdlContactList.SelectedValue == "2")
                { GetGeneralListDetail(); }
                else
                { GetContactListDetail(); }
            }
            else if (DdlContactList.SelectedValue == "0")
            {
                RptUploadContacts.DataSource = null;
                RptUploadContacts.DataBind();
            }
        }

        protected void GetContactListDetail()
        {
            try
            {
                ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                DataTable dt = ObjContactListDAL.GetContactListDetail(ObjContactListBOL);
                if (ViewState["Contacts"] != null)
                {
                    ViewState["Contacts"] = null;
                }
                ViewState["Contacts"] = dt;
                RptUploadContacts.DataSource = dt;
                RptUploadContacts.DataBind();
                BtnDownload.Visible = true;
                if (DdlContactList.SelectedValue != "2")
                {
                    BtnDeleteList.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void GetGeneralListDetail()
        {
            try
            {
                ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
                DataTable dt = ObjContactListDAL.GetContactListDetail(ObjContactListBOL);
                if (ViewState["Contacts"] != null)
                {
                    ViewState["Contacts"] = null;
                }
                ViewState["Contacts"] = dt;
                RptUploadContacts.DataSource = dt;
                RptUploadContacts.DataBind();
                BtnDownload.Visible = true;
                if (DdlContactList.SelectedValue != "2")
                {
                    BtnDeleteList.Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public bool Validation()
        {
            if (TxtName.Text == "")
            {
                MessageBox(div2, LblMsg1, "Enter Name", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div2, LblMsg1, "Enter Email", 0);
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void RptUploadContacts_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            try
            {
                Clear();
                int Cont_DID = int.Parse(e.CommandArgument.ToString());
                Label LblName = e.Item.FindControl("LblName") as Label;
                Label LblEmail = e.Item.FindControl("LblEmail") as Label;
                Label LblTitle = e.Item.FindControl("LblTitle") as Label;
                Label LblCompany = e.Item.FindControl("LblCompany") as Label;
                Label LblZipCode = e.Item.FindControl("LblZipCode") as Label;
                ViewState["Cont_DID"] = Cont_DID;

                switch (e.CommandName)
                {
                    case "Edit":
                        LoadContactListPop();
                        DdlContactListPop.SelectedValue = DdlContactList.SelectedValue;

                        TxtName.Text = LblName.Text;
                        TxtEmail.Text = LblEmail.Text;
                        TxtTitle.Text = LblTitle.Text;
                        TxtCompany.Text = LblCompany.Text;
                        TxtZipCode.Text = LblZipCode.Text;

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        break;

                    case "Delete":
                        DeleteContactListDetails();
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void LoadContactListPop()
        {
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);

            DdlContactListPop.DataSource = dt;
            DdlContactListPop.DataTextField = "ListTitle";
            DdlContactListPop.DataValueField = "Cont_ID";
            DdlContactListPop.DataBind();
            DdlContactListPop.Items.Insert(0, new ListItem("Select Contact List", "0"));
            DdlContactListPop.Items.Insert(1, new ListItem("General List", "2"));

        }

        public void Clear()
        {
            ViewState["Cont_DID"] = null;
            TxtName.Text = TxtLName.Text = TxtEmail.Text = TxtCompany.Text = TxtTitle.Text = TxtZipCode.Text = "";
        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                    Match match = regex.Match(TxtEmail.Text);
                    if (!match.Success)
                    {
                        MessageBox(div2, LblMsg1, "Invalid Email Address", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }
                    if (DdlContactListPop.SelectedValue == "0")
                    {
                        MessageBox(div2, LblMsg1, "Select Contact List", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }

                    ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                    ObjContactListBOL.Cont_DID = int.Parse(ViewState["Cont_DID"].ToString());
                    ObjContactListBOL.Name = TxtName.Text;
                    ObjContactListBOL.LastName = "";
                    ObjContactListBOL.Email = TxtEmail.Text;
                    ObjContactListBOL.Company = TxtCompany.Text;
                    ObjContactListBOL.Title = TxtTitle.Text;
                    ObjContactListBOL.ZipCode = TxtZipCode.Text;

                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjContactListDAL.UpdateContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();
                        Clear();
                        MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
                if (DdlContactList.SelectedValue != "0")
                {
                    GetContactListDetail();
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void BtnMove_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                    Match match = regex.Match(TxtEmail.Text);
                    if (!match.Success)
                    {
                        MessageBox(div2, LblMsg1, "Invalid Email Address", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }
                    if (DdlContactListPop.SelectedValue == "0")
                    {
                        MessageBox(div2, LblMsg1, "Select Contact List", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }
                    if (DdlContactListPop.SelectedValue == DdlContactList.SelectedValue)
                    {
                        MessageBox(div2, LblMsg1, "Its Already in List", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }

                    ObjContactListBOL.Cont_DID = int.Parse(ViewState["Cont_DID"].ToString());
                    ObjContactListBOL.Cont_MID = int.Parse(DdlContactListPop.SelectedValue);

                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjContactListDAL.UpdateMoveContact(ObjContactListBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();
                        Clear();
                        MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div2, LblMsg1, "Error Occurred While Saving", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }

                if (DdlContactList.SelectedValue != "0")
                {
                    GetContactListDetail();
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        protected void BtnCopy_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                    Match match = regex.Match(TxtEmail.Text);
                    if (!match.Success)
                    {
                        MessageBox(div2, LblMsg1, "Invalid Email Address", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }
                    if (DdlContactListPop.SelectedValue == "0")
                    {
                        MessageBox(div2, LblMsg1, "Select Contact List", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }
                    if (DdlContactListPop.SelectedValue == DdlContactList.SelectedValue)
                    {
                        MessageBox(div2, LblMsg1, "Its Already in List", 0);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                        return;
                    }

                    ObjContactListBOL.Cont_MID = int.Parse(DdlContactListPop.SelectedValue);
                    ObjContactListBOL.Cont_DID = int.Parse(ViewState["Cont_DID"].ToString());
                    ObjContactListBOL.Name = TxtName.Text;
                    ObjContactListBOL.LastName = "";
                    ObjContactListBOL.Email = TxtEmail.Text;
                    ObjContactListBOL.Company = TxtCompany.Text;
                    ObjContactListBOL.Title = TxtTitle.Text;
                    ObjContactListBOL.ZipCode = TxtZipCode.Text;
                    ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                    //if (ObjContactListDAL.UpdateContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();
                        Clear();
                        MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
                if (DdlContactList.SelectedValue != "0")
                {
                    GetContactListDetail();
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DeleteContactListDetails()
        {
            try
            {
                ObjContactListBOL.Cont_DID = int.Parse(ViewState["Cont_DID"].ToString());
                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjContactListDAL.DeleteContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                if (DdlContactList.SelectedValue != "0")
                {
                    GetContactListDetail();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DeleteContactList()
        {
            try
            {
                ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjContactListDAL.DeleteContactList(ObjContactListBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
                LoadContactList();
                RptUploadContacts.DataSource = null;
                RptUploadContacts.DataBind();


            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnDeleteList_Click(object sender, EventArgs e)
        {
            DeleteContactList();
        }

        private void ExportGridToExcel()
        {
            try
            {
                DataTable resultTable = (DataTable)ViewState["Contacts"];
                DataView view = new DataView(resultTable);
                DataTable dt = view.ToTable(false, "Name", "Email", "Title", "Company");
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    string FileName = DdlContactList.SelectedItem + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=" + FileName);
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }


                #region Another Way
                //Response.Clear();
                //Response.Buffer = true;
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.Charset = "";
                //DataTable dt = (DataTable)ViewState["Contacts"];
                //DataView view = new DataView(dt);
                //DataTable resultTable = view.ToTable(false, "Name", "Email", "Title", "Company");

                //string FileName = DdlContactList.SelectedItem + ".xls";
                //Response.ClearContent();
                //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                //Response.ContentType = "application/vnd.ms-excel";
                //string tab = "";
                //foreach (DataColumn dc in resultTable.Columns)
                //{
                //    Response.Write(tab + dc.ColumnName);
                //    tab = "\t";
                //}
                //Response.Write("\n");
                //int i;
                //foreach (DataRow dr in resultTable.Rows)
                //{
                //    tab = "";
                //    for (i = 0; i < resultTable.Columns.Count; i++)
                //    {
                //        Response.Write(tab + dr[i].ToString());
                //        tab = "\t";
                //    }
                //    Response.Write("\n");
                //}
                //Response.End(); 
                #endregion

                #region Another Way To Export to Excel
                //Response.Clear();
                //Response.Buffer = true;
                //Response.ClearContent();
                //Response.ClearHeaders();
                //Response.Charset = "";
                //DataTable dt = (DataTable)ViewState["Contacts"];
                //GridView gv = new GridView();
                //DataView view = new DataView(dt);
                //DataTable resultTable = view.ToTable(false, "Name", "Email", "Title", "Company");
                //gv.DataSource = resultTable;
                //gv.DataBind();
                //string FileName = DdlContactList.SelectedItem + ".xls";
                //StringWriter strwritter = new StringWriter();
                //HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                //Response.ContentType = "application/vnd.ms-excel";
                //Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
                //gv.RenderControl(htmltextwrtter);
                //Response.Write(strwritter.ToString());
                //Response.End(); 
                #endregion
            }
            catch (Exception ex)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                ExportGridToExcel();
            }
            catch (Exception ex)
            {
                div1.Visible = true;
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

    }
}