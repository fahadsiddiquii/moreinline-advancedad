﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ClosedXML.Excel;

namespace MoreInline.Contacts
{
    public partial class UploadContacts : System.Web.UI.Page
    {
        ContactListBOL ObjContactListBOL = new ContactListBOL();
        ContactListDAL ObjContactListDAL = new ContactListDAL();

        SqlTransaction Trans;
        SqlConnection Conn;
        int Count = 0;
        int Email = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LblCount.Visible = div1.Visible = false;
                if (!IsPostBack)
                {
                    LoadContactList();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void LoadContactList()
        {
            try
            {
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
                DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);
                //Cont_ID,ListTitle
                DdlContactList.DataSource = dt;
                DdlContactList.DataTextField = "ListTitle";
                DdlContactList.DataValueField = "Cont_ID";
                DdlContactList.DataBind();
                DdlContactList.Items.Insert(0, new ListItem("Select Contact List", "0"));
                DdlContactList.Items.Insert(1, new ListItem("Create New List", "1"));
                DdlContactList.Items.Insert(2, new ListItem("General List", "2"));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DownloadContactlistTemp()
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Clear();
                dt.Columns.Add("Name");
                dt.Columns.Add("Email");
                dt.Columns.Add("Title");
                dt.Columns.Add("Company");

                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt, "ContactListTemplate");

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    string FileName = DdlContactList.SelectedItem + ".xlsx";
                    Response.AddHeader("content-disposition", "attachment;filename=ContactListTemplate.xlsx");
                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }


        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnUpload_Click(object sender, EventArgs e)
        {
            try
            {

                if (DdlContactList.SelectedValue == "0")
                {
                    MessageBox(div1, LblMsg, "Select Contact List", 0);
                    return;
                }
                if (FileUploadList.HasFile)
                {
                    string FileName = Path.GetFileName(FileUploadList.PostedFile.FileName);
                    string Extension = Path.GetExtension(FileUploadList.PostedFile.FileName);
                    string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
                    string FilePath = Server.MapPath(FolderPath + FileName);
                    FileUploadList.SaveAs(FilePath);

                    if (BindRepeater(FilePath, Extension))
                    {
                        File.Delete(FilePath);
                        BtnSave.Visible = true;
                        BtnUpload.Visible = false;
                    }
                }
                else
                {
                    MessageBox(div1, LblMsg, "Please upload Excel File", 0);
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        private bool BindRepeater(string FilePath, string Extension)
        {
            try
            {
                string conStr = "";
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        break;
                    case ".xlsx": //Excel 07
                        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        break;
                }
                conStr = String.Format(conStr, FilePath, "yes");
                OleDbConnection connExcel = new OleDbConnection(conStr);
                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();

                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                oda.SelectCommand = cmdExcel;
                oda.Fill(dt);
                connExcel.Close();

                if (dt.Rows.Count > 0)
                {
                    RptUploadContacts.DataSource = dt;
                    RptUploadContacts.DataBind();
                    MessageBox(div1, LblMsg, "List Uploaded", 1);
                }
                return true;
            }
            catch (Exception)
            {
                MessageBox(div1, LblMsg, "Download Template", 0);
                return false;
            }

        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int CustID = int.Parse(Session["Cust_ID"].ToString());
                if (DdlContactList.SelectedValue == "0")
                {
                    MessageBox(div1, LblMsg, "Select Contact List", 0);
                    return;
                }

                if (DdlContactList.SelectedValue == "1")
                {
                    if (TxtListTitle.Text == "")
                    {
                        MessageBox(div1, LblMsg, "Enter File Name", 0);
                        return;
                    }
                    else if (RptUploadContacts.Items.Count <= 0)
                    {
                        MessageBox(div1, LblMsg, "Please upload Excel File", 0);
                        return;
                    }
                    SavingNewContacts();
                }
                else if (DdlContactList.SelectedValue == "2")
                {
                    SavingGeneralContacts();
                }
                else
                {
                    Conn = DBHelper.GetConnection();
                    Trans = Conn.BeginTransaction();

                    foreach (RepeaterItem item in RptUploadContacts.Items)
                    {
                        Label LblName = (Label)item.FindControl("LblName");
                        Label LblEmail = (Label)item.FindControl("LblEmail");
                        Label LblTitle = (Label)item.FindControl("LblTitle");
                        Label LblCompany = (Label)item.FindControl("LblCompany");

                        Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                        Match match = regex.Match(LblEmail.Text);

                        if (match.Success)
                        {
                            ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                            if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, LblEmail.Text, CustID, Trans, Conn) == false)
                            {
                                ObjContactListBOL.Name = LblName.Text;
                                ObjContactListBOL.LastName = "";
                                ObjContactListBOL.Email = LblEmail.Text;
                                ObjContactListBOL.Company = LblCompany.Text;
                                ObjContactListBOL.Title = LblTitle.Text;
                                ObjContactListBOL.ZipCode = "";

                                if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == false)
                                {
                                    Trans.Rollback();
                                    Conn.Close();
                                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                                    break;
                                }
                            }
                            else { Email++; }
                        }
                        else
                        { Count++; }
                    }

                    if (Count != 0 && Email == 0)
                    {
                        if (Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        } 
                    }
                    else if (Count == 0 && Email != 0)
                    {
                        if (Email == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist.";
                        }
                        else if (Email > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist.";
                        }
                    }
                    else if (Count != 0 && Email != 0)
                    {
                        if (Email == 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and "+ Count.ToString()+ " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email == 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                    }

                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    MessageBox(div1, LblMsg, "Saved Successfully", 1);
                }
                LoadContactList();
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public void Clear()
        {
            DdlContactList.SelectedIndex = -1;
            TxtListTitle.Text = "";
            RptUploadContacts.DataSource = null;
            RptUploadContacts.DataBind();

            BtnSave.Visible = false;
            BtnUpload.Visible = true;
        }

        public void SavingNewContacts()
        {
            try
            {
                //@ListTitle,@CreatedBy
                ObjContactListBOL.ListTitle = TxtListTitle.Text;
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();
                if (ObjContactListDAL.InsertContactListMaster(ObjContactListBOL, Trans, Conn) == true)
                {
                    //@Cont_MID,@FirstName,@LastName,@Email,@Company,@Title,@ZipCode

                    foreach (RepeaterItem item in RptUploadContacts.Items)
                    {
                        Label LblName = (Label)item.FindControl("LblName");
                        Label LblEmail = (Label)item.FindControl("LblEmail");
                        Label LblTitle = (Label)item.FindControl("LblTitle");
                        Label LblCompany = (Label)item.FindControl("LblCompany");

                        Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                        Match match = regex.Match(LblEmail.Text);

                        if (match.Success)
                        {
                            if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, LblEmail.Text, ObjContactListBOL.CreatedBy, Trans, Conn) == false)
                            {
                                ObjContactListBOL.Name = LblName.Text;
                                ObjContactListBOL.LastName = "";
                                ObjContactListBOL.Email = LblEmail.Text;
                                ObjContactListBOL.Company = LblCompany.Text;
                                ObjContactListBOL.Title = LblTitle.Text;
                                ObjContactListBOL.ZipCode = "";

                                if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == false)
                                {
                                    Trans.Rollback();
                                    Conn.Close();
                                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                                    break;
                                }
                            }
                            else { Email++; }
                        }
                        else
                        { Count++; }
                    }

                    if (Count != 0 && Email == 0)
                    {
                        if (Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                    }
                    else if (Count == 0 && Email != 0)
                    {
                        if (Email == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist.";
                        }
                        else if (Email > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist.";
                        }
                    }
                    else if (Count != 0 && Email != 0)
                    {
                        if (Email == 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and " + Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email == 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                    }

                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    MessageBox(div1, LblMsg, "Saved Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void SavingGeneralContacts()
        {
            try
            {
                //@ListTitle,@CreatedBy
                ObjContactListBOL.ListTitle = "";
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjContactListDAL.InsertContactListMaster(ObjContactListBOL, Trans, Conn) == true)
                {
                    foreach (RepeaterItem item in RptUploadContacts.Items)
                    {
                        Label LblName = (Label)item.FindControl("LblName");
                        Label LblEmail = (Label)item.FindControl("LblEmail");
                        Label LblTitle = (Label)item.FindControl("LblTitle");
                        Label LblCompany = (Label)item.FindControl("LblCompany");

                        Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                        Match match = regex.Match(LblEmail.Text);

                        if (match.Success)
                        {
                            ObjContactListBOL.Cont_MID = 2;
                            if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, LblEmail.Text, ObjContactListBOL.CreatedBy, Trans, Conn) == false)
                            {
                                ObjContactListBOL.Name = LblName.Text;
                                ObjContactListBOL.LastName = "";
                                ObjContactListBOL.Email = LblEmail.Text;
                                ObjContactListBOL.Company = LblCompany.Text;
                                ObjContactListBOL.Title = LblTitle.Text;
                                ObjContactListBOL.ZipCode = "";

                                if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == false)
                                {
                                    Trans.Rollback();
                                    Conn.Close();
                                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                                    break;
                                } 
                            }
                            else { Email++; }
                        }
                        else
                        { Count++; }
                    }

                    if (Count != 0 && Email == 0)
                    {
                        if (Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                    }
                    else if (Count == 0 && Email != 0)
                    {
                        if (Email == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist.";
                        }
                        else if (Email > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist.";
                        }
                    }
                    else if (Count != 0 && Email != 0)
                    {
                        if (Email == 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and " + Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count == 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contact is skipped due to wrong format of Email.";
                        }
                        else if (Email == 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contact is skipped because same Email is already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                        else if (Email > 1 && Count > 1)
                        {
                            LblCount.Visible = true;
                            LblCount.Text = Email.ToString() + " contacts are skipped because same Emails are already exist and " + Count.ToString() + " contacts are skipped due to wrong format of Email.";
                        }
                    }

                    Trans.Commit();
                    Conn.Close();
                    Clear();
                    MessageBox(div1, LblMsg, "Saved Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }
            }
            catch (Exception ex)
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void DdlContactList_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnChangeDdlContactList();
        }
        public void OnChangeDdlContactList()
        {
            try
            {
                if (DdlContactList.SelectedValue == "1")
                { TxtListTitle.Visible = true; }
                else
                { TxtListTitle.Visible = false; }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnTemplate_Click(object sender, EventArgs e)
        {
            try
            {
                DownloadContactlistTemp();
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
    }
}