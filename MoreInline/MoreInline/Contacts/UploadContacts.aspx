﻿<%@ Page Title="Upload Contacts" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="UploadContacts.aspx.cs" Inherits="MoreInline.Contacts.UploadContacts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>
    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h2 style="color: #001b00;">Upload Contact List</h2>
        </div>
        <div class="col-lg-4"></div>
    </div>

    <div class="row p-t-10">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 text-center">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <br />
    <div class="row p-t-10">
        <div class="col-lg-3 text-center">
            <asp:Button ID="BtnTemplate" runat="server" Text="Download Template" CssClass="btn btn-sm btn-light-green" OnClick="BtnTemplate_Click" /></div>
        <div class="col-lg-3 form-group-sm">
            <asp:DropDownList ID="DdlContactList" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;"
                OnSelectedIndexChanged="DdlContactList_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <div class="col-lg-3">
            <asp:TextBox ID="TxtListTitle" runat="server" Visible="false" CssClass="form-control" placeholder="New List Name"></asp:TextBox>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row p-t-10">
        <div class="col-lg-3"></div>
        <div class="col-lg-5">
            <%--<asp:Button ID="BtnXlsx" runat="server" Text="Select XLSX File" CssClass="btn btn-sm btn-green" Style="background-color: red;" />--%>
            <span style="float: left; padding-right: 5px; color: #006332;"><b>Select XLSX File</b></span>
            <asp:FileUpload ID="FileUploadList" runat="server" CssClass="txtBox-style" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
            <asp:Button ID="BtnUpload" runat="server" Text="Upload" CssClass="btn btn-sm btn-light-green m-t-10"  OnClick="BtnUpload_Click" />
            <asp:Button ID="BtnSave" runat="server" Text="Save" Visible="false" CssClass="btn btn-sm btn-light-green m-t-10" Style="width: 75px;" OnClick="BtnSave_Click" />
        </div>
        <div class="col-lg-4"></div>
        <div class="col-lg-12"></div>
        <div class="col-lg-3"></div>
        <div class="col-lg-8 p-t-5">
            <small>
                <asp:Label ID="LblCount" runat="server" ForeColor="Red" Visible="false"></asp:Label></small>
        </div>
    </div>

    <div class="row p-t-10">
        <div class="col-lg-1"></div>
        <div class="col-lg-10" style="overflow-x: auto;">
            <asp:Repeater ID="RptUploadContacts" runat="server">
                <HeaderTemplate>
                    <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                        <thead class="table-head">
                            <tr style="background-color: #A9C502">
                                <th class="text-center text-black">Name</th>
                                <th class="text-center text-black">Email</th>
                                <th class="text-center text-black">Title</th>
                                <th class="text-center text-black">Company</th>
                            </tr>                       
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
               
                </FooterTemplate>
            </asp:Repeater>
            <%--<asp:GridView ID="RptUploadContacts1" runat="server" CssClass="mydatagrid" PagerStyle-CssClass="pager"
                HeaderStyle-CssClass="header" RowStyle-CssClass="rows" OnPageIndexChanging="RptUploadContacts_PageIndexChanging" AllowPaging="true">
            </asp:GridView>--%>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
