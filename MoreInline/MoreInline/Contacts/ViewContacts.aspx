﻿<%@ Page Title="View Contacts" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="ViewContacts.aspx.cs" Inherits="MoreInline.Contacts.ViewContacts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>

    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>

    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h2 style="color: #001b00;">View Contacts</h2>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1" runat="server">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-3">
            <asp:DropDownList ID="DdlContactList" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;"
                OnSelectedIndexChanged="DdlContactList_SelectedIndexChanged" AutoPostBack="true">
            </asp:DropDownList>
        </div>
        <div class="col-lg-8"></div>
    </div>
    <br />
    <div class="row p-t-10">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <asp:Repeater ID="RptUploadContacts" runat="server" OnItemCommand="RptUploadContacts_ItemCommand">
                <HeaderTemplate>
                    <table id="example1" class="table table-bordered" border="0">
                        <thead class="table-head">
                            <tr style="background-color: #A9C502">
                                <th class="text-center text-black">SNo.</th>
                                <th class="text-center text-black">Name</th>
                                <th class="text-center text-black">Email</th>
                                <th class="text-center text-black">Title</th>
                                <th class="text-center text-black">Company</th>
                                <th class="text-center text-black">Edit/ Delete</th>
                            </tr>
                            <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="text-center">
                            <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                            <asp:Label ID="LblCodeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cont_DID") %>' Visible="false" />
                            <asp:Label ID="LblCodeMID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cont_MID") %>' Visible="false" />
                            <asp:Label ID="LblZipCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ZipCode") %>' Visible="false" />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblTitle" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Title") %>' />
                        </td>
                        <td class="text-center">
                            <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />

                        </td>
                        <td class="text-center">
                            <asp:ImageButton ID="ImgEdit" runat="server" ImageUrl="~/Images/MoreInline Edit Icon.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cont_DID") %>' CommandName="Edit" />
                            <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/Images/Icons/Delete.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cont_DID") %>' CommandName="Delete" OnClientClick='javascript:return confirm("Do you want to delete?")' />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody></table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-6"></div>
        <div class="col-lg-5 text-right">
            <asp:Button ID="BtnDownload" runat="server" Visible="false" OnClick="BtnDownload_Click" Text="Download" CssClass="btn btn-sm btn-light-green" />
            <asp:Button ID="BtnDeleteList" runat="server" Visible="false" OnClick="BtnDeleteList_Click" Text="Delete List" CssClass="btn btn-sm btn-danger" />
        </div>
        <div class="col-lg-1"></div>
    </div>
    <%------------------------------------------Pop Up----------------------------------------------------------%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="color: #001b00;">Edit Contact</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div id="div2" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                            </div>
                            <br />
                        </div>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-7">
                            <asp:DropDownList ID="DdlContactListPop" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;">
                            </asp:DropDownList>
                            <br />
                            <asp:TextBox ID="TxtName" runat="server"  CssClass="form-control m-t-5" placeholder="Name"></asp:TextBox>
                            <asp:TextBox ID="TxtLName" runat="server"  Visible="false" CssClass="form-control m-t-5" placeholder="Last Name"></asp:TextBox>
                            <asp:TextBox ID="TxtEmail" runat="server"  CssClass="form-control m-t-5" placeholder="Email"></asp:TextBox>
                            <asp:TextBox ID="TxtCompany" runat="server"  CssClass="form-control m-t-5" placeholder="Company (Optional)"></asp:TextBox>
                            <asp:TextBox ID="TxtTitle" runat="server"  CssClass="form-control m-t-5" placeholder="Title (Optional)"></asp:TextBox>
                            <asp:TextBox ID="TxtZipCode" runat="server"  CssClass="form-control m-t-5" placeholder="Zip Code (Optional)"></asp:TextBox>

                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <div class="text-center">
                    <asp:Button ID="BtnUpdate" runat="server" Text="Save Changes" OnClick="BtnUpdate_Click" CssClass="btn btn-sm btn-light-green" Style="width: 110px;" />
                    <asp:Button ID="BtnMove" runat="server" Text="Move" OnClick="BtnMove_Click" CssClass="btn btn-sm btn-light-green" Style="width: 110px;" />
                    <asp:Button ID="BtnCopy" runat="server" Text="Copy" OnClick="BtnCopy_Click" CssClass="btn btn-sm btn-black-green" Style="width: 110px;" />
                </div>
            </div>
        </div>
    </div>
    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
