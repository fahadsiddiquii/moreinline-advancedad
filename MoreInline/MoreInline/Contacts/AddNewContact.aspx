﻿<%@ Page Title="Add New Contact" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="AddNewContact.aspx.cs" Inherits="MoreInline.Contacts.AddNewContact" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>
    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>

    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-4">
            <h2 style="color: #001b00;">Add New Contact</h2>
            <div class="row p-t-10">
                <div class="col-lg-12">
                    <div id="div1" runat="server" visible="false">
                        <strong>
                            <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-lg-7">
                    <asp:DropDownList ID="DdlContactList" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;"
                        OnSelectedIndexChanged="DdlContactList_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <div class="col-lg-5"></div>
            </div>
            <asp:TextBox ID="TxtListTitle" runat="server"  CssClass="form-control m-t-5" Visible="false" placeholder="List Title"></asp:TextBox>
            <br />
            <asp:TextBox ID="TxtName" runat="server"  CssClass="form-control m-t-5" placeholder="Name"></asp:TextBox>
            <asp:TextBox ID="TxtLName" runat="server"  Visible="false" CssClass="form-control m-t-5" placeholder="Last Name"></asp:TextBox>
            <asp:TextBox ID="TxtEmail" runat="server"  CssClass="form-control m-t-5" placeholder="Email"></asp:TextBox>
            <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control m-t-5" placeholder="Company (Optional)"></asp:TextBox>
            <asp:TextBox ID="TxtTitle" runat="server"  CssClass="form-control m-t-5" placeholder="Title (Optional)"></asp:TextBox>
            <asp:TextBox ID="TxtZipCode" runat="server"  CssClass="form-control m-t-5" placeholder="Zip Code (Optional)"></asp:TextBox>

            <div class="row m-t-10">
                <div class="col-lg-7"></div>
                <div class="col-lg-5 text-right">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="btn btn-sm btn-light-green" Style="width: 90px;" OnClick="BtnSave_Click" />
                </div>
            </div>

        </div>
        <div class="col-lg-5"></div>
    </div>
    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
