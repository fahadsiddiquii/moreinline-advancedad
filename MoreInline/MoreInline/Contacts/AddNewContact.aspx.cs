﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.Contacts
{
    public partial class AddNewContact : System.Web.UI.Page
    {
        ContactListBOL ObjContactListBOL = new ContactListBOL();
        ContactListDAL ObjContactListDAL = new ContactListDAL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    LoadContactList();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }
        public void LoadContactList()
        {
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);
            //Cont_ID,ListTitle
            DdlContactList.DataSource = dt;
            DdlContactList.DataTextField = "ListTitle";
            DdlContactList.DataValueField = "Cont_ID";
            DdlContactList.DataBind();
            DdlContactList.Items.Insert(0, new ListItem("Select Contact List", "0"));
            DdlContactList.Items.Insert(1, new ListItem("Create New List", "1"));
            DdlContactList.Items.Insert(2, new ListItem("General List", "2"));
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div1.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void DdlContactList_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnChangeDdlContactList();
        }

        public void OnChangeDdlContactList()
        {
            try
            {
                if (DdlContactList.SelectedValue == "1")
                { TxtListTitle.Visible = true; }
                else
                { TxtListTitle.Visible = false; }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DdlContactList.SelectedValue == "0")
                {
                    MessageBox(div1, LblMsg, "Select Contact List", 0);
                    return;
                }
                if (DdlContactList.SelectedValue == "1")
                {
                    if (TxtListTitle.Text == "")
                    {
                        MessageBox(div1, LblMsg, "Enter List Title", 0);
                        return;
                    }
                }
                if (Validation() == true)
                {
                    Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                    Match match = regex.Match(TxtEmail.Text);
                    if (!match.Success)
                    {
                        div1.Visible = true;
                        MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                        return;
                    }

                    //DataTable dt = ObjContactListDAL.GetContactListMaster(ObjContactListBOL);

                    if (DdlContactList.SelectedValue == "1")
                    {
                        SavingNewContacts();
                    }
                    else if (DdlContactList.SelectedValue == "2")
                    {
                        SavingGeneralContacts();
                    }
                    else
                    {

                        ObjContactListBOL.Cont_MID = int.Parse(DdlContactList.SelectedValue);
                        ObjContactListBOL.Name = TxtName.Text;
                        ObjContactListBOL.LastName = "";
                        ObjContactListBOL.Email = TxtEmail.Text;
                        ObjContactListBOL.Company = TxtCompany.Text;
                        ObjContactListBOL.Title = TxtTitle.Text;
                        ObjContactListBOL.ZipCode = TxtZipCode.Text;
                        ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

                        Conn = DBHelper.GetConnection();
                        Trans = Conn.BeginTransaction();

                        if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, ObjContactListBOL.Email, ObjContactListBOL.CreatedBy, Trans, Conn) == false)
                        {
                            if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == false)
                            {
                                Trans.Rollback();
                                Conn.Close();
                                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                            }
                            else
                            {
                                Trans.Commit();
                                Conn.Close();
                                Clear();
                                MessageBox(div1, LblMsg, "Saved Successfully", 1);

                            }
                        }
                        else
                        {
                            Trans.Rollback();
                            Conn.Close();
                            MessageBox(div1, LblMsg, "Email already exists.", 0);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void SavingNewContacts()
        {
            //@ListTitle,@CreatedBy

            ObjContactListBOL.ListTitle = TxtListTitle.Text;
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            if (ObjContactListDAL.InsertContactListMaster(ObjContactListBOL, Trans, Conn) == true)
            {
                //@Cont_MID,@FirstName,@LastName,@Email,@Company,@Title,@ZipCode

                ObjContactListBOL.Name = TxtName.Text;
                ObjContactListBOL.LastName = "";
                ObjContactListBOL.Email = TxtEmail.Text;
                ObjContactListBOL.Company = TxtCompany.Text;
                ObjContactListBOL.Title = TxtTitle.Text;
                ObjContactListBOL.ZipCode = TxtZipCode.Text;

                if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, ObjContactListBOL.Email, ObjContactListBOL.CreatedBy, Trans, Conn) == false)
                {
                    if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();
                        ClearSave();
                        MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Email already exists.", 0);
                }

            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }
        public void SavingGeneralContacts()
        {
            //@ListTitle,@CreatedBy

            ObjContactListBOL.ListTitle = "";
            ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());

            Conn = DBHelper.GetConnection();
            Trans = Conn.BeginTransaction();

            if (ObjContactListDAL.InsertContactListMaster(ObjContactListBOL, Trans, Conn) == true)
            {
                //@Cont_MID,@FirstName,@LastName,@Email,@Company,@Title,@ZipCode

                ObjContactListBOL.Cont_MID = 2;
                ObjContactListBOL.CreatedBy = int.Parse(Session["Cust_ID"].ToString());
                ObjContactListBOL.Name = TxtName.Text;
                ObjContactListBOL.LastName = "";
                ObjContactListBOL.Email = TxtEmail.Text;
                ObjContactListBOL.Company = TxtCompany.Text;
                ObjContactListBOL.Title = TxtTitle.Text;
                ObjContactListBOL.ZipCode = TxtZipCode.Text;

                // Conn = DBHelper.GetConnection();
                ///   Trans = Conn.BeginTransaction();

                if (ObjContactListDAL.GetIsContactExist(ObjContactListBOL.Cont_MID, ObjContactListBOL.Email, ObjContactListBOL.CreatedBy, Trans, Conn) == false)
                {
                    if (ObjContactListDAL.InsertContactListDetails(ObjContactListBOL, Trans, Conn) == true)
                    {
                        Trans.Commit();
                        Conn.Close();
                        Clear();
                        MessageBox(div1, LblMsg, "Operation Performed Successfully", 1);
                    }
                    else
                    {
                        Trans.Rollback();
                        Conn.Close();
                        MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                    }
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Email already exists.", 0);
                }
            }
            else
            {
                Trans.Rollback();
                Conn.Close();
                MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
            }
        }

        public bool Validation()
        {
            if (TxtName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Name", 0);
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        public void Clear()
        {
            TxtListTitle.Text = TxtName.Text = TxtLName.Text = TxtEmail.Text = TxtCompany.Text = TxtTitle.Text = TxtZipCode.Text = "";
            DdlContactList.SelectedValue = "0";
            OnChangeDdlContactList();
        }

        public void ClearSave()
        {
            TxtListTitle.Text = TxtName.Text = TxtLName.Text = TxtEmail.Text = TxtCompany.Text = TxtTitle.Text = TxtZipCode.Text = "";
            LoadContactList();
            OnChangeDdlContactList();
        }
    }
}