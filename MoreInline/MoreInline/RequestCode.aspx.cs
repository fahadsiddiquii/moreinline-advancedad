﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class RequestCode : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            ValidationSettings.UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
            div1.Visible = false;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                if (Local_Validation())
                {
                    if (IsCustomerEmailExist())
                    {
                        string Emailbody = Email_Construction();
                        if (SendEmail(Emailbody))
                        {
                            MessageBox(div1, LblMsg, "Email successfully sent. Thank you for request.", 1);
                            Response.AppendHeader("Refresh", "5;url=LoginPage.aspx");
                            //Response.Redirect("LoginPage.aspx");
                        }
                    }
                }
            }
             catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        private bool IsCustomerEmailExist()
        {
            AdvertisementDAL ObjAdvertisementDAL = new AdvertisementDAL();
            DataTable dt = ObjAdvertisementDAL.CheckEmail(TxtEmail.Text);
            DataTable dtCust = ObjAdvertisementDAL.CheckActiveEmail(TxtEmail.Text);
            if (dt.Rows.Count > 0)
            {
                MessageBox(div1, LblMsg, "We already have your request with this email. We will send you promo code soon.", 0);
                return false;
            }
            else if (dtCust.Rows.Count > 0)
            {
                MessageBox(div1, LblMsg, "This email is already an active member.", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        private string Email_Construction()
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerEmail.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{name}", TxtFName.Text + " " + TxtLName.Text);
            body = body.Replace("{email}", TxtEmail.Text);
            body = body.Replace("{contact}", TxtContact.Text);
            body = body.Replace("{company}", TxtCompany.Text);
            body = body.Replace("{message}", "This person wants to use our service and applied for 15 days free trial.");
            return body;
        }

        private bool Local_Validation()
        {
            TxtFName.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtLName.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtCompany.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtEmail.Attributes["style"] = "border: 1px solid #4B4A4A;";
            TxtContact.Attributes["style"] = "border: 1px solid #4B4A4A;";

            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                TxtFName.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                TxtLName.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Company Name", 0);
                TxtCompany.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                TxtEmail.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else if (TxtContact.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Contact", 0);
                TxtContact.Attributes["style"] = "border: 1px solid red;";
                return false;
            }
            else
            {
                return true;
            }
        }

       
        private bool SendEmail(string body)
        {
            MailMessage mm = new MailMessage("donotreply@moreinline.com", "stan.ferrell64@gmail.com");
            mm.Subject = "Free Trial Request";
            if (AddFile.HasFile)
            {
                if (Page.IsValid)
                {
                    string filename = Path.GetFileName(AddFile.PostedFile.FileName);
                    mm.Attachments.Add(new Attachment(AddFile.PostedFile.InputStream, filename));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                MessageBox(div1, LblMsg, "Upload Logo", 0);
                return false;
            }
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
            return true;
        }

        protected void CustomValidatorImg_ServerValidate(object source, ServerValidateEventArgs args)
        {
            decimal size = Math.Round(((decimal)AddFile.PostedFile.ContentLength / (decimal)1024), 2);
            if (Regxcheck.IsValidImage(AddFile))
            {
                if (size > 200)
                {
                    CustomValidatorImg.ErrorMessage = "Image size exceeds 200 KB.";
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
            else
            {
                CustomValidatorImg.ErrorMessage = "Only .jpg,.png,.jpeg Files are allowed.";
                args.IsValid = false;
            }
        }

        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }


    }
}