﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class CheckingAccountDAL
    {
        // CA_ID AccountType AccountName AccountNumber   RoutingNumber DL_Number   AO_BirthDate
        int result = 0;
        public bool InsertCheckingAccount(CheckingAccountBOL ObjCheckingAccountBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertCheckingAccount";

                cmd.Parameters.Add("@AccountType ", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.AccountType;
                cmd.Parameters.Add("@AccountName", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.AccountName;
                cmd.Parameters.Add("@AccountNumber", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.AccountNumber;
                cmd.Parameters.Add("@RoutingNumber", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.RoutingNumber;
                cmd.Parameters.Add("@DL_Number", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.DL_Number;
                cmd.Parameters.Add("@AO_BirthDate", SqlDbType.VarChar).Value = ObjCheckingAccountBOL.AO_BirthDate;

                cmd.Parameters.Add("@CA_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    ObjCheckingAccountBOL.CA_ID = int.Parse(cmd.Parameters["@CA_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetCheckingAccountInformation(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetCheckingAccountInfo";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }

        public bool UpdateCAInfo(CheckingAccountBOL ObjCheckingAccountBOL, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.CommandText = "SP_UpdateCAInfo";

                cmd.Parameters.Add("@CA_ID", SqlDbType.Int).Value = ObjCheckingAccountBOL.CA_ID;
                cmd.Parameters.Add("@AccountType", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.AccountType;
                cmd.Parameters.Add("@AccountName", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.AccountName;
                cmd.Parameters.Add("@AccountNumber", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.AccountNumber;
                cmd.Parameters.Add("@RoutingNumber", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.RoutingNumber;
                cmd.Parameters.Add("@DL_Number", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.DL_Number;
                cmd.Parameters.Add("@AO_BirthDate", SqlDbType.NVarChar).Value = ObjCheckingAccountBOL.AO_BirthDate;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}