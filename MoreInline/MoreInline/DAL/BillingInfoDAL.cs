﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class BillingInfoDAL
    {
        int result = 0;
        public bool InsertBillingInformation(BillingInfoBOL ObjBillingInfoBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertBillingInformation";
                //Bill_ID	FirstName	LastName	Organization  CountryCode  Phone	Email	TaxID	Country	Address	Address2	City	Province	PostalCode

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjBillingInfoBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjBillingInfoBOL.LastName;
                cmd.Parameters.Add("@Organization", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Organization;
                cmd.Parameters.Add("@CountryCode", SqlDbType.VarChar).Value = ObjBillingInfoBOL.CountryCode;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Email;
                cmd.Parameters.Add("@TaxID", SqlDbType.VarChar).Value = ObjBillingInfoBOL.TaxID;
                cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Country;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Address;
                cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Address2;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ObjBillingInfoBOL.City;
                cmd.Parameters.Add("@Province", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Province;
                cmd.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = ObjBillingInfoBOL.PostalCode;
                

                cmd.Parameters.Add("@Bill_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    ObjBillingInfoBOL.Bill_ID = int.Parse(cmd.Parameters["@Bill_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetBillingInformation(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetBillingInformation";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }

        public bool UpdateBillingInfo(BillingInfoBOL ObjBillingInfoBOL,  SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.CommandText = "SP_UpdateBillingInfo";

                cmd.Parameters.Add("@Bill_ID", SqlDbType.Int).Value = ObjBillingInfoBOL.Bill_ID;
                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjBillingInfoBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjBillingInfoBOL.LastName;
                cmd.Parameters.Add("@Organization", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Organization;
                cmd.Parameters.Add("@CountryCode", SqlDbType.VarChar).Value = ObjBillingInfoBOL.CountryCode;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Phone;
                cmd.Parameters.Add("@TaxID", SqlDbType.VarChar).Value = ObjBillingInfoBOL.TaxID;
                cmd.Parameters.Add("@Country", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Country;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Address;
                cmd.Parameters.Add("@Address2", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Address2;
                cmd.Parameters.Add("@City", SqlDbType.VarChar).Value = ObjBillingInfoBOL.City;
                cmd.Parameters.Add("@Province", SqlDbType.VarChar).Value = ObjBillingInfoBOL.Province;
                cmd.Parameters.Add("@PostalCode", SqlDbType.VarChar).Value = ObjBillingInfoBOL.PostalCode;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}