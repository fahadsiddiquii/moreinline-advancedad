﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class CustomerProfileMasterDAL
    {
        int result = 0;

        public bool InsertCustomerProfileMaster(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertCustomerProfileMaster";
                //Cust_ID	FirstName	LastName	Company	Website	Address	ZipCode	Email	Phone	UserName	Password	Photo	CodeID

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.LastName;
                cmd.Parameters.Add("@Company", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Company;
                cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Website;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Address;
                cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.ZipCode;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Email;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Phone;
                cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.UserName;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Password;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Photo;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CodeID;
                cmd.Parameters.Add("@Bill_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Bill_ID;
                cmd.Parameters.Add("@CDP_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CDP_ID;
                cmd.Parameters.Add("@CA_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CA_ID;
                cmd.Parameters.Add("@AccTypeID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.AccTypeID;
                cmd.Parameters.Add("@EmailsLimit", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.EmailsLimit;
                cmd.Parameters.Add("@CompanyLogo", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.CompanyLogo;
                
                cmd.Parameters.Add("@Cust_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(cmd.Parameters["@Cust_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetCustomerProfileExist(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetCustomerProfileExist";
                cmd.Parameters.Add("@ColumnName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.ColumnName;
                cmd.Parameters.Add("@ColumnText", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.ColumnText;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetAboutExpCust()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetAboutExpCust";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetSnapByEmail(string email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
                cmd.CommandText = "SP_GetSnapByEmail";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public DataTable GetCustForWeb()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetCustForWeb";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return new DataTable();
            }
        }


        public DataTable GetWebsiteData(int CustID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetWebsiteData";
                cmd.Parameters.Add("@CustID",SqlDbType.Int).Value = CustID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return new DataTable();
            }
        }
        
        public bool UpdateExpStatus(int id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateExpStatus";
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = id;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public DataTable CustomerLogin(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_CustomerLogin";
                //UserName,Password
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Email;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Password;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetExistActiveCustomerAccount(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetExistActiveCustomerAccount";
                //UserName,Password
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetExistActiveCustomerAccount(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;

                cmd.CommandText = "SP_GetExistActiveCustomerAccount";
                //UserName,Password
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateActiveCustomerProfileMaster(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateActiveCustomerProfileMaster";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetCustomerProfile(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetCustomerProfile";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return new DataTable();
            }
        }

        public DataTable GetCustomerProfile()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Select (FirstName +' '+ LastName)Name,*  from Tbl_CustomerProfileMaster";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSnapStatus(string Email)
        {
            try
            {
                SqlConnection con = DBHelper.GetConnection();
                SqlCommand cmd = new SqlCommand("update Tbl_Snap set Status = 1 where Email = '" + Email + "'", con);
              //  con.Open();
                int res = cmd.ExecuteNonQuery();
                //con.Close();
                if (res >= 1)
                {
                    return true;
                 
                }
                else
                {
                    return false;
                }
           
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateCustomerProfile(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCustomerProfile";
                // @FirstName @LastName    @Company @Website @Address @ZipCode @Password   @Phone  @Photo @UpdatedBy 

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.LastName;
                cmd.Parameters.Add("@Company", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Company;
                cmd.Parameters.Add("@Website", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Website;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Address;
                cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.ZipCode;
                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Password;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Phone;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Photo;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.UpdatedBy;
                cmd.Parameters.Add("@CLogo", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.CompanyLogo;
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetViewCustomers()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetViewCustomers";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetViewAddsByUserEmail(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@CustEmail", SqlDbType.VarChar).Value = Email;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetAddsByEmail";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetViewPendingCustomers(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetViewPendingCustomers";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDeletingPendingCustomer(int Ads)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select * From Tbl_Advertisement where AdsID="+Ads;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public DataTable GetExpiredCustomers()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetExpiredCustomers";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public DataTable GetPrepaidCustomers()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "select *  from Tbl_Snap where Status = 0";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetViewCodeExpireCustomers(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetViewCodeExpireCustomers";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetCustomerOrderDetails(int CreatedByID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetCustomerOrderDetails";
                //UserName,Password
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = CreatedByID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //To Find if user accept code from the link
        public DataTable GetExistActiveCode(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetExistActiveCode";
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CodeID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetEmailExist(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetEmailExist";
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Email;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateUserPassword(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCustomerProfileMasterPassword";

                cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Password;
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateRegularPlanCustomerProfile(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateRegularPlanCustomerProfile";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;
                cmd.Parameters.Add("@Bill_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Bill_ID;
                cmd.Parameters.Add("@CDP_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CDP_ID;
                cmd.Parameters.Add("@CA_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.CA_ID;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;
                cmd.Parameters.Add("@AccTypeID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.AccTypeID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public int GetCodeNo(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetCodeID";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;
            int CodeID = int.Parse(cmd.ExecuteScalar().ToString());
            return CodeID;
        }
        public DataTable GetValidDaysDate(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetValidDaysDate";
                cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetAccountType(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetAccountType";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;
            string accountName = cmd.ExecuteScalar().ToString();
            return accountName;
        }

        public DataTable GetCustomerValidations(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetCustomerValidations";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = ObjCustomerProfileMasterBOL.Cust_ID;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }

        public string GetCustomerName(int Cust_ID)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "Select (FirstName+' '+LastName)Name From Tbl_CustomerProfileMaster Where Cust_ID="+Cust_ID;
            string CustName = cmd.ExecuteScalar().ToString();
            return CustName;
        }


        public bool UpdateAccountType(int Cust_ID, int AccTypeID, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.CommandText = "SP_UpdateCustomerAccType";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;
                cmd.Parameters.Add("@AccTypeID", SqlDbType.Int).Value = AccTypeID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool Unsubscribe(int Cust_ID, string Columname)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
               string Query = @"declare @column_name NVARCHAR(MAX) 
                declare @Cust int;
                declare @sql nvarchar(MAX) 
                set @Cust = p1 
                set @column_name ='p2'
                set @sql = N'update dbo.Tbl_CustomerProfileMaster set ' + @column_name + '=0 where Cust_ID = @Cust'
                exec sp_executesql @sql, N'@Cust int', @Cust = @Cust";
                Query = Query.Replace("p1", Cust_ID.ToString());
                Query = Query.Replace("p2", Columname);
                cmd.CommandText = Query;
               result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Unsubscribe(int Cust_ID, string Email,int ListID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustID", SqlDbType.Int).Value = Cust_ID;
                cmd.Parameters.Add("@ListID", SqlDbType.Int).Value = ListID;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;

                cmd.CommandText = "SP_UnsubscribeEmails";
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCDP_ID(int Cust_ID, int CDP_ID, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCustomerCDPID";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;
                cmd.Parameters.Add("@CDP_ID", SqlDbType.Int).Value = CDP_ID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCA_ID(int Cust_ID, int CA_ID, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCustomerCAID";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;
                cmd.Parameters.Add("@CA_ID", SqlDbType.Int).Value = CA_ID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateBill_ID(int Cust_ID, int Bill_ID, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCustomerBillID";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;
                cmd.Parameters.Add("@Bill_ID", SqlDbType.Int).Value = Bill_ID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateCustomerTour(string Page, int Cust_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateCustomerTour";

                cmd.Parameters.Add("@Page", SqlDbType.VarChar).Value = Page;
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteCode(int CodeID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Delete From Tbl_CodeMaster where CodeID="+CodeID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteAd(int AdsID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Delete From Tbl_Advertisement where AdsID=" + AdsID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCustomerActivation(CustomerProfileMasterBOL ObjCustomerProfileMasterBOL)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateCustomerActivation";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.Cust_ID;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.IsActive;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjCustomerProfileMasterBOL.UpdatedBy;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool InsertSnap(string Fname,string Email,string AcType, int Amount,string logo,int TransactionId,string PyamnetType)
        {
            try
            {
                
                
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InserSnap";
                cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = Email;
                cmd.Parameters.Add("@AcType", SqlDbType.VarChar).Value = AcType;
                cmd.Parameters.Add("@amnt", SqlDbType.Money).Value = Amount;
                cmd.Parameters.Add("@cd", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@clogo", SqlDbType.VarChar).Value = logo;
                cmd.Parameters.Add("@fname", SqlDbType.VarChar).Value = Fname;
                cmd.Parameters.Add("@transId", SqlDbType.VarChar).Value = TransactionId.ToString();
                cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar).Value = PyamnetType; 


                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception rx)
            {
                return false;
            }
        }


    }
}