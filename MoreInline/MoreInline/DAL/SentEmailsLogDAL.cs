﻿using MoreInline.BOL;
using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using System.Data;
using System.Data.SqlClient;


namespace MoreInline.DAL
{
    public class SentEmailsLogDAL
    {
        
        public DataTable GetSentAudioPath(SentEmailLogBOL ObjSentEmailLogBOL ,int assetID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetEmailSetAudio";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = assetID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}