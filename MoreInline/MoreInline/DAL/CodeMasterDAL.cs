﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class CodeMasterDAL
    {
        int result = 0;
        public DataTable GetCodeNo(CodeMasterBOL ObjCodeMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetCodeNo";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertCodeMaster(CodeMasterBOL ObjCodeMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertCodeMaster";
                //@CodeNumber,@CodeText,@FullCode,@ValidationDays,@IsUtilized,@IsPaid,@CreatedBy

                cmd.Parameters.Add("@CodeNumber", SqlDbType.VarChar).Value = ObjCodeMasterBOL.CodeNumber;
                cmd.Parameters.Add("@CodeText", SqlDbType.VarChar).Value = ObjCodeMasterBOL.CodeText;
                cmd.Parameters.Add("@FullCode", SqlDbType.VarChar).Value = ObjCodeMasterBOL.FullCode;
                cmd.Parameters.Add("@ValidationDays", SqlDbType.VarChar).Value = ObjCodeMasterBOL.ValidationDays;
                cmd.Parameters.Add("@IsUtilized", SqlDbType.Int).Value = ObjCodeMasterBOL.IsUtilized;
                //cmd.Parameters.Add("@IsPaid", SqlDbType.Int).Value = ObjCodeMasterBOL.IsPaid;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjCodeMasterBOL.CreatedBy;

                cmd.Parameters.Add("@CodeID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    ObjCodeMasterBOL.CodeID = int.Parse(cmd.Parameters["@CodeID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateCodeMaster(CodeMasterBOL ObjCodeMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateCodeMaster";
                //@CodeNumber,@CodeText,@FullCode,@ValidationDays,@IsPaid,@CreatedBy

                cmd.Parameters.Add("@CodeNumber", SqlDbType.VarChar).Value = ObjCodeMasterBOL.CodeNumber;
                cmd.Parameters.Add("@CodeText", SqlDbType.VarChar).Value = ObjCodeMasterBOL.CodeText;
                cmd.Parameters.Add("@FullCode", SqlDbType.VarChar).Value = ObjCodeMasterBOL.FullCode;
                cmd.Parameters.Add("@ValidationDays", SqlDbType.VarChar).Value = ObjCodeMasterBOL.ValidationDays;
                cmd.Parameters.Add("@IsPaid", SqlDbType.Int).Value = ObjCodeMasterBOL.IsPaid;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjCodeMasterBOL.UpdatedBy;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCodeMasterBOL.CodeID;


                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateValidationDays(CodeMasterBOL ObjCodeMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateValidationDays";
                //@ValidationDays,@UpdatedBy

                cmd.Parameters.Add("@ValidationDays", SqlDbType.VarChar).Value = ObjCodeMasterBOL.ValidationDays;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCodeMasterBOL.CodeID;


                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetCodeMaster(CodeMasterBOL ObjCodeMasterBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetCodeMaster";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateValidDateCodeMaster(CodeMasterBOL ObjCodeMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateValidDateCodeMaster";

                cmd.Parameters.Add("@ValidDate", SqlDbType.DateTime).Value = ObjCodeMasterBOL.ValidDate;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCodeMasterBOL.CodeID;


                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateDateDays(CodeMasterBOL ObjCodeMasterBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateDateDays";
                //@ValidationDays,@UpdatedBy

                cmd.Parameters.Add("@ValidDate", SqlDbType.VarChar).Value = ObjCodeMasterBOL.ValidDate;
                cmd.Parameters.Add("@ValidationDays", SqlDbType.VarChar).Value = ObjCodeMasterBOL.ValidationDays;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = ObjCodeMasterBOL.CodeID;


                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateUtilizationCode(int CodeID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Update Tbl_CodeMaster SET IsUtilized=1 where CodeID="+CodeID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



    }
}