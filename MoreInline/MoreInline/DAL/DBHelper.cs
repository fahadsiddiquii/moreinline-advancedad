﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace MoreInline.DAL
{
    public class DBHelper
    {
        public static string str = ConfigurationManager.ConnectionStrings["MoreInline"].ConnectionString;

        public static SqlConnection GetConnection()
        {
            SqlConnection con = new SqlConnection(str);
            if (con.State == ConnectionState.Closed || con.State == ConnectionState.Broken)
            {
                con.Open();
            }
            return con;
        }
    }
}