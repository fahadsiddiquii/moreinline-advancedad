﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class WebsiteDAL
    {
        int result = 0;
        public bool InsertCustomerWebsite(WebsiteBOL Website)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertWebsite";
                cmd.Parameters.Add("@logoPath", SqlDbType.VarChar).Value = Website.logoPath;
                cmd.Parameters.Add("@BannerPath", SqlDbType.VarChar).Value = Website.BannerPath;
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Website.Cust_ID;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public DataTable GetCustomerForWebsite(int CustID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Select * From Tbl_Website where Cust_ID="+CustID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}