﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class AdsBroadcast
    {
        public int Ad_ID { get; set; }
        public string AdUrl { get; set; }
        public int SendBy { get; set; }
        public DateTime SendOn { get; set; }

        int result;
        public DataTable GetBroadcastHistory()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select Ad_ID, AdUrl, SendOn, (FirstName + ' ' + LastName)SendBy From BroadcastHistory BH Inner Join Tbl_UserProfile UP On Up.UserID = BH.SendBy";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetCustomersIds()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select Cust_ID From Tbl_CustomerProfileMaster";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertAdsBroadcast(AdsBroadcast Ads)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Sp_InsertAdsBroadcast";

                cmd.Parameters.Add("@AdUrl", SqlDbType.VarChar).Value = Ads.AdUrl;
                cmd.Parameters.Add("@SendBy", SqlDbType.Int).Value = Ads.SendBy;
                cmd.Parameters.Add("@SendOn", SqlDbType.DateTime).Value = Ads.SendOn;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



    }
}