﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class UserSignupDAL
    {
        int result = 0;

        public DataTable GetUserTypes(UserSignupBOL ObjUserSignupBOL)// No need to use Entity here you can get data without Entity
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetUserTypes";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertUserProfile(UserSignupBOL ObjUserSignupBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertUserProfile";
                ////@FirstName,@LastName,@UserEmail,@UserPassword,@UserPhone,@UserAddress,@UserType,@Photo

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjUserSignupBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjUserSignupBOL.LastName;
                cmd.Parameters.Add("@UserEmail", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserEmail;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserPassword;
                cmd.Parameters.Add("@UserPhone", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserPhone;
                cmd.Parameters.Add("@UserAddress", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserAddress;
                cmd.Parameters.Add("@UserType", SqlDbType.Int).Value = ObjUserSignupBOL.UserType;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = ObjUserSignupBOL.Photo;

                cmd.Parameters.Add("@UserID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    ObjUserSignupBOL.UserID = int.Parse(cmd.Parameters["@UserID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateUserProfile(UserSignupBOL ObjUserSignupBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateUserProfile";
                ////@FirstName,@LastName,@UserEmail,@UserPassword,@UserPhone,@UserAddress,@UserType,@Photo

                cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = ObjUserSignupBOL.FirstName;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjUserSignupBOL.LastName;
                cmd.Parameters.Add("@UserEmail", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserEmail;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserPassword;
                cmd.Parameters.Add("@UserPhone", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserPhone;
                cmd.Parameters.Add("@UserAddress", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserAddress;
                cmd.Parameters.Add("@UserType", SqlDbType.Int).Value = ObjUserSignupBOL.UserType;
                cmd.Parameters.Add("@Photo", SqlDbType.VarChar).Value = ObjUserSignupBOL.Photo;
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = ObjUserSignupBOL.UserID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetUserExist(UserSignupBOL ObjUserSignupBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetUserExist";
                cmd.Parameters.Add("@UserEmail", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserEmail;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable UserLogin(UserSignupBOL ObjUserSignupBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_UserLogin";
                //UserEmail,UserPassword
                cmd.Parameters.Add("@UserEmail", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserEmail;
                cmd.Parameters.Add("@UserPassword", SqlDbType.VarChar).Value = ObjUserSignupBOL.UserPassword;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetUserProfile(UserSignupBOL ObjUserSignupBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetUserProfile";
                //UserEmail,UserPassword
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = ObjUserSignupBOL.UserID;

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        
        public DataTable GetAdminInfo()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetAdminInfo";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateUserLimits(UserSignupBOL ObjUserSignupBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;

                cmd.CommandText = "SP_UpdateUserLimits";

                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = ObjUserSignupBOL.UserRoleID;
                cmd.Parameters.Add("@UserLimits", SqlDbType.Int).Value = ObjUserSignupBOL.UserLimits;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetUserAccounts(UserSignupBOL ObjUserSignupBOL)// No need to use Entity here you can get data without Entity
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetUserAccounts";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAccountActivation(UserSignupBOL ObjUserSignupBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;

                cmd.CommandText = "SP_UpdateAccountActivation";

                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = ObjUserSignupBOL.UserID;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = ObjUserSignupBOL.IsActive;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjUserSignupBOL.UpdatedBy;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}