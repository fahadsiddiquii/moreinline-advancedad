﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class PaymentDAL
    {
        public bool InsertPaymentDetail(PaymentBOL ObjPaymentBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            int result = 0;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertPayment";
                //@Cust_ID,@Amount

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjPaymentBOL.Cust_ID;
                cmd.Parameters.Add("@Amount", SqlDbType.Decimal).Value = ObjPaymentBOL.Amount;
               

                cmd.Parameters.Add("@Payment_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    ObjPaymentBOL.Payment_ID = int.Parse(cmd.Parameters["@Payment_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}