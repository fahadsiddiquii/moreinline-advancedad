﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class AdditionalAdDAL
    {
        int result = 0;
        AdditionalAdBOL ObjAABOL = new AdditionalAdBOL();
        public bool InsertAdditionalAds(string FilePath, int userid)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertAdditionalAds";

                cmd.Parameters.Add("@ImageUrl", SqlDbType.VarChar).Value = "~/AdsImages/"+FilePath;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = userid;
                cmd.Parameters.Add("@Img_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    
                    ObjAABOL.Img_ID = int.Parse(cmd.Parameters["@Img_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataTable GetAds()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select * From Tbl_AdditionalAds";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAdsActivation(AdditionalAdBOL ObjAAdsBOL)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateAdsActivation";

                cmd.Parameters.Add("@Img_ID", SqlDbType.Int).Value = ObjAAdsBOL.Img_ID;
                cmd.Parameters.Add("@IsActive", SqlDbType.Int).Value = ObjAAdsBOL.IsActive;
                cmd.Parameters.Add("@UpdatedBy", SqlDbType.Int).Value = ObjAAdsBOL.UpdatedBy;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool DeleteAD(int Img_ID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Delete From Tbl_AdditionalAds where Img_ID=" + Img_ID;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}