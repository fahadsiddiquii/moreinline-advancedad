﻿using MoreInline.BOL;
using MoreInline.Crypto;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class CDPCardDAL
    {
        int result = 0;
        public bool InsertCDPCardInfo(CDPCardBOL ObjCDPCardBOL)
        {
            try
            {
                Cryptography Crypto= new Cryptography();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_InsertCDPCardInfo";
                SqlConnection connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@NameOnCard ", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.NameOnCard);
                cmd.Parameters.Add("@CardNumber", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.CardNumber);
                cmd.Parameters.Add("@SecurityCode", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.SecurityCode);
                cmd.Parameters.Add("@ExpiryDate", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.ExpiryDate);
                cmd.Parameters.Add("@CustID", SqlDbType.NVarChar).Value = ObjCDPCardBOL.Cust_ID; 

                cmd.Connection = connection;
                cmd.Parameters.Add("@CDP_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    ObjCDPCardBOL.CDP_ID = int.Parse(cmd.Parameters["@CDP_ID"].Value.ToString());
                    connection.Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetCDPInformation(int custid)
        {
            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();

            cmd.CommandText = "SP_GetCDPInformation";
            cmd.Parameters.Add("@Cust_ID", SqlDbType.VarChar).Value = custid;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }
        public bool UpdateCDPInfo(CDPCardBOL ObjCDPCardBOL)
        {
            try
            {
                Cryptography Crypto = new Cryptography();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateCDPInfo";

                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjCDPCardBOL.Cust_ID;
                cmd.Parameters.Add("@NameOnCard", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.NameOnCard);
                cmd.Parameters.Add("@CardNumber", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.CardNumber);
                cmd.Parameters.Add("@SecurityCode", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.SecurityCode);
                cmd.Parameters.Add("@ExpiryDate", SqlDbType.NVarChar).Value = Crypto.Encrypt(ObjCDPCardBOL.ExpiryDate);
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}