﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class NotificationInfoDAL
    {
        int result = 0;
        public DataTable GetNotification(int UID)
        {

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();
            cmd.CommandText = "SP_GetNotificationInfo";
            cmd.Parameters.Add("@UID", SqlDbType.VarChar).Value = UID;

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }

        public DataTable GetOrderData(int UID,int UTID)
        {

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();
            cmd.CommandText = "SP_GetOrderData";
            cmd.Parameters.Add("@UID", SqlDbType.VarChar).Value = UID;
            cmd.Parameters.Add("@UserRoleID", SqlDbType.VarChar).Value = UTID;
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return dt;
        }
        
        public int GetManagerID()
        {

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = DBHelper.GetConnection();
            cmd.CommandText = "SP_GetManagerID";

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public int GetAdminID()
        {

            DataTable dt = new DataTable();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.Connection = DBHelper.GetConnection();
            cmd.CommandText = "select TU.UserID from Tbl_UserProfile  TU inner join Tbl_UserRoles TR on TU.UserType = TR.UserRoleID WHERE TR.UserRoles ='admin'";
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            return int.Parse(dt.Rows[0][0].ToString());
        }

        public bool InsertNotification(NotificationInfo Notification, SqlTransaction trns = null, SqlConnection Conn = null)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn == null ? DBHelper.GetConnection() : Conn;
                cmd.Transaction = trns;
                cmd.CommandText = "Sp_InsertNotification";
                cmd.Parameters.Add("@OID", SqlDbType.Int).Value = Notification.OID;
                cmd.Parameters.Add("@UID", SqlDbType.Int).Value = Notification.UID;
                cmd.Parameters.Add("@FormPath", SqlDbType.VarChar).Value = Notification.FormPath;
                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = Notification.UserRoleID; 
                cmd.Parameters.Add("@NotiDate", SqlDbType.DateTime).Value = Notification.NotiDate;
                result = cmd.ExecuteNonQuery();

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertNotificationTrans(NotificationInfo Notification, SqlTransaction trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = trans;
                cmd.CommandText = "Sp_InsertNotification";
                cmd.Parameters.Add("@OID", SqlDbType.Int).Value = Notification.OID;
                cmd.Parameters.Add("@UID", SqlDbType.Int).Value = Notification.UID;
                cmd.Parameters.Add("@FormPath", SqlDbType.VarChar).Value = Notification.FormPath;
                cmd.Parameters.Add("@UserRoleID", SqlDbType.Int).Value = Notification.UserRoleID;
                cmd.Parameters.Add("@NotiDate", SqlDbType.DateTime).Value = Notification.NotiDate;
                result = cmd.ExecuteNonQuery();

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateNoti(int id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateNoti";
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                //cmd.Parameters.Add("@UID", SqlDbType.Int).Value = Notification.UID;
                result = cmd.ExecuteNonQuery();

                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}