﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class ReceivedOrdersDAL
    {
        public DataTable GetReceivedOrders()
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetReceivedOrders";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }


        public DataTable GetUsersName(int Type)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetUsersName";
                cmd.Parameters.Add("@Type", SqlDbType.Int).Value = Type;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

      public bool SP_InsertOrderLog(int DID,int QAID, int CustID,int AssetID,int CreatedByID)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertOrderLog";
                cmd.Parameters.Add("@DID", SqlDbType.Int).Value = DID;
                cmd.Parameters.Add("@QAID", SqlDbType.Int).Value = QAID; 
                cmd.Parameters.Add("@CusID", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@CreateById", SqlDbType.Int).Value = CreatedByID;
                cmd.Parameters.Add("@AssetID", SqlDbType.Int).Value = AssetID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           catch (Exception)
            {
                return false;
            }
        }
        public bool SP_UpdateOrderLog(int DID, int QAID, int ID)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateOrderLog";
                cmd.Parameters.Add("@DID", SqlDbType.Int).Value = DID;
                cmd.Parameters.Add("@QAID", SqlDbType.Int).Value = QAID;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateOrderStatus(int id, int status)
        {


            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateOrderStatus";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}