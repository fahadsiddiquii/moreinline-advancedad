﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class ContactListDAL
    {
        int result = 0;
        public DataTable GetContactListMaster(ContactListBOL ObjContactListBOL)// No need to use Entity here you can get data without Entity
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjContactListBOL.CreatedBy;
                cmd.CommandText = "SP_GetContactListMaster";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool GetIsContactExist(int id, string email, int CustID, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;

                cmd.CommandText = "SP_GetIsContactExist";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = email;
                cmd.Parameters.Add("@CustID", SqlDbType.Int).Value = CustID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
               
                if (dt.Rows[0]["Email"].ToString() == "0")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool InsertContactListMaster(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertContactListMaster";
                //@ListTitle,@CreatedBy

                cmd.Parameters.Add("@ListTitle", SqlDbType.VarChar).Value = ObjContactListBOL.ListTitle;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjContactListBOL.CreatedBy;

                cmd.Parameters.Add("@Cont_ID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    ObjContactListBOL.Cont_MID = int.Parse(cmd.Parameters["@Cont_ID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool InsertContactListDetails(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertContactListDetails";
                //@Cont_MID,@FirstName,@LastName,@Email,@Company,@Title,@ZipCode
               
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjContactListBOL.CreatedBy;
                cmd.Parameters.Add("@Cont_MID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = ObjContactListBOL.Name;
                cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = ObjContactListBOL.LastName;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjContactListBOL.Email;
                cmd.Parameters.Add("@Company", SqlDbType.VarChar).Value = ObjContactListBOL.Company;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = ObjContactListBOL.Title;
                cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar).Value = ObjContactListBOL.ZipCode;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateContactListDetails(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateContactListDetails";
                //@Cont_MID,@Name,@Email,@Company,@Title,@ZipCode

                cmd.Parameters.Add("@Cont_MID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
                cmd.Parameters.Add("@Cont_DID", SqlDbType.Int).Value = ObjContactListBOL.Cont_DID;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = ObjContactListBOL.Name;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjContactListBOL.Email;
                cmd.Parameters.Add("@Company", SqlDbType.VarChar).Value = ObjContactListBOL.Company;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = ObjContactListBOL.Title;
                cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar).Value = ObjContactListBOL.ZipCode;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateMoveContact(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateMoveContact";
                //@Cont_MID,@Name,@Email,@Company,@Title,@ZipCode

                cmd.Parameters.Add("@Cont_MID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
                cmd.Parameters.Add("@Cont_DID", SqlDbType.Int).Value = ObjContactListBOL.Cont_DID;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable GetContactListDetail(ContactListBOL ObjContactListBOL)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@Cont_MID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
                if (ObjContactListBOL.Cont_MID ==2)
                {
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjContactListBOL.CreatedBy;
                    cmd.CommandText = "SP_GetContactListDetailsGernal";
                }
                else
                {
                    cmd.CommandText = "SP_GetContactListDetails";
                }
             
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetContactListDetail(ContactListBOL ObjContactListBOL, string Searchtxt)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@Cont_MID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
                cmd.Parameters.Add("@Searchtxt", SqlDbType.VarChar).Value = Searchtxt;
                if (ObjContactListBOL.Cont_MID == 2)
                {
                    cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjContactListBOL.CreatedBy;
                    cmd.CommandText = "SP_GetContactListDetailsGernalSrch";
                }
                else
                {
                    cmd.CommandText = "SP_GetContactListDetailsSrch";
                }

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public bool DeleteContactListDetails(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_DeleteContactListDetails";
                //@Cont_MID,@Name,@Email,@Company,@Title,@ZipCode

                cmd.Parameters.Add("@Cont_DID", SqlDbType.Int).Value = ObjContactListBOL.Cont_DID;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteContactList(ContactListBOL ObjContactListBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_DeleteContactList";
              
                cmd.Parameters.Add("@Cont_ID", SqlDbType.Int).Value = ObjContactListBOL.Cont_MID;
               

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

    }
}