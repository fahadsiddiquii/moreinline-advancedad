﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class OrdersDAL
    {

        public DataTable GetPaymentOrderReport()
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = @"    
                                    Select O.*,(CPM.FirstName+' '+CPM.LastName)Name,A.OrderNum From Tbl_Orders O
                                    Inner Join Tbl_CustomerProfileMaster CPM On O.CustID=CPM.Cust_ID
                                    Inner Join Tbl_Assests A ON O.AssetID = A.AssestID
                                  ";
                
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }



        public DataTable GetOrdersForDesigners(int Id)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetOrdersForDesigners";
                cmd.Parameters.Add("@UserID", SqlDbType.Int).Value = Id;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }



        public DataTable GetDesignList()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetDesignList";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetClientAssets(int CreatedById)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetClientAssets";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = CreatedById;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetFinalizedGfx(int CreatedById)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetFinalizedGfx";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = CreatedById;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;

            }
        }
        public DataTable GetArchiveClientAssets(int CreatedById)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetArchiveClientAssets";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = CreatedById;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public DataTable GetAssetDetails(int Assets)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetAssetDetails";
                cmd.Parameters.Add("@Assets", SqlDbType.Int).Value = Assets;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public DataTable GetClientAssetsList(int CreatedById)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetClientAssetsList";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = CreatedById;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetDesignMngr()
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetDesignMngr";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetImageUrl(int AssetID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "Select FilePath From Tbl_OrderdDesign where AssestID =" + AssetID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;

            }
           catch (Exception)
            {
                return null;
            }
        }


        public bool InsertOrderdDesign(DesignBOL dbol)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertOrderdDesign";
                cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = dbol.FilePath;
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = dbol.AssestID;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = dbol.CreatedByID;
                cmd.Parameters.Add("@QAID", SqlDbType.Int).Value = dbol.QAID;
                cmd.Parameters.Add("@QaApproval", SqlDbType.Int).Value = dbol.QaApproval;
                cmd.Parameters.Add("@MngrApproval", SqlDbType.Int).Value = dbol.MngrApproval;
                cmd.Parameters.Add("@CustApproval", SqlDbType.Int).Value = dbol.CustApproval;
                cmd.Parameters.Add("@QaRemarks", SqlDbType.VarChar).Value = dbol.QaRemarks;
                cmd.Parameters.Add("@MngrRemarks", SqlDbType.VarChar).Value = dbol.MngrRemarks;
                cmd.Parameters.Add("@CustRemarks", SqlDbType.VarChar).Value = dbol.CustRemarks;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           catch (Exception)
            {
                return false;
            }

        }

        public bool InsertOrderdDesign(DesignBOL dbol, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertOrderdDesign";
                cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = dbol.FilePath;
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = dbol.AssestID;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = dbol.CreatedByID;
                cmd.Parameters.Add("@QAID", SqlDbType.Int).Value = dbol.QAID;
                cmd.Parameters.Add("@QaApproval", SqlDbType.Int).Value = dbol.QaApproval;
                cmd.Parameters.Add("@MngrApproval", SqlDbType.Int).Value = dbol.MngrApproval;
                cmd.Parameters.Add("@CustApproval", SqlDbType.Int).Value = dbol.CustApproval;
                cmd.Parameters.Add("@QaRemarks", SqlDbType.VarChar).Value = dbol.QaRemarks;
                cmd.Parameters.Add("@MngrRemarks", SqlDbType.VarChar).Value = dbol.MngrRemarks;
                cmd.Parameters.Add("@CustRemarks", SqlDbType.VarChar).Value = dbol.CustRemarks;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool UpdateDesignApproval(int id, int Status)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignApproval";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateDesignApMngr(int id, int Status)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignApMngr";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateDesignApCust(int id, int Status)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignApCust";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateDesignRejected(int id, int Status, string Msg)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignRejected";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar).Value = Msg;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateDesignRejMng(int id, int Status, string Msg)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignRejMng";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar).Value = Msg;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateDesignRejCust(int id, int Status, string Msg)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateDesignRejCust";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = Status;
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar).Value = Msg;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public DataTable CheckDesign(int id)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_CheckDesign";
                cmd.Parameters.Add("@AssestId", SqlDbType.Int).Value = id;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }



        public bool UpdateOrderdDesign(int id, string FilePath)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateOrderdDesign";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = FilePath;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateFilePath(int AssetID)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = String.Empty;
                cmd.CommandText = "Update Tbl_OrderdDesign set FilePath=@FilePath where AssestID="+AssetID;
               
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateIsArchiveFalse(int OrderID)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                //cmd.Parameters.Add("@IsArchive", SqlDbType.VarChar).Value = "False";
                cmd.CommandText = "Update Tbl_OrderdDesign set IsArchive=0 where ID=" + OrderID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateIsArchiveTrue(int OrderID)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@IsArchive", SqlDbType.Int).Value = 1;
                cmd.CommandText = "Update Tbl_OrderdDesign set IsArchive=@IsArchive where ID=" + OrderID;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }



    }
}