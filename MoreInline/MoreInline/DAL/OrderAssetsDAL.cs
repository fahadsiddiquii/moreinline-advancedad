﻿
﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class OrderAssetsDAL
    {
        public bool InsertOrderAssests(OrderAssetsBOL ObjOrderAssetsBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertOrderAssests";
                ObjOrderAssetsBOL.OrderNum = GetAssetOrderID();
                cmd.Parameters.Add("@OrderNum", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.OrderNum;
                cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Subject;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                if (ObjOrderAssetsBOL.Date == null)
                {
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = ObjOrderAssetsBOL.Date;
                }
                cmd.Parameters.Add("@Hours", SqlDbType.Int).Value = ObjOrderAssetsBOL.Hours;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@Message", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Message;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath == null ? "" : ObjOrderAssetsBOL.AudioPath;
                cmd.Parameters.Add("@AddAudio", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddAudio;
                cmd.Parameters.Add("@AddContact", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddContact;
                cmd.Parameters.Add("@AddDirection", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddDirection;
                cmd.Parameters.Add("@AddReminder", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddReminder;
                cmd.Parameters.Add("@AddWebUrl", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddWebUrl;
                cmd.Parameters.Add("@AddUerProfile ", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddUerProfile;
                cmd.Parameters.Add("@AssestFilePath", SqlDbType.VarChar).Value = "";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.CreatedByID;
                cmd.Parameters.Add("@ApprovalStatus", SqlDbType.Int).Value = ObjOrderAssetsBOL.ApprovalStatus;
                cmd.Parameters.Add("@TimeSpan", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.TimSpan;
                cmd.Parameters.Add("@Category", SqlDbType.Int).Value = ObjOrderAssetsBOL.Category; 

                cmd.Parameters.Add("@AssestID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    ObjOrderAssetsBOL.ID = int.Parse(cmd.Parameters["@AssestID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertOrderAssestsDetail(OrderAssetsDetailBOL ObjOrderAssetsDetailBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertOrderAssestsDetail";
                cmd.Parameters.Add("@AssestID", SqlDbType.VarChar).Value = ObjOrderAssetsDetailBOL.AssestID;
                cmd.Parameters.Add("@FilePath", SqlDbType.VarChar).Value = ObjOrderAssetsDetailBOL.FilePath;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private String GetAssetOrderID()
        {
            try
            {
                SqlConnection con = DBHelper.GetConnection();
                SqlDataAdapter Adapter = new SqlDataAdapter("SELECT [dbo].[GetAssetID] ()", con);
                DataTable dt = new DataTable();
                Adapter.Fill(dt);
                return dt.Rows[0][0].ToString(); ;
            }
            catch (Exception)
            {

                return "";
            }

        }

        
        public String GetInvoiceID()
        {
            try
            {
                SqlConnection con = DBHelper.GetConnection();
                SqlDataAdapter Adapter = new SqlDataAdapter("SELECT [dbo].[GetInvoiceID] ()", con);
                DataTable dt = new DataTable();
                Adapter.Fill(dt);
                return dt.Rows[0][0].ToString(); ;
            }
            catch (Exception)
            {

                return "";
            }

        }

        public String GetOrderNo()
        {
            try
            {
                SqlConnection con = DBHelper.GetConnection();
                SqlDataAdapter Adapter = new SqlDataAdapter("SELECT [dbo].[GetOrderNumber] ()", con);
                DataTable dt = new DataTable();
                Adapter.Fill(dt);
                return dt.Rows[0][0].ToString(); ;
            }
            catch (Exception)
            {

                return "";
            }

        }

        public bool InsertTransaction(TransactionBOL Transaction)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "InsertTransaction";
                cmd.Parameters.Add("@Guid", SqlDbType.VarChar).Value = Transaction.GUID;
                cmd.Parameters.Add("@Token", SqlDbType.VarChar).Value = Transaction.Token==null?"": Transaction.Token;
                cmd.Parameters.Add("@PayerID", SqlDbType.VarChar).Value = Transaction.PayerID;
                cmd.Parameters.Add("@PaymentId", SqlDbType.VarChar).Value = Transaction.PaymentId;
                cmd.Parameters.Add("@Cust_Id", SqlDbType.Int).Value = Transaction.Cust_Id;
                cmd.Parameters.Add("@TransactionDate", SqlDbType.DateTime).Value = Transaction.TransactionDate;
                cmd.Parameters.Add("@Item", SqlDbType.VarChar).Value = Transaction.Item;
                cmd.Parameters.Add("@Amount", SqlDbType.VarChar).Value = Transaction.Amount;
                cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = Transaction.IsActive;
                cmd.Parameters.Add("@InvoiceNumber", SqlDbType.VarChar).Value = Transaction.InvoiceNumber;
                cmd.Parameters.Add("@TransactionID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    Transaction.TransactionId = int.Parse(cmd.Parameters["@TransactionID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateTransaction(TransactionBOL Transaction)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateTransaction";
                cmd.Parameters.Add("@Cust_Id", SqlDbType.Int).Value = Transaction.Cust_Id;
                cmd.Parameters.Add("@TransactionID", SqlDbType.Int).Value = Transaction.TransactionId;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool InsertNewOrder(ConfirmOrderBOL NewOrder)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                

                
                cmd.CommandText = "InsertNewOrders";
                cmd.Parameters.Add("@Design", SqlDbType.Int).Value = NewOrder.DesignID;
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = NewOrder.AssetID;
                cmd.Parameters.Add("@PayerID", SqlDbType.VarChar).Value = NewOrder.PayerID;
                cmd.Parameters.Add("@PaymentId", SqlDbType.VarChar).Value = NewOrder.PaymentId;
                cmd.Parameters.Add("@Cust_Id", SqlDbType.Int).Value = NewOrder.Cust_Id;
                cmd.Parameters.Add("@OrderDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = NewOrder.CreateDate;
                cmd.Parameters.Add("@RequestType", SqlDbType.Int).Value = NewOrder.RequestType;
                cmd.Parameters.Add("@Amount", SqlDbType.Money).Value = Convert.ToDecimal(NewOrder.Amount);
                cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = NewOrder.Quantity;
                cmd.Parameters.Add("@OrderNumber", SqlDbType.VarChar).Value = NewOrder.OrderNum;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetOrderStatus(OrderAssetsBOL ObjOrderAssetsBOL)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = ObjOrderAssetsBOL.CreatedByID;
                cmd.CommandText = "SP_GetOrderAssets";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        
       public DataSet GetCustDetail(int CustID)
        {
            try
            {

                DataSet dt = new DataSet();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustID;
                cmd.CommandText = "SP_GetCustDetail";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetOrderAssetsMaster(int id)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.CommandText = "SP_GetOrderAssetsMaster";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public DataTable GetCategories(int id)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = id==2?"select * from Category where Id=1": "select * from Category";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public int GetCategory()
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = " select Id from Category where Name like '%Broadcast Ad%'";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception)
            {

                return 0;
            }
        }
        public DataTable GetOrderAssetsDetail(int id)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.CommandText = "SP_GetOrderAssetsDetail";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataSet GetSentDatesAll(int CustID)
        {
            try
            {

                DataSet dt = new DataSet();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustID;
                cmd.CommandText = "SP_GetSentDates";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetSentDatesDay(int CustID, string Day)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@Day", SqlDbType.VarChar).Value = Day;
                cmd.CommandText = "SP_GetSentDatesDay";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetSentDatesMonth(int CustID, string Month)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@Month", SqlDbType.VarChar).Value = Month;
                cmd.CommandText = "SP_GetSentDatesMonth";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetSentDatesYear(int CustID, string Year)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@Year", SqlDbType.VarChar).Value = Year;
                cmd.CommandText = "SP_GetSentDatesYear";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }



        public bool UpdateOrderAssets(OrderAssetsBOL ObjOrderAssetsBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateOrderAssets";
                cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Subject;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                cmd.Parameters.Add("@Date", SqlDbType.Date).Value = ObjOrderAssetsBOL.Date;
                cmd.Parameters.Add("@Hours", SqlDbType.Int).Value = ObjOrderAssetsBOL.Hours;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@Message", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Message;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath == null ? "" : ObjOrderAssetsBOL.AudioPath;
                cmd.Parameters.Add("@AddAudio", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddAudio;
                cmd.Parameters.Add("@AddContact", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddContact;
                cmd.Parameters.Add("@AddDirection", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddDirection;
                cmd.Parameters.Add("@AddReminder", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddReminder;
                cmd.Parameters.Add("@AddWebUrl", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddWebUrl;
                cmd.Parameters.Add("@AddUserProfile ", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddUerProfile;
                cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@UpdatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.UpdatedByID;
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = ObjOrderAssetsBOL.ID;
                cmd.Parameters.Add("@TimeSpan", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.TimSpan;
                // Conn = DBHelper.GetConnection();



                result = cmd.ExecuteNonQuery();

                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateOrderAssetsForGeneric(OrderAssetsBOL ObjOrderAssetsBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateOrderAssets";
                cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Subject;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                cmd.Parameters.Add("@Date", SqlDbType.Date).Value = ObjOrderAssetsBOL.Date;
                cmd.Parameters.Add("@Hours", SqlDbType.Int).Value = ObjOrderAssetsBOL.Hours;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@Message", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Message;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath == null ? "" : ObjOrderAssetsBOL.AudioPath;
                cmd.Parameters.Add("@AddAudio", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddAudio;
                cmd.Parameters.Add("@AddContact", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddContact;
                cmd.Parameters.Add("@AddDirection", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddDirection;
                cmd.Parameters.Add("@AddReminder", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddReminder;
                cmd.Parameters.Add("@AddWebUrl", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddWebUrl;
                cmd.Parameters.Add("@AddUserProfile ", SqlDbType.Bit).Value = false;
                cmd.Parameters.Add("@UpdatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@UpdatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.UpdatedByID;
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = ObjOrderAssetsBOL.ID;
                cmd.Parameters.Add("@TimeSpan", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.TimSpan;
                // Conn = DBHelper.GetConnection();
               
                result = cmd.ExecuteNonQuery();

                if (result == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteAssetsDetail(int id)
        {


            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_DeleteAssetsDetail";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                result = cmd.ExecuteNonQuery();
                if (result == 1)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateOrderStatus(int id, int status)
        {


            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateOrderStatus";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateOrderStatus(int id, int status, SqlTransaction Trans, SqlConnection Conn)
        {

            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_UpdateOrderStatus";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateOrderStatuscust(int id, int status)
        {


            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateOrderStatusCust";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@Status", SqlDbType.Int).Value = status;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool InsertEmailLandingAssetsMaster(OrderAssetsBOL ObjOrderAssetsBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertEmailLandingAssetsMaster";

                //[Subject],Phone,Email,Location,Date,WebUrl,Massage,AudioPath,AddAudio,AddContact,AddDirection,AddReminder,AddWebUrl,AddUerProfile,
                //AssestFilePath,CreatedByID,ApprovalStatus
                cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Subject;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                if (ObjOrderAssetsBOL.Date == null)
                {
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@Date", SqlDbType.Date).Value = ObjOrderAssetsBOL.Date;
                }
                cmd.Parameters.Add("@Hours", SqlDbType.Int).Value = ObjOrderAssetsBOL.Hours;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath == null ? "" : ObjOrderAssetsBOL.AudioPath;
                cmd.Parameters.Add("@AddAudio", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddAudio;
                cmd.Parameters.Add("@AddContact", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddContact;
                cmd.Parameters.Add("@AddDirection", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddDirection;
                cmd.Parameters.Add("@AddReminder", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddReminder;
                cmd.Parameters.Add("@AddWebUrl", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddWebUrl;
                cmd.Parameters.Add("@AddUerProfile ", SqlDbType.Bit).Value = ObjOrderAssetsBOL.AddUerProfile;
                cmd.Parameters.Add("@AssestFilePath", SqlDbType.VarChar).Value = "";
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.CreatedByID;
                cmd.Parameters.Add("@ApprovalStatus", SqlDbType.Int).Value = ObjOrderAssetsBOL.ApprovalStatus;
                cmd.Parameters.Add("@TimeSpan", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.TimSpan;
                cmd.Parameters.Add("@AssetID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    ObjOrderAssetsBOL.ID = int.Parse(cmd.Parameters["@AssetID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        

      public bool InsertGenricInfo(OrderAssetsBOL ObjOrderAssetsBOL)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertGenricInfo";
                cmd.Parameters.Add("@Subject", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Subject;
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath == null ? "" : ObjOrderAssetsBOL.AudioPath;
                cmd.Parameters.Add("@CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@CreatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.CreatedByID;
                cmd.Parameters.Add("@IsActive", SqlDbType.Bit).Value = true;
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = ObjOrderAssetsBOL.CreatedByID; 
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Message;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                   // ObjOrderAssetsBOL.ID = int.Parse(cmd.Parameters["@GIID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateGinfo(OrderAssetsBOL ObjOrderAssetsBOL)
        {
            try
            {
                int result = 0;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateGinfo";
                cmd.Parameters.Add("@Phone", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Phone;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Email;
                cmd.Parameters.Add("@Location", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Location;
                cmd.Parameters.Add("@WebUrl", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.WebUrl;
                cmd.Parameters.Add("@UpdateDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@UpdatedByID", SqlDbType.Int).Value = ObjOrderAssetsBOL.UpdatedByID;
                cmd.Parameters.Add("@Cust_id", SqlDbType.Int).Value = ObjOrderAssetsBOL.UpdatedByID;
                cmd.Parameters.Add("@message", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.Message;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = ObjOrderAssetsBOL.AudioPath;

                result = cmd.ExecuteNonQuery();
                if (result == 1)
                {
                    // ObjOrderAssetsBOL.ID = int.Parse(cmd.Parameters["@GIID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetGenericInfo(int Cust_ID)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@Cust_ID", SqlDbType.Int).Value = Cust_ID;
                cmd.CommandText = "SP_GetGenericInfo";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetEmailLandingAssetsMaster(int AssestID)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = AssestID;
                cmd.CommandText = "SP_GetEmailLandingAssetsMaster";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataTable GetAssetVoice(int AssestID)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@AssetID", SqlDbType.Int).Value = AssestID;
                cmd.CommandText = "SP_GetAssetVoice";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public DataTable GetAssetContacts(int AssestID)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@AssetID", SqlDbType.Int).Value = AssestID;
                cmd.CommandText = "SP_GetAssetContacts";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public DataSet GetAssetReminder(int AssestID,int CustId)
        {
            try
            {

                DataSet dt = new DataSet();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = AssestID;
                cmd.Parameters.Add("@CustId", SqlDbType.Int).Value = CustId;
                cmd.CommandText = "SP_GetAssetReminder";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }
        public DataTable GetAssetDirection(int AssestID)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@AssetID", SqlDbType.Int).Value = AssestID;
                cmd.CommandText = "SP_GetAssetDirection";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {

                return null;
            }
        }

        //-------------------Reporting Work
        public DataTable GetPendingOrdersReport()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetPendingOrdersReport";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable GetInProcessOrdersReport()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetInProcessOrdersReport";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DataTable GetDeliveredOrdersReport()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetDeliveredOrdersReport";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DataTable GetLiveOrdersReport()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_GetLiveOrdersReport";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
