﻿using MoreInline.BOL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MoreInline.DAL
{
    public class AdvertisementDAL
    {
        int result = 0;

        public bool InsertAdvertisement(AdvertisementBOL AdvertisementBOL, SqlTransaction Trans, SqlConnection Conn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = Conn;
                cmd.Transaction = Trans;
                cmd.CommandText = "SP_InsertAdvertisement";
                //CompanyName,CustomerEmail,CodeID,AppointmentAds,CustAds1,CustAds2,CustAds3,CustAds4,EmailStatusID,CreatedBy

                cmd.Parameters.Add("@CompanyName", SqlDbType.VarChar).Value = AdvertisementBOL.CompanyName;
                cmd.Parameters.Add("@CustomerEmail", SqlDbType.VarChar).Value = AdvertisementBOL.CustomerEmail;
                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = AdvertisementBOL.CodeID;
                cmd.Parameters.Add("@AppointmentAds", SqlDbType.VarChar).Value = AdvertisementBOL.AppointmentAds;
                cmd.Parameters.Add("@LockedAppointmentAds", SqlDbType.VarChar).Value = AdvertisementBOL.LockedAppointmentAds;
                cmd.Parameters.Add("@CustAds1", SqlDbType.VarChar).Value = AdvertisementBOL.CustAds1;
                cmd.Parameters.Add("@CustAds2", SqlDbType.VarChar).Value = AdvertisementBOL.CustAds2;
                cmd.Parameters.Add("@CustAds3", SqlDbType.VarChar).Value = AdvertisementBOL.CustAds3;
                cmd.Parameters.Add("@CustAds4", SqlDbType.VarChar).Value = AdvertisementBOL.CustAds4;
                cmd.Parameters.Add("@EmailStatusID", SqlDbType.Int).Value = AdvertisementBOL.EmailStatusID;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = AdvertisementBOL.CreatedBy;

                cmd.Parameters.Add("@AdsID", System.Data.SqlDbType.Int).Direction = System.Data.ParameterDirection.Output;

                result = cmd.ExecuteNonQuery();
                if (result != 0)
                {
                    AdvertisementBOL.AdsID = int.Parse(cmd.Parameters["@AdsID"].Value.ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
        

        public bool InsertSentEmailsLog(int AssetID,string Name,string Email,string Title,string Company,int CustID,DateTime AppointmentDate,string Hours,string TimeSpn,int ContactId,int Catid, string AudioPath)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_InsertSentEmailsLog";
                cmd.Parameters.Add("@AssestID", SqlDbType.Int).Value = AssetID;
                cmd.Parameters.Add("@Name", SqlDbType.VarChar).Value = Name;
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value =Email;
                cmd.Parameters.Add("@Title", SqlDbType.VarChar).Value = Title;
                cmd.Parameters.Add("@Company", SqlDbType.VarChar).Value = Company;
                cmd.Parameters.Add("@CreateDate", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@AppointmentDate", SqlDbType.DateTime).Value = AppointmentDate; 
                cmd.Parameters.Add("@Hours", SqlDbType.VarChar).Value = Hours;
                cmd.Parameters.Add("@ContactId ", SqlDbType.Int).Value = ContactId;
                cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = Catid; 
                cmd.Parameters.Add("@TimeSpan", SqlDbType.VarChar).Value = TimeSpn;
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@AudioPath", SqlDbType.VarChar).Value = AudioPath;
                cmd.Parameters.Add("@IsOver", SqlDbType.Bit).Value = false;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void UpdateAdvertisementStatus(AdvertisementBOL AdvertisementBOL)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_UpdateAdvertisementStatus";
                cmd.Parameters.Add("@AdsID", SqlDbType.Int).Value = AdvertisementBOL.AdsID;

                result = cmd.ExecuteNonQuery();
               
            }
            catch (Exception)
            {
               
            }
        }

        public DataTable GetAdvertisementDataList(AdvertisementBOL AdvertisementBOL)// No need to use Entity here you can get data without Entity
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.Parameters.Add("@CodeID", SqlDbType.Int).Value = AdvertisementBOL.CodeID;
                cmd.CommandText = "SP_GetAdvertisementDataList";

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetSentAssetLog(int CustID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@createdBy", SqlDbType.Int).Value = CustID;
                cmd.CommandText = "SP_GetSentAssetLog";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetSentAssetLogByDate(DateTime Date, int CustID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CustID", SqlDbType.Int).Value = CustID;
                cmd.Parameters.Add("@createdOn", SqlDbType.DateTime).Value = Date;
                cmd.CommandText = "SP_GetSentAssetLogByDate";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public bool UpdateEmailsLogStatus(int ID,bool IsOver)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_UpdateEmailsLogStatus";
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
                cmd.Parameters.Add("@IsOver", SqlDbType.Int).Value = IsOver;
                
                result = cmd.ExecuteNonQuery();
                if (result >0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }
        public DataTable GetSentEmailCount(int CustID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = CustID;
                cmd.CommandText = "SP_GetSentEmailCount";
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetAdvertisement(int AdsID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();
               
                cmd.CommandText = "Select * From Tbl_Advertisement where AdsID="+AdsID;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetGetCodeIDEmail(string FullCode)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_GetCodeIDEmail";
                cmd.Parameters.Add("@FullCode", SqlDbType.VarChar).Value = FullCode;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable CheckEmail(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select CustomerEmail From Tbl_Advertisement where CustomerEmail=@Email";
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;
               SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable CheckActiveEmail(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Select Email From Tbl_CustomerProfileMaster where Email=@Email";
                cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateEmailsLimit(int id,int EmailLimit)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "SP_UpdateEmailsLimit";
                cmd.Parameters.Add("@CreatedBy", SqlDbType.Int).Value = id;
                cmd.Parameters.Add("@EmailLimit", SqlDbType.Int).Value = EmailLimit;
                result = cmd.ExecuteNonQuery();
                if (result>0)
                {
                   return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateCustomer(AdvertisementBOL AdvertisementBOL)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = DBHelper.GetConnection();
                cmd.CommandText = "SP_UpdateCustomerAdvertisement";

                cmd.Parameters.Add("@AdsID", SqlDbType.Int).Value = AdvertisementBOL.AdsID;
                cmd.Parameters.Add("@CompanyName ", SqlDbType.VarChar).Value = AdvertisementBOL.CompanyName;
                cmd.Parameters.Add("@CustomerEmail ", SqlDbType.VarChar).Value = AdvertisementBOL.CustomerEmail;

                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UnsubscribeEmail(int AdsID)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.Connection = DBHelper.GetConnection();

                cmd.CommandText = "Update Tbl_Advertisement set IsSubscribed = 0 where AdsID =" + AdsID;
                result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}