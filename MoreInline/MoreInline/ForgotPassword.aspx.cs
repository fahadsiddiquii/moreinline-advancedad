﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace MoreInline
{
    public partial class ForgotPassword : System.Web.UI.Page
    {
        private static string application_path = ConfigurationManager.AppSettings["application_path"].ToString();
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    if (Request.QueryString["Cust_ID"] != null)
                    {
                        PnlMail.Visible = false;
                        PnlPass.Visible = true;
                    }
                    else
                    {
                        PnlMail.Visible = true;
                        PnlPass.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }

        public bool ConfirmEmail()
        {
            if (TxtEmail.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Email", 0);
                return false;
            }
            else
            {
                return true;
            }
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (ConfirmEmail())
                {
                    if (TxtEmail.Text != "")
                    {
                        Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
                        Match match = regex.Match(TxtEmail.Text);
                        if (!match.Success)
                        {
                            MessageBox(div1, LblMsg, "Invalid Email Address", 0);
                            return;
                        }
                    }

                    ObjCustomerProfileMasterBOL.Email = TxtEmail.Text;
                    DataTable dt = ObjCustomerProfileMasterDAL.GetEmailExist(ObjCustomerProfileMasterBOL);

                    if (dt.Rows.Count == 0)
                    {
                        MessageBox(div1, LblMsg, "Email Dose Not Exist", 0);
                        return;
                    }

                    SendEmail(dt.Rows[0]["Cust_ID"].ToString());
                }
            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
        public void SendEmail(string Cust_ID)
        {
            string Link = application_path + "ForgotPassword.aspx?Cust_ID=" + Cust_ID;

            string Body = "<h3>Dear User,</h3><br><span style='font-size: 10pt;'>It seems you forgot your login details.";
            Body += " A request to reset your password has been received. For security reasons the initial password cannot be sent over email, ";
            Body += "you must reset it.</p><br><p>Please use the Link below to reset your password.<br/>";
            Body += "<a href='" + Link + "' targt='_blank'>Click Me</a></span>";

            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("donotreply@moreinline.com", "Password Recovery");
            mm.Subject = "Password Recovery";
            mm.To.Add(TxtEmail.Text.Trim());
            mm.Body = Body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);

            MessageBox(div1, LblMsg, "Check Your Email", 1);
        }

        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (TxtPass.Text == "")
                {
                    MessageBox(div1, LblMsg, "Enter Password", 0);
                    return;
                }
                else if (TxtCPass.Text == "")
                {
                    MessageBox(div1, LblMsg, "Enter Confirm Password", 0);
                    return;
                }
                else if (TxtCPass.Text != TxtPass.Text)
                {
                    MessageBox(div1, LblMsg, "Password is not matching", 0);
                    return;
                }

                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                ObjCustomerProfileMasterBOL.Password = TxtPass.Text;
                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Request.QueryString["Cust_ID"]);

                if (ObjCustomerProfileMasterDAL.UpdateUserPassword(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();

                    Response.Redirect("~/LoginPage.aspx");
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }

            }
            catch (Exception ex)
            { MessageBox(div1, LblMsg, ex.Message, 0); }
        }
    }
}