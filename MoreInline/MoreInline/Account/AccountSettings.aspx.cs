﻿using MoreInline.BOL;
using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace MoreInline.Account
{
    public partial class AccountSettings : System.Web.UI.Page
    {
        CustomerProfileMasterDAL ObjCustomerProfileMasterDAL = new CustomerProfileMasterDAL();
        CustomerProfileMasterBOL ObjCustomerProfileMasterBOL = new CustomerProfileMasterBOL();

        SqlTransaction Trans;
        SqlConnection Conn;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                div1.Visible = false;
                if (!IsPostBack)
                {
                    GetProfile();
                }
            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }



        public void GetProfile()
        {
            // @FirstName @LastName    @Company @Website @Address @ZipCode @Password   @Phone  @Photo @UpdatedBy
            ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

            TxtFName.Text = dt.Rows[0]["FirstName"].ToString();
            TxtLName.Text = dt.Rows[0]["LastName"].ToString();
            TxtEmail.Text = dt.Rows[0]["Email"].ToString();
            TxtCompany.Text = dt.Rows[0]["Company"].ToString();
            TxtWebsite.Text = dt.Rows[0]["Website"].ToString();
            TxtAddress.Text = dt.Rows[0]["Address"].ToString();
            TxtNumber.Text = dt.Rows[0]["Phone"].ToString();
            TxtUsername.Text = dt.Rows[0]["Username"].ToString();
            TxtPassword.Text = dt.Rows[0]["Password"].ToString();
            TxtZipCode.Text = dt.Rows[0]["ZipCode"].ToString();
            ImgCustomer.ImageUrl = "~/upload/" + dt.Rows[0]["Photo"].ToString();
            CompanyLg.ImageUrl= "~/upload/" + dt.Rows[0]["CompanyLogo"].ToString();
            ViewState["ProfilePhoto"] = dt.Rows[0]["Photo"].ToString();
        }


        public void MessageBox(HtmlControl div, Label LblMsg, string Msg, int State)
        {
            div.Visible = true;
            if (State == 1) //Success
            {
                div.Attributes["class"] = "alert alert-success text-center m-b-0";
                LblMsg.Text = Msg;
            }
            else //Danger
            {
                div.Attributes["class"] = "alert alert-danger text-center m-b-0";
                LblMsg.Text = Msg;
            }
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel('" + div.ID + "');", true);
        }

        public bool Local_Validation()
        {
            // @FirstName @LastName    @Company @Website @Address @ZipCode @Password  @Phone  
            if (TxtFName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter First Name", 0);
                return false;
            }
            else if (TxtLName.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Last Name", 0);
                return false;
            }
            else if (TxtCompany.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Company", 0);
                return false;
            }
            else if (TxtWebsite.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Website", 0);
                return false;
            }
            else if (TxtNumber.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Phone Number", 0);
                return false;
            }
            else if (TxtAddress.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Address", 0);
                return false;
            }
            else if (TxtZipCode.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter ZipCode", 0);
                return false;
            }
            else if (TxtPassword.Text == "")
            {
                MessageBox(div1, LblMsg, "Enter Password", 0);
                return false;
            }
            else
            {
                return true;
            }
        }

        public void UpdateSavedProfile()
        {
            try
            {
                // @FirstName @LastName    @Company @Website @Address @ZipCode @Password   @Phone  @Photo @UpdatedBy 

                ObjCustomerProfileMasterBOL.FirstName = TxtFName.Text;
                ObjCustomerProfileMasterBOL.LastName = TxtLName.Text;
                ObjCustomerProfileMasterBOL.Company = TxtCompany.Text;
                ObjCustomerProfileMasterBOL.Website = TxtWebsite.Text;
                ObjCustomerProfileMasterBOL.Address = TxtAddress.Text;
                ObjCustomerProfileMasterBOL.ZipCode = TxtZipCode.Text;
                ObjCustomerProfileMasterBOL.Password = TxtPassword.Text;
                ObjCustomerProfileMasterBOL.Phone = TxtNumber.Text;
                ObjCustomerProfileMasterBOL.Photo = ViewState["ProfilePhoto"].ToString();
                ObjCustomerProfileMasterBOL.CompanyLogo = ViewState["CompanyLogo"].ToString(); 
                Session["Photo"] = ViewState["ProfilePhoto"];
                ObjCustomerProfileMasterBOL.UpdatedBy = int.Parse(Session["Cust_ID"].ToString());
                ObjCustomerProfileMasterBOL.Cust_ID = int.Parse(Session["Cust_ID"].ToString());



                Conn = DBHelper.GetConnection();
                Trans = Conn.BeginTransaction();

                if (ObjCustomerProfileMasterDAL.UpdateCustomerProfile(ObjCustomerProfileMasterBOL, Trans, Conn) == true)
                {
                    Trans.Commit();
                    Conn.Close();
                    //ClearControls();
                    EnableControls(false);

                    BtnEdit.Visible = true;
                    BtnSave.Visible = false;
                    SendEmailNotification(int.Parse(Session["Cust_ID"].ToString()));
                    GetProfile();
                    MessageBox(div1, LblMsg, "Profile Updated Successfully", 1);
                }
                else
                {
                    Trans.Rollback();
                    Conn.Close();
                    MessageBox(div1, LblMsg, "Error Occurred While Saving", 0);
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        public void EnableControls(bool State)
        {
            TxtFName.Enabled = State;
            TxtLName.Enabled = State;
            TxtCompany.Enabled = State;
            TxtWebsite.Enabled = State;
            TxtAddress.Enabled = State;
            TxtNumber.Enabled = State;
            TxtPassword.Enabled = State;
            TxtZipCode.Enabled = State;
            ImgUpload.Enabled = State;
            FCompanyLogo.Enabled = State;
        }

        public void ClearControls()
        {
            TxtFName.Text = "";
            TxtLName.Text = "";
            TxtEmail.Text = "";
            TxtCompany.Text = "";
            TxtWebsite.Text = "";
            TxtAddress.Text = "";
            TxtNumber.Text = "";
            TxtUsername.Text = "";
            TxtZipCode.Text = "";
            TxtPassword.Text = "";
            TxtZipCode.Text = "";
            ViewState["ProfilePhoto"] = null;
            ViewState["CompanyLogo"] = null;
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            EnableControls(true);
            BtnEdit.Visible = false;
            BtnSave.Visible = true;
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ImgUpload.HasFile)
                {
                    string fileName = Path.GetFileName(ImgUpload.PostedFile.FileName);
                    ViewState["ProfilePhoto"] = fileName;
                    ImgUpload.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                }
                if (FCompanyLogo.HasFile)
                {
                    string fileName = Path.GetFileName(FCompanyLogo.PostedFile.FileName);
                    ViewState["CompanyLogo"] = fileName;
                    FCompanyLogo.PostedFile.SaveAs(Server.MapPath("~/upload/") + fileName);
                }


                if (Local_Validation() == true)
                {
                    UpdateSavedProfile();
                }

            }
            catch (Exception ex)
            {
                MessageBox(div1, LblMsg, ex.Message, 0);
            }
        }

        private void SendEmailNotification(int custId)
        {
            EmailNotification EN = new EmailNotification();
            #region Email For Admin
            // EmailNotification.SendNotification(EN.GetAdminFilledObject());
            #endregion


            #region Email For Customer
            ObjCustomerProfileMasterBOL.Cust_ID = custId;
            DataTable CustData = ObjCustomerProfileMasterDAL.GetCustomerProfile(ObjCustomerProfileMasterBOL);

            if (CustData.Rows[0]["IsPswdChangeNActive"].ToString().Equals("True"))
            {
                EmailNotification EmailData = EN.GetAdminFilledObject();
                EmailData.ButtonText = "View Profile";
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailData.NotificationType = "IsPswdChangeNActive";
                EmailData.Content = "This message is to confirm that your MOREINLINE  account profile has been successfully changed. If you did not make changes to your MOREINLINE account profile, please contact us immediately.";
                EmailData.ReceiverEmailAddress = CustData.Rows[0]["Email"].ToString();
                EmailData.EmailSubject = "Your MOREINLINE Account Profile Has Been Changed";
                EmailData.ReceiverName = CustData.Rows[0]["FirstName"].ToString();
                EmailData.SenderIdForUnSubscription = ObjCustomerProfileMasterBOL.Cust_ID;
                EmailNotification.SendNotification(EmailData);
            }
            #endregion
        }



    }
}