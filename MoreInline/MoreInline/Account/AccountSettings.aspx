﻿<%@ Page Title="Account Settings" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="AccountSettings.aspx.cs" Inherits="MoreInline.Account.AccountSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- ----------------------------------- Script Start --------------------------------------------- --%>

    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <script>
        $(document).ready(function () {
            $("body").on('click', '.toggle-password', function () {
                $(this).toggleClass("fa-eye-slash fa-eye");
                var input = $("#ContentPlaceHolder1_TxtPassword");
                if (input.attr("type") === "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });
        });
    </script>


    <%-- ----------------------------------- Script End --------------------------------------------- --%>

    <%-- ----------------------------------- Page Start --------------------------------------------- --%>

    <div class="row">
        <div class="col-lg-12 text-center" style="padding-right: 150px;">
            <h3 style="color: #019c7c;">Account Settings</h3>
        </div>
    </div>

    <asp:Panel ID="PnlCustomerProfile" runat="server">
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-8 p-b-10">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-2">
                <asp:Image ID="ImgCustomer" runat="server" CssClass="rounded-image" />
                <asp:FileUpload ID="ImgUpload" runat="server" Enabled="false" CssClass="m-b-10 m-t-10" />
                <label class="p-t-0" style="color: #6C6C6C; font-size: 10px;">Profile photos allows you to communicate with potential clients to tell a little about yourself. This is how you'll be viewed to other businesses and customers when sending out your assets. It matters and it works.</label>
           <br />
                <asp:Image ID="CompanyLg" runat="server" CssClass="rounded-image" />
                <asp:FileUpload ID="FCompanyLogo" runat="server" Enabled="false" CssClass="m-b-10 m-t-10" />
                <label class="p-t-0" style="color: #6C6C6C; font-size: 10px;">Company logo allows you to communicate with potential clients to tell a little about your company. This is how you'll be viewed to other businesses and customers when sending out your assets. It matters and it works.</label>
            </div>
            <div class="col-lg-3">
                <div class="form-group-sm">
                    <label>First Name</label>
                    <asp:TextBox ID="TxtFName" runat="server"  Enabled="false" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                    <label class="m-t-5">Last Name</label>
                    <asp:TextBox ID="TxtLName" runat="server"  Enabled="false" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                    <label class="m-t-5">Company</label>
                    <asp:TextBox ID="TxtCompany" runat="server"  Enabled="false" CssClass="form-control" placeholder="Company"></asp:TextBox>
                    <label class="m-t-5">Website</label>
                    <asp:TextBox ID="TxtWebsite" runat="server"  Enabled="false" CssClass="form-control" placeholder="Website"></asp:TextBox>
                    <label class="m-t-5">Address</label>
                    <asp:TextBox ID="TxtAddress" runat="server"  Enabled="false" CssClass="form-control" placeholder="Residential Address"></asp:TextBox>
                    <label class="m-t-5">Zip Code</label>
                    <asp:TextBox ID="TxtZipCode" runat="server"  Enabled="false" CssClass="form-control" placeholder="Zip Code"></asp:TextBox>

                    <label class="m-t-5">Email</label>
                    <asp:TextBox ID="TxtEmail" runat="server"  Enabled="false" CssClass="form-control" placeholder="Email"></asp:TextBox>
                    <label class="m-t-5">Contact Number</label>
                    <asp:TextBox ID="TxtNumber" runat="server" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11"  Enabled="false" CssClass="form-control" placeholder="Contact Number"></asp:TextBox>

                    <%--<label class="m-t-5">Username</label>--%>
                    <asp:TextBox ID="TxtUsername" runat="server"  Enabled="false" Visible="false" CssClass="form-control" placeholder="Username"></asp:TextBox>
                    <label class="m-t-5">Password</label>
                    <asp:TextBox ID="TxtPassword" runat="server" type="password"  Enabled="false" CssClass="form-control" placeholder="Password"></asp:TextBox>
                    <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                </div>
            </div>
            <div class="col-lg-5"></div>
        </div>
        <div class="row m-t-10">
            <div class="col-lg-4"></div>
            <div class="col-lg-3 text-right">
                <a href="../Dashboard/Dashboard.aspx" class="btn btn-sm btn-black-green m-b-10" style="width: 90px;">Dashboard</a>
                <asp:Button ID="BtnEdit" runat="server" Text="Edit" OnClick="BtnEdit_Click" CssClass="btn btn-sm btn-light-green m-b-10" Style="width: 90px;" />
                <asp:Button ID="BtnSave" runat="server" Visible="false" Text="Save" OnClick="BtnSave_Click" CssClass="btn btn-sm btn-light-green m-b-10" Style="width: 90px;" />
            </div>
            <div class="col-lg-5"></div>
        </div>
    </asp:Panel>




</asp:Content>
