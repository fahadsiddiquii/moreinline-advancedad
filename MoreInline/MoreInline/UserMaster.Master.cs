﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline
{
    public partial class UserMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if ((Session["UserName"] != null || Session["UserName"].ToString() != "") && (Session["UserRoles"] != null || Session["UserRoles"].ToString() != "") &&
                    (Session["UserID"] != null || Session["UserID"].ToString() != "") && (Session["UserEmail"] != null || Session["UserEmail"].ToString() != ""))
                {

                    Menu();

                    LblUserName.Text = Session["UserName"].ToString();
                    LblUserType.Text = Session["UserRoles"].ToString();
                    /*
                    Session["UserID"]
                    Session["UserName"]
                    Session["UserEmail"]
                    Session["Photo"]
                    Session["UserRoleID"]
                    Session["UserRoles"]
                    */

                    if (Session["Photo"].ToString() != "")
                    {
                        ImgLogin.ImageUrl = "~/upload/" + Session["Photo"].ToString();
                        ImgLogin1.ImageUrl = "~/upload/" + Session["Photo"].ToString();
                    }
                    else
                    {
                        ImgLogin.ImageUrl = "~/dist/img/user2-160x160.jpg";
                    }
                }
                else
                {
                    Response.Redirect(@"~\Admin.aspx", false);
                }
            }
            catch (Exception)
            {
                Response.Redirect(@"~\Admin.aspx", false);
            }
        }
        public void Menu()
        {
            if (Session["UserRoles"].ToString() == "Admin")
            {
                PnlAdmin.Visible = true;
                PnlManager.Visible = false;
                PnlDesigner.Visible = false;
                PnlQA.Visible = false;
            }
            else if (Session["UserRoles"].ToString() == "Manager")
            {
                PnlAdmin.Visible = false;
                PnlManager.Visible = true;
                PnlDesigner.Visible = false;
                PnlQA.Visible = false;
            }
            else if (Session["UserRoles"].ToString() == "Designer")
            {
                PnlAdmin.Visible = false;
                PnlManager.Visible = false;
                PnlDesigner.Visible = true;
                PnlQA.Visible = false;
            }
            else if (Session["UserRoles"].ToString() == "QA")
            {
                PnlAdmin.Visible = false;
                PnlManager.Visible = false;
                PnlDesigner.Visible = false;
                PnlQA.Visible = true;
            }

        }
    }
}