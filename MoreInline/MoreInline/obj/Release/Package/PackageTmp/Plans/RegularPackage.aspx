﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegularPackage.aspx.cs"  EnableEventValidation="false" Inherits="MoreInline.Plans.RegularPackage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Packages</title>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link href="../agency-video/img/favicon.png" rel="icon" />

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script Start --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script>
        $(document).ready(function () {
            $("body").on('click', '.toggle-password2', function () {
                $(this).toggleClass("fa-eye-slash fa-eye");
                var input2 = $("#TxtConfirmPassword");
                if (input2.attr("type") === "password") {
                    input2.attr("type", "text");
                } else {
                    input2.attr("type", "password");
                }
            });
        });
        $(document).ready(function () {
            $("body").on('click', '.toggle-password1', function () {
                $(this).toggleClass("fa-eye-slash fa-eye");
                var input1 = $("#TxtPassword");
                if (input1.attr("type") === "password") {
                    input1.attr("type", "text");
                } else {
                    input1.attr("type", "password");
                }
            });
        });
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 3;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    
    <style>
        .hm-gradient {
            background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
        }

        .darken-grey-text {
            color: #2E2E2E;
        }

        .danger-text {
            color: #ff3547;
        }

        .default-text {
            color: #2BBBAD;
        }

        .info-text {
            color: #33b5e5;
        }

        .form-white .md-form label {
            color: #fff;
        }

        .form-white input[type=text]:focus:not([readonly]) {
            border-bottom: 1px solid #fff;
            -webkit-box-shadow: 0 1px 0 0 #fff;
            box-shadow: 0 1px 0 0 #fff;
        }

            .form-white input[type=text]:focus:not([readonly]) + label {
                color: #fff;
            }

        .form-white input[type=password]:focus:not([readonly]) {
            border-bottom: 1px solid #fff;
            -webkit-box-shadow: 0 1px 0 0 #fff;
            box-shadow: 0 1px 0 0 #fff;
        }

            .form-white input[type=password]:focus:not([readonly]) + label {
                color: #fff;
            }

        .form-white input[type=password], .form-white input[type=text] {
            border-bottom: 1px solid #fff;
        }

        .form-white .form-control:focus {
            color: #fff;
        }

        .form-white .form-control {
            color: #fff;
        }

        .form-white textarea.md-textarea:focus:not([readonly]) {
            border-bottom: 1px solid #fff;
            box-shadow: 0 1px 0 0 #fff;
            color: #fff;
        }

        .form-white textarea.md-textarea {
            border-bottom: 1px solid #fff;
            color: #fff;
        }

            .form-white textarea.md-textarea:focus:not([readonly]) + label {
                color: #fff;
            }

        .ripe-malinka-gradient {
            background-image: linear-gradient(120deg, #f093fb 0%, #f5576c 100%);
        }

        .near-moon-gradient {
            background-image: linear-gradient(to bottom, #5ee7df 0%, #b490ca 100%);
        }
    </style>

    <%-- ----------------------------------- Script End --------------------------------------------- --%>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header" style="background-color: #000000;">
                <!-- Logo -->
                <a href="../LoginPage.aspx" class="logo" style="background-color: #000000;">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="160px" height="35px" />
                </a>
                <nav class="navbar navbar-static-top" role="navigation" style="background-color: #000000;">
                </nav>
            </header>
              
    
            <div class="content-wrapper m-l-0" style="background-color: #f7f8f9;">
                <section class="content container-fluid p-t-0 p-b-0 p-l-0">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8 p-b-10">
                            <div id="div1" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                    <asp:Panel ID="PnlOrderSummary" runat="server">

                        <div class="row">
                            <div class="col-lg-4 col-sm-4"></div>
                            <div class="col-lg-4 col-sm-4">
                                <div class="row m-l-10 m-r-10">
                                    <div class="col-lg-12 text-center p-t-20">
                                        <%--<h3 style="color: #A9C502;"><b>You've added Regular Plan</b></h3>--%>
                                        <h3 style="color: #001b00;"><b>
                                            <asp:Label ID="LblHeading" runat="server"></asp:Label></b></h3>
                                        <label style="color: #6C6C6C;">Order Summary</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <div id="div2" runat="server" visible="false">
                                            <strong>
                                                <asp:Label ID="LblMsg2" runat="server"></asp:Label></strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-25">
                                        <label style="color: #019c7c;"><b>Select a payment option</b></label>
                                    </div>
                                    <div class="col-lg-5 form-group-sm">
                                        <asp:DropDownList ID="DdlRegularPlan" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlRegularPlan_SelectedIndexChanged" CssClass="form-control" Style="background-color: #007f9f; color: #ffffff;">
                                        </asp:DropDownList>

                                    </div>
                                    <div class="col-lg-12 p-t-10">
                                        <span class="text-bold">Emails Limit : <span id="spLimit" runat="server"></span></span>
                                        <br />
                                        <label style="color: red; font-size: 10px;">*Select more months and save your money.</label>
                                    </div>
                                    <div class="col-lg-12 p-t-20" style="border-bottom: 1px solid #6C6C6C;"></div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12 p-t-10">
                                                <label style="color: #707070; font-size: 20px;">Total</label>
                                                <asp:Label ID="LblPayment" runat="server" Style="color: #ABC502; font-size: 20px; float: right;"></asp:Label><label style="color: #ABC502; font-size: 20px; float: right;">$</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-10" style="border-bottom: 1px solid #6C6C6C;"></div>
                                    <div class="col-lg-12 p-t-15">
                                        <div class="row">
                                            <div class="col-lg-12 text-right">
                                                <a href="../Dashboard/Upgrade.aspx" class="btn btn-sm btn-primary m-t-10">Back</a>
                                                <asp:Button ID="btnaCP" runat="server" Visible="false" CssClass="btn btn-sm btn-light-green m-t-10" Text="Purchase With Card" OnClick="btnaCP_Click" />
                                                <asp:Button ID="BtnPurchase" runat="server" Enabled="false" CssClass="btn btn-sm btn-light-green m-t-10" Text="Purchase with Paypal" OnClick="BtnPurchase_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-sm-4"></div>
                        </div>
                    </asp:Panel>
                </section>
            </div>
        </div>
        <%----------------------------------------- Pop Up -------------------------------------------------------%>


        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="color: #001b00;">Add Account Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div id="div3" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg3" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <asp:Label ID="LblNote" CssClass="text-red m-l-15" runat="server"></asp:Label>
                    <center>
                         <img id="loading-image" style="position:absolute;top:49px;left:33%;z-index: 999;" src="../Images/Loader.gif" alt="Loader Image" />
                    </center>
                     <div class="row">
                            <br />
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                            <div class="col-lg-8 col-sm-8 col-xs-8">
                                <label class="p-t-10">Name on Card<span style="color: red;">*</span></label>
                                <input type="text" id="TxtNameOnCard" name="name" value="" placeholder="Card Owner Name" class="form-control" />
                                <label class="p-t-10">Card Number<span style="color: red;">*</span></label>
                                <input type="text" id="TxtCardNumber" onkeypress="return isNumberKey(event)" name="TxtCardNumber" maxlength="16" value="" placeholder="0000 0000 0000 0000" class="form-control" />
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Security Code<span style="color: red;">*</span></label>
                                        <input type="text" id="TxtSecurityCode" onkeypress="return isNumberKey(event)" maxlength="3" value="" placeholder="000" class="form-control" />
                                         </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Expiration Date<span style="color: red;">*</span></label>
                                         <input type="text" id="TxtExpiryDate" name="name" value=""  maxlength="7" placeholder="MM/YYYY" class="form-control" />
                                      
                                    </div>
                                </div>

                                <div class="row m-t-25 m-b-10">
                                    <div class="col-lg-12 text-center">
                                        <input id="BtnConfirmPayment" type="button" value="Confirm Payment" class="btn btn-sm btn-light-green" />      
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>    

    <script type="text/javascript">
        $('document').ready(function () {
            debugger;
           $('#loading-image').hide();
           Stripe.setPublishableKey('pk_live_CODr3gUvLLUli1xTyykryLv500B4HZmqUk');

            $('#BtnConfirmPayment').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                debugger;
                var FullDate = $('#TxtExpiryDate').val();
                var date = FullDate.split('/');
                Stripe.card.createToken({
                    number: $('#TxtCardNumber').val(),
                    cvc: $('#TxtSecurityCode').val(),
                    exp_month: date[0],
                    exp_year: date[1]
                }, stripeResponseHandler);
            });

            function stripeResponseHandler(status, response) {
                debugger;
                if (response.error) {
                    alert(response.error.message);  
                } else {
                    var token = response.id;
                    var CDPCard = {};
                    CDPCard.TokenId = token;
                    CDPCard.Amount = "200";
                    CDPCard.ExpiryDate = $('#TxtExpiryDate').val();
                    CDPCard.SecurityCode = $('#TxtSecurityCode').val();
                    CDPCard.CardNumber = $('#TxtCardNumber').val();
                    CDPCard.NameOnCard = $('#TxtNameOnCard').val()
                    CDPCard.Currency = "usd";
                    $('#loading-image').show();
                    $.ajax({
                        type: 'post',
                        url: '../Plans/RegularPackage.aspx/Charge',
                        data:"{CDPCard:"+ JSON.stringify(CDPCard)+"}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            debugger;
                            if (response.d === "Success") {
                                window.location.href = "/MKT_Assets/Info.aspx?IsSuccess=1";
                            }
                            else {
                                alert("Payment Failed Please Try Again ");
                            }
                        },
                        complete: function(){
                            $('#loading-image').hide();
                        },
                        error: function (error) {
                            debugger;
                            alert(error);
                        }
                    })
                    //();
                }
            }
        });
    </script>
    </form>
</body>
</html>
