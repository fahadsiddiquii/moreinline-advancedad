﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RequestCode.aspx.cs" Inherits="MoreInline.RequestCode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <%-- <meta http-equiv="Refresh" content="5;url=LoginPage.aspx" />--%>
    <title>Request a Code</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="agency-video/img/favicon.png" rel="icon" />

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="header-Login" style="background-color: #000;">
                <nav class="navbar navbar-static-top m-b-0">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3 p-l-0">
                                <div class="container-fluid p-l-0 p-r-0">
                                    <a href="Dashboard/Dashboard.aspx">
                                        <img src="Images/moreinline_logo%20Green.png" class="img-responsive" style="width:300px;" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-5"></div>
                        </div>
                    </div>
                </nav>
            </header>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper m-l-0" style="background-color: #f7f8f9;">
                <!-- Main content -->
                <section class="content container-fluid" style="padding-bottom: 0px;">
                    <div class="row p-t-15">
                        <%--<div class="col-lg-4 col-md-4"></div>--%>
                        <div class="col-lg-12 col-md-12 text-center">
                            <h3 style="color: #019c7c;">Request a Trial</h3>
                        </div>
                    </div>
                    <div class="row m-t-5">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div id="div1" runat="server">
                                <strong>
                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                        <br />
                    </div>
                    <div class="row p-t-15">
                        <div class="col-lg-4 col-md-4 col-sm-4 p-t-23 text-center">
                           <%-- <img src="Images/o2.png" class="img-responsive" />--%>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <label class="m-t-10">Upload company logo</label>
                            <asp:FileUpload ID="AddFile"  runat="server" accept='image/*' />
                            <span>Image size must not exceed 200 KB. Upload only .jpg, .png, .jpeg files.</span><br />
                            <asp:CustomValidator ID="CustomValidatorImg" OnServerValidate="CustomValidatorImg_ServerValidate" ControlToValidate="AddFile" runat="server" ForeColor="Red" />
                            <label class="m-t-10">Enter First Name</label>
                            <asp:TextBox ID="TxtFName" runat="server" CssClass="form-control" placeholder="Enter First Name"></asp:TextBox>
                            <label class="m-t-10">Enter Last Name</label>
                            <asp:TextBox ID="TxtLName" runat="server" CssClass="form-control" placeholder="Enter Last Name"></asp:TextBox>
                            <label class="m-t-10">Enter Company Name</label>
                            <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control" placeholder="Enter Company Name"></asp:TextBox>
                            <label class="m-t-10">Enter Email Address</label>
                            <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control" placeholder="Enter Email Address"></asp:TextBox>
                            <label class="m-t-10">Enter Contact Number</label>
                            <asp:TextBox ID="TxtContact" runat="server" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" CssClass="form-control" placeholder="Enter Contact Number"></asp:TextBox>

                            <div class="row m-t-18 m-b-5">
                                <div class="col-lg-7"></div>
                                <div class="col-lg-5 m-b-20 text-right">
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="btn btn-sm btn-dark-green" Style="width: 90px;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </form>
</body>
</html>
