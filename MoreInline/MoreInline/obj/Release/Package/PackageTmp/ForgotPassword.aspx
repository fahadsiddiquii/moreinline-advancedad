﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="MoreInline.ForgotPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forgot Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->
     <link href="agency-video/img/favicon.png" rel="icon" />

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />
    <!-- Morris chart -->
    <%--<link rel="stylesheet" href="bower_components/morris.js/morris.css" />--%>
    <!-- jvectormap -->
    <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css" />
    <!-- Date Picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />

    <%-- ----------------------------------- Script --------------------------------------------- --%>

    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
</head>
<body style="background-color: #f7f8f9; overflow-x: hidden;">
    <form id="form1" runat="server">
        <h1 class="text-center" style="padding-top: 20px;"><span style="color: #000;">More</span><span style="color: #ABC502;">Inline</span></h1>
        <div class="container">
            <br />
            <h1 style="color: #007f9f;" class="text-center">Forgot Password</h1>
            <br />
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8">
                    <div id="div1" runat="server" visible="false">
                        <strong>
                            <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
            <div class="row">
                <div class="col-lg-12"></div>
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div id="div2" runat="server" visible="false">
                        <b>
                            <asp:Label ID="Label1" runat="server"></asp:Label></b>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <asp:Panel ID="PnlMail" runat="server" Visible="false" BorderWidth="1px" BorderColor="#cccccc">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2><b>Find Your Account</b></h2>
                                <hr />
                            </div>
                            <div class="col-lg-12 p-b-20">
                                <div class="col-lg-12 text-center">
                                    Please enter your email address to search for your account.
                                </div>

                                <div class="col-lg-12 form-group-sm p-t-10">
                                    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control" placeholder="Email Address" Style="width: 245px; margin: 0 auto;" autofocus></asp:TextBox>
                                </div>

                                <div class="col-lg-12 text-center p-t-10">
                                    <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-sm btn-light-green" OnClick="BtnSearch_Click" Style="width: 90px" />
                                    <a href="LoginPage.aspx" class="btn btn-sm btn-light-green" style="width: 70px">Back</a>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="PnlPass" runat="server" Visible="false" BorderWidth="1px" BorderColor="#cccccc">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2><b>Reset Your Account Password</b></h2>
                                <hr />
                            </div>
                            <div class="col-lg-12 p-b-20">
                                <div class="col-lg-12 text-center">
                                    Please Enter your Password to Reset.
                                </div>

                                <%-- -------------------------- --%>
                                <div class="col-lg-12 form-group-sm p-t-10">
                                    <asp:TextBox ID="TxtPass" runat="server" TextMode="Password" placeholder="Password" Style="width: 200px; margin: 0 auto;" CssClass="form-control"></asp:TextBox>
                                </div>
                                <%-- -------------------------- --%>
                                <div class="col-lg-12 form-group-sm p-t-10">
                                    <asp:TextBox ID="TxtCPass" TextMode="Password" runat="server" placeholder="Confirm Password" Style="width: 200px; margin: 0 auto;" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="col-lg-12 p-t-10 text-center">
                                    <asp:Button ID="BtnUpdate" runat="server" Text="Save Changes" CssClass="btn btn-sm btn-light-green" OnClick="BtnUpdate_Click" />
                                    <%--<a href="LoginPage.aspx" class="btn signbackbtn">Back</a>--%>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>
    </form>
</body>
</html>
