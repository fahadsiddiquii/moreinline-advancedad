﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="MoreInline.LoginPage" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Tell the browser to be responsive to screen width -->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title>MoreInline Login</title>

    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>


    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->
    <link href="agency-video/img/favicon.png" rel="icon" />

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->


    <link href="css/MoreInline_Style.css" rel="stylesheet" />

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />
    <!-- Morris chart -->
    <%--<link rel="stylesheet" href="bower_components/morris.js/morris.css" />--%>
    <!-- jvectormap -->
    <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css" />
    <!-- Date Picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />


    <%-- ----------------------------------- Script --------------------------------------------- --%>

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="bower_components/raphael/raphael.min.js"></script>
    <script src="bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>


    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 6;
            setTimeout(function () {
                if (Text == "div1") {
                  <%-- document.getElementById("<%=row1.ClientID %>").style = "margin-top: 25px;";--%>
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>

    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=da9162b7-9464-47f3-9b3c-d9b8c4785b9f"> </script>
    <style>
        .img-background {
            background-image: url('../Images/bgLogin.jpg');
            background-size: cover;
        }

        .login-area {
            width: 390px;
            height: 315px;
            margin: 0 auto;
            background-color: #ffffff;
            border-radius: 10px;
        }

        .btn-size {
            width: 370px;
            border-radius: 6px;
        }

        @media (max-width:1200px) {

            .btn-size {
                width: 268px;
                border-radius: 6px;
            }
        }

        @media (max-width:410px) {
            .login-area {
                width: 300px;
            }
        }
    </style>
</head>
<%--<body class="hold-transition skin-blue sidebar-mini">--%>
<body>
    <form id="form1" runat="server" style="margin-bottom: 0px;">
        <div class="wrapper img-background">
            <asp:Panel runat="server" Visible="false">
                <header class="header-Login" style="background-color: #000;">
                    <nav class="navbar navbar-static-top m-b-0">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-3 p-l-0">
                                    <div class="container-fluid p-l-0 p-r-0 p-t-15">
                                        <img src="Images/moreinline_logo%20Green.png" class="img-responsive" />
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                                <i class="fa fa-bars" style="font-size: xx-large;"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 p-l-0">
                                    <%--style="margin-top: 40px;">--%>
                                    <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                                        <ul class="navbar-nav p-l-0 p-t-35">
                                            <%-- <li><a href="#" class="header-nav-size">HOME<span class="sr-only">(current)</span></a></li>--%>
                                            <li><a href="#MoreInline" class="header-nav-size">WHY DO YOU MOREINLINE?</a></li>
                                            <li><a href="#options" class="header-nav-size">OPTIONS</a></li>
                                            <li><a href="#contact" class="header-nav-size">CONTACT US</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <%--<div class="row">
                                        <div class="col-lg-12">
                                            <div id="div1" runat="server" visible="false">
                                                <strong>
                                                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <%--<div id="row1" runat="server" class="row p-l-0" style="margin-top: 25px;">
                                        <div class="col-lg-5 user-name form-group-sm">
                                            <span style="color: #707070;">Username</span>
                                            <asp:TextBox ID="TxtUserName" runat="server" CssClass="form-control" placeholder="Username" autofocus></asp:TextBox>
                                        </div>
                                        <div class="col-lg-5 form-group-sm user-pass">
                                            <span style="color: #707070;">Password</span>
                                            <asp:TextBox ID="TxtPassword" type="password" runat="server" CssClass="form-control navbar-search-input" placeholder="Password"></asp:TextBox>
                                            <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                                            <a href="../ForgotPassword.aspx" style="color: #707070; font-size: 13px;" class="p-l-0">Forgotten account?</a>
                                        </div>
                                        <div class="col-lg-2 form-group-sm u-l-p">
                                            &nbsp;
                                <asp:Button ID="BtnLogin" runat="server" Text="Login" OnClick="BtnLogin_Click" CssClass="btn btn-sm p-login btn-green" />
                                            <span style="color: #000;">..</span>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </nav>
                </header>
                <div class="container-fluid p-l-0 p-r-0">
                    <%--<img src="Images/Slider001.png" class="img-responsive" />--%>
                    <img src="Images/Slider001@2x.png" class="img-responsive" />
                </div>
                <div id="MoreInline" class="container-fluid p-l-0 p-r-0 text-center">
                    <br />
                    <br />
                    <div class="container text-center">
                        <h1 style="color: #A9C500;"><i><b>ENGAGE YOUR CUSTOMERS MORE EFFECTIVELY.</b></i>
                        </h1>
                        <br />
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-2" style="border-bottom: 2px solid #A9C500"></div>
                            <div class="col-lg-5"></div>
                        </div>
                    </div>
                    <%--<h3 style="color: #fff">We created a platform which allows us to connect with hundreds of creative designers around the world and pay 
                </h3>
                <h3 style="color: #fff">minimal price for their downtime. We're passing that creativity and savings onto you. We're MoreInline with you!
                </h3>--%>
                    <h3 style="color: #fff">We created a platform connected to hundreds of creative Ad designers around the world and pay minimum pricing for their downtime. </h3>
                    <h3 style="color: #fff">We then pass that creativity and cost savings to you. We work with your online needs because we’re MoreInline with You!</h3>
                    <br />
                    <h1 style="color: #A9C500; margin-bottom: 40px;"><b>What you can get</b>
                    </h1>

                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2">
                            <center>
                            <img src="Images/1.png" class="img-responsive" />
                            <h3 style="color:#FFFFFF; margin-bottom:20px;">Your own communication database</h3>

                            <p style="color:#FFFFFF; font-size:15px;"><i>You’ll get access to Ads for occasions that work for your industry and work with your schedule.</i>
                            </p>
                        </center>
                        </div>
                        <div class="col-lg-2">
                            <center>
                            <img src="Images/2.png" class="img-responsive" />
                            <h3 style="color:#FFFFFF;">Experience</h3><br />
                            <p style="color:#FFFFFF; font-size:15px;"><i>We have 25 years of creating and providing Ads to customers all over the world. We have talented people providing talented presentations.</i>
                            </p>
                        </center>
                        </div>
                        <div class="col-lg-2">
                            <center>
                            <img src="Images/4.png" class="img-responsive" />
                            <h3 style="color:#FFFFFF;">Guaranteed</h3><br />
                            <p style="color:#FFFFFF; font-size:15px;"><i>We guarantee that you’ll get images that reflect your business directly and not generically based Ads.</i>
                            </p>
                        </center>
                        </div>
                        <div class="col-lg-2">
                            <center>
                            <img src="Images/3.png" class="img-responsive" />
                            <h3 style="color:#FFFFFF;">24/7 Support/Assistance</h3><br />
                            <p style="color:#FFFFFF; font-size:15px;"><i>We’re here when you need us to be. We want to keep you communicating so you don’t skip a beat. We have a 99.9% uptime access.</i>
                            </p>
                        </center>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>

                    <br />
                    <br />
                    <br />
                </div>
                <div id="options" class="container-fluid text-center" style="border-top: 1px solid #707070; background-color: #222222;">
                    <br />
                    <br />
                    <h1 style="color: #A9C500; text-transform: uppercase"><i><b>Sign-up with our entry level features to get you started</b></i></h1>
                    <br />
                    <div class="row">
                        <div class="col-lg-5 col-sm-5"></div>
                        <div class="col-lg-2 col-sm-2" style="border-bottom: 2px solid #A9C500"></div>
                        <div class="col-lg-5 col-sm-5"></div>
                        <br />
                        <br />
                    </div>
                    <div class="row">
                        <div class="col-lg-1 col-md-1"></div>
                        <div class="col-lg-4 col-md-4">
                            <br />
                            <br />
                            <h1 style="color: #fff;">We have several plans that grow with you and your online communication needs.<br />
                                <br />
                                We even have the flexibility to craft a plan tailored excusably for your exact requirements. Just ask.</h1>
                        </div>
                        <div class="col-lg-1 col-md-1"></div>
                        <div class="panel col-lg-5 col-md-5 text-left" style="background-color: #222222;">
                            <h1 style="color: #00FFEE;"><b>LET'S GET STARTED!</b></h1>
                            <h4 style="color: #ffffff;"><b>Enter Promotional Code Here</b></h4>
                            <div class="row">
                                <div class="col-lg-10 form-group-lg">
                                    <div class="input-group">
                                        <asp:TextBox ID="TxtMX11" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="MX11-XXXXX"></asp:TextBox>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0 p-l-18">
                                            <h1 class="m-t-0 m-b-0" style="color: #A9C500;"><b>MX11</b></h1>
                                        </span>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0">
                                            <asp:Button ID="BtnMX11Go" runat="server" OnClick="BtnMX11Go_Click" Text="Go" CssClass="btn btn-lg btn-green" Style="width: 120%;" /></span>
                                    </div>
                                    <%--</div>

                             <div class="col-lg-11 form-group-lg">--%>
                                    <div class="input-group p-t-10">
                                        <asp:TextBox ID="TxtIN001" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="IN001-XXXXX"></asp:TextBox>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0 p-l-15">
                                            <h1 class="m-t-0 m-b-0" style="color: #A9C500;"><b>IN001</b></h1>
                                        </span>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0">
                                            <asp:Button ID="BtnIN001Go" runat="server" OnClick="BtnIN001Go_Click" Text="Go" CssClass="btn btn-lg btn-green" Style="width: 120%;" /></span>
                                    </div>
                                    <%-- </div>

                            <div class="col-lg-11 form-group-lg">--%>
                                    <div class="input-group p-t-10">
                                        <asp:TextBox ID="TxtMM02" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="MM02-XXXXX"></asp:TextBox>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0">
                                            <h1 class="m-t-0 m-b-0" style="color: #A9C500;"><b>MM02</b></h1>
                                        </span>
                                        <span style="background-color: #222222; border-color: #222222;" class="input-group-addon p-t-0 p-b-0">
                                            <asp:Button ID="BtnMM02Go" runat="server" OnClick="BtnMM02Go_Click" Text="Go" CssClass="btn btn-lg btn-green" Style="width: 120%;" /></span>
                                    </div>
                                </div>
                                <div class="col-lg-2 p-l-0"></div>
                                <div class="col-lg-12 p-t-15">
                                    <a href="RequestCode.aspx" class="btn btn-lg btn-green">Request a Code</a>
                                </div>
                                <div class="col-lg-12 p-t-10">
                                    <p style="color: #FFFFFF; font-size: 15px;">
                                        <i>Enter the promotion code you received in your inbox; this indicates we’ve directly contacted you
                                        and have already created an Appointment Ad for your company. 
                                        The code provided determine the account settings until you change them. Don’t have a code? Not a problem, 
                                        select the ‘Request a code’ option and we’ll get you Inline for success.
                                        </i>
                                    </p>
                                </div>

                            </div>
                        </div>
                        <%--  <div class="col-lg-2 col-sm-6 text-left">
                        <div class="panel-heading free-img">
                            <div class="img-content-free">
                                <h1 class="H-fontSize" style="font-family: 'Arial Rounded MT'; color: #000;"><b>FREE</b></h1>
                            </div>
                        </div>
                        <div class="panel-body" style="background-color: #000000; border: 1px solid #4B4A4A">
                            <h3 class="text-center H-bodyFontSize" style="color: #A9C500;"><i><b>15 DAY TRAIL</b></i>
                            </h3>
                            <div class="row">
                                <div class="col-lg-5"></div>
                                <div class="col-lg-2" style="border-bottom: 2px solid #A9C500"></div>
                                <div class="col-lg-5"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-12 B-FontSize" style="color: #fff;">
                                    <br />
                                    <p class="m-t-5">
                                        <i>Send up to 10 files a day for 15 days. We’ll upload a graphic that to your account 
                                            so you’ll be able to send your customers via email and get to understand the power 
                                            of inline communications. Click the ‘Sign up’ option to get your Appointment ad started.
                                        </i>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 B-FontSize" style="color: #ABC502;">
                                    <p>• 1 appointment style Ad</p>
                                    <p>• Up to 10 appointment emails a day</p>
                                    <p>• 24/7 online support</p>
                                    <p>• 1 revision included*</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer m-b-10" style="height: 80px; background-color: #ABC502;">
                            <div class="text-center m-t-8">
                                <a href="SignUpFree.aspx" class="btn btn-lg btn-black">SIGN UP</a>
                            </div>
                        </div>

                    </div>--%>

                        <%-- <div class="col-lg-2 col-sm-6 text-left">
                        <div class="panel-heading month-img">
                            <div class="img-content-month">
                                <h1 class="H-fontSize" style="font: 'Arial Rounded MT'; color: #000;"><b>.99/MONTH</b></h1>
                            </div>
                        </div>

                        <div class="panel-body" style="background-color: #000000; border: 1px solid #4B4A4A">
                            <h3 class="text-center H-bodyFontSize" style="color: #A9C500;"><i><b>APPOINTMENT CARD ACCESS</b></i>
                            </h3>
                            <div class="row">
                                <div class="col-lg-5"></div>
                                <div class="col-lg-2" style="border-bottom: 2px solid #A9C500"></div>
                                <div class="col-lg-5"></div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-12 B-FontSize" style="color: #fff;">
                                    <p>
                                        <i>Send up to 25 files a day. We’ll upload a graphic that to your account so you’ll be able to send your customers via 
                                            email and get to understand the power of inline communications. Click the ‘Sign up’ option to get your Appointment ad started and skip the trail period.</i>
                                    </p>
                                </div>
                            </div>
                            <br />
                            <div class="row m-b-4">
                                <div class="col-lg-12 B-FontSize" style="color: #ABC502;">
                                    <p>• 1 appointment style Ad</p>
                                    <p>• Up to 25 appointment emails a day</p>
                                    <p>• 24/7 online support</p>
                                    <p>• 2 revision included*</p>
                                </div>
                                <div class="col-md-12">
                                    <p style="color: #fff;" class="B-FontSize"><i>Each revision after the listed will cost $25.00. We try our best to make sure your Ad is exactly what you’re looking for the first time.</i></p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer" style="height: 80px; background-color: #ABC502;">
                            <div class="text-center m-t-8">
                                <a href="SignUpPaid.aspx" class="btn btn-lg btn-black">SIGN UP</a>
                            </div>
                        </div>
                    </div>--%>
                        <div class="col-lg-1 col-md-1"></div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-xs-6" style="background-color: #ABC502;">
                            <img src="Images/log1.png" class="img-responsive" />
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-6" style="background-color: #ABC502;">
                            <img src="Images/log1.png" class="img-responsive" />
                        </div>
                    </div>
                </div>
                <section id="contact" class="img-contact">
                    <div class="container-fluid text-center">
                        <br />
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div id="div2" runat="server">
                                    <strong>
                                        <asp:Label ID="LblMsg2" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-3"></div>
                            <br />
                        </div>
                        <h1 style="color: #A9C500; text-transform: uppercase;"><i><b>Contact MoreInline Today</b></i></h1>
                        <br />
                        <div class="row">
                            <div class="col-lg-5"></div>
                            <div class="col-lg-2" style="border-bottom: 2px solid #A9C500"></div>
                            <div class="col-lg-5"></div>

                            <div class="col-lg-12">
                                <h3 style="color: #fff;"><i>Send us a direct message if you need more information than what’s provided.</i></h3>
                                <h3 style="color: #fff;"><i>We’re here to make sure you keep your communication lines open.</i></h3>
                            </div>
                            <div class="col-lg-12">&nbsp;</div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6 form-group-lg p-t-15">
                                    <asp:TextBox ID="TxtName" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="Name"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 form-group-lg p-t-15">
                                    <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="Email"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 form-group-lg p-t-15">
                                    <asp:TextBox ID="TxtCompany" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="Company"></asp:TextBox>
                                </div>
                                <div class="col-lg-6 form-group-lg p-t-15">
                                    <asp:TextBox ID="TxtNumber" runat="server" CssClass="form-control navbar-dark-input" Style="border: 1px solid #4B4A4A" placeholder="Contact Number"></asp:TextBox>
                                </div>
                                <div class="col-lg-12 form-group-lg p-t-15">
                                    <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control navbar-dark-input textarea-resize" Style="border: 1px solid #4B4A4A;" placeholder="Message" EnableTheming="True"></asp:TextBox>
                                </div>
                                <div class="col-lg-12 p-t-25 p-b-5">
                                    <asp:Button ID="BtnSend" runat="server" Text="SEND MESSAGE" CssClass="btn btn-lg btn-green" OnClick="BtnSend_Click" />
                                </div>
                            </div>

                        </div>
                    </div>
                    <br />
                    <br />

                </section>
                <br />
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <center><p style="color: #8A8A8A; font: Bold 18px/20px Segoe UI;"><a href="../User/UserSignUp.aspx"><span style="color: #fff;">More</span><span style="color: #ABC502;">Inline</span></a> 754 Peachtree St NW - Suite 9 - Atlanta, GA 30308</p></center>
                            </div>
                            <div class="col-lg-3"></div>

                        </div>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <center><p style="color: #8A8A8A; font: 18px/20px Segoe UI;">8888-900-2095 &nbsp;&nbsp; |&nbsp;&nbsp;  moreinline.com &nbsp;&nbsp; | &nbsp;&nbsp;sales@moreinline.com</p></center>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <center><p style="color: #8A8A8A; font: 18px/20px Segoe UI;">© Copyright 2019. All Rights Reserved.</p></center>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </footer>
            </asp:Panel>
            <div class="row">
                <div class="col-lg-12">
                    <div id="div1" runat="server" visible="false">
                        <strong>
                            <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4"></div>
                <div class="col-lg-4 col-md-4 col-sm-4 m-t-40 m-l-10 m-r-10">
                    <br /><br /><br /><br /><br />
                    <div class="login-area">
                        <div class="text-center">
                            <img src="../Images/moreinline_black.png" style="width: 230px; padding-top: 15px;" />
                        </div>
                        <br />
                        <br />
                        <div class="p-l-10 p-r-10">
                            <span style="color: #707070;">Username</span>
                            <asp:TextBox ID="TxtUserName" runat="server" CssClass="form-control" placeholder="Username" autofocus></asp:TextBox><br />
                            <span style="color: #707070;">Password</span>
                            <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" type="password" placeholder="Password"></asp:TextBox>
                            <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                            <br />
                            <div class="text-right">
                                <a href="../ForgotPassword.aspx" style="color: #707070; font-size: 13px;" class="p-l-0">Forgotten account?</a>
                            </div>
                            <div class="text-center m-t-5">
                                <asp:Button ID="BtnLogin" runat="server" Text="Login" OnClick="BtnLogin_Click" CssClass="btn btn-light-green btn-size" />
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4"></div>
            </div>

        </div>
        <%--  <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>--%>
        <%----------------------------------------- Pop Up -------------------------------------------------------%>
        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #141414; border: 1px solid #A9C502;">
                    <div class="modal-body text-center">
                        <center>
                            <img src="Images/moreinline_logo.png" width="300px" class="img-responsive" /></center>
                        <small style="padding-left: 60px; color: #828282;"><i><b>Better Communication - Better Results</b></i></small>

                        <h2 style="color: #A9C502;">Email has been sent successfully.</h2>
                        <h3 style="color: #fff;">An email has been sent. We’ll be in contact with you soon. Thank you.</h3>
                        <button type="button" class="btn btn-sm btn-green" data-dismiss="modal" aria-label="Close">Close</button>
                    </div>
                </div>
            </div>
        </div>



    </form>
    <script>
        $("body").on('click', '.toggle-password', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input = $("#TxtPassword");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
    </script>
    <script>
        $(document).ready(function () {
            $("a").on('click', function (event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();
                    // Store hash
                    var hash = this.hash;
                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {
                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                }
            });
        });
    </script>
    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function () {      // When arrow is clicked
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 500);
        });
    </script>

</body>
</html>
