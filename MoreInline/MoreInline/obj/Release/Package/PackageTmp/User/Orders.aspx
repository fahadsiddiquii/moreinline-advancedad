﻿<%@ Page Title="Orders Information" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="Orders.aspx.cs" Inherits="MoreInline.User.Orders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <style>
        .img {
            border: 1px solid #ddd;
            border-radius: 4px;
            padding: 5px;
            width: 50px;
        }

            .img:hover {
                box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
            }
    </style>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': false,
                'info': true,
                'autoWidth': false
            })
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': false,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function openModal2() {
            $('#myModal2').modal('show');
        }
    </script>
    <script type="text/javascript">
        function openModal3() {
            $('#myModal3').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>


    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #019c7c;">Orders Information</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>

            <%--   <div class="col-lg-12 text-right">
                <asp:Button ID="BtnAdd" runat="server" Text="Add New" CssClass="btn btn-info" />
            </div>--%>
        </div>


        <br />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">

                <asp:Repeater ID="RptReceivedOrder" runat="server" OnItemDataBound="RptReceivedOrder_ItemDataBound" OnItemCommand="RptReceivedOrder_ItemCommand">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">Order #</th>
                                     <th class="text-center text-black">Order Type</th>
                                    <th class="text-center text-black">Customer</th>
                                    <%--    <th class="text-center text-black">Title</th>--%>
                                    <th class="text-center text-black">Date/Time Ordered</th>
                                    <%--             <th class="text-center text-black">QA</th>--%>
                                    <th class="text-center text-black">Status</th>
                                    <th class="text-center text-black">Action</th>
                                    <th class="text-center text-black" style="<%= HiddenClassName %>">Upload</th>
                                    <th class="text-center text-black">Designs </th>
                                    <th class="text-center text-black">Design Status</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>

                        <tr>
                            <td class="text-center">
                                <asp:Label ID="lblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                                <%--      <asp:Label ID="lblQAID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QAID") %>' />
                                <asp:Label ID="LblQaApproval" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QaApproval") %>' />
                                <asp:Label ID="LblMngrApproval" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MngrApproval") %>' />--%>
                                <asp:Label ID="LblCustApproval" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustApproval") %>' />
                                <asp:Label ID="lblcustappid" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"custapp") %>' />
                                <%--          <asp:Label ID="LblQaRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"QaRemarks") %>' />--%>
                                <%--                     <asp:Label ID="LblMngrRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MngrRemarks") %>' />--%>
                                <asp:Label ID="LblCustRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>' />

                                <asp:Label ID="Cust_ID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />

                            </td>
                                <td class="text-center">
                                <asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderType") %>'  />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Customer") %>' />
                            </td>
                            <%--    <td class="text-center">
                                <asp:Label ID="lblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' />
                            </td>--%>
                            <td class="text-center">
                                <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>' />
                            </td>


                            <%-- <td class="text-center">
                                <asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Qa") %>' />
                            </td>--%>
                            <td class="text-center">
                                <asp:Label ID="lblApprovalStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ApprovalStatus") %>' />

                            </td>
                            <td class="text-center">

                                <asp:Button ID="btnDetail" ToolTip="Details" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' class="btn btn-sm btn-dark-green" ForeColor="#ffffff" BackColor="#000000" Text="Details" CommandName="Detail" />

                            </td>
                            <td class="text-center" id="td_upload" runat="server">
                                <%--                           <button type="button" id="btnUpload" class="btn btn-sm btn-dark-green" data-toggle="modal" data-target="#myModal" onclick="SetId('<%#DataBinder.Eval(Container.DataItem,"AssestID") %>','<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>','<%#DataBinder.Eval(Container.DataItem,"QAID") %>')">Upload</button>--%>
                                <asp:Button ID="BtnUpload" ToolTip="Upload" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' class="btn btn-sm btn-dark-green" ForeColor="#ffffff" Text="Upload" CommandName="Upload" />
                            </td>
                            <td>
                                <asp:ImageButton ID="FilePath" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' CssClass="img" runat="server" CommandName="btnimg" />
                            </td>
                            <td>
                                <asp:Button ID="Btnstuts" Visible="false" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>' class="btn btn-sm btn-dark-green" ForeColor="#ffffff" Text="View Status" CommandName="custstatus" />
                            </td>
                        </tr>

                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>

    <script>
        function SetId(id, OrderNum, Qid) {

            $("#hfid").val(id);
            $("#hfQid").val(Qid);
            document.getElementById("<%=lblCode.ClientID %>").innerHTML = OrderNum;
        }
        function LoadStatus(QaApproval, MngrApproval, CustApproval, QaRemarks, MngrRemarks, CustRemarks) {

           <%-- document.getElementById("<%=txtQR.ClientID %>").innerHTML = QaRemarks;
            document.getElementById("lblQAp").innerHTML = QaApproval;

            document.getElementById("<%=txtMr.ClientID %>").innerHTML = MngrRemarks;
            document.getElementById("lblMA").innerHTML = MngrApproval;

            document.getElementById("<%=txtCr.ClientID %>").innerHTML = CustRemarks;
            document.getElementById("lblCA").innerHTML = CustApproval;--%>
        }
    </script>
    <%-- Pop Up --%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content ">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <div class="row p-t-10">
                        <div class="col-lg-12">
                            <div id="div2" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                            </div>
                            <br />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">

                            <asp:HiddenField runat="server" ID="hfid" />
                            <asp:HiddenField runat="server" ID="hfQid" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3">
                            <span style="background-color: #fff; border-color: #fff; padding-top: 10px;" class="input-group-addon">Order Number : </span>
                            <asp:Label Style="background-color: #fff; border-color: #fff; padding-top: 10px;" class="input-group-addon" ID="lblCode" runat="server" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3">
                            <asp:FileUpload ID="AddFile" runat="server" />
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <div class="modal-footer" style="background-color: #fff;">
                <center>
                    <asp:Button Text="Upload" ID="BtnUoload" CssClass="btn btn-success btn-sm" OnClick="BtnUoload_Click" runat="server" />
                </center>
            </div>
        </div>
    </div>
    <%-- End --%>


    <%-- Pop Up --%>
    <div class="modal fade" id="myModal3" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content ">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <div class="row">
                    </div>
                    <br />
                    <div class="row">
                        <%--     <div class="col-lg-4">
                            <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px; float: left;" class="input-group-addon">Qa Approval: <span id="lblQAp" runat="server"></span></span>
                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="txtQR" Enabled="false" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="" EnableTheming="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px; float: left;" class="input-group-addon">Manager Approval: <span id="lblMA" runat="server"></span></span>
                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="txtMr" Enabled="false" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="" EnableTheming="True"></asp:TextBox>
                            </div>
                        </div>--%>
                        <div class="col-lg-12">
                            <span>Customer Approval: <span id="lblCA" runat="server"></span></span>

                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="txtCr" Enabled="false" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="" EnableTheming="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />

                </div>
            </div>

        </div>
    </div>
    <%-- End --%>



    <%-- image view --%>
    <div class="modal fade" id="myModal2">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #f7f8f9; border: 1px solid #4B4A4A;">
                <div class="modal-body">
                    <img src="#" id="imgpop" runat="server" alt="Alternate Text" style="width: 100%; height: 100%;" />
                </div>
            </div>
        </div>
    </div>

    <%-- end --%>
</asp:Content>
