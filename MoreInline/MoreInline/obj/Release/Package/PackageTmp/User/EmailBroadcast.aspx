﻿<%@ Page Title="EmailBroadcast" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="EmailBroadcast.aspx.cs" Inherits="MoreInline.User.EmailBroadcast" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="../dist/js/BadWords.js"></script>
    <script>
        function validate_text() {
            debugger;
            var compare_text = $("#ContentPlaceHolder1_TxtAdSubject").val();
            var array = compare_text.split(" ");
            let obj = {};

            for (let i = 0; i < array.length; i++) {

                if (!obj[array[i]]) {
                    const element = array[i];
                    obj[element] = true;
                }
            }

            for (let j = 0; j < swear_words_arr.length; j++) {

                if (obj[swear_words_arr[j]]) {
                    $("#ContentPlaceHolder1_TxtAdSubject").val('');
                    alert("The message will not be sent!!!\nThe following unethical words are found:\n_______________________________\n" + swear_words_arr[j] + "\n_______________________________");
                }
            }

        }
    </script>

    <script type="text/javascript">
        function checkSpcialChar(event) {
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
                if (event.keyCode != 32) {
                    event.returnValue = false;
                    alert("Special characters are not allowed");
                    return;
                }
            }
            event.returnValue = true;
        }
    </script>


    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <style>
        .LabelSpan {
            color: #000;
            background-color: #fafafa !important;
            padding-top: 10px;
            border-color: #fafafa !important;
        }
    </style>

    <script type="text/javascript">
        function checkAll(bx) {
            var cbs = document.getElementsByTagName('input');
            for (var i = 0; i < cbs.length; i++) {
                if (cbs[i].type == 'checkbox') {
                    cbs[i].checked = bx.checked;

                    if (cbs[i].checked == false) {
                        bx.checked = false;
                    }
                }
            }
        }
    </script>
    <%----------------------------------------- Page -------------------------------------------------------%>


    <div class="container">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #019c7c;">Email Broadcasting</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label>
                    </strong>
                </div>
                <br />
            </div>
        </div>
        <div class="input-group m-t-10">
            <span class="input-group-addon LabelSpan">Upload Image:</span>
            <asp:FileUpload runat="server" ID="FUImage" CssClass="form-control m-t-5 txtBox-style" />
        </div>
        <div class="input-group m-t-10">
            <span class="input-group-addon LabelSpan">Email Subject:</span>
            <asp:TextBox ID="TxtAdSubject" runat="server" CssClass="form-control m-t-5" placeholder="Enter email subject" onchange="validate_text()" onkeypress="return checkSpcialChar(event)"></asp:TextBox>

        </div>
        <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="5" CssClass="form-control textarea-resize m-t-10" placeholder="Enter Message here.." EnableTheming="True"></asp:TextBox>

        <hr />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">
                <asp:Repeater ID="RptContacts" runat="server" OnItemDataBound="RptContacts_ItemDataBound">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0" cellpadding="0" cellspacing="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th style="width: 5px;">
                                        <asp:CheckBox ID="ChkHeader" runat="server" onclick="checkAll(this);" />
                                    </th>
                                    <th class="text-center text-black">Name</th>
                                    <th class="text-center text-black">Email</th>
                                    <th class="text-center text-black">Company</th>
                                    <th class="text-center text-black">Subscription</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="text-center">
                                <asp:CheckBox Text="" ID="ChkRow" runat="server" />
                                <asp:Label ID="LblCustID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' Visible="false" />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />

                            </td>
                            <td class="text-center">
                                <asp:Label ID="lblSubscribed" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsPromoActive") %>' />
                            </td>

                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <div class="col-lg-12 text-right">
                <asp:Button ID="BtnSend" Text="Send" runat="server" OnClick="BtnSend_Click" CssClass="btn btn-sm btn-light-green" />
            </div>
        </div>
    </div>


    <%----------------------------------------- Pop Up -------------------------------------------------------%>
</asp:Content>
