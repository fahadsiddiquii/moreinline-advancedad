﻿<%@ Page Title="Add New Customer" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="AddNewCustomer.aspx.cs" Inherits="MoreInline.User.AddNewCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } <%--else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <%----------------------------------------- Page -------------------------------------------------------%>
    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #019c7c;">Add New Customer</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>
        <div class="row p-t-10">
            <div class="col-lg-2"></div>
            <div class="col-lg-8">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row p-t-10">
            <div class="col-lg-4 col-sm-4"></div>
            <div class="col-lg-4 col-sm-4">
                <asp:TextBox ID="TxtCName" runat="server" CssClass="form-control m-t-10" placeholder="Company Name"></asp:TextBox>
                <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control m-t-10" placeholder="Enter Email Address"></asp:TextBox>
                <asp:DropDownList ID="DdlCode" runat="server" CssClass="form-control m-t-10" Style="background-color: #007f9f; color: #fff;"></asp:DropDownList>
            </div>
            <div class="col-lg-4 col-sm-4"></div>
        </div>
        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Appointment ad</span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0 col-sm-4">
                <asp:FileUpload ID="UploadAppointment" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>

        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Locked Appointment ad</span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0">
                <asp:FileUpload ID="UploadLockAppointment" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>
        <asp:Panel runat="server" Visible="true">
        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Customer ad 1 Locked</span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0">
                <asp:FileUpload ID="UploadCust1" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Customer ad 1 Unlocked</span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0">
                <asp:FileUpload ID="UploadCust2" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Customer ad 2 Locked </span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0">
                <asp:FileUpload ID="UploadCust3" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>
        <div class="row p-t-25">
            <div class="col-lg-4 col-sm-3"></div>
            <div class="col-lg-2 p-r-0 col-sm-3">
                <span >Customer ad 2 Unlocked</span>
            </div>
            <div class="col-lg-2 p-l-0 p-r-0">
                <asp:FileUpload ID="UploadCust4" runat="server" accept="image/jpeg"  />
            </div>
            <div class="col-lg-4"></div>
        </div>
        </asp:Panel>
        <div class="row p-t-25">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <center><asp:Button ID="BtnSave" runat="server" Text="Save" OnClick="BtnSave_Click" CssClass="btn btn-sm btn-light-green" Width="90px" /></center>
            </div>
            <div class="col-lg-4"></div>
        </div>
        <br />
    </div>
</asp:Content>
