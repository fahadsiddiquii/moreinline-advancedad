﻿<%@ Page Title="Order Detail" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="OrdersDetail.aspx.cs" Inherits="MoreInline.User.OrdersDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script>
        // var pathm;


        function SetAudio(path) {
            // alert(path);
            //pathm = path;
            debugger;
            $("#audio").attr("src", path);

            // $("#audio")[0].play();

        }


        function Disabled() {
            $("#record").addClass("disabled");
        }
    </script>
    <%-- <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 text-center">
            <h3 class="p-l-18" style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>Order Detail</b></h3>
        </div>
        <div class="col-lg-1"></div>
    </div>




    <div class="row p-t-10">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
            <br />
        </div>
        <div class="col-lg-2"></div>
    </div>
    <asp:HiddenField runat="server" ID="HfAssetId" />
    <asp:HiddenField runat="server" ID="HfVoicePath" />
    <asp:HiddenField runat="server" ID="hffilecount" />

    <div class="row">
        <div class="col-lg-3"></div>
        <section class="col-lg-6 connectedSortable">
            <div class="row">
                <div class="col-lg-12">
          
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-lg-12">
                    <div class="box box-solid">
                        <div class="box-body border-radius-none">
                            <asp:TextBox ID="TxtMessage" TextMode="MultiLine" Enabled="false" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="Message to your customer..." EnableTheming="True"></asp:TextBox>
                        </div>

                        <div class="box-footer no-border" style="background-color: #DFDFDF;">
                            <div class="row">
                                <div class="col-lg-12" hidden="hidden">

                                    <audio controls id="audio" style="height: 23px; margin-left: 5px; vertical-align: text-top;" class="pull-right;"></audio>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-lg-3 p-t-3">
                    <span>
                        <asp:CheckBox ID="ChkProfilePic" Enabled="false" runat="server" Style="float: left; margin-top: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding-left: 4px;">USE MY PROFILE PHOTO</span>
                    </span>
                </div>
                <div class="col-lg-12">
                    <asp:Repeater ID="RptOrderAssets" runat="server" OnItemDataBound="RptOrderAssets_ItemDataBound">
                        <HeaderTemplate>
                            <table id="example1" class="table table-bordered" border="0">
                                <thead class="table-head">
                                    <tr style="background-color: #A9C502">
                                        <th class="text-center text-black">File Name</th>
                                        <th class="text-center text-black">Action</th>
                                    </tr>
                                    <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>

                                <td class="text-center text-white">
                                    <asp:Label ID="FilePath" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                                </td>
                                <td class="text-center text-white">
                                    <asp:Button ID="BtnDownload" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ADID") %>' OnClick="BtnDownload_Click" CommandName='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' Text="Download" class="btn btn-sm btn-green" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody></table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row m-t-20">

                <div class="col-lg-12">
                    <p style="color: #707070;"><b>Uploaded assets by customer! type : content, photos, logos, etc.</b></p>
                    <asp:Label Text="" Style="color: #707070;" ID="lblFiles" runat="server" />
                </div>

            </div>

           
        </section>

    </div>--%>

    <asp:HiddenField runat="server" ID="HfAssetId" />
    <asp:HiddenField runat="server" ID="HfVoicePath" />
    <asp:HiddenField runat="server" ID="hffilecount" />

    <div class="row">
        <div class="col-lg-3"></div>
        <section class="col-lg-6 connectedSortable">
            <div class="row">
                <div class="col-lg-12">
                    <h3 style="color: #019c7c; float: left;">Order Detail</h3>
                </div>
            </div>
         <%--   <div class="row m-t-20">
                <div class="col-lg-6 form-group-sm">
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Ad Subject:</span>
                        <asp:TextBox ID="TxtAdSubject" Enabled="false" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Enter message title"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email: </span>
                        <asp:TextBox ID="TxtEmail" Enabled="false" TextMode="Email" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Enter email address"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date: </span>
                        <asp:TextBox ID="TxtDate" Enabled="false" onpaste="return false" TextMode="Date" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Select date of service"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time:</span>
                        <asp:DropDownList ID="DdlHour" Enabled="false" runat="server" Style="border: 1px solid #4B4A4A; background-color: #A9C502; color: #000; width: 107px;" CssClass="form-control m-t-5">
                            <asp:ListItem Value="0" Text="Select Time"></asp:ListItem>
                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                            <asp:ListItem Value="2" Text="2"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3"></asp:ListItem>
                            <asp:ListItem Value="4" Text="4"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            <asp:ListItem Value="11" Text="11"></asp:ListItem>
                            <asp:ListItem Value="12" Text="12"></asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DdlTT" Enabled="false" runat="server" Style="border: 1px solid #4B4A4A; background-color: #A9C502; color: #000; width: 62px;" CssClass="form-control m-t-5 m-l-5">
                            <asp:ListItem Value="0" Text="pm"></asp:ListItem>
                            <asp:ListItem Value="1" Text="am"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-6 form-group-sm">
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Phone:</span>
                        <asp:TextBox ID="TxtPhone" Enabled="false" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5 num " placeholder="Enter phone number"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;Location:</span>
                        <asp:TextBox ID="TxtLocation" Enabled="false" runat="server" onkeydown="return checkShortcut();" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Enter business address"></asp:TextBox>
                    </div>
                    <div class="input-group">
                        <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;Website:</span>
                        <asp:TextBox ID="TxtWebsite" Enabled="false" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Enter URL"></asp:TextBox>
                    </div>
                </div>
            </div>--%>
            <div class="row m-t-20">
                <div class="col-lg-12">
                    <div class="box box-solid">
                        <div class="box-body border-radius-none">
                            <asp:TextBox ID="TxtMessage" Enabled="false" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="Message to your customer..." EnableTheming="True"></asp:TextBox>
                        </div>

                        <%-- <div class="box-footer no-border" style="background-color: #DFDFDF;">
                            <div class="row">
                                <div class="col-lg-12">

                                    <img src="../Images/Icons/mic.png" alt="Alternate Text" id="record" class="img-circle" style="background-color: #A9C502; float: left;" />
                                    <img src="../Images/Icons/mic.gif" alt="Alternate Text" id="pause" hidden class="img-circle" style="background-color: #A9C502; float: left;" />
                                  
                                    <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>
                                 
                                    <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                                    <img id="stop" src="../Images/delete.png" hidden class="img-circle" style="background-color: #fff; float: right;" />
                                    <img id="save" src="../Images/up.png" hidden class="img-circle" style="background-color: #fff; float: right;" />

                                </div>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>


           <%-- <div class="row m-t-5">
                <div class="col-lg-3 p-t-5">
                    <span>
                        <asp:CheckBox ID="ChkAddVoice" Enabled="false" runat="server" Style="float: left; margin-top: 5px;" />
                        <img src="../Images/Icons/voice-message1.png" style="padding-left: 2px;" />
                        <span style="color: #fff; font-size: 10px;" class="text-right">ADD VOICE MESSAGE</span>
                    </span>
                </div>
                <div class="col-lg-3 p-t-5">
                    <span>
                        <asp:CheckBox ID="ChkAddContact" Enabled="false" runat="server" Style="float: left; margin-top: 5px;" />
                        <img src="../Images/Icons/email1.png" style="padding-left: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding: 7px 0 0 4px;">ADD CONTACT</span>
                    </span>
                </div>
                <div class="col-lg-3 p-t-5">
                    <span>
                        <asp:CheckBox ID="ChkAddDirection" Enabled="false" runat="server" Style="float: left; margin-top: 5px;" />
                        <img src="../Images/Icons/directions1.png" style="padding-left: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding: 7px 0 0 0;">ADD LOCATION</span>
                    </span>
                </div>
                <div class="col-lg-3 p-t-5">
                    <span>
                        <asp:CheckBox ID="ChkAddReminder" Enabled="false" runat="server" Style="float: left; margin-top: 5px;" />
                        <img src="../Images/Icons/reminder1.png" style="padding-left: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding: 7px 0 0 4px;">ADD DATE</span>
                    </span>
                </div>
            </div>

            <div class="row m-t-10">
                <div class="col-lg-3">
                    <span>
                        <asp:CheckBox ID="chkAddUrl" Enabled="false" runat="server" Style="float: left; margin-top: 5px;" />
                        <img src="../Images/Icons/web1.png" style="padding-left: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding: 10px 0 0 4px;">ADD WEBSITE</span>
                    </span>
                </div>
                <div class="col-lg-3 p-t-3">
                    <span>
                        <asp:CheckBox ID="ChkProfilePic" Enabled="false" runat="server" Style="float: left; margin-top: 2px;" />
                        <span style="color: #fff; font-size: 10px; padding-left: 4px;">USE MY PROFILE PHOTO</span>
                    </span>
                </div>

                <div class="col-lg-6"></div>
            </div>--%>


            <div class="row">
                <div class="col-lg-12 m-t-15">
                    <asp:Repeater ID="RptOrderAssets" runat="server" OnItemDataBound="RptOrderAssets_ItemDataBound" OnItemCommand="RptOrderAssets_ItemCommand">
                        <HeaderTemplate>
                            <table id="example1" class="table table-bordered" border="0">
                                <thead class="table-head">
                                    <tr style="background-color: #A9C502">
                                        <%--<th class="text-center text-black">ID #</th>--%>
                                        <th class="text-center text-black">File Name</th>
                                        <th class="text-center text-black">Action</th>
                                        <th class="text-center text-black">View</th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="text-center">
                                    <span class="m-t-10">
                                        <asp:Label ID="FilePath" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' /></span>
                                </td>
                                <td class="text-center">
                                    <asp:Button ID="BtnDownload" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ADID") %>' OnClick="BtnDownload_Click"
                                        CommandName='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' Text="Download" class="btn btn-sm btn-light-green m-t-10" />
                                </td>
                                <td class="text-center">
                                    <asp:ImageButton ID="ImgFile" runat="server" Height="40px" CommandName="View" ToolTip="View" /></td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody></table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>

        </section>

    </div>

    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
