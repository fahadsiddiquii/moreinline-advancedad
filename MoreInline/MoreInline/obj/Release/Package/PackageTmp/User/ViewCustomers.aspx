﻿<%@ Page Title="View Customers" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="ViewCustomers.aspx.cs" Inherits="MoreInline.User.ViewCustomers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#example3 [id*=ChkHeader]").click(function () {
                if ($(this).is(":checked")) {
                    $("#example3 [id*=ChkRow]").attr("checked", "checked");
                } else {
                    $("#example3 [id*=ChkRow]").removeAttr("checked");
                }
            });
            $("#example3 [id*=ChkRow]").click(function () {
                if ($("#example3 [id*=ChkRow]").length == $("#example3 [id*=ChkRow]:checked").length) {
                    $("#example3 [id*=ChkHeader]").attr("Checked", "Checked");
                } else {
                    $("#example3 [id*=ChkHeader]").removeAttr("checked");
                }
            });
        });
    </script>--%>

    <script type="text/javascript">
        function checkAll(bx) {
            var cbs = document.getElementsByTagName('input');
            for (var i = 0; i < cbs.length; i++) {
                if (cbs[i].type == 'checkbox') {
                    cbs[i].checked = bx.checked;

                    if (cbs[i].checked == false) {
                        bx.checked = false;
                    }
                }
            }
        }
    </script>

    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            $('#example3').DataTable()
            $('#example4').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>

    <script>
        $(function () {
            $('#example5').DataTable()
            $('#example6').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script>
        $(function () {
            $('#example7').DataTable()
            $('#example8').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } <%--else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script type="text/javascript">
        function LockedModal() {
            $('#LockedModal').modal('show');
        }
    </script>

    <script type="text/javascript">
        function OrderDetailsModal() {
            $('#OrderDetails').modal('show');
        }
    </script>

    <script type="text/javascript">
        function EditCustomerModal() {
            $('#EditCustomer').modal('show');
        }
    </script>

    <style>
        .TabHeading {
            font-size: 30px;
        }

            .TabHeading > li > a {
                color: #fff;
            }

                .TabHeading > li > a:hover {
                    background-color: #A9C502;
                    color: #000;
                }

            .TabHeading > li.active > a {
                background-color: #A9C502 !important;
                color: #000;
            }

                .TabHeading > li.active > a:focus {
                    background-color: #A9C502 !important;
                    color: #000;
                }

                .TabHeading > li.active > a:hover {
                    background-color: #A9C502 !important;
                    color: #000;
                }

        .btn-tab {
            color: #000;
            background-color: #A9C502;
            border-color: #fff;
            border-radius: 16px 16px 0 0;
        }

            .btn-tab:hover {
                color: #000;
                border-color: #A9C502;
                background-color: #fff;
            }
    </style>
    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">
        <div class="row">
            <%-- <div class="col-lg-1"></div>--%>
            <div class="col-lg-12 text-center">
                <h3 style="color: #019c7c;">View Customers</h3>
            </div>
            <%--<div class="col-lg-1"></div>--%>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12 text-center">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
                <u>
                    <asp:Label ID="LblName" runat="server" Style="color: #001b00; font-size: 20px; font-weight: bold;"></asp:Label></u>
            </div>
        </div>
        <br />
        <div class="panel">
            <div id="Tabs" role="tabpanel">
                <!-- Nav tabs -->
                <ul class="nav TabHeading nav-tabs" role="tablist">
                    <%-- <li class="active"><a href="#Active" aria-controls="personal" role="tab" data-toggle="tab">Active
                    </a></li>
                    <li><a href="#Pending" aria-controls="employment" role="tab" data-toggle="tab">Pending</a></li>
                    <li><a href="#Expire" aria-controls="Expire" role="tab" data-toggle="tab">Expire</a></li>--%>
                    <li>
                        <asp:Button ID="BtnActive" Text="Active" runat="server" CssClass="btn-tab" OnClick="BtnActive_Click" />
                    </li>
                    <li>
                        <asp:Button ID="BtnPending" Text="Pending" runat="server" CssClass="btn-tab" OnClick="BtnPending_Click" /></li>
                    <li>
                        <asp:Button ID="BtnExpire" Text="Expire" runat="server" CssClass="btn-tab" OnClick="BtnExpire_Click" /></li>
                         <li>
                        <asp:Button ID="btnprepaid" Text="Prepaid Customers" runat="server" CssClass="btn-tab" OnClick="btnprepaid_Click" />

                         </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="padding-top: 20px">
                    <%--<div role="tabpanel" class="tab-pane active" id="Active">--%>
                    <asp:Panel ID="PnlActive" runat="server">
                        <div class="row">
                            <div class="col-lg-12" style="overflow-x: auto">
                                <%--Cust_ID	Name	Email	Company	AccType	ExpireDate--%>

                                <asp:Repeater ID="RptViewCustomers" runat="server" OnItemCommand="RptViewCustomers_ItemCommand" OnItemDataBound="RptViewCustomers_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="example1" class="table table-bordered" border="0">
                                            <thead class="table-head">
                                                <tr style="background-color: #A9C502">
                                                    <th class="text-center text-black">SNo.</th>
                                                    <th class="text-center text-black">Cust ID</th>
                                                    <th class="text-center text-black">Name</th>
                                                    <th class="text-center text-black">Email</th>
                                                    <th class="text-center text-black">Company</th>
                                                    <th class="text-center text-black">Account Type</th>
                                                    <th class="text-center text-black">Expire Date</th>
                                                    <th class="text-center text-black">Active</th>
                                                    <th class="text-center text-black" style="display: none">View</th>
                                                    <th class="text-center text-black">Details</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblCodeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />
                                                <asp:Label ID="LblIsActive" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsActive") %>' Visible="false" />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblAccType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AccType") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpireDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExpireDate") %>' />
                                            </td>
                                            <td class="text-center">
                                                <%--<asp:ImageButton ID="ImgDel" ToolTip="Block Customer" runat="server" ImageUrl="~/Images/Icons/blocked.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' CommandName="DeleteCust" />--%>
                                                <asp:ImageButton ID="IBtnCheck" runat="server" ImageUrl="~/Images/Icons/check.png" Width="30px"
                                                    CommandName="Active" ToolTip="Active" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />

                                                <asp:ImageButton ID="IBtnCross" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                                    CommandName="InActive" ToolTip="InActive" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />
                                            </td>
                                            <td class="text-center" style="display: none">
                                                <asp:ImageButton ID="ImgEye" runat="server" ImageUrl="~/Images/iconEye.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' CommandName="ViewEye" />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton Text="Details" CommandName="details" ToolTip="Details" ImageUrl="~/Images/Icons/details.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody></table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- </div>--%>
                    <%--div role="tabpanel" class="tab-pane" id="Pending">--%>
                    <asp:Panel ID="PnlPending" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12" style="overflow-x: auto">
                                <%--Ads.AdsID,Ads.CustomerEmail,Ads.CompanyName,ES.EmailStatus,EmailSendOn--%>
                                <asp:Repeater ID="RptViewPendingCustomers" runat="server" OnItemCommand="RptViewPendingCustomers_ItemCommand" OnItemDataBound="RptViewPendingCustomers_ItemDataBound" EnableViewState="true">
                                    <HeaderTemplate>
                                        <table id="example3" class="table table-bordered" border="0">
                                            <thead class="table-head">
                                                <tr style="background-color: #A9C502">
                                                    <th class="text-center" width="10px">
                                                        <asp:CheckBox ID="ChkHeader" runat="server" onclick="checkAll(this);" /></th>
                                                    <th class="text-center text-black">Company</th>
                                                    <th class="text-center text-black">Email</th>
                                                    <th class="text-center text-black">Status</th>
                                                    <th class="text-center text-black">Full Code</th>
                                                    <th class="text-center text-black">Subscription</th>
                                                    <th class="text-center text-black">Edit | Delete</th>
                                                    <th class="text-center text-black">View Ad</th>
                                                </tr>
                                                <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:CheckBox ID="ChkRow" runat="server" />

                                                <asp:Label ID="LblAdsID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AdsID") %>' Visible="false" />
                                                <asp:Label ID="LblCodeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodeID") %>' Visible="false" />
                                                <asp:Label ID="LblAppointmentAds" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AppointmentAds") %>' Visible="false" />
                                                <asp:Label ID="LblLockedAppointmentAds" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LockedAppointmentAds") %>' Visible="false" />

                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CompanyName") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustomerEmail") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblEmailStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EmailStatus") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblFullCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FullCode") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblSubscription" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsSubscribed") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ImgEditCust" ToolTip="Edit Customer" runat="server" ImageUrl="~/Images/Icons/editNew.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AdsID") %>' CommandName="EditCust" />
                                                <asp:ImageButton ID="ImgDeleteCust" ToolTip="Delete Customer" runat="server" ImageUrl="~/Images/Icons/bin.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AdsID") %>' CommandName="DeleteCust" OnClientClick='javascript:return confirm("Do you want to delete?")' />
                                            </td>
                                            <td class="text-center">
                                                <asp:ImageButton ID="ImgViewUnLocked" ToolTip="View UnLocked" runat="server" ImageUrl="~/Images/Icons/unlock_Icon.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AdsID") %>' CommandName="ViewUnLocked" />
                                                <asp:ImageButton ID="ImgViewLocked" ToolTip="View Locked" runat="server" ImageUrl="~/Images/Icons/Lock_Icon.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AdsID") %>' CommandName="ViewLocked" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody></table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="col-lg-10"></div>
                            <div class="col-lg-2">
                                <%--<asp:Button ID="BtnNext" runat="server" Text="Send Invite" CssClass="btn btn-sm btn-light-green pull-right" Style="width: 130px;" data-toggle="modal" data-target="#Message" />--%><%--OnClick="BtnNext_Click"--%>
                                <button type="button" class="btn btn-sm btn-light-green pull-right" data-toggle="modal" data-target="#Message">Send Invite</button>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--</div>--%>

                    <%-- <div role="tabpanel" class="tab-pane" id="Expire">--%>
                    <asp:Panel ID="PnlExpire" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12" style="overflow-x: auto">
                                <%--Ads.AdsID,Ads.CustomerEmail,Ads.CompanyName,ES.EmailStatus,EmailSendOn--%>
                                <asp:Repeater ID="RptExpire" runat="server" OnItemCommand="RptExpire_ItemCommand" OnItemDataBound="RptExpire_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="example7" class="table table-bordered" border="0">
                                            <thead class="table-head">
                                                <tr style="background-color: #A9C502">
                                                    <th class="text-center" width="10px">
                                                        <asp:CheckBox ID="ChkHeader" runat="server" onclick="checkAll(this);" /></th>
                                                    <th class="text-center text-black">Cust ID</th>
                                                    <th class="text-center text-black">Name</th>
                                                    <th class="text-center text-black">Company</th>
                                                    <th class="text-center text-black">Email</th>
                                                    <th class="text-center text-black">Account Type</th>
                                                    <th class="text-center text-black">Expire Date</th>
                                                    <th class="text-center text-black">Subscription</th>
                                                    <th class="text-center text-black">Active</th>
                                                </tr>
                                                <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:CheckBox ID="ChkRowExp" runat="server" />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblCustID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />
                                                <asp:Label ID="LblIsActiveExp" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsActive") %>' Visible="false" />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpCompany" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Company") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpAccType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AccType") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblExpire" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ExpireDate") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblSubscribed" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsPackageExpNActive") %>' />
                                            </td>
                                            <td class="text-center">
                                                <%-- <asp:ImageButton ID="ImgDelete" ToolTip="Block Customer" runat="server" ImageUrl="~/Images/Icons/blocked.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' CommandName="DeleteCust" />--%>
                                                <asp:ImageButton ID="IBtnCheckExp" runat="server" ImageUrl="~/Images/Icons/check.png" Width="30px"
                                                    CommandName="Active" ToolTip="Active" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />

                                                <asp:ImageButton ID="IBtnCrossExp" runat="server" ImageUrl="~/Images/Icons/cross.png" Width="30px"
                                                    CommandName="InActive" ToolTip="InActive" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Cust_ID") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody></table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="col-lg-10"></div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-sm btn-light-green pull-right" data-toggle="modal" data-target="#EmailMessage">Send Rejoining Email</button>
                            </div>
                        </div>

                    </asp:Panel>
                    <%--</div>--%>


                      <asp:Panel ID="pnlPrePaid" runat="server" Visible="false">
                        <div class="row">
                            <div class="col-lg-12" style="overflow-x: auto">
                                <%--Ads.AdsID,Ads.CustomerEmail,Ads.CompanyName,ES.EmailStatus,EmailSendOn--%>
                                <asp:Repeater ID="RptPrePaid" runat="server" OnItemCommand="RptPrePaid_ItemCommand" OnItemDataBound="RptPrePaid_ItemDataBound">
                                    <HeaderTemplate>
                                        <table id="example7" class="table table-bordered" border="0">
                                            <thead class="table-head">
                                                <tr style="background-color: #A9C502">
                                                      <th class="text-center text-black">Id</th>
                                                     <th class="text-center text-black">Full Name</th>
                                                    <th class="text-center text-black">Email</th>
                                                    <th class="text-center text-black">Amount</th>
                                                    <th class="text-center text-black">Transaction Id</th>
                                                    <th class="text-center text-black">Payment Type</th>
                                                    <th class="text-center text-black">Status</th>
                                                    <th class="text-center text-black">Created Date</th>
                                                     <th class="text-center text-black">Register</th>
                                                </tr>
                                                <tbody>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td class="text-center">
                                                <asp:Label ID="Lblid" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"id") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblFname" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Fname") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblAmount" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Amount") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblTransactionId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TransactionId") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="LblPaymentType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentType") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Status") %>' />
                                            </td>
                                             <td class="text-center">
                                                <asp:Label ID="LblCreateDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CreateDate") %>' />
                                            </td>
                                            <td class="text-center">
                                                <asp:LinkButton Text="Register" CommandName="Register"  CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Email") %>'  runat="server" />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </tbody></table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                      
                        </div>

                    </asp:Panel>
                </div>
            </div>
            <asp:HiddenField ID="TabName" runat="server" />
        </div>
        <script type="text/javascript">
            $(function () {
                var tabName = $("[id*=TabName]").val() != "" ? $("[id*=TabName]").val() : "personal";
                $('#Tabs a[href="#' + tabName + '"]').tab('show');
                $("#Tabs a").click(function () {
                    $("[id*=TabName]").val($(this).attr("href").replace("#", ""));
                });
            });
        </script>

    </div>
    <%----------------------------------------- Pop Up -------------------------------------------------------%>
    <div class="modal fade" id="OrderDetails">
        <div class="modal-dialog">
            <div class="modal-content" style="border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h3>Order Details</h3>
                </div>
                <div class="modal-body">
                    <asp:Repeater ID="RptOrderDetails" runat="server">
                        <HeaderTemplate>
                            <table id="example5" class="table table-bordered" border="0">
                                <thead class="table-head">
                                    <tr style="background-color: #A9C502">
                                        <th class="text-center text-black">Order#</th>
                                        <th class="text-center text-black">Subject</th>
                                        <th class="text-center text-black">Status</th>
                                        <th class="text-center text-black">Order Date/Time</th>
                                    </tr>
                                    <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td class="text-center">
                                    <asp:Label ID="LblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                                </td>
                                <td class="text-center">
                                    <asp:Label ID="LblSubject" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' />
                                </td>
                                <td class="text-center">
                                    <asp:Label ID="LblApprovalStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ApprovalStatus") %>' />
                                </td>
                                <td class="text-center">
                                    <asp:Label ID="LblOrderDateTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"OrderDateTime") %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody></table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-body">
                <div class="well-lg">
                    <asp:Image ID="ImgUnLocked" runat="server" CssClass="img-responsive bordercolor" />
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="LockedModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-body">
                <div class="well-lg">
                    <asp:Image ID="ImgLocked" runat="server" CssClass="img-responsive bordercolor" />

                    <%-- <div class="code-block">
                        <img src="../Images/Locked.png" class="code-Lock" />
                        <h3>We've done the work for you. Send out this appointment card up to 5,000 times a month.</h3>
                        <h2>go to: More<span style="color: #A9C502">Inline</span>.com</h2>
                        <h3>Look for the matching <span style="color: #A9C502">greenGocode</span> and click on <span style="color: #A9C502">Go</span></h3>
                        <h1>
                            <asp:Label ID="LblFullCodePop" runat="server"></asp:Label></h1>
                        <h3>Send out as many Appointment cards as you desire. There is no limit set.</h3>
                        <h3 style="color: #A9C502">Click on lock icon to unlock.</h3>
                    </div>--%>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="Message" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <h3>Enter Message For Customer</h3>
                </div>
                <div class="modal-body">
                    <asp:TextBox ID="TxtMessage" runat="server" TextMode="MultiLine" Rows="10" CssClass="form-control textarea-resize" placeholder="Enter Message here.." EnableTheming="True" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnSendCode" runat="server" Text="Send Promo Code" OnClick="BtnSendCode_Click" CssClass="btn btn-sm btn-light-green" Style="width: 140px;" />
                    <button type="button" class="btn btn-sm btn-danger" style="background-color: #eb4034" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="EditCustomer">
        <div class="modal-dialog">
            <div class="modal-content" style="border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <h3>Edit Customer</h3>
                </div>
                <div class="modal-body">
                    <label>Enter Company Name</label><br />
                    <asp:TextBox ID="TxtCompanyName" runat="server" CssClass="form-control form-group-sm textarea-resize" placeholder="Enter Company Name" /><br />
                    <label>Enter Company Email</label><br />
                    <asp:TextBox ID="TxtCustomerEmail" runat="server" CssClass="form-control form-group-sm textarea-resize" placeholder="Enter Company Email" />

                </div>
                <div class="modal-footer">
                    <asp:Button ID="btnEditCust" runat="server" Text="Update" OnClick="btnEditCust_Click" CssClass="btn btn-sm btn-light-green" />
                    <button type="button" class="btn btn-sm btn-danger" style="background-color: #eb4034" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="EmailMessage" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content" style="border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <h3>Enter Rejoining Email For Customers</h3>
                </div>
                <div class="modal-body">
                    <label>Enter Email Subject</label><br />
                    <asp:TextBox ID="TxtEmailSubject" runat="server" Style="border: 1px solid #4B4A4A;" CssClass="form-control form-group-sm" placeholder="Enter Email Subject" /><br />
                    <label>Enter Email Message</label><br />
                    <asp:TextBox ID="TxtEmailMsg" runat="server" TextMode="MultiLine" Rows="10" CssClass="form-control" placeholder="Enter Email Message here.." EnableTheming="True" />
                </div>
                <div class="modal-footer">
                    <asp:Button ID="BtnSendEmail" runat="server" Text="Send Email" CssClass="btn btn-sm btn-light-green" OnClick="BtnSendEmail_Click" />
                    <button type="button" class="btn btn-sm btn-danger" style="background-color: #eb4034" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <%----------------------------------------- Pop Up -------------------------------------------------------%>
    <style>
        .code-block {
            position: absolute;
            background-color: black;
            height: 88%;
            top: 42px;
            right: 41px;
            width: 43%;
            color: #fff;
            padding-left: 8px;
        }


        .code-Lock {
            position: absolute;
            top: 260px;
            left: -83px;
        }

        .bordercolor {
            border: 2px solid yellow;
        }
    </style>
</asp:Content>
