﻿<%@ Page Title="Design List - Quality Assurance" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="DesignListQA.aspx.cs" Inherits="MoreInline.User.DesignListQA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%-- ----------------------------------- Script --------------------------------------------- --%>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 7;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
    }
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>



    <style>
        .overlap {
            position: absolute;
            top: 0px;
        }

        .Title {
            font-size: 13px;
            color: #ffffff;
        }
    </style>

    <div class="wrapper">


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper m-l-0" style="background-color: #141414;">
            <!-- Main content -->
            <section class="content container" style="padding-bottom: 0px;">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h3 style="color: #ABC502; font-family: 'Microsoft JhengHei'"><b>Design List</b></h3>
                    </div>
                    <div class="col-lg-8"></div>

                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12 m-b-10">
                        <div id="div1" runat="server" visible="false">
                            <strong>
                                <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3 text-right m-b-20">
                        <asp:DropDownList ID="DdlDesignList" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000;"
                          OnSelectedIndexChanged="DdlDesignList_SelectedIndexChanged"  AutoPostBack="true">
                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Approved" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Not Approved" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <asp:Panel ID="PnlStep1" runat="server">

                    <asp:DataList ID="Dl_DesignList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" OnItemCommand="Dl_DesignList_ItemCommand" OnItemDataBound="Dl_DesignList_ItemDataBound">
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-lg-2">
                                            </div>
                                            <div class="col-lg-10">
                                                <span class="Title">Order Number:
                                                <asp:Label ID="LblOrderNum" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' runat="server" /></span>
                                                <asp:Label ID="HfQaApproval" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"QaApproval")%>' runat="server" />
                                                <asp:Label ID="LblQaRemarks" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"QaRemarks")%>' runat="server" />
                                                <asp:Label ID="LblAssestId" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"AssestId")%>' runat="server" />
                                                 <asp:Label ID="LblDid" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"DID")%>' runat="server" />
                                                 <asp:Label ID="LblManagerid" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"Managerid")%>' runat="server" />

                                                
                                                <asp:Label ID="LblCustApproval" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"CustApproval")%>' runat="server" />
                                                  <asp:Label ID="LblMngApproval" Visible="false" CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"MngrApproval")%>' runat="server" />
                                                <br />
                                                <span class="Title">Orderd By: 
                                                    <asp:Label CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"Customer") %>' runat="server" /></span>
                                                <br />
                                                <span class="Title">Designed By: 
                                                    <asp:Label CssClass="Title" Text='<%#DataBinder.Eval(Container.DataItem,"Designer") %>' runat="server" /></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-8 m-b-30 p-l-0 p-r-0" style="background-color: #000000;">
                                                <asp:Image ID="ImgApproved" Visible="false" runat="server" ImageUrl="~/Images/Approved.png" CssClass="img-responsive overlap" />
                                                <asp:Image ID="ImgNotApproved" Visible="false" runat="server" ImageUrl="~/Images/NOTAPPROVED.png" CssClass="img-responsive overlap" />
                                                <asp:Image ID="FilePath" runat="server" ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' CssClass="img-responsive" />

                                            </div>
                                            <div class="col-lg-2">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-8 p-b-20 text-center">
                                                <asp:Button ID="BtnDownload" runat="server" Text="Download" CommandName="Download" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' CssClass="btn btn-sm btn-green m-t-5" />
                                                <%--<button type="button" id="btnUpload" style="width: 100px; height: 43px;" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal" onclick="SetId('<%#DataBinder.Eval(Container.DataItem,"AssestID") %>','<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>',0)">Not Approved </button>
                                                --%>
                                                <asp:Button ID="BtnNotApproved" runat="server" Text="Not Approved" CssClass="btn btn-sm btn-danger m-t-5" CommandName="NotApproved" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' />
                                                <asp:Button ID="BtnAccept" runat="server" Text="Send to Manager" CssClass="btn btn-sm btn-green m-t-5" CommandName="Accept" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestId") %>' />
                                                <button type="button" id="btnstatus" class="btn btn-sm btn-danger m-t-5" data-toggle="modal" data-target="#myModal2" onclick="LoadStatus('<%#DataBinder.Eval(Container.DataItem,"MngrApproval") %>','<%#DataBinder.Eval(Container.DataItem,"CustApproval") %>','<%#DataBinder.Eval(Container.DataItem,"MngrRemarks") %>','<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>')">Status</button>

                                            </div>
                                            <div class="col-lg-2"></div>

                                        </div>
                                    </td>
                                </tr>

                            </table>
                        </ItemTemplate>
                    </asp:DataList>
                    <br />
                </asp:Panel>

            </section>
        </div>

    </div>


    <%----------------------------------------- Pop Up -------------------------------------------------------%>

    <script>
        $("body").on('click', '.toggle-password2', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input2 = $("#TxtConfirmPassword");
            if (input2.attr("type") === "password") {
                input2.attr("type", "text");
            } else {
                input2.attr("type", "password");
            }
        });

        $("body").on('click', '.toggle-password1', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input1 = $("#TxtPassword");
            if (input1.attr("type") === "password") {
                input1.attr("type", "text");
            } else {
                input1.attr("type", "password");
            }

        });
    </script>

    <script>
        function SetId(id, OrderNum, Qid) {

            $("#hfid").val(id);
            $("#hfQid").val(Qid);
            document.getElementById("<%=lblCode.ClientID %>").innerHTML = OrderNum;
        }

        function LoadStatus(MngrApproval, CustApproval, MngrRemarks, CustRemarks) {
            document.getElementById("<%=txtMr.ClientID %>").innerHTML = MngrRemarks;
            document.getElementById("lblMA").innerHTML = MngrApproval;
            if (MngrRemarks == "Approved") {
                lblMA.style.color = 'green';
            } else {
                lblMA.style.color = 'red';
            }

            document.getElementById("<%=txtCr.ClientID %>").innerHTML = CustRemarks;
            document.getElementById("lblCA").innerHTML = CustApproval;
            if (CustRemarks == "Approved") {
                lblCA.style.color = 'green';
            } else {
                lblCA.style.color = 'red';
            }

        }
    </script>

    <%-- Pop Up --%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div id="div2" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">

                            <asp:HiddenField runat="server" ID="hfid" ClientIDMode="Static" />
                             <asp:HiddenField runat="server" ID="hfdid" ClientIDMode="Static" />
                             <asp:HiddenField runat="server" ID="hfmnid" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="hfQid" ClientIDMode="Static" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-3">
                            <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon">Order Number : </span>
                            <asp:Label Style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px;" class="input-group-addon" ID="lblCode" runat="server" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="Please Mention Reasons of Rejecting Design..." EnableTheming="True"></asp:TextBox>
                            </div>

                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:Button Text="Send" ID="BtnUoload" CssClass="btn btn-success btn-sm" OnClick="BtnUoload_Click" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
            </div>
        </div>
    </div>
    <%-- End --%>



    <%-- Pop Up --%>
    <div class="modal fade" id="myModal2" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog  modal-lg">
            <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <br />
                    <div class="row">
                        <div class="col-lg-6">
                            <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px; float: left;" class="input-group-addon">Manager Approval:
                                <label id="lblMA"></label>
                            </span>
                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="txtMr" TextMode="MultiLine" Enabled="false" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="" EnableTheming="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <span style="color: #fff; background-color: #141414; border-color: #141414; padding-top: 10px; float: left;" class="input-group-addon">Customer Approval:
                                <label id="lblCA"></label>
                            </span>

                            <div class="box-body border-radius-none">
                                <asp:TextBox ID="txtCr" TextMode="MultiLine" Enabled="false" runat="server" Rows="10" CssClass="form-control textarea-resize" placeholder="" EnableTheming="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <br />

                </div>
            </div>
            <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
            </div>
        </div>
    </div>
    <%-- End --%>
</asp:Content>
