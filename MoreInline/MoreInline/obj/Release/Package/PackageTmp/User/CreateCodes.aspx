﻿<%@ Page Title="Create Codes" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="CreateCodes.aspx.cs" Inherits="MoreInline.User.CreateCodes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%----------------------------------------- Scripts -------------------------------------------------------%>

    <script>
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
            } else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }
        }, seconds * 1000);
        }
    </script>
    <%----------------------------------------- Page -------------------------------------------------------%>

    <div class="container">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10 text-center">
                <h3 class="p-l-18" style="color: #007f9f;">Create/View Codes</h3>
            </div>
            <div class="col-lg-1"></div>
        </div>

        <div class="row p-t-10">
            <div class="col-lg-12">
                <div id="div1" runat="server" visible="false">
                    <strong>
                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                </div>
                <br />
            </div>

            <div class="col-lg-12 text-right">
                <asp:Button ID="BtnAdd" runat="server" Text="Add New" CssClass="btn btn-light-green" OnClick="BtnAdd_Click" />
            </div>
        </div>


        <br />
        <div class="row">
            <div class="col-lg-12" style="overflow-x: auto">
                <asp:Repeater ID="RptViewCodes" runat="server" OnItemCommand="RptViewCodes_ItemCommand">
                    <HeaderTemplate>
                        <table id="example1" class="table table-bordered" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th class="text-center text-black">SNo.</th>
                                    <th class="text-center text-black">Full Code</th>
                                    <th class="text-center text-black">Validation Days</th>
                                    <%--<th class="text-center text-black">Code Status</th>--%>
                                    <th class="text-center text-black">Action</th>
                                </tr>
                                <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <%--CodeID,CodeNumber,CodeText,FullCode,ValidationDays--%>
                            <td class="text-center">
                                <asp:Label ID="LblSNo" runat="server" Text='<%# Container.ItemIndex + 1 %>' />
                                <asp:Label ID="LblCodeID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodeID") %>' Visible="false" />
                                <asp:Label ID="LblCodeText" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodeText") %>' Visible="false" />
                                <%-- <asp:Label ID="LblIsPaid" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"IsPaid") %>' Visible="false" />--%>
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblFullCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FullCode") %>' />
                            </td>
                            <td class="text-center">
                                <asp:Label ID="LblValidationDays" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ValidationDays") %>' />
                            </td>
                            <%--<td class="text-center text-white">
                                <asp:Label ID="LblCodeStatus" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CodeStatus") %>' />
                            </td>--%>
                            <td class="text-center">
                                <asp:ImageButton ID="TmgEdit" runat="server" ImageUrl="~/Images/MoreInline Edit Icon.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"CodeID") %>' CommandName="Edit" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody></table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>



    <%----------------------------------------- Pop Up -------------------------------------------------------%>


    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content" border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="color: #019c7c;">Create Codes</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div id="div2" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                            </div>
                            <br />
                        </div>
                        <div class="col-lg-3"></div>
                        <div class="col-lg-7">
                            <strong><asp:Label ID="LblCodeName" runat="server"></asp:Label></strong>
                            <asp:DropDownList ID="DdlCodeType" runat="server" CssClass="form-control m-t-10" Style="background-color: #007f9f; color: #fff; width: 180px;"
                                OnSelectedIndexChanged="DdlCodeType_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                            <asp:TextBox ID="TxtFullCode" runat="server" MaxLength="3" CssClass="form-control m-t-10" placeholder="Full Code" Enabled="false"></asp:TextBox>
                            <div class="row">
                                <div class="col-lg-7">
                                    <asp:TextBox ID="TxtValidDays" runat="server"
                                        onpaste="return false;" onkeypress="return isNumberKey(event)" CssClass="form-control m-t-10" placeholder="Duration in Days"></asp:TextBox>
                                </div>
                                <div class="col-lg-5">
                                    <%--<asp:DropDownList ID="DdlPaid" runat="server" CssClass="form-control m-t-10" Visible="false" Style="background-color: #A9C502; color: #000; width: 100px;">
                                        <asp:ListItem Value="0" Text="Is Paid"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">Close</button>
                    <asp:Button ID="BtnCreate" runat="server" Text="Create" OnClick="BtnCreate_Click" Visible="false" CssClass="btn btn-sm btn-light-green" Style="width: 90px;" />
                    <asp:Button ID="BtnSave" runat="server" Text="Save Changes" OnClick="BtnSave_Click" Visible="false" CssClass="btn btn-sm btn-light-green" Style="width: 110px;" />

                </div>
            </div>
        </div>
    </div>
</asp:Content>
