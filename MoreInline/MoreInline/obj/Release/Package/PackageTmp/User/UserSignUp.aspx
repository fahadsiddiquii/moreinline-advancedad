﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserSignUp.aspx.cs" Inherits="MoreInline.User.UserSignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>User Sign Up</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />

    <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css" />
    <!-- Morris chart -->
    <%--<link rel="stylesheet" href="bower_components/morris.js/morris.css" />--%>
    <!-- jvectormap -->
    <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css" />
    <!-- Date Picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <!-- Daterange picker -->
    <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" />

    <%-- ----------------------------------- Script --------------------------------------------- --%>

    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="../bower_components/raphael/raphael.min.js"></script>
    <script src="../bower_components/morris.js/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="../bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="../plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="../plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../bower_components/moment/min/moment.min.js"></script>
    <script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="../dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="../dist/js/demo.js"></script>


    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>
    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>
    <style>
        .btn-dark-green {
            color: #fff;
            background-color: #006332;
            font-weight: bold;
        }

            .btn-dark-green:hover, .btn-dark-green:active, .btn-dark-green.hover {
                background-color: #A9C502;
                color: #001b00;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="header-Login" style="background-color: #000;">
                <nav class="navbar navbar-static-top m-b-0">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3 p-l-0">
                                
                            </div>
                            <div class="col-lg-1"></div>
                            <div class="col-lg-4">
                                <div id="div2" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg2" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-4">
                               
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper m-l-0">
                <!-- Main content -->
                <section class="content container-fluid" style="padding-bottom: 0px;">
                    <asp:Panel runat="server" Visible="false">
                        <div class="row p-t-15">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-3 text-center">
                                <h3 class="p-l-18" style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>Sign Up</b></h3>
                            </div>
                            <div class="col-lg-5"></div>
                        </div>
                        <div class="row p-t-15">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-3">
                                <div id="div1" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-5"></div>
                        </div>
                        <div class="row p-t-15">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-3 form-group-sm">
                                <label class="m-b-0 m-t-5" style="color: #707070; margin-bottom: 2px;">Upload Picture</label>
                                <asp:FileUpload ID="Upload" runat="server" />
                                <asp:DropDownList ID="DdlUserType" runat="server" CssClass="form-control m-t-10" Style="background-color: #A9C502; color: #000;"></asp:DropDownList>
                                <asp:TextBox ID="TxtFName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="First Name"></asp:TextBox>
                                <asp:TextBox ID="TxtLName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Last Name"></asp:TextBox>
                                <asp:TextBox ID="TxtUEmail" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email"></asp:TextBox>
                                <asp:TextBox ID="TxtUPass" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" type="password" placeholder="Password"></asp:TextBox>
                                <asp:TextBox ID="TxtUCPassword" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" type="password" placeholder="Confirm Password"></asp:TextBox>
                                <asp:TextBox ID="TxtPhone" onpast="return isNumberKey(event)" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Phone Number"></asp:TextBox>
                                <asp:TextBox ID="TxtAddress" runat="server" onkeydown="return checkShortcut();" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Address"></asp:TextBox>
                                <div class="row m-t-18 m-b-5">
                                    <div class="col-lg-7"></div>
                                    <div class="col-lg-5 text-right">
                                        <asp:Button ID="BtnSignUp" runat="server" Text="Sign Up" CssClass="btn btn-sm btn-green" Style="width: 90px;" OnClick="BtnSignUp_Click" />
                                    </div>
                                </div>
                            </div>
                    </asp:Panel>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4"></div>
                        <div class="col-lg-4 col-md-4 col-sm-4 text-center" style="background-color: #000;margin:10px;">
                            <img src="../Images/moreinline_logo%20Green.png" style="width:230px;padding-top:15px;" />
                            <br />
                            <br />
                            <asp:TextBox ID="TxtEmail" runat="server" CssClass="form-control" placeholder="Email" autofocus></asp:TextBox><br />
                            <asp:TextBox ID="TxtPassword" runat="server" CssClass="form-control" type="password" placeholder="Password"></asp:TextBox><br />
                            <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password"></span>
                            <div class="text-right">
                                <asp:Button ID="BtnLogin" runat="server" Text="Login" OnClick="BtnLogin_Click" CssClass="btn btn-sm btn-dark-green m-b-10" />
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4"></div>
                    </div>
                </section>

            </div>
        </div>
    </form>

    <script>
        $("body").on('click', '.toggle-password', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input = $("#TxtPassword");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
    </script>

</body>
</html>
