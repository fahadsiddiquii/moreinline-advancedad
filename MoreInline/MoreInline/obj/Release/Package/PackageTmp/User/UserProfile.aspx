﻿<%@ Page Title="User Profile" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="UserProfile.aspx.cs" Inherits="MoreInline.User.UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
     <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                }<%-- else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>
    <%-- --------------------------------------------------------------------------------------------------- --%>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper m-l-0">
        <!-- Main content -->
        <section class="content container-fluid" style="padding-bottom: 0px;">
            <div class="row p-t-15">
                <div class="col-lg-4"></div>
                <div class="col-lg-3 text-center">
                    <h3 class="p-l-18" style="color: #019c7c; font-family: 'Microsoft JhengHei'"><b>User Profile</b></h3>
                </div>
                <div class="col-lg-5"></div>
            </div>
            <div class="row p-t-15">
                <div class="col-lg-4"></div>
                <div class="col-lg-3">
                    <div id="div1" runat="server" visible="false">
                        <strong>
                            <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                    </div>
                </div>
                <div class="col-lg-5"></div>
            </div>
            <div class="row p-t-15">
                <div class="col-lg-2"></div>
                <div class="col-lg-2">
                    <asp:Image ID="ImgProfile" runat="server" CssClass="rounded-image" />
                </div>
                <div class="col-lg-3 form-group-sm">
                    <label class="m-b-0 m-t-5" style="margin-bottom: 2px;">Upload Picture</label>
                    <asp:FileUpload ID="Upload" runat="server" CssClass="txtBox-style" Enabled="false" />
                    <asp:DropDownList ID="DdlUserType" runat="server" CssClass="form-control m-t-10" Enabled="false" Style="background-color: #007f9f; color: #fff;"></asp:DropDownList>
                    <asp:TextBox ID="TxtFName" runat="server"  CssClass="form-control m-t-10" Enabled="false" placeholder="First Name"></asp:TextBox>
                    <asp:TextBox ID="TxtLName" runat="server"  CssClass="form-control m-t-10" Enabled="false" placeholder="Last Name"></asp:TextBox>
                    <asp:TextBox ID="TxtUEmail" runat="server"  CssClass="form-control m-t-10" Enabled="false" placeholder="Email"></asp:TextBox>
                    <asp:TextBox ID="TxtUPass" runat="server"  CssClass="form-control m-t-10" Enabled="false" type="password" placeholder="Password"></asp:TextBox>
                    <asp:TextBox ID="TxtPhone" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server"  CssClass="form-control m-t-10" Enabled="false" placeholder="Phone Number"></asp:TextBox>
                    <asp:TextBox ID="TxtAddress" runat="server" onkeydown="return checkShortcut();"  CssClass="form-control m-t-10" Enabled="false" placeholder="Address"></asp:TextBox>
                    <div class="row m-t-18 m-b-5">
                        <div class="col-lg-7"></div>
                        <div class="col-lg-5 text-right">
                            <asp:Button ID="BtnEdit" runat="server" Text="Edit" CssClass="btn btn-sm btn-light-green" Style="width: 90px;" OnClick="BtnEdit_Click" />

                            <asp:Button ID="BtnSave" runat="server" Text="Update Profile" Visible="false" CssClass="btn btn-sm btn-light-green" Style="width: 110px;" OnClick="BtnSave_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script>
        $("body").on('click', '.toggle-password', function () {
            $(this).toggleClass("fa-eye-slash fa-eye");
            var input = $("#TxtUPass");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
    </script>
</asp:Content>
