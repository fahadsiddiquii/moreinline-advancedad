﻿<%@ Page Title="Upgrade Plan" Language="C#" MasterPageFile="~/AdminMaster.Master" AutoEventWireup="true" CodeBehind="Upgrade.aspx.cs" Inherits="MoreInline.Dashboard.Upgrade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>


    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
  
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h3 style="color: #007f9f; text-transform: uppercase;"><i>Choose a Plan</i></h3>
                <div class="col-lg-5"></div>
                <div class="col-lg-2 p-t-5 borderLine"></div>
                <div class="col-lg-5"></div>
                <div class="col-lg-12 p-t-5">
                    <h4><i>Create and track smart emails for all your campaigns</i></h4>
                </div>
            </div>
        </div>
        
        <div class="row m-t-40" >
            <div class="col-lg-4 p-t-15 text-center" id="AplvDiv" runat="server" >
                <div class="left-box left-box-1">
                    <h4><b>Appointment Level</b></h4>
                    <p style="color: #727979; margin-bottom: 46px;">A great way to get your engine started.</p>
                    <h1 style="color: #ABC502; margin-bottom: 7px; line-height: 0; font-size: 50px;">$12<span style="font-size: 25px;">/year</span></h1>
                    <%--<span style="font-size: 11px; margin-left: 80px; color: #707070;">5 months minimum</span>--%>
                    <br />
                    <div class="text-left p-l-15 p-t-10 m-b-55" style="color: #707070;">
                        <p class="m-b-5">• 2 graphical enhanced files a month</p>
                        <p class="m-b-5">• 2 Proofs provided per campaign</p>
                        <p class="m-b-5">• 2 Revisions included</p>
                        <p class="m-b-5">• Access to send created mailer at your discretion</p>
                        <p>• 24/7 Support/Assistance</p>
                    </div>
                    <a href="../Plans/RegularPackage.aspx?AccType=2" class="btn btn-lg btn-light-green m-b-20">Add to Cart</a>

                    <%--<asp:Button ID="BtnAddCart99" OnClick="BtnAddCart199_Click" runat="server" Text="Add to Cart" CssClass="btn btn-lg btn-green m-b-20" />--%>
                </div>
            </div>
            <div class="col-lg-4 p-t-15 text-center" id="RegDiv" runat="server" >
                <div class="left-box left-box-1">
                    <h4><b>Regular Plan</b></h4>
                    <p style="color: #727979; margin-bottom: 46px;">A great way to get your engine started.</p>
                    <h1 style="color: #ABC502; margin-bottom: 7px; line-height: 0; font-size: 50px;">$99<span style="font-size: 25px;">/month</span></h1>
                    <span style="font-size: 11px; margin-left: 80px; color: #707070;">5 months minimum</span>
                    <div class="text-left p-l-15 p-t-10 m-b-55" style="color: #707070;">
                        <p class="m-b-5">• 2 graphical enhanced files a month</p>
                        <p class="m-b-5">• 2 Proofs provided per campaign</p>
                        <p class="m-b-5">• 2 Revisions included</p>
                        <p class="m-b-5">• Access to send created mailer at your discretion</p>
                        <p>• 24/7 Support/Assistance</p>
                    </div>
                    <a href="../Plans/RegularPackage.aspx?AccType=3" class="btn btn-lg btn-light-green m-b-20">Add to Cart</a>

                    <%--<asp:Button ID="BtnAddCart99" OnClick="BtnAddCart199_Click" runat="server" Text="Add to Cart" CssClass="btn btn-lg btn-green m-b-20" />--%>
                </div>
            </div>
            <div class="col-lg-4 p-t-15 m-b-5 text-center" id="PrmDiv" runat="server" >
                <div class="left-box left-box-1">
                    <h4><b>Premium Plan</b></h4>
                    <p style="color: #727979; margin-bottom: 46px;">A great way to stay on the super highway.</p>
                    <h1 style="color: #ABC502; margin-bottom: 7px; line-height: 0; font-size: 50px;">$199<span style="font-size: 25px;">/month</span></h1>
                    <span style="font-size: 11px; margin-left: 106px; color: #707070;">3 months minimum</span>
                    <div class="text-left p-l-15 p-t-10 m-b-30" style="color: #707070;">
                        <p class="m-b-5">• Supplied High-end images</p>
                        <p class="m-b-5">• 4 graphical enhanced files a month</p>
                        <p class="m-b-5">• 3 Proofs provided per campaign</p>
                        <p class="m-b-5">• 3 Revisions included</p>
                        <p class="m-b-5">• Access to send created mailer at your discretion</p>
                        <p>• 24/7 Support/Assistance</p>
                    </div>
                    <a href="../Plans/RegularPackage.aspx?AccType=4" class="btn btn-lg btn-light-green m-b-20">Add to Cart</a>
                     <%--<asp:Button ID="BtnAddCart199" runat="server" OnClick="BtnAddCart199_Click" Text="Add to Cart" CssClass="btn btn-lg btn-green m-b-20" />--%>
                </div>
            </div>
        </div>
        <div class="m-t-40 m-b-20 text-center"><a href="../Dashboard/Dashboard.aspx" class="btn btn-primary">Back</a></div>
    </div>


    <%----------------------------------------- Page End -------------------------------------------------------%>
</asp:Content>
