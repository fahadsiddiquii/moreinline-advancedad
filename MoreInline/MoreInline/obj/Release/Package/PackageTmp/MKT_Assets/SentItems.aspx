﻿<%@ Page Title="Appointment Calendar" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="SentItems.aspx.cs" Inherits="MoreInline.MKT_Assets.SentItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>

    <!-- DataTables -->
    <script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        function openModal1() {
            $('#myModal1').modal('show');
        }
    </script>
    <style>
        .body {
            background-color: #fafafa;
        }

        .card {
            background: #fff;
            border-radius: 5px;
            /*border:3px solid #A9C502;*/
            display: inline-block;
            height: 150px;
            margin: 1rem;
            position: relative;
            width: 200px;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

            .card-1:hover {
                box-shadow: 0 14px 28px #808080, 0 10px 10px #808080;
            }

        #ad-div {
            border-radius: 17px;
            background: #FFC77A;
            padding: 3px;
            width: 85px;
            height: 52px;
        }

        .ad-div-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

            .ad-div-1:hover {
                box-shadow: 0 14px 28px #808080, 0 10px 10px #808080;
            }
    </style>
    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="text-center m-b-20">
        <h1 style="color: #001b00; font-family: 'Microsoft JhengHei';"><b>Appointments Calendar</b></h1>
    </div>
    <br />
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1" runat="server">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="row">
        <div class="col-lg-2">
            <h4>Search Appointments By</h4>
        </div>
        <div class="col-lg-8 m-t-10">
            <asp:RadioButtonList ID="RdSearch" runat="server" OnSelectedIndexChanged="RdSearch_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
                <asp:ListItem class="p-r-15" Value="0" Text=" All" />
                <asp:ListItem class="p-r-15" Value="1" Text=" Day" />
                <asp:ListItem class="p-r-15" Value="2" Text=" Month" />
                <asp:ListItem Value="3" Text=" Year" />
            </asp:RadioButtonList>
        </div>
        <div class="col-lg-3 m-t-5">
        </div>
        <div class="col-lg-8 m-t-5">
        </div>

    </div>

    <asp:Panel ID="PnlDay" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-3">
                <asp:DropDownList ID="DdlDay" runat="server" CssClass="form-control" style="background-color: #007f9f; color: #fff;" OnSelectedIndexChanged="DdlDay_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Text="- Select Day -" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Monday" Value="Monday"></asp:ListItem>
                    <asp:ListItem Text="Tuesday" Value="Tuesday"></asp:ListItem>
                    <asp:ListItem Text="Wednesday" Value="Wednesday"></asp:ListItem>
                    <asp:ListItem Text="Thursday" Value="Thursday"></asp:ListItem>
                    <asp:ListItem Text="Friday" Value="Friday"></asp:ListItem>
                    <asp:ListItem Text="Saturday" Value="Saturday"></asp:ListItem>
                    <asp:ListItem Text="Sunday" Value="Sunday"></asp:ListItem>
                </asp:DropDownList>
                
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlMonth" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-3">
                <asp:DropDownList ID="DdlMonth" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;" OnSelectedIndexChanged="DdlMonth_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Text="- Select Month -" Value="0"></asp:ListItem>
                    <asp:ListItem Text="January" Value="January"></asp:ListItem>
                    <asp:ListItem Text="February" Value="February"></asp:ListItem>
                    <asp:ListItem Text="March" Value="March"></asp:ListItem>
                    <asp:ListItem Text="April" Value="April"></asp:ListItem>
                    <asp:ListItem Text="May" Value="May"></asp:ListItem>
                    <asp:ListItem Text="June" Value="June"></asp:ListItem>
                    <asp:ListItem Text="July" Value="July"></asp:ListItem>
                    <asp:ListItem Text="August" Value="August"></asp:ListItem>
                    <asp:ListItem Text="September" Value="September"></asp:ListItem>
                    <asp:ListItem Text="October" Value="October"></asp:ListItem>
                    <asp:ListItem Text="November" Value="November"></asp:ListItem>
                    <asp:ListItem Text="December" Value="December"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="PnlYear" runat="server" Visible="false">
        <div class="row">
            <div class="col-lg-3">
                <asp:DropDownList ID="DdlYear" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;" OnSelectedIndexChanged="DdlYear_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Text="- Select Year -" Value="0"></asp:ListItem>
                    <asp:ListItem Text="2020" Value="2020"></asp:ListItem>
                    <asp:ListItem Text="2021" Value="2021"></asp:ListItem>
                    <asp:ListItem Text="2022" Value="2022"></asp:ListItem>
                    <asp:ListItem Text="2023" Value="2023"></asp:ListItem>
                    <asp:ListItem Text="2024" Value="2024"></asp:ListItem>
                    <asp:ListItem Text="2025" Value="2025"></asp:ListItem>
                    <asp:ListItem Text="2026" Value="2026"></asp:ListItem>
                    <asp:ListItem Text="2027" Value="2027"></asp:ListItem>
                    <asp:ListItem Text="2028" Value="2028"></asp:ListItem>
                    <asp:ListItem Text="2029" Value="2029"></asp:ListItem>
                    <asp:ListItem Text="2030" Value="2030"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </asp:Panel>

    <br />

    <center>
     <div style="width:90%">
    <div class="row">
        <div class="body col-lg-12 col-sm-12 col-md-12">
        <asp:repeater ID="rptInvoices" runat="server" OnItemCommand="rptInvoices_ItemCommand">
           <itemtemplate> 
            <div  class="card card-1">
                <div id='<%#DataBinder.Eval(Container.DataItem,"Date") %>'>
                    <h4><%#DataBinder.Eval(Container.DataItem,"Month") %> - <%#DataBinder.Eval(Container.DataItem,"Day") %> - <%#DataBinder.Eval(Container.DataItem,"Year") %> </h4>
                    <h4><%#DataBinder.Eval(Container.DataItem,"Day Name") %> </h4>
                </div>
                <div id="ad-div" class="ad-div-1">
                    <asp:linkbutton text='<%#DataBinder.Eval(Container.DataItem,"Ads") %>' runat="server" style="font-size:xx-large;color:#2C5F2D" id="btnDetails" CommandName="Details" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Date") %>' />
                 
                </div>
            </div>
           </itemtemplate>
        </asp:repeater>
        </div> 
    </div>
         </div>
        </center>

    <%----------------------------------------- Page End -------------------------------------------------------%>
    <%-- image view --%>

    <%-- end --%>
</asp:Content>
