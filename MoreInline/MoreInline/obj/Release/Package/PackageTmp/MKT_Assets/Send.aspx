﻿<%@ Page Title="Send" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="Send.aspx.cs" Inherits="MoreInline.MKT_Assets.Send" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        .modal-header .close {
            margin-top: 5px;
            margin-right: 45px;
            color: #000000;
        }

        .LabelSpan {
            background-color: #fff !important;
            border-color: #fff !important;
            padding-top: 10px;
        }

        hr {
            border-top: 1px solid black;
        }

        .carouselOfImages {
            max-width: 500px;
            margin: auto;
            overflow: visible;
        }

        .carouselImage {
            width: 100px;
            height: 100px;
            margin-top: 65px;
            margin-bottom: 100px;
            border-radius: 5px;
            counter-increment: carousel-cell;
            transition: transform 0.5s;
            transform: scale(1);
        }

            .carouselImage img {
                height: 100px;
            }

            .carouselImage.is-selected {
                z-index: 10;
                transform: scale(1.5);
            }

            .carouselImage.nextToSelected {
                transform: scale(1.25);
                z-index: 5;
            }

        .flickity-enabled {
            position: relative;
        }

            .flickity-enabled:focus {
                outline: none;
            }

        .flickity-viewport {
            overflow: hidden;
            position: relative;
            height: 100%;
        }

        .flickity-slider {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        /* draggable */

        .flickity-enabled.is-draggable {
            -webkit-tap-highlight-color: transparent;
            tap-highlight-color: transparent;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .flickity-enabled.is-draggable .flickity-viewport {
                cursor: move;
                cursor: -webkit-grab;
                cursor: grab;
            }

                .flickity-enabled.is-draggable .flickity-viewport.is-pointer-down {
                    cursor: -webkit-grabbing;
                    cursor: grabbing;
                }

        /* ---- previous/next buttons ---- */

        .flickity-prev-next-button {
            position: absolute;
            top: 50%;
            width: 44px;
            height: 44px;
            border: none;
            border-radius: 50%;
            background: white;
            background: hsla(0, 0%, 100%, 0.75);
            cursor: pointer;
            /* vertically center */
            -webkit-transform: translateY(-50%);
            transform: translateY(-50%);
        }

            .flickity-prev-next-button:hover {
                background: white;
            }

            .flickity-prev-next-button:focus {
                outline: none;
                box-shadow: 0 0 0 5px #09F;
            }

            .flickity-prev-next-button:active {
                opacity: 0.6;
            }

            .flickity-prev-next-button.previous {
                left: 10px;
            }

            .flickity-prev-next-button.next {
                right: 10px;
            }
        /* right to left */
        .flickity-rtl .flickity-prev-next-button.previous {
            left: auto;
            right: 10px;
        }

        .flickity-rtl .flickity-prev-next-button.next {
            right: auto;
            left: 10px;
        }

        .flickity-prev-next-button:disabled {
            opacity: 0.3;
            cursor: auto;
        }

        .flickity-prev-next-button svg {
            position: absolute;
            left: 20%;
            top: 20%;
            width: 60%;
            height: 60%;
        }

        .flickity-prev-next-button .arrow {
            fill: #333;
        }

        /* ---- page dots ---- */

        .flickity-page-dots {
            position: absolute;
            width: 100%;
            bottom: -25px;
            padding: 0;
            margin: 0;
            list-style: none;
            text-align: center;
            line-height: 1;
        }

        .flickity-rtl .flickity-page-dots {
            direction: rtl;
        }

        .flickity-page-dots .dot {
            display: inline-block;
            width: 10px;
            height: 10px;
            margin: 0 8px;
            background: #333;
            border-radius: 50%;
            opacity: 0.25;
            cursor: pointer;
        }

            .flickity-page-dots .dot.is-selected {
                opacity: 1;
            }

        .product-detail-close {
            position: absolute;
            top: 1rem;
            right: 1rem;
            cursor: pointer;
            font-size: 21px;
            font-weight: 700;
        }

        #lightbox {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            margin: auto;
            width: 90vw;
            height: 90vh;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
        }

        #dark {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            background: rgba(0,0,0,.9);
            width: 100vw;
            height: 100vh;
        }

        .product-detail {
            position: fixed;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            overflow-y: auto;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgba(0, 0, 0, 0.7);
        }

        .product-detail-inner {
            display: flex;
            background-color: white;
            width: 75vw;
            height: 75vh;
            position: relative;
            padding: 1rem;
            overflow-y: auto;
        }

        .product-detail-image {
            flex: 0 1 30%;
            padding: 15px;
        }

            .product-detail-image img {
                width: 100%;
            }

        .hide {
            display: none;
        }

        .modal-dialog {
            max-width: 800px;
            margin: 30px auto;
        }

        .modal-body {
            position: relative;
            padding: 0px;
        }

        .modal-header {
            padding: 0;
        }

        a.btn.btn-primary {
            color: #fff;
        }

        .close {
            position: absolute;
            right: -30px;
            top: 0;
            z-index: 999;
            font-size: 2rem;
            font-weight: normal;
            color: #fff;
            opacity: 1;
        }
    </style>


    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>

    <script type="text/javascript">
        function checkSpcialChar(event) {
            if (!((event.keyCode >= 65) && (event.keyCode <= 90) || (event.keyCode >= 97) && (event.keyCode <= 122) || (event.keyCode >= 48) && (event.keyCode <= 57))) {
                if (event.keyCode != 32) {
                    event.returnValue = false;
                    //  alert("Special characters are not allowed due to security reason.");
                    swal('Special characters Found', 'Special characters are not allowed due to security reason.', 'erorr');
                    return;
                }
            }
            event.returnValue = true;
        }
    </script>
    <script src="../dist/js/BadWords.js"></script>
    <script>
        function validate_text() {

            var compare_text = $("#TxtAdSubject").val();
            var array = compare_text.split(" ");
            let obj = {};

            for (let i = 0; i < array.length; i++) {

                if (!obj[array[i]]) {
                    const element = array[i].toLowerCase();
                    obj[element] = true;
                }
            }

            for (let j = 0; j < swear_words_arr.length; j++) {

                if (obj[swear_words_arr[j]]) {
                    $("#TxtAdSubject").val('');
                    swal('Invalid words found', "The message will not be sent!!!\nThe following unethical words are found:\n\n" + swear_words_arr[j] + "\n", 'info');
                }
            }

        }
    </script>

    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1">
                <strong>
                    <label id="LblMsg"></label>
                </strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
        <br />
    </div>
  
    <div class="row">
        <div class="col-lg-6">
    
    <h3>1. Select Image To Send</h3>
    <img id="loading-imageDirect" style="position: absolute; top: 38%; left: 49%; z-index: 999;" src="../Images/Loader.gif" alt="Loader Image" />
    <section class="items">
        <div id="adsContainer" class="carouselOfImages">

        </div>
    </section>

    <div id="dark"></div>
    <div id="lightbox"></div>
    <div aria-labelledby="modal-1-label" class="modal fade modal-media modal-video modal-slim modal-1" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true">X</span></button>
                </div>

                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe allowfullscreen="" frameborder="0" src="" width="50%"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br />
    <hr />
    <div class="row">
        <div class="col-lg-12">
            <h3>2. Select Options</h3>
        </div>
        <div class="col-lg-6">
            
            <asp:RadioButton ID="rdbSavedInfo" Text="Select saved information" GroupName="Sendopt"  runat="server" OnCheckedChanged="rdbSavedInfo_CheckedChanged" AutoPostBack="true" />
            
           <%-- <button type="button" id="btnDirectSend01" onclick="DirectSend()" class="btn btn-sm btn-light-green m-t-20">Send With Saved Information</button>--%>
        </div>
        <div class="col-lg-6">
            <asp:RadioButton ID="rdbNewInfo" Text="Select new information" GroupName="Sendopt" runat="server" OnCheckedChanged="rdbNewInfo_CheckedChanged" AutoPostBack="true"  />
          <%--  <input type="radio" id="rdbNewInfos" name="ctl00$ContentPlaceHolder1$Sendopt"><label>Select new information</label></input>--%>
          <%--  <button type="button" class="btn btn-sm btn-light-green m-t-20" data-toggle="modal" data-target="#myModal">Send With New Information</button>--%>
        </div>
        <div class="col-lg-12 m-b-10"></div>
    </div>
    <hr />

    
            <div class="row">
       
        <div class="col-md-12">
            <h3>3. Select Your Contact List Or Enter Single Email Address</h3>
        </div>
        <div class="col-md-2 m-t-5">
            <button type="button" onclick="LoadContactList()" class="btn btn-sm btn-light-green" data-toggle="modal" data-target="#contactModal">Select Contact</button>
        </div>
        <%--    <div class="col-md-1 m-t-5"><b>Or</b></div>--%>
        <div class="col-md-3 m-t-5">
            <%--   <input id="semail" type="text" hidden="hidden" class="form-control" placeholder="Enter Email Address" />--%>
        </div>
        <div class="col-md-6"></div>
        <div class="col-md-12">
            <br />
        </div>
    </div>
    <hr />
    <asp:Panel ID="Panel1" runat="server" Visible="false">
         <div class="row">
        <div class="col-lg-12">
            <h3>4. Customization</h3>
        </div>
        <div class="col-lg-12">
           <div class="row" style="padding-left: 10px">
                        <div class="col-lg-12">
                            <h2 style="color: #007f9f;">Customize your Subject Line</h2>
                            <div class="row m-b-10">
                                <div class="col-lg-12 m-t-5">
                                    <label>If you do not enter any information here, the subject line will be used from company information page.</label>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group m-t-5">
                                        <span class="input-group-addon LabelSpan">Ad Subject:</span>
                                        <input id="TxtAdSubject" class="form-control m-t-10" placeholder="Enter message title" onkeypress="return checkSpcialChar(event)" type="text" name="TxtAdSubject" value="" onchange="validate_text()" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <h2 style="color: #007f9f;">Select options below to send with your email</h2>
                            <div class="row">
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddContact" type="checkbox"  style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/email_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD CONTACT</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddDirection" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/directions_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 0;">ADD DIRECTIONS</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="chkAddUrl" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/web_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 10px 0 0 4px;">ADD WEBSITE</span>
                                    </span>
                                </div>

                            </div>
                            <hr />

                            <h2 style="color: #007f9f;">Modify company Voice Message / Add Reminder time</h2>
                            <div class="row">
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddReminder" onchange="ShowTimeOps()" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/reminder_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD REMINDER</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddVoice" type="checkbox" onchange="ShowAudioOps()" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/voice-message_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px;" class="text-right test-11">ADD VOICE MESSAGE</span>
                                    </span>
                                </div>
                            </div>
                            <div class="row">


                                <div id="divaudio" class="col-lg-12 m-t-5" hidden="hidden">
                                    <label class="radio-inline">
                                        <input type="radio" id="addgenric" onchange="HideVoiceOps()" name="optradio" value="0" />Select Old Voice
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" id="shwadd" onchange="ShowAddvoice()" name="optradio" value="1" />Add New Voice
                                    </label>
                                    <%--   <label class="radio-inline">
                                        <input id="shwupload"  onchange="Showupload()" type="radio" name="optradio" value="2" />Upload Voice MessageAdd Generic Voice
                                    </label>--%>
                                </div>

                            </div>
                              <div id="pnlOldVoice" hidden="hidden">
                                  <asp:DropDownList ID="ddlOldVoice" runat="server"></asp:DropDownList>
                                  <br />
                                  <audio id="player" controls="controls">
                                      <source id="mp3_src" src="/teachings/2011_01_09_Cut.mp3" type="audio/mp3" />Your browser does not support the audio element.
                                  </audio>
                            </div>
                            <div id="pnlUploadVoice" hidden="hidden">
                                <input type="file" id="fuvoice" name="fuvoice">
                            </div>
                            <div id="PnlRecordVoice" hidden="hidden">
                                <div class="row">
                                    <div class="col-lg-12 m-b-10">
                                        <h2 style="color: #007f9f;">Record a personal message for this Ad</h2>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-8">
                                        <div class="box-footer no-border" style="background-color: #DFDFDF;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img src="../Images/Icons/mic.png" alt="Mic" id="record" class="img-circle test-10" style="background-color: #A9C502; float: left;" />
                                                    <img src="../Images/Icons/mic.gif" alt="Mic" id="pause" hidden class="img-circle" style="background-color: #A9C502; float: left;" />

                                                    <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>

                                                    <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                                                    <img id="stop" src="../Images/Icons/delete_voice.jpg" title="Delete" alt="delete" hidden class="img-circle" style="background-float: right;" />
                                                    <img id="save" src="../Images/Icons/upload_voice.jpg" title="Upload" alt="upload" hidden class="img-circle" style="background-float: right;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </div>
                            <div id="PnlTime" hidden="hidden">
                                <div class="row">
                                    <div class="col-lg-12 m-b-10">
                                        <h2 style="color: #007f9f;">Appointment Time</h2>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span style="border-color: #ffffff; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</span>
                                            <input type="date" id="datepicker" class="datepicker-inline" name="name" value="" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 130px; height: 34px; margin-top: 5px;" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                        <div class="input-group">
                                            <span style="border-color: #ffffff; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time:</span>
                                            <select id="DdlHour" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 125px;" class="form-control m-t-5">
                                                <option value="0">Select Time</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <select id="DdlTT" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 70px;" class="form-control m-t-5 m-l-5 test-5">
                                                <option value="0">pm</option>
                                                <option value="1">am</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 style="color: #007f9f;">Copy that will be sent with Ad</h2>
                                </div>
                                <div class="col-lg-12 m-t-5">
                                    <label>If you do not enter any information here, the message will be used from company information page.</label>
                                </div>
                                <div class="col-lg-8 m-t-5">
                                    <textarea id="TxtMessage" rows="10" class="form-control textarea-resize" placeholder="Enter Message here.." style="resize: none;" enabletheming="True"></textarea>

                                </div>
                                <div class="col-lg-12">
                                    
                                    <input type="button" id="BtnSend" class="btn btn-lg btn-light-green pull-right m-r-5" onclick="SendWithMod()" style="width: 85px;" value="Send" />
                                </div>
                            </div>
                            <hr />


                        </div>

                    </div>
                    <center>
                         <img id="loading-image" style="position:absolute;top:50%;left:33%;z-index: 999;" src="../Images/Loader.gif" alt="Loader Image" />
                    </center>
        </div>
        <div class="col-lg-12 m-b-10"></div>
    </div>
    </asp:Panel>
    <asp:Panel ID="pnlSavedSend" runat="server" Visible="false">
          <div class="row">
        <div class="col-lg-12">
            <h3>4. Send Email</h3>
        </div>
        <div class="col-lg-6">
           
            <button type="button" id="btnDirectSend" onclick="DirectSend()" class="btn btn-sm btn-light-green m-t-20">Send With Saved Information</button>
        </div>
        
        <div class="col-lg-12 m-b-10"></div>
    </div>
    <hr />

    </asp:Panel>

</div>
        <div>
            <h3 style="text-align: center">Preview</h3>
            <br />
        </div>
         <div class="col-lg-6">
             <div bgcolor="#f2f2f2" style="margin:0 auto;padding:0;width:100% !important;">
        <!-- big image section -->
        <table border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#f2f2f2" style="table-layout:fixed;" width="100%">
            <tbody>
                <tr>
                    <td align="center">
                        <table border="0" width="600px" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="background-color:#fff;margin:0 auto;max-width:600px;width:inherit;">
                          
                            <tr>
                                <td align="center" class="section-img" style="padding-top:5px; background-color:#000; height:80px" >
                                    <%--<img src="{CompanyLogo}" style="display: block; width: 150px;" border="0" alt="Logo" />--%>
                                    <img src="../Images/moreinline_logo.png" style="display: block; width: 150px;" border="0" alt="Logo" />
                                </td>
                            </tr> 
                              <tr>
                               <td align="center" class="section-img imageAdds" style="padding:15px;">
                                    <%--<img src="{image}" style="display: block; width: 600px;" border="0" alt="Appointment Ad" />--%>
                                     <asp:Image ID="imgadd" ImageUrl="#" AlternateText="Appointment Ad" class="img-responsive" runat="server" />
                                </td>
                            </tr> 
                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">
                                    <table border="0" width="40" align="center" cellpadding="0" cellspacing="0" bgcolor="eeeeee">
                                        <tr>
                                            <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="center">
                                    <table border="0" width="650" align="center" cellpadding="0" cellspacing="0" class="container590">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="color: #888888; font-size: 16px;  line-height: 24px;">


                                                    <div style="line-height: 24px">
                                                        <p id="messText" style="padding-left:5px;padding-right:5px;">
                                                            <asp:Label ID="lblMailMsg" runat="server" Text=""></asp:Label>
                                                            <%--{message}--%>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                            </tr>

                            <tr>
                                <td id="VoiceBox"  align="center" style="display:none">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px; background-color:#A9C500"">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height" style="padding:5px 15px 5px 15px">
                                                        <a href="{voice}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/voice-message1.png" /></span><span style="vertical-align:top;padding-left:3px;">LISTEN VOICE MESSAGE</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                                <%--{reminderVisible}--%>
                                <td id="ReminderBox" align="center" style="display:none;">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px; background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height" style="padding:5px 15px 5px 15px">
                                                        <a href="{reminder}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/reminder1.png" /></span><span style="vertical-align:top;padding-left:3px;">SAVE REMINDER</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                               <%-- {directionVisible}--%>
                                <td id="DirectBox" align="center" style="display:none;">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px; background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height"  style="padding:5px 15px 5px 15px">
                                                        <a href="{direction}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/directions1.png" /></span><span style="vertical-align:top;padding-left:3px;">DIRECTIONS</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px">
                                <td id="ContactBox" align="center" style="display:none">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px;background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">


                                                    <div class="button-line-height"  style="padding:5px 15px 5px 15px">
                                                        <a href="tel:{contact}" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/email1.png" /></span><span style="vertical-align:top;padding-left:3px;">CONTACT</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="margin-bottom:6px;">
                                <%--{webVisible}--%>
                                <td id="WebBox" align="center" style="display: none;">
                                    <table border="0" align="center" class="button" cellpadding="0" cellspacing="0" bgcolor="A9C500" style="margin-bottom:6px;background-color:#A9C500">
                                        <tbody>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center" style="color: #ffffff; font-size: 14px;  line-height: 26px;">
                                                    <div class="button-line-height" style="padding:5px 15px 5px 15px">
                                                        <a href="{web}" target="_blank" style="color: #ffffff; text-decoration: none;"><span><img src="https://www.moreinline.com/Images/Icons/web1.png" /></span><span style="vertical-align:top;padding-left:3px;">VISIT US</span></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="25" style="font-size: 15px; line-height: 25px;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="center" style="font-family: sans-serif; line-height: 20px;" bgcolor="#a9c502" width="100%">
                                    <br />
                                    <font color="black">
                                        <p id="locText" style="color: #000000;">
                                            <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label><br />
                                        </p>
                                        <p style="{Fishing}">
                                           <span id="custweb"></span><asp:Label ID="lblcustweb" runat="server" Text=""></asp:Label> | <span id="custemail">
                                                <asp:Label ID="lblcustemail" runat="server" Text=""></asp:Label></span>
                                            <br />
                                            <a href="{unsubLink}" style="{unsubStyle}">unsubscribe</a>
                                        </p>
                                        <br />
                                    </font>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>






    </div>
             </div>

        </div>
    <%--<div class="row" hidden="hidden">
        <div class="col-md-12">
            <h3>4. Preview And Send</h3>
        </div>

        <div class="col-md-12">
            <br />
        </div>
    </div>--%>
   

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../js/SweetAlert.js"></script>

    <script src="../js/flickity.min.js"></script>
    <script src="../js/Send.js?v=2"></script>
    <%----------------------------------------- Pop up -------------------------------------------------------%>

    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-left: 10px">
                        <div class="col-lg-12">
                            <h2 style="color: #007f9f;">Customize your Subject Line</h2>
                            <div class="row m-b-10">
                                <div class="col-lg-12 m-t-5">
                                    <label>If you do not enter any information here, the subject line will be used from company information page.</label>
                                </div>
                                <div class="col-lg-6">
                                    <div class="input-group m-t-5">
                                        <span class="input-group-addon LabelSpan">Ad Subject:</span>
                                        <input id="TxtAdSubject" class="form-control m-t-10" placeholder="Enter message title" onkeypress="return checkSpcialChar(event)" type="text" name="TxtAdSubject" value="" onchange="validate_text()" />
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <h2 style="color: #007f9f;">Select options below to send with your email</h2>
                            <div class="row">
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddContact" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/email_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD CONTACT</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddDirection" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/directions_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 0;">ADD DIRECTIONS</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="chkAddUrl" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/web_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 10px 0 0 4px;">ADD WEBSITE</span>
                                    </span>
                                </div>

                            </div>
                            <hr />

                            <h2 style="color: #007f9f;">Modify company Voice Message / Add Reminder time</h2>
                            <div class="row">
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddReminder" onchange="ShowTimeOps()" type="checkbox" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/reminder_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px; padding: 7px 0 0 4px;">ADD REMINDER</span>
                                    </span>
                                </div>
                                <div class="col-lg-3 m-t-5">
                                    <span>
                                        <input id="ChkAddVoice" type="checkbox" onchange="ShowAudioOps()" style="float: left; margin-top: 5px;" />
                                        <img src="../Images/Icons/voice-message_black.png" style="padding-left: 2px;" />
                                        <span style="font-size: 10px;" class="text-right test-11">ADD VOICE MESSAGE</span>
                                    </span>
                                </div>
                            </div>
                            <div class="row">


                                <div id="divaudio" class="col-lg-12 m-t-5" hidden="hidden">
                                    <label class="radio-inline">
                                        <input type="radio" id="addgenric" onchange="HideVoiceOps()" name="optradio" value="0" />Add Generic Voice
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" id="shwadd" onchange="ShowAddvoice()" name="optradio" value="1" />Add New Voice
                                    </label>
                                    <%--   <label class="radio-inline">
                                        <input id="shwupload"  onchange="Showupload()" type="radio" name="optradio" value="2" />Upload Voice Message
                                    </label>--%>
                                </div>

                            </div>
                         



                            <div id="pnlUploadVoice" hidden="hidden">
                                <input type="file" id="fuvoice" name="fuvoice">
                            </div>
                            <div id="PnlRecordVoice" hidden="hidden">
                                <div class="row">
                                    <div class="col-lg-12 m-b-10">
                                        <h2 style="color: #007f9f;">Record a personal message for this Ad</h2>
                                    </div>
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-8">
                                        <div class="box-footer no-border" style="background-color: #DFDFDF;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img src="../Images/Icons/mic.png" alt="Mic" id="record" class="img-circle test-10" style="background-color: #A9C502; float: left;" />
                                                    <img src="../Images/Icons/mic.gif" alt="Mic" id="pause" hidden class="img-circle" style="background-color: #A9C502; float: left;" />

                                                    <span id="basicUsage" style="padding: 5px 0 0 10px; color: #828282; vertical-align: -webkit-baseline-middle;">Record Accompaning Voice Message For Your Customer</span>

                                                    <audio controls id="audio" style="height: 23px; width: 277px; vertical-align: text-top;" class="pull-right;"></audio>

                                                    <img id="stop" src="../Images/Icons/delete_voice.jpg" title="Delete" alt="delete" hidden class="img-circle" style="background-float: right;" />
                                                    <img id="save" src="../Images/Icons/upload_voice.jpg" title="Upload" alt="upload" hidden class="img-circle" style="background-float: right;" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </div>
                            <div id="PnlTime" hidden="hidden">
                                <div class="row">
                                    <div class="col-lg-12 m-b-10">
                                        <h2 style="color: #007f9f;">Appointment Time</h2>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span style="border-color: #ffffff; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date:</span>
                                            <input type="date" id="datepicker" class="datepicker-inline" name="name" value="" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 130px; height: 34px; margin-top: 5px;" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6">

                                        <div class="input-group">
                                            <span style="border-color: #ffffff; padding-top: 10px;" class="input-group-addon">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time:</span>
                                            <select id="DdlHour" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 125px;" class="form-control m-t-5">
                                                <option value="0">Select Time</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <select id="DdlTT" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 70px;" class="form-control m-t-5 m-l-5 test-5">
                                                <option value="0">pm</option>
                                                <option value="1">am</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-lg-12">
                                    <h2 style="color: #007f9f;">Copy that will be sent with Ad</h2>
                                </div>
                                <div class="col-lg-12 m-t-5">
                                    <label>If you do not enter any information here, the message will be used from company information page.</label>
                                </div>
                                <div class="col-lg-8 m-t-5">
                                    <textarea id="TxtMessage" rows="10" class="form-control textarea-resize" placeholder="Enter Message here.." style="resize: none;" enabletheming="True"></textarea>

                                </div>
                                <div class="col-lg-12">
                                    <input type="button" id="BtnPreviw" class="btn btn-lg btn-light-green pull-right m-r-5" onclick="SendWithPreview()" style="width: 85px;" value="Preview" />
                                    <input type="button" id="BtnSend" class="btn btn-lg btn-light-green pull-right m-r-5" onclick="SendWithMod()" style="width: 85px;" value="Send" />
                                </div>
                            </div>
                            <hr />


                        </div>

                    </div>
                    <center>
                         <img id="loading-image" style="position:absolute;top:50%;left:33%;z-index: 999;" src="../Images/Loader.gif" alt="Loader Image" />
                    </center>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="contactModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <button type="button" class="pull-right" onclick="Confirm()">x</button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <h3>Contact List</h3>
                    </div>
                    <select id="DdlContact" onchange="GetList()" style="border: 1px solid #4B4A4A; background-color: #007f9f; color: #fff; width: 250px;" class="form-control m-t-5 m-l-5 test-5">
                        <option value="0">Select Contact List</option>
                        <option value="1">#</option>
                    </select>
                    <br />
                    <input id="search" type="text" onkeyup="GetList()" class="form-control m-l-5" style="border: 1px solid #4B4A4A; width: 250px;" placeholder="Search by Name" />
                    <br />
                    <div class="m-l-5 m-r-5" style="overflow-x:auto;">
                        <table class="table table-bordered table-sm" id="tbllist" border="0">
                            <thead class="table-head">
                                <tr style="background-color: #A9C502">
                                    <th>
                                        <input id="checkAll" onchange="checkAll()" type="checkbox" disabled="disabled" style="width: 18px; height: 18px;" name="checkAll" value="true" />

                                    </th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Title</th>
                                    <th>Company</th>
                                    <th>Subscription</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <br />
                    <input id="btnGet" type="button" data-dismiss="modal" value="Confirm" class="btn btn-sm btn-light-green m-l-5 m-b-5" />
                    <br />

                </div>
            </div>
        </div>
    </div>


    <%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>--%>
    <script>
        $(document).ready(function () {

            var ddlOldVoice = '<%= ddlOldVoice.ClientID  %>'
            $('#ContentPlaceHolder1_ddlOldVoice').on('change', function () {
                var value = $("#ContentPlaceHolder1_ddlOldVoice option:selected");
                //  alert(value.text().replace('~','http://localhost:65331'));
                changea(value.val().replace('~', 'http://localhost:65331'));
            });

        });


        function changea(sourceUrl) {
            var audio = document.getElementById("player");
            var source = document.getElementById("mp3_src");

            audio.pause();

            if (sourceUrl) {
                source.src = sourceUrl;
                audio.load();
                audio.play();
            }
        }
    </script>
    <script>
        $('#TxtMessage').keyup(function () {
            $('#messText').text($(this).val());
        });


        $("#ChkAddContact").change(function () {
            if (this.checked) {
                $('#ContactBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#ContactBox').attr("style", "display:none");
            }

        });

        $("#ChkAddReminder").change(function () {
            if (this.checked) {
                $('#ReminderBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#ReminderBox').attr("style", "display:none");
            }

        });

        $("#ChkAddDirection").change(function () {
            if (this.checked) {
                $('#DirectBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#DirectBox').attr("style", "display:none");
            }

        });

        $("#chkAddUrl").change(function () {
            if (this.checked) {
                $('#WebBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#WebBox').attr("style", "display:none");
            }

        });
     

        $("#ChkAddVoice").change(function () {
            if (this.checked) {
                $('#VoiceBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#VoiceBox').attr("style", "display:none");
            }

        });

        var checkID = '<%= rdbSavedInfo.ClientID  %>'
        var value = document.getElementById(checkID).checked;
        
            if (document.getElementById(checkID).checked) {
                $('#VoiceBox').attr("style", "display:none");
                $('#WebBox').attr("style", "");
                $('#DirectBox').attr("style", "");
                $('#ReminderBox').attr("style", "display:none");
                $('#ContactBox').attr("style", "");
                //Do stuff
            }
            else {
                $('#VoiceBox').attr("style", "display:none");
                $('#WebBox').attr("style", "display:none");
                $('#DirectBox').attr("style", "display:none");
                $('#ReminderBox').attr("style", "display:none");
                $('#ContactBox').attr("style", "display:none");
            }

        
        


        


    </script> 

    <script>
       


    </script>



</asp:Content>
