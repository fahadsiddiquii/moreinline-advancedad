﻿<%@ Page Title="View Assets" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="ViewAssets.aspx.cs" Inherits="MoreInline.MKT_Assets.ViewAssets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <%----------------------------------------- Script -------------------------------------------------------%>


    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>
    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <script type="text/javascript">
        function OrderStatus() {
            $('#OrderStatus').modal('show');
        }
    </script>
    
    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
          <script type="text/javascript">
        function openStripe() {
            debugger;
            $('#StripeModel').modal('show');
        }
    </script>
    
    <style>
        .span {
            color: #001b00;
            font-weight: bold;
        }

        .p-t-10 {
            padding-top: 10px;
        }

        .p-b-10 {
            padding-bottom: 10px;
        }

        .p-t-20 {
            padding-top: 20px;
        }

        .m-l-0 {
            margin-left: 0;
        }

        .m-r-0 {
            margin-right: 0;
        }

        .Social-Icon-Width {
            width: 5%;
        }

        .container1 {
            position: relative;
            width: 280px;
        }

        .image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            opacity: 0;
            transition: .3s ease;
            background-color: #4b4f4c;
        }

        .container1:hover .overlay {
            opacity: 0.8;
        }

        .icon {
            color: white;
            font-size: 100px;
            position: absolute;
            top: 48%;
            left: 50%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .card {
            background: #fff;
            border-radius: 2px;
            /*border:3px solid #A9C502;*/
            display: inline-block;
            margin: 1rem;
            position: relative;
            width: 349px;
            height: 500px;
            padding-left: 10px;
            float: left;
        }

        .card-1 {
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
            transition: all 0.3s cubic-bezier(.25,.8,.25,1);
        }

            .card-1:hover {
                box-shadow: 0 14px 28px #808080, 0 10px 10px #808080;
            }

       

        @media (max-width:1166px) {
            .m-t-5 {
                margin-top: 3px;
                margin-bottom: 2px;
            }

            .btn-sm {
                padding: 3px 4px;
                font-size: 11px;
            }
        }


        @media (max-width:760px) {
            .Social-Icon-Width {
                width: 7%;
            }
        }

        @media (max-width:606px) {
            .btn-green {
                font-size: 9px;
            }
        }

        @media (max-width:527px) {
            .btn-green {
                font-size: 7px;
            }
        }
    </style>

    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 text-center">
            <h1 style="color: #001b00;">View Assets</h1>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <br />
    <div class="row p-t-10 m-b-5">
        <div class="col-lg-2"></div>
        <div class="col-lg-8 text-center">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <%--    <center>
        <div class="row">
    <div class="col-lg-10">
            <div class="form-inline pull-right">
            <input type="text" class="form-control" placeholder="Search by Order Number">
            <asp:button ID="btnsearch" text="Search" runat="server" OnClick="btnsearch_Click" class="btn btn-green"  />
    </div>
    </div>
</div>
    </center>--%>
    <br />
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
                <asp:Repeater ID="RptAssets" runat="server" OnItemDataBound="RptAssets_ItemDataBound" OnItemCommand="RptAssets_ItemCommand">
                    <ItemTemplate>
                        <div class="card card-1">
                            <asp:HiddenField runat="server" ID="AssetID" Value='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                            <div class="p-l-10 p-r-15">
                                <div class="m-t-20 m-b-10" runat="server" id="divInfo">
                                    <span class="span">Ad Type:</span><asp:Label ID="Label1" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>' runat="server" CssClass="p-l-10 span" /><br />
                                    <span class="span">Order number:</span><asp:Label ID="lblOrder" Text='<%#DataBinder.Eval(Container.DataItem,"OrderNum") %>' runat="server" CssClass="p-l-10 span" /><br />
                                    <span class="span">Title:</span><asp:Label ID="lblTitle" Text='<%#DataBinder.Eval(Container.DataItem,"Subject") %>' runat="server" CssClass="p-l-10 span" />
                                    <br />
                                    <span class="span">Date Placed:</span><asp:Label ID="lblPdate" Text='<%#DataBinder.Eval(Container.DataItem,"OrderDate") %>' runat="server" CssClass="p-l-10 span" /><br />
                                    <span class="span">Date Received:</span><asp:Label ID="lblRdate" Text='<%#DataBinder.Eval(Container.DataItem,"DeliverDate") %>' runat="server" CssClass="p-l-10 span" /><br />
                                </div>
                                <div runat="server" id="divPriceLabel" class="m-t-15 m-b-10">
                                    <asp:Label ID="LblPrice" runat="server" Style="color: red;" />
                                </div>
                                <div class="m-t-15 m-b-10" runat="server" visible="false" id="divUpgrade">
                                    <h4>You Can Unlock This Ad by clicking Upgrade</h4>
                                    <a href="../Dashboard/Upgrade.aspx" class="btn btn-lg btn-green m-b-20">Upgrade</a>
                                </div>
                                <asp:Label ID="LblFilePath" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                                <asp:Label ID="LblStatusID" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ID") %>' />
                                <asp:Label ID="LblCustRemarks" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustRemarks") %>' />
                                <asp:Label ID="LblApprov" Visible="false" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CustApproval") %>' />

                                <div class="p-l-10">
                                    <div class="container1">
                                        <div>
                                            <img alt='Asset Image' src='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' class="img-thumbnail" style="width: 280px; height: 230px" />
                                        </div>
                                        <%-- <asp:Image runat="server" ID="Img-<%#DataBinder.Eval(Container.DataItem,"AssestID") %>" alt='Asset Image' ImageUrl='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' class="img-responsive" Style="width: 280px" />--%>
                                        <div class="overlay">
                                            <a class="icon" id="pop">
                                                <%--<img src="../Images/Icons/eye.png" title="Click to preview" alt="EyeView" />--%>
                                                <asp:ImageButton ID="ImgEye" runat="server" AlternateText="EyeView" ToolTip="Click to preview" ImageUrl="~/Images/Icons/eye.png" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' CommandName="ViewPic" />
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                <div class="m-t-10 m-l-10 m-b-10">
                                    <asp:Button ID="btnArchive" Text="Archive" Visible="false" CommandName="MoveToArchive" runat="server" CssClass="btn btn-sm btn-light-green" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ID") %>' />
                                    <asp:Button ID="BtnRequestModification" Text="Request Modification " Visible="false" CommandName="ReqModfication" runat="server" CssClass="btn btn-sm btn-light-green" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                                    <asp:Button ID="btnDappr" Text="Order Status" runat="server" CssClass="btn btn-sm btn-light-green" Visible="false" CommandName="OrderStatus" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                                    <asp:Button ID="BtnSend" Visible="false" Text="Send" runat="server" CssClass="btn btn-sm btn-light-green m-r-10" CommandName="Send" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AssestID") %>' />
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>



    </div>


    <%----------------------------------------- Page End -------------------------------------------------------%>


    <%----------------------------------------- Pop up Start -------------------------------------------------------%>

    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-body">
                <asp:Image ID="ImgProfile" runat="server" CssClass="img-responsive" />
            </div>
        </div>
    </div>

    <%----------------------------------------- Pop up End -------------------------------------------------------%>



    <%-- Pop Up --%>
    <div class="modal fade" id="OrderStatus" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content " style="border: 1px solid #4B4A4A;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                            <div id="div2" runat="server" visible="false">
                                <strong>
                                    <asp:Label ID="LblMsg1" runat="server"></asp:Label></strong>
                            </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <asp:HiddenField runat="server" ID="hfid" />
                            <asp:HiddenField runat="server" ID="hfStatusID" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 p-b-5">
                            <span style="color: #000; background-color: #fff; border-color: #fff; padding-top: 10px;" class="input-group-addon">Order Number : </span>
                            <asp:Label Style="color: #000; background-color: #fff; border-color: #fff; padding-top: 10px;" class="input-group-addon" ID="lblCode" runat="server" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <%--<img src="#" id="imgdsg"  />--%>
                            <asp:Image ImageUrl="#" ID="img" runat="server" CssClass="img-responsive" Style="width: 567px; height: auto; align-self: center" alt="Ad Image" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:TextBox runat="server" ID="txtmodifications" TextMode="MultiLine" Rows="4" CssClass="form-control textarea-resize m-t-10" placeholder="Request Modification details.." />
                            <asp:Button Text="Approve" ID="BtnAp" CssClass="btn btn-success btn-sm m-t-10" OnClick="BtnAp_Click" runat="server" />
                              <asp:Button Text="Request Modification Using Card" ID="btnRMwithStripe" OnClick="btnRMwithStripe_Click" Visible="false" CssClass="btn btn-sm btn-blue-green m-t-10" runat="server"  />
                             <asp:Button Text="Request Modification" Visible="true" ID="btnDAp" CssClass="btn btn-sm btn-danger m-t-10" runat="server" OnClick="btnDAp_Click" />
                            <asp:Button Text="Request Modification Using Paypal" ID="btnRequestMDF" Visible="false" CssClass="btn btn-sm btn-danger m-t-10" runat="server" OnClick="btnRequestMDF_Click" />
                            <asp:Button Text="Download" ID="BtnDownload" CssClass="btn btn-success btn-sm m-t-10" OnClick="BtnDownload_Click" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


     <div class="modal fade"  id="StripeModel" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="color: #001b00;">Add Account Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div id="div3" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg3" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                     <label class="text-red m-l-15">$25 will be charge on each request modification</label>
                    <center>
                         <img id="loading-image" style="position:absolute;top:49px;left:33%;z-index: 999;" src="../Images/Loader.gif" alt="Alternate Tex" />
                    </center>
                     <div class="row">
                            <br />
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                            <div class="col-lg-8 col-sm-8 col-xs-8">
                                <label class="p-t-10">Name on Card<span style="color: red;">*</span></label>
                                <input type="text" id="TxtNameOnCard" name="name" value="" placeholder="Card Owner Name" class="form-control" />
                                <label class="p-t-10">Card Number<span style="color: red;">*</span></label>
                                <input type="text" id="TxtCardNumber" onkeypress="return isNumberKey(event)" name="TxtCardNumber" maxlength="16" value="" placeholder="0000 0000 0000 0000" class="form-control" />
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Security Code<span style="color: red;">*</span></label>
                                        <input type="text" id="TxtSecurityCode" onkeypress="return isNumberKey(event)" maxlength="3" value="" placeholder="000" class="form-control" />
                                         </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Expiration Date<span style="color: red;">*</span></label>
                                         <input type="text" id="TxtExpiryDate" name="name" value=""  maxlength="7" placeholder="MM/YYYY" class="form-control" />
                                      
                                    </div>
                                </div>

                                <div class="row m-t-25 m-b-10">
                                    <div class="col-lg-12 text-center">
                                        <input id="BtnConfirmPayment" type="button" value="Confirm Payment" class="btn btn-sm btn-light-green" />      
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>    

    <script type="text/javascript">
        $('document').ready(function () {
            $('#loading-image').hide();
            debugger;
            Stripe.setPublishableKey('pk_test_QpIGUHE1KKANZu8Q9AUfMwya00ClZHtbT7'); ///pk_live_CODr3gUvLLUli1xTyykryLv500B4HZmqUk

            $('#BtnConfirmPayment').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                debugger;
                var FullDate = $('#TxtExpiryDate').val();
                var date = FullDate.split('/');
                Stripe.card.createToken({
                    number: $('#TxtCardNumber').val(),
                    cvc: $('#TxtSecurityCode').val(),
                    exp_month: date[0],
                    exp_year: date[1]
                }, stripeResponseHandler);
            });
   
            function stripeResponseHandler(status, response) {
                debugger;
                if (response.error) {
                    alert(response.error.message);  
                } else {
                    var token = response.id;
                    var CDPCard = {};
                    CDPCard.TokenId = token;
                    CDPCard.Amount = "25";
                    CDPCard.ExpiryDate = $('#TxtExpiryDate').val();
                    CDPCard.SecurityCode = $('#TxtSecurityCode').val();
                    CDPCard.CardNumber = $('#TxtCardNumber').val();
                    CDPCard.NameOnCard = $('#TxtNameOnCard').val()
                    CDPCard.Currency = "usd";
                    $('#loading-image').show();
                    if ($('#TxtExpiryDate').val() != "" || $('#TxtExpiryDate').val() != undefined) {
                    $.ajax({
                        type: 'post',
                        url: '../MKT_Assets/ViewAssets.aspx/Charge',
                        data:"{CDPCard:"+ JSON.stringify(CDPCard)+"}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            debugger;
                            if (response.d != "" || response.d != undefined) {
                                window.location.href = "/MKT_Assets/Info.aspx?Success=1&id=" + response.d + "&IsRM=true";
                    }
                    else {
                                alert("Payment Failed Please Try Again ");
                    }
                    },
                        complete: function () {
                            $('#loading-image').hide();
                    },
                        error: function (error) {
                            debugger;
                            alert(error);
                    }
                    })
                   }
                   else {
                        alert("Enter Date In Valid Format");
                    }
                }
            }
        });
    </script>




    <%-- End --%>
</asp:Content>
