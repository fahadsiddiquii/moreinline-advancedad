﻿<%@ Page Title="Order Assets" Language="C#" MasterPageFile="~/WithMenuMaster.Master" AutoEventWireup="true" CodeBehind="OrderAssets.aspx.cs" Inherits="MoreInline.MKT_Assets.OrderAssets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style>
        #fixedbutton {
            position: fixed;
            bottom: 10px;
            left: 235px;
        }

        .button {
            background-color: #1425de;
            border: none;
            color: white;
            padding: 13px 30px 11px 30px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            margin: 4px 2px;
            cursor: pointer;
            border-radius: 30px;
        }

            .button:hover {
                background-color: #f1f1f1;
                color: black;
            }
    </style>

    <%-- <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css" />--%>

    <link href="../css/tour.css" rel="stylesheet" type="text/css" />


    <script src="../js/src/recorder.js"></script>
    <script src="../js/src/Fr.voice.js"></script>
    <script src="../js/js/jquery.js"></script>
    <script src="../js/js/app.js"></script>
    <%----------------------------------------- Script -------------------------------------------------------%>
    <!-- DataTables -->

    <script src="../js/EasyTimer.js"></script>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link href="../agency-video/img/favicon.png" rel="icon" />

    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script Start --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
      <script type="text/javascript">
        function openStripe() {
            debugger;
            $('#StripeModel').modal('show');
        }
    </script>
    
    
    

    <script>

        //function SetAudio(path) {
        //    debugger;
        //    ("#audio").attr("src", path);
        //    $("#stop").removeAttr("hidden");
        //}
        //   SetAudio(pathm);

        function Disabled(path, btnid) {
            //$("#record").addClass("disabled");
            if (btnid === "ImgPreview") {
                $("#audio").attr("src", path);
            }
            else {
                $("#audio").attr("src", path);
                $("#stop").removeAttr("hidden");
                $("#record").removeClass("disabled");
            }


        }

    </script>


    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 9;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } <%--else if (Text == "div2") {
                document.getElementById("<%=div2.ClientID %>").style.display = "none";
            }--%>
            }, seconds * 1000);
        }
    </script>

    <%--<script src='http://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtLocation.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>--%>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>
    <%----------------------------------------- Script End -------------------------------------------------------%>
    <%----------------------------------------- Page Start -------------------------------------------------------%>

    <div class="row p-t-10">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div id="div1" runat="server" visible="false">
                <strong>
                    <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
            </div>
            <br />
        </div>
        <div class="col-lg-2"></div>
    </div>
    <asp:HiddenField runat="server" ID="HfAssetId" />
    <asp:HiddenField runat="server" ID="HfVoicePath" />
    <asp:HiddenField runat="server" ID="hffilecount" />

    <div class="row">
        <div class="col-lg-6">
            <asp:LinkButton Text="Back To Order Status" class="btn btn-sm btn-green pull-left" Visible="false" runat="server" ID="btnbtorder" PostBackUrl="~/MKT_Assets/OrderStatus.aspx" />
        </div>

    </div>
    <div class="row">

        <div class="col-lg-3"></div>
        <section class="col-lg-6 connectedSortable">
            <div class="row">
                <div class="col-lg-12">
                    <h3 style="color: #001b00; float: left;">Order New Assets</h3>

                    <button type="button" class="btn btn-sm btn-light-green" data-toggle="modal" data-target="#myModal" style="float: right; margin: 25px 0 10px 0;">Ad Example</button>
                </div>
            </div>
            <div class="row m-t-20">
                <div class="col-lg-12">
                    <label style="color: #eb4034;">Charges for each order are $99.</label>
                    <br />
                    <br />
                    <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control" Style="background-color: #007f9f; color: #fff;">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <div class="box box-solid">
                        <div class="box-body border-radius-none">
                            <asp:TextBox ID="TxtMessage" TextMode="MultiLine" runat="server" Rows="10" CssClass="form-control textarea-resize test-9" placeholder="Write Requirements Here ..." EnableTheming="True"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <asp:Repeater ID="RptOrderAssets" runat="server" OnItemDataBound="RptOrderAssets_ItemDataBound">
                        <HeaderTemplate>
                            <table id="example1" class="table table-bordered" border="0">
                                <thead class="table-head">
                                    <tr style="background-color: #A9C502">

                                        <th class="text-center text-black">File Name</th>
                                        <th class="text-center text-black">Action</th>
                                    </tr>
                                    <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>

                                <td class="text-center">
                                    <asp:Label ID="FilePath" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' />
                                </td>
                                <td class="text-center">
                                    <asp:Button ID="ImgDelete" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ADID") %>' OnClick="ImgDelete_Click" CommandName='<%#DataBinder.Eval(Container.DataItem,"FilePath") %>' Text="Delete" class="btn btn-sm btn-green" OnClientClick="<script>confirm('Do you want to delete filed ?')</script>" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody></table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="row m-t-20">
                <div class="col-lg-12">
                    <p><b>Upload any other assets we will need for the designs! Think: content, photos, logos, etc.</b></p>
                    <asp:Label Text="" Style="color: #707070;" ID="lblFiles" runat="server" />
                </div>

            </div>
            <asp:FileUpload ID="AddFile" runat="server" AllowMultiple="true" CssClass="txtBox-style" />
            <div class="row m-t-20">
                <div class="col-lg-12 text-right m-b-10">
                    <%--<asp:Button ID="BtnSavedraft" runat="server" class="btn btn-lg btn-green test-18" ForeColor="#ffffff" OnClick="BtnSend_Click" BackColor="#000000" Text="Save as draft" />--%>
                    <button type="button" class="btn btn-lg btn-light-green" data-toggle="modal" data-target="#COModal" style="float: right; margin: 25px 0 10px 0;">Place Order Using Paypal </button> 
                    <asp:Button ID="Button1"  runat="server" Visible="false" Text="Place Order Using Card" OnClick="Button1_Click"  class="btn btn-lg btn-light-green" style="float: right; margin: 25px 11px 10px 0;" />
                    <%--<button type="button" class="btn btn-lg btn-light-green" data-toggle="modal" data-target="#StripeModel" style=" margin: 25px 11px 10px 0;">Place Order Using Card </button>--%>
                      <asp:Button ID="BtnQuickOrder" runat="server" Visible="false" Text="Quick Order" OnClick="BtnQuickOrder_Click" CssClass="btn btn-lg btn-light-green" />

                    <asp:Button ID="BtnUpdate" Visible="false" runat="server" Text="Update" OnClick="BtnSend_Click" CssClass="btn btn-lg btn-light-green" />

                </div>
            </div>
        </section>
    
    </div>

    <%----------------------------------------- Page End -------------------------------------------------------%>




    <%-- Pop Up --%>
    <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content " style="background-color: #141414; border: 1px solid #4B4A4A;">

                <div class="modal-body">
                    <img src="../Images/pic.jpg" alt="Alternate Text" style="width: 100%; height: 100%;" />
                </div>
            </div>
            <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
            </div>
        </div>
    </div>
    <%-- End --%>



        <div class="modal fade"  id="StripeModel" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="color: #001b00;">Add Account Information </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                                <div id="div3" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg3" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <Label class="text-red m-l-15">$99 will be charged for each new order</Label>
                      <center>
                         <img id="loading-image" style="position:absolute;top:49px;left:33%;z-index: 999;" src="../Images/Loader.gif" alt="Loader Image" />
                    </center>
                     <div class="row">
                            <br />
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                            <div class="col-lg-8 col-sm-8 col-xs-8">
                                <label class="p-t-10">Name on Card<span style="color: red;">*</span></label>
                                <input type="text" id="TxtNameOnCard" name="name" value="" placeholder="Card Owner Name" class="form-control" />
                                <label class="p-t-10">Card Number<span style="color: red;">*</span></label>
                                <input type="text" id="TxtCardNumber" onkeypress="return isNumberKey(event)" name="TxtCardNumber" maxlength="16" value="" placeholder="0000 0000 0000 0000" class="form-control" />
                                <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Security Code<span style="color: red;">*</span></label>
                                        <input type="text" id="TxtSecurityCode" onkeypress="return isNumberKey(event)" maxlength="3" value="" placeholder="000" class="form-control" />
                                         </div>
                                    <div class="col-lg-6 col-sm-6 col-xs-6">
                                        <label class="p-t-10">Expiration Date<span style="color: red;">*</span></label>
                                         <input type="text" id="TxtExpiryDate" name="name" value=""  maxlength="7" placeholder="MM/YYYY" class="form-control" />
                                      
                                    </div>
                                </div>

                                <div class="row m-t-25 m-b-10">
                                    <div class="col-lg-12 text-center">
                                        <input id="BtnConfirmPayment" type="button" value="Confirm Payment" class="btn btn-sm btn-light-green" />      
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-2"></div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>    

    <script type="text/javascript">
        $('document').ready(function () {
            $('#loading-image').hide();
            debugger;
            Stripe.setPublishableKey('pk_live_CODr3gUvLLUli1xTyykryLv500B4HZmqUk');

            $('#BtnConfirmPayment').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                debugger;
                var FullDate = $('#TxtExpiryDate').val();
                var date = FullDate.split('/');
                Stripe.card.createToken({
                    number: $('#TxtCardNumber').val(),
                    cvc: $('#TxtSecurityCode').val(),
                    exp_month: date[0],
                    exp_year: date[1]
                }, stripeResponseHandler);
            });
   
            function stripeResponseHandler(status, response) {
                debugger;
                if (response.error) {
                    alert(response.error.message);  
                } else {
                    var token = response.id;
                    var CDPCard = {};
                    CDPCard.TokenId = token;
                    CDPCard.Amount = "99";
                    CDPCard.ExpiryDate = $('#TxtExpiryDate').val();
                    CDPCard.SecurityCode = $('#TxtSecurityCode').val();
                    CDPCard.CardNumber = $('#TxtCardNumber').val();
                    CDPCard.NameOnCard = $('#TxtNameOnCard').val()
                    CDPCard.Currency = "usd";
                    $('#loading-image').show();
                    $.ajax({
                        type: 'post',
                        url: '../MKT_Assets/OrderAssets.aspx/Charge',
                        data:"{CDPCard:"+ JSON.stringify(CDPCard)+"}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            debugger;
                            if (response.d != "" || response.d != undefined) {
                                window.location.href = "/MKT_Assets/Info.aspx?Success=1&id=" + response.d;
                            }
                            else {
                                alert("Payment Failed Please Try Again ");
                            }
                        },
                        complete: function () {
                            $('#loading-image').hide();
                        },
                        error: function (error) {
                            debugger;
                            alert(error);
                        }
                    })
                    //();
                }
            }
        });
    </script>



    <%-- Pop Up --%>
    <div class="modal fade" id="COModal" data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="text-center m-b-5">
                        <br />
                        <h5>$99 will be charged for this order. Do you want to proceed & confirm order?
                        </h5>
                        <br />
                        <asp:Button ID="BtnSend" runat="server" Text="Confirm Using Paypal" OnClick="BtnSend_Click" CssClass="btn btn-sm btn-light-green test-19" />
                    </div>
                    <%--       <asp:LinkButton Text="Confirm" PostBackUrl="~/MKT_Assets/ConfirmOrder.aspx" runat="server" />--%>
                </div>
            </div>
            <%-- <div class="modal-footer" style="background-color: #141414; border: 1px solid #4B4A4A;">
                <center>
                <button type="button" class="btn btn-sm btn-green" style="background-color: #eb4034" data-dismiss="modal">Close</button>    
                </center>
            </div>--%>
        </div>
    </div>
    <%-- End --%>
</asp:Content>

