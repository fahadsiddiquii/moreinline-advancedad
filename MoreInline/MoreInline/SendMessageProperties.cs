﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreInline
{
    public class SendMessageProperties
    {
        public string name { get; set; }
        public string email { get; set; }
        public string contact { get; set; }
        public string company { get; set; }
        public string message { get; set; }
    }
}