﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MoreInline
{
  
    public class NotificationModel
    {
        NotificationInfoDAL NotiDAL = new NotificationInfoDAL();
        public string id { get; set; }
        public string Type { get; set; }

        public string Text { get; set; }

        public string Code { get; set; }

        public string Status { get; set; }
        public string Path { get; set; }
        public string FormPath { get; set; }

        public List<NotificationModel> GetData(int userid,int UTID)
        {
            List<NotificationModel> Listnotification = new List<NotificationModel>();
            DataTable dt = NotiDAL.GetOrderData(userid, UTID);
            int count = 0;
            foreach (DataRow item in dt.Rows)
            {
                count++;
                NotificationModel notification = new NotificationModel();
                notification.id = item["id"].ToString();
                notification.Text = "Order Received";
                notification.Path = "";
                notification.Code = item["OrderNum"].ToString();
                string MnApproval = item["MngrApproval"].ToString();
                string CustApproval = item["CustApproval"].ToString();
                string QaApproval = item["QaApproval"].ToString();

                int RoleID = int.Parse(item["UserRoleID"].ToString());
                int id = int.Parse(item["id"].ToString());
                if (RoleID.Equals(4) && (QaApproval.Equals("0") || string.IsNullOrWhiteSpace(QaApproval)) && (CustApproval.Equals("0") || string.IsNullOrWhiteSpace(CustApproval)))
                {
                    notification.Text = "Order Received";
                    notification.Path = "../User/ReceivedOrders.aspx?NotiId=" +id;
                }
                if (RoleID.Equals(4) && QaApproval.Equals("1") && CustApproval.Equals("0"))
                {
                    notification.Text = "Design Received";
                    notification.Path = "../User/DesignsApproval.aspx?NotiId=" + id;
                }
                else if (RoleID.Equals(5) && (QaApproval.Equals("0") || string.IsNullOrWhiteSpace(QaApproval)))
                {
                    notification.Text = "Order Received";
                    notification.Path = "../User/Orders.aspx?id=" + id;
                }

                else if (RoleID.Equals(5) && QaApproval.Equals("2"))
                {
                    notification.Text = "Design Received for Modification";
                    notification.Path = "../User/Orders.aspx?id=" + id;
                }
                else if (RoleID.Equals(5) && QaApproval.Equals("1"))
                {
                    notification.Text = "Design Accepted";
                    notification.Path = "../User/Orders.aspx?id=" + id;
                }
                else if (RoleID.Equals(6) && (MnApproval.Equals("0") || string.IsNullOrWhiteSpace(MnApproval)))
                {
                    notification.Text = "Design Received";
                    notification.Path = "../User/DesignListQA.aspx?id=" + id;
                }
                else if (RoleID.Equals(6) && MnApproval.Equals("2"))
                {
                    notification.Text = "Design Not Approved";
                    notification.Path = "../User/DesignListQA.aspx?id=" + id;
                }
                else if (RoleID.Equals(-1))
                {
                    notification.Text = "Order Received";
                    notification.Path = item["FormPath"].ToString() + "?id=" + id;
                }
                else if ((CustApproval.Equals("2")) && (RoleID.Equals(4)))
                {
                    notification.Text = "Request For Modification";
                    notification.Path = "../User/DesignsApproval.aspx?NotiId=" + id;
                }
                else if ((CustApproval.Equals("1")) && (RoleID.Equals(4)))
                {
                    notification.Text = "Order Accepted ";
                    notification.Path = "../User/DesignsApproval.aspx?NotiId=" + id;
                }
                else if (RoleID.Equals(2))
                {
                    if (CustApproval.Equals("1"))
                    {
                        notification.Text = "Order Accepted ";
                        notification.Path = item["FormPath"].ToString() + "?id=" + id;
                    }
                    else if (CustApproval.Equals("2"))
                    {
                        notification.Text = "Request For Modification";
                        notification.Path = item["FormPath"].ToString() + "?id=" + id;
                    }
                    else
                    {
                        notification.Text = "Order Received";
                        notification.Path = item["FormPath"].ToString() + "?id=" + id;
                    }
                 
                }
                notification.Type = "Order";

                //var IsSuccess = dc.NotificationInfoes.Where(x => x.OID == item.OID && x.UID == userid && x.IsViewed != true).FirstOrDefault();
                //if (IsSuccess != null)
                //{
                //    IsSuccess.IsViewed = true;
                //    dc.Entry(IsSuccess).State = EntityState.Modified;
                //    dc.SaveChanges();
                //    //   dc.NotificationInfoes.Remove(IsSuccess);
                //}
                Listnotification.Add(notification);
            }
         
            return Listnotification;
       }
        public string Notify(int userid, int UTID) 
        {
            int count = 0;
            DataTable dt = NotiDAL.GetOrderData(userid, UTID);
            string txt = string.Empty;
            count = dt.Rows.Count;
            if (HttpContext.Current.Session["Count"] == null)
            {
                HttpContext.Current.Session["Count"] = count;
                txt = "notify";
            }
            else if (int.Parse(HttpContext.Current.Session["Count"].ToString()) < count)
            {
                txt = "notify";
            }
            return txt;
        }
    }
}