﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace MoreInline
{
    public class EmailNotification
    {
        UserSignupDAL ObjUserSignupDAL = new UserSignupDAL();

        public string ReceiverEmailAddress { get; set; }
        public string EmailSubject { get; set; }
        public string ReceiverName { get; set; }
        public string NotificationType { get; set; }
        public string Content { get; set; }
        public string CompanyName { get; set; }
        public string Contact { get; set; }
        public string CompanyEMailAddress { get; set; }
        public string CompanyAddress { get; set; }
        public string ImagePath { get; set; }
        public string Website { get; set; }
        public int SenderIdForUnSubscription { get; set; }
        public string IsOrderNotif { get; set; }
        public string PeriviousLimit { get; set; }
        public string CurrentLimit { get; set; }
        public string PreviousPkg { get; set; }
        public string CurrentPkg { get; set; }
        public string ButtonText { get; set; }
        public string isButtonVisible { get; set; }


        public static bool SendNotification(EmailNotification NotificationInfo)
        {
            //switch (NotificationInfo.NotificationType)
            //{
            //    case "ExpiryNotificaion":
            //        return SendEmail(NotificationInfo);
            //        break;
            //    case "EmailNotification":
            //        return SendEmail(NotificationInfo);
            //        break;
            //}
            return SendEmail(NotificationInfo);
        }

        private static bool SendEmail(EmailNotification NotificationInfo)
        {
            #region EmailConstruction
            string body = string.Empty;
            string UnsubscriptionPath = "https://moreinline.com/Emails/Unsubscribe.aspx?CustID=" + NotificationInfo.SenderIdForUnSubscription.ToString() + "&Type=" + NotificationInfo.NotificationType;
            if (!string.IsNullOrWhiteSpace(NotificationInfo.IsOrderNotif)) {
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Emails/UpdatedSubscription.html")))
                {
                    body = reader.ReadToEnd();
                }
            }
            else
            {
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Emails/EmailNotification.html")))
                {
                    body = reader.ReadToEnd();
                }
            }
            if (!string.IsNullOrWhiteSpace(NotificationInfo.IsOrderNotif))
            {
                body = body.Replace("{pplan}", NotificationInfo.PreviousPkg);
                body = body.Replace("{pelimit}", NotificationInfo.PeriviousLimit);
                body = body.Replace("{cplan}", NotificationInfo.CurrentPkg);
                body = body.Replace("{celimit}", NotificationInfo.CurrentLimit);
                body = body.Replace("{message}", "");
            }
            else { body = body.Replace("{message}", NotificationInfo.Content); }
            if (!string.IsNullOrWhiteSpace(NotificationInfo.isButtonVisible))
            {
                body = body.Replace("{btnstyle}", "margin-left:200px;display:none;"); ///
            }
            else
            {
                body = body.Replace("{btnstyle}", "margin-left:200px;text-decoration:none;color:#000;"); ///
                body = body.Replace("{buttontext}", NotificationInfo.ButtonText);
            }
            body = body.Replace("{logo}", NotificationInfo.ImagePath);
            body = body.Replace("{receiver}", NotificationInfo.ReceiverName);
            body = body.Replace("{companySender}", NotificationInfo.CompanyName);
            body = body.Replace("{senderAddress}", NotificationInfo.CompanyAddress);
            body = body.Replace("{senderContact}", NotificationInfo.Contact);
            body = body.Replace("{senderWeb}", NotificationInfo.Website);
            body = body.Replace("{senderEmail}", NotificationInfo.CompanyEMailAddress);
            if (NotificationInfo.SenderIdForUnSubscription.ToString().Equals("-1"))
                body = body.Replace("{unsubStyle}", "display:none;");
            else
            {
                body = body.Replace("{Unsubscribe}", UnsubscriptionPath);
                body = body.Replace("{unsubStyle}", "text-decoration:none;color:#000;");
            }
            #endregion

            SendEmail(body, NotificationInfo.ReceiverEmailAddress, NotificationInfo.EmailSubject, "MoreInline Notification");
            return true;
        }


        public EmailNotification GetAdminFilledObject()
        {
            string AdminEmil = ObjUserSignupDAL.GetAdminInfo().Rows[0]["UserEmail"].ToString();
            EmailNotification EmailData = new EmailNotification();
            EmailData.CompanyName = "MoreInline";
            EmailData.CompanyAddress = "754 Peachtree St NW-Suite 9 - Atlanta, GA 30308";
            EmailData.Contact = "888-900-2095";
            EmailData.Content = "New Order Received";
            EmailData.ReceiverEmailAddress = AdminEmil;
            EmailData.Website = "moreinline.com";
            EmailData.CompanyEMailAddress = "sales@moreinline.com";
            EmailData.EmailSubject = "New Order Received";
            EmailData.ImagePath = "https://moreinline.com/Images/moreinline_logo.png";
            EmailData.ReceiverName = "Admin";
            EmailData.NotificationType = "EmailNotification";
            EmailData.SenderIdForUnSubscription = -1;
            return EmailData;
        }

        public static void SendEmail(string body, string Email, string subject, string Title)
        {

            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("donotreply@moreinline.com", Title);
            mm.To.Add(Email);
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
        }





    }
}