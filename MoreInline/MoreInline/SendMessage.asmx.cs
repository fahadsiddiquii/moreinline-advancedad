﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Services;

namespace MoreInline
{
    /// <summary>
    /// Summary description for SendMessage
    /// </summary>
    [WebService(Namespace = "https://moreinline.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class SendMessage : System.Web.Services.WebService
    {

       [WebMethod]
        public void Send(SendMessageProperties smd)
        {

            string Emailbody = Email_Construction(smd.name,smd.email,smd.contact,smd.company,smd.message);
            SendEmail(Emailbody);

        }

        private string Email_Construction(string name, string email, string contact, string company, string message)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(Server.MapPath("~/Emails/CustomerEmail.html")))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{name}", name);
            body = body.Replace("{email}", email);
            body = body.Replace("{contact}", contact);
            body = body.Replace("{company}", company);
            body = body.Replace("{message}", message);
            return body;
        }
        private void SendEmail(string body)
        {
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("donotreply@moreinline.com", "Customer Message");
            mm.To.Add("stan.ferrell64@gmail.com");
            mm.Subject = "MoreInline User Message";
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "mail.moreinline.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "donotreply@moreinline.com";
            NetworkCred.Password = "12@1EJqOgg6meMwk";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
            
        }

    }
}
