﻿using MoreInline.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MoreInline.Websites
{
    public partial class DemoWebsite : System.Web.UI.Page
    {
        WebsiteDAL ObjWebsiteDAL = new WebsiteDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    GetWebData();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void GetWebData()
        {
            int custID = int.Parse(Session["Cust_ID"].ToString());
            DataTable dt = ObjWebsiteDAL.GetCustomerForWebsite(custID);
            Logo.ImageUrl = dt.Rows[0]["logoPath"].ToString().Replace("~/", "../");
            Banner.ImageUrl = dt.Rows[0]["BannerPath"].ToString().Replace("~/", "../");
        }

    }
}