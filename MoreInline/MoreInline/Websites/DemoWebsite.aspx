﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DemoWebsite.aspx.cs" Inherits="MoreInline.Websites.DemoWebsite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Demo Website</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="../agency-video/img/favicon.png" rel="icon" />
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css" />
    <link href="../css/MoreInline_Style.css" rel="stylesheet" />
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" />
    <link href="css/websites_style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header header-class">

                <div class="logo">
                    <asp:Image ID="Logo" AlternateText="Logo" runat="server" width="160" height="35" />
                </div>


                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="#banner">Home</a>
                            </li>
                            <li>
                                <a href="#service">Our Services</a>
                            </li>
                            <li>
                                <a href="#about">About Us</a>
                            </li>
                            <li>
                                <a href="#contact">Contact Us</a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </header>

            <section id="banner">
                <div class="container-fluid p-l-0 p-r-0">
                    <asp:Image ID="Banner" AlternateText="Banner" runat="server" CssClass="img-responsive" />
                </div>
            </section>

            <section id="service" class="servicesSection p-b-90">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center">
                                <h1><b>Our Services</b></h1>
                                <p class="sub-heading">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-7 col-sm-9 m-t-10">
                            <div class="card card-1 m-t-40">
                                <div class="row">
                                    <div class="col-lg-7 col-xs-7">
                                        <h1>Graphic Design</h1>
                                    </div>
                                    <div class="col-lg-5 col-xs-5">
                                        <img src="images/graphic.png" width="145" height="145" alt="grapgic design" class="img-responsive" />
                                    </div>
                                </div>

                                <p class="m-t-15">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                <a href="#" style="color: #0023C4">
                                    <h3><b>LEARN MORE</b></h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-7 col-sm-9 m-t-10">
                            <div class="card card-1 m-t-40">
                                <div class="row">
                                    <div class="col-lg-7 col-xs-7">
                                        <h1>Website Design</h1>
                                    </div>
                                    <div class="col-lg-5 col-xs-5">
                                        <img src="images/design-book.png" width="145" height="145" alt="website design" class="img-responsive" />
                                    </div>
                                </div>

                                <p class="m-t-15">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                <a href="#" style="color: #0023C4">
                                    <h3><b>LEARN MORE</b></h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-7 col-sm-9 m-t-10">
                            <div class="card card-1 m-t-40">
                                <div class="row">
                                    <div class="col-lg-7 col-xs-7">
                                        <h1>Email Marketing</h1>
                                    </div>
                                    <div class="col-lg-5 col-xs-5">
                                        <img src="images/emal_market.png" width="145" height="145" alt="email marketing" class="img-responsive" />
                                    </div>
                                </div>

                                <p class="m-t-15">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>
                                <a href="#" style="color: #0023C4">
                                    <h3><b>LEARN MORE</b></h3>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section id="about" class="p-t-50 p-b-50 m-t-50 m-b-50">
                <div class="container">
                    <div class="text-center">
                        <h1><b>About Us</b></h1>
                    </div>
                    <br />
                    <p>
                        Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
                        <br />
                        <br />
                        The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
                    </p>
                </div>
            </section>



            <section id="contact" class="servicesSection p-b-90">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="text-center">
                                <h1><b>Contact Us</b></h1>
                                <p class="sub-heading">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="contact-wrapper form-style-two p-t-50">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-input m-t-25">
                                            <label>Name</label>
                                            <div class="input-items default">
                                                <input name="name" type="text" placeholder="Name" style="padding-left: 10px;" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-input m-t-25">
                                            <label>Email</label>
                                            <div class="input-items default">
                                                <input type="email" name="email" placeholder="Email" style="padding-left: 10px;" />
                                            </div>
                                        </div>
                                        <!-- form input -->
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-input m-t-25">
                                            <label>Message</label>
                                            <div class="input-items default">
                                                <textarea name="message" placeholder="Message" style="padding-left: 10px;"></textarea>
                                            </div>
                                        </div>
                                        <!-- form input -->
                                    </div>
                                    <div class="col-md-12 m-t-15">
                                        <asp:Button Text="Send Message" runat="server" CssClass="btn btn-lg btn-blue" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </section>



            <footer class="footer-area footer-dark">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <p class="p-b-10 text-white">Address: 754 Peachtree St NW - Suite 9 - Atlanta, GA 30308</p>
                            <p class="p-b-10 text-white">8888-900-2095 </p>
                        </div>
                    </div>
                </div>
            </footer>
            <a href="javascript:" id="return-to-top"><i class="icon-chevron-up"></i></a>
        </div>
    </form>

    <!------------------------------------- Scripts ----------------------------------------------->

    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>

    <script>
        $(document).ready(function () {
            $("a").on('click', function (event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();
                    // Store hash
                    var hash = this.hash;
                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function () {
                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                }
            });
        });
    </script>

    <script>
        $(window).scroll(function () {
            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                $('#return-to-top').fadeIn(200);    // Fade in the arrow
            } else {
                $('#return-to-top').fadeOut(200);   // Else fade out the arrow
            }
        });
        $('#return-to-top').click(function () {      // When arrow is clicked
            $('body,html').animate({
                scrollTop: 0                       // Scroll to top of body
            }, 500);
        });
    </script>


</body>
</html>
