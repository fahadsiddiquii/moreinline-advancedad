﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUpPaid.aspx.cs" Inherits="MoreInline.SignUpPaid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>$99 Sign Up</title>
    <script type="text/javascript">
        function preventBack() { window.history.forward(); }
        setTimeout("preventBack()", 0);
        window.onunload = function () { null };
    </script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />

    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Ionicons -->

    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css" />
    <!-- Theme style -->

    <link href="css/MoreInline_Style.css" rel="stylesheet" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" />

    <%-- ----------------------------------- Script Start --------------------------------------------- --%>
    <!-- jQuery 3 -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/adminlte.min.js"></script>


    <%-- <script type="text/javascript">
        function HidePnlButtons() {
            document.getElementById("PnlButtons").style.display = "none";
            document.getElementById('PnlCard').style.visibility = 'visible';
        }
        function ShowPnlChecking() {
            document.getElementById("PnlButtons").style.display = "none";
            document.getElementById("PnlCheckingAccount").style.display = "visible";
        }
        function CreditCardHide() {
            document.getElementById("PnlCard").style.display = "none";
            document.getElementById("PnlButtons").style.display = "visible";
        }
    </script>--%>

    <script>
        $(document).ready(function () {
            $("body").on('click', '.toggle-password2', function () {
                $(this).toggleClass("fa-eye-slash fa-eye");
                var input2 = $("#TxtConfirmPassword");
                if (input2.attr("type") === "password") {
                    input2.attr("type", "text");
                } else {
                    input2.attr("type", "password");
                }
            });
        });
        $(document).ready(function () {
            $("body").on('click', '.toggle-password1', function () {
                $(this).toggleClass("fa-eye-slash fa-eye");
                var input1 = $("#TxtPassword");
                if (input1.attr("type") === "password") {
                    input1.attr("type", "text");
                } else {
                    input1.attr("type", "password");
                }
            });
        });
    </script>
    <script src='https://maps.googleapis.com/maps/api/js?v=3&sensor=false&amp;libraries=places&key=AIzaSyCLEgUdoNg0AK1adIZqFBQkXYK4Wd57s3Q'></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtBAddress.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('<%=TxtAddress2.ClientID%>'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
            });
        });
    </script>

    <script type="text/javascript">
        function HideLabel(Text) {
            var seconds = 3;
            setTimeout(function () {
                if (Text == "div1") {
                    document.getElementById("<%=div1.ClientID %>").style.display = "none";
                } else if (Text == "div2") {
                    document.getElementById("<%=div2.ClientID %>").style.display = "none";
                }
            }, seconds * 1000);
        }
    </script>

    <script language="Javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function checkShortcut() {
            if (event.keyCode == 13) {
                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function openModal() {
            $('#myModal').modal('show');
        }
    </script>

    <%-- ----------------------------------- Script End --------------------------------------------- --%>
</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <header class="main-header" style="background-color: #000000;">
                <!-- Logo -->
                <a href="../LoginPage.aspx" class="logo" style="background-color: #000000;">
                    <img id="m_Logo" src="../Images/moreinline_logo.png" width="160px" height="35px" />
                </a>
                <nav class="navbar navbar-static-top" role="navigation" style="background-color: #000000;">
                </nav>
            </header>
            <div class="content-wrapper m-l-0" style="background-color: #141414;">
                <!-- Main content -->
                <section class="content container-fluid p-t-0 p-b-0 p-l-0">
                    <%--<div class="row">
                           id="up" onclick="Up()" 
                        <div class="col-lg-3 text-right active-div" style="z-index: 3; padding:11px 30px 0 0;">
                            <span style="font-family: 'Microsoft JhengHei'"><b>User Profile</b></span>
                        </div>
                         id="bl" onclick="Bl()"
                        <div class="col-lg-1 text-right rounded-div" style="margin-left: -12px; z-index: 2; padding:11px 30px 0 0;">
                            <span style="font-family: 'Microsoft JhengHei'"><b>Billing</b></span>
                        </div>
                        <%--id="pd" onclick="PD()" 
                        <div class="col-lg-1 text-right rounded-div" style="margin-left: -12px; padding:11px 30px 0 0;">
                            <span style="font-family: 'Microsoft JhengHei'"><b>Payment Details</b></span>
                        </div>
                    </div>--%>
                    <asp:Panel ID="MainPanel" runat="server">
                        <ul class="list-inline2">
                            <li id="up" runat="server" class="active-div text-right u-p"><span><b>User Profile</b></span></li>
                            <%-- <li id="up1" visible="false" runat="server" class="rounded-div text-right u-p"><span><b>User Profile</b></span></li>
                            <li id="bl" runat="server" class="rounded-div text-right bl"><span><b>Billing</b></span></li>
                            <li id="bl1" visible="false" runat="server" class="active-div text-right bl"><span><b>Billing</b></span></li>
                            <li id="pd" runat="server" class="rounded-div text-right payment"><span><b>Payment Details</b></span></li>
                            <li id="pd2" visible="false" runat="server" class="active-div text-right payment"><span><b>Payment Details</b></span></li>--%>
                        </ul>
                        <br />
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8 p-b-10">
                                <div id="div1" runat="server" visible="false">
                                    <strong>
                                        <asp:Label ID="LblMsg" runat="server"></asp:Label></strong>
                                </div>
                            </div>
                            <div class="col-lg-2"></div>
                        </div>
                        <asp:Panel ID="PnlUserProfile" runat="server">
                            <div class="row p-t-5 p-l-15">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-2">
                                    <img src="Images/userpic.png" class="rounded-image" />
                                    <asp:FileUpload ID="ImgUpload" runat="server" CssClass="m-b-10" />
                                    <span class="text-white">Image size must not exceed 200 KB. Upload only .jpg, .png, .jpeg, .gif files.</span>
                                    <asp:CustomValidator ID="CustomValidatorImg" OnServerValidate="CustomValidatorImg_ServerValidate" ControlToValidate="ImgUpload" runat="server" ForeColor="Red" />
                                    <%--<label class="p-t-0" style="color: #6C6C6C; font-size: 10px;">Profile photos allows you to communicate with potential clients to tell a little about yourself. This is how you'll be viewed to other businesses and customers when sending out your assets. It matters and it works.</label>--%>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group-sm">
                                        <asp:TextBox ID="TxtFName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                                        <asp:TextBox ID="TxtLName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Last Name"></asp:TextBox>
                                        <asp:TextBox ID="TxtCompany" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Company"></asp:TextBox>
                                        <asp:TextBox ID="TxtWebsite" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Website"></asp:TextBox>
                                        <asp:TextBox ID="TxtAddress" runat="server" onkeydown="return checkShortcut();" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Residential Address"></asp:TextBox>
                                        <asp:TextBox ID="TxtZipCode" runat="server" onkeypress="return isNumberKey(event)" MaxLength="11" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Zip Code"></asp:TextBox>
                                        <asp:TextBox ID="TxtEmail" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Email"></asp:TextBox>
                                        <asp:TextBox ID="TxtPhone" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Contact Number"></asp:TextBox>
                                        <asp:TextBox ID="TxtUsername" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-10" placeholder="Username"></asp:TextBox>
                                        <asp:TextBox ID="TxtPassword" runat="server" type="password" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Password"></asp:TextBox>
                                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password1"></span>
                                        <asp:TextBox ID="TxtConfirmPassword" runat="server" type="password" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control m-t-5" placeholder="Confirm Password"></asp:TextBox>
                                        <span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password2"></span>
                                    </div>
                                </div>
                                <div class="col-lg-5"></div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-3 text-right">
                                    <asp:Button ID="BtnContinue" runat="server" Text="Continue" OnClick="BtnContinue_Click" CssClass="btn btn-sm btn-green" Style="width: 90px;" />
                                </div>
                                <div class="col-lg-5"></div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PnlBilling" runat="server" Visible="false">
                            <div class="row p-t-20 p-l-15">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-4 text-center">
                                    <h3 style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>Add Billing Information</b></h3>
                                </div>
                                <div class="col-lg-4"></div>
                            </div>
                            <div class="row m-t-10">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-3 m-l-15 form-group-sm">
                                    <label class="p-t-10" style="color: #707070">First Name<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtBFname" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Last Name<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtBLname" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Organization</label>
                                    <asp:TextBox ID="TxtOrganization" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Contact Number<span style="color: red;">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <asp:DropDownList ID="DdlPhone" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000; width: 148px;">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:TextBox ID="TxtBPhone" onpaste="return false" onkeypress="return isNumberKey(event)" MaxLength="11" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="p-t-10" style="color: #707070">Email<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtBEmail" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Tax ID</label>
                                    <asp:TextBox ID="TxtTaxID" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>

                                </div>
                                <div class="col-lg-3 m-l-15 form-group-sm">
                                    <label class="p-t-10" style="color: #707070">Country/Region<span style="color: red;">*</span></label>
                                    <asp:DropDownList ID="DdlCountry" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000;">
                                    </asp:DropDownList>
                                    <label class="p-t-10" style="color: #707070">Address<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtBAddress" onkeydown="return checkShortcut();" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Address 2</label>
                                    <asp:TextBox ID="TxtAddress2" onkeydown="return checkShortcut();" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">City<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtCity" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Province/Region<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtProvince" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <label class="p-t-10" style="color: #707070">Postal Code<span style="color: red;">*</span></label>
                                    <asp:TextBox ID="TxtPostalCode" runat="server" onkeypress="return isNumberKey(event)" MaxLength="11" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    <div class="row m-t-10">
                                        <div class="col-lg-12 text-right">
                                            <asp:Button ID="BtnB_GoBack" runat="server" OnClick="BtnB_GoBack_Click" Text="Go back" CssClass="btn btn-sm btn-green" Style="width: 90px; background-color: #707070;" />
                                            <asp:Button ID="BtnB_Continue" runat="server" OnClick="BtnB_Continue_Click" Text="Continue" CssClass="btn btn-sm btn-green" Style="width: 90px;" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="PnlPayment" runat="server" Visible="false">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-4 text-center">
                                    <h3 style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>Add Payment Method</b></h3>
                                    <label style="color: #6C6C6C;">Payment Details</label>
                                </div>
                                <div class="col-lg-4"></div>
                            </div>

                            <asp:Panel ID="PnlButtons" runat="server">
                                <div class="row p-t-15">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-2 text-center p-t-10" style="display: none">
                                        <asp:ImageButton ID="BtnDebit" runat="server" ImageUrl="~/Images/Icons/debitButton.png" OnClick="BtnDebit_Click" />
                                    </div>
                                    <div class="col-lg-2 text-center p-t-10" style="display: none">
                                        <asp:ImageButton ID="BtnAccount" runat="server" ImageUrl="~/Images/Icons/AccountButton.png" OnClick="BtnAccount_Click" />
                                    </div>
                                    <div class="col-lg-2 text-center p-t-10">
                                        <asp:ImageButton ID="BtnPaypal" runat="server" ImageUrl="~/Images/Icons/paypalButton.png" OnClick="BtnPaypal_Click" />
                                    </div>
                                    <div class="col-lg-12">
                                        <br />
                                        <br />
                                    </div>
                                    <div class="col-lg-12 text-center">
                                        <asp:Button ID="BtnPnlGo" runat="server" OnClick="BtnPnlGo_Click" Text="Go back" CssClass="btn btn-sm btn-green" Style="width: 90px; background-color: #FF0000;" />
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="PnlCard" runat="server" Visible="false">
                                <div class="PaymentDetails">
                                    <div class="row">
                                        <br />
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="p-t-10" style="color: #707070">Name on Card<span style="color: red;">*</span></label>
                                            <asp:TextBox ID="TxtNameOnCard" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                            <label class="p-t-10" style="color: #707070">Credit Card Number<span style="color: red;">*</span></label>
                                            <asp:TextBox ID="TxtCardNumber" onkeypress="return isNumberKey(event)" MaxLength="16" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label class="p-t-10" style="color: #707070">Security Code<span style="color: red;">*</span></label>
                                                    <asp:TextBox ID="TxtSecurityCode" onkeypress="return isNumberKey(event)" MaxLength="3" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="p-t-10" style="color: #707070">Expiration Date<span style="color: red;">*</span></label>
                                                    <asp:TextBox ID="TxtExpiryDate" placeholder="19/25" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="row m-t-25">
                                                <div class="col-lg-12 text-right">
                                                    <asp:Button ID="BtnCreditGo" runat="server" OnClick="BtnCreditGo_Click" Text="Go back" CssClass="btn btn-sm btn-green" Style="width: 90px; background-color: #707070;" />
                                                    <asp:Button ID="BtnCreditContinue" runat="server" OnClick="BtnCreditContinue_Click" Text="Continue" CssClass="btn btn-sm btn-green" Style="width: 90px;" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="PnlCheckingAccount" runat="server" Visible="false">
                                <div class="row">
                                    <br />
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-2 form-group-sm">
                                        <asp:DropDownList ID="DdlAccountType" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000;">
                                            <asp:ListItem Value="0" Text="Account Type"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Business"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Personal"></asp:ListItem>
                                        </asp:DropDownList>
                                        <label class="p-t-10" style="color: #707070">Account Name</label>
                                        <asp:TextBox ID="TxtAccName" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                        <label class="p-t-10" style="color: #707070">Account Number</label>
                                        <asp:TextBox ID="TxtAccNum" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                        <label class="p-t-10" style="color: #707070">Routing Number</label>
                                        <asp:TextBox ID="TxtRouting" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-2 form-group-sm">
                                        <%--<asp:DropDownList ID="DdlLicense" runat="server" CssClass="form-control" Style="background-color: #A9C502; color: #000;">
                                        <asp:ListItem Value="0" Text="Driver's License State"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                        <label class="p-t-10" style="color: #707070">Driver’s License Number</label>
                                        <asp:TextBox ID="TxtDL_Num" runat="server" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                        <label class="p-t-10" style="color: #707070">Account Owner Birthdate</label>
                                        <asp:TextBox ID="TxtAO_BirthDate" runat="server" Type="date" Style="border: 1px solid #4B4A4A; background-color: #000;" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <div class="col-lg-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6"></div>
                                    <div class="col-lg-2 text-right">
                                        <asp:Button ID="BtnAccountGo" runat="server" OnClick="BtnAccountGo_Click" Text="Go back" CssClass="btn btn-sm btn-green" Style="width: 90px; background-color: #707070;" />
                                        <asp:Button ID="BtnAccountContinue" runat="server" OnClick="BtnAccountContinue_Click" Text="Continue" CssClass="btn btn-sm btn-green" Style="width: 90px;" />
                                    </div>
                                </div>
                            </asp:Panel>


                        </asp:Panel>

                    </asp:Panel>
                    <asp:Panel ID="PnlOrderSummary" runat="server" Visible="false">

                        <div class="row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12 text-center p-t-20">
                                        <h3 style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>You've added Regular Plan</b></h3>
                                        <label style="color: #6C6C6C;">Order Summary</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <div id="div2" runat="server" visible="false">
                                            <strong>
                                                <asp:Label ID="LblMsg2" runat="server"></asp:Label></strong>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-25">
                                        <label style="color: #A9C502; font-family: 'Microsoft JhengHei'"><b>Select a payment option</b></label>
                                    </div>
                                    <div class="col-lg-5 form-group-sm">
                                        <asp:DropDownList ID="DdlRegularPlan" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlRegularPlan_SelectedIndexChanged" CssClass="form-control" Style="background-color: #A9C502; color: #000;">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-lg-12 p-t-10">
                                        <span style="color: #A9C502; font-family: 'Microsoft JhengHei'">Emails Limit : <span id="spLimit" style="color: #A9C502; font-family: 'Microsoft JhengHei'" runat="server"></span></span>
                                        <br />
                                        s
                                        <label style="color: red; font-size: 10px;">*Select more months and save your money.</label>
                                    </div>
                                    <%--<div class="col-lg-12">
                                    <label style="color: #707070; font-size: 10px;">Term lengths adjustable prior to checkout.</label>
                                </div>--%>
                                    <div class="col-lg-12 p-t-20" style="border-bottom: 1px solid #6C6C6C;"></div>
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12 p-t-10">
                                                <label style="color: #707070; font-size: 20px;">Total</label>
                                                <asp:Label ID="LblPayment" runat="server" Style="color: #ABC502; font-size: 20px; float: right;"></asp:Label><label style="color: #ABC502; font-size: 20px; float: right;">$</label>
                                            </div>
                                            <%--<div class="col-lg-6 text-right p-t-10">
                                                
                                                
                                            </div>--%>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 p-t-10" style="border-bottom: 1px solid #6C6C6C;"></div>
                                    <div class="col-lg-12 p-t-15 text-right">
                                        <asp:Button ID="BtnPurchase" runat="server" CssClass="btn btn-sm btn-green" Text="Complete Purchase" OnClick="BtnPurchase_Click" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4"></div>
                        </div>
                    </asp:Panel>
                </section>
            </div>
        </div>

        <%----------------------------------------- Pop Up -------------------------------------------------------%>


        <div class="modal fade" id="myModal" runat="server" data-backdrop="static" data-keyboard="false" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content" style="background-color: #141414; border: 1px solid #4B4A4A;">
                    <div class="modal-body">
                        <div class="row p-t-20 p-b-10">
                            <div class="col-lg-12 text-center">
                                <img src="Images/Icons/check2.png" />
                                <h3 style="color: #A9C405;">Your payment method has been added.</h3>
                                <asp:Button ID="BtnPaymentAdded" runat="server" OnClick="BtnPaymentAdded_Click" Text="Continue" CssClass="btn btn-sm btn-green" Style="width: 90px;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </form>

    <%-- <script type="text/javascript">
        debugger;
        function Up() {
            $("#up").addClass("active-div");
            $("#bl").removeClass("active-div");
            $("#pd").removeClass("active-div");
        }

        function Bl() {
            $("#bl").addClass("active-div");
            $("#up").removeClass("active-div");
            $("#pd").removeClass("active-div");
        }

        function PD() {
            $("#pd").addClass("active-div");
            $("#up").removeClass("active-div");
            $("#bl").removeClass("active-div");
        }

    </script>--%>
</body>
</html>
